package uk.co.ribot.easyadapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class EasyLoadMoreRecyclerAdapter<T> extends EasyRecyclerAdapter<T> {

    private boolean loadingMore = false;
    private Integer mItemLayoutLoadMoreId;
    private View mHeader;

    private static final float SCROLL_MULTIPLIER = 0.5f;

    public static class VIEW_TYPES {
        public static final int NORMAL = 1;
        public static final int HEADER = 2;
        public static final int FIRST_VIEW = 3;
        public static final int LOADING_MORE = 4;
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, and list of items.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems) {
        super(context, itemViewHolderClass, listItems);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, and list of items.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems);
        this.mItemLayoutLoadMoreId = itemLayoutLoadMoreId;
    }

    /**
     * Constructs and EasyAdapter with a Context and an {@link ItemViewHolder} class.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass);
        this.mItemLayoutLoadMoreId = itemLayoutLoadMoreId;
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener) {
        super(context, itemViewHolderClass, listItems, listener);
    }


    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, listener);
        this.mItemLayoutLoadMoreId = itemLayoutLoadMoreId;
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     * @param context
     * @param itemViewHolderClass
     * @param listItems
     * @param listener
     * @param itemLayoutLoadMoreId
     * @param header
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, View header) {
        super(context, itemViewHolderClass, listItems, listener);
        this.mItemLayoutLoadMoreId = itemLayoutLoadMoreId;
        this.mHeader = header;
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public EasyLoadMoreRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listener);
        this.mItemLayoutLoadMoreId = itemLayoutLoadMoreId;
    }
    
    @Override
    public EasyRecyclerAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewtype) {

        if (viewtype == VIEW_TYPES.LOADING_MORE) {
            return new EasyRecyclerAdapter.RecyclerViewHolder(new DefaultViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(mItemLayoutLoadMoreId, viewGroup, false)));
        } else if (viewtype == VIEW_TYPES.HEADER && mHeader != null) {
            return new EasyRecyclerAdapter.RecyclerViewHolder(new DefaultViewHolder(mHeader));
        } else if (viewtype == VIEW_TYPES.FIRST_VIEW && mHeader != null && mRecyclerView != null) {
            final RecyclerView.ViewHolder holder = mRecyclerView.findViewHolderForAdapterPosition(0);
            if (holder != null) {
                translateHeader(-holder.itemView.getTop());
            }
        }
        return super.onCreateViewHolder(viewGroup, viewtype);
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolder recyclerViewHolder, int position) {
        if (DefaultViewHolder.class.isInstance(recyclerViewHolder.itemViewHolder)) {
            //bind loadingview or header
        } else {
            super.onBindViewHolder(recyclerViewHolder, getRealPosition(position));
        }
    }

    private int getRealPosition(int position) {
        return position - (mHeader != null ? 1 : 0);
    }

    public void addLoadMore() {
        loadingMore = true;
        notifyItemInserted(getItemCount());
    }

    public void removeLoadMore() {
        loadingMore = false;
        notifyDataSetChanged();
    }

    @Override
    public final int getItemCount() {
        if (getItems() != null) {
            return getItems().size() + (loadingMore ? 1 : 0) + (mHeader != null ? 1 : 0);
        }
        return (loadingMore ? 1 : 0) + (mHeader != null ? 1 : 0);
    }

    @Override
    public final int getItemViewType(int position) {
        if (position == 0 && mHeader != null) {
            return VIEW_TYPES.HEADER;
        } else if (position >= getItems().size() + (mHeader != null ? 1 : 0)) {
            return VIEW_TYPES.LOADING_MORE;
        } else if (position == 1 && mHeader != null) {
            return VIEW_TYPES.FIRST_VIEW;
        } else {
            return super.getItemViewType(getRealPosition(position));
        }
    }

    public class DefaultViewHolder extends ItemViewHolder<String> {

        public DefaultViewHolder(View view) {
            super(view);
        }

        @Override
        public void onSetValues(String item, PositionInfo positionInfo) {
        }

        @Override
        public void onSetListeners() {
        }
    }

    public void removeAllItems() {
        this.removeItems(this.getItems());
    }

    public void notifyItemChanged(T item, CompareObject<T> compareObject) {

        int position = -1;

        for (int i = 0; i < getItems().size(); i++) {
            if (compareObject.isEqual(item, getItems().get(i))) {
                position = i;
                break;
            }
        }
        if (position >= 0) {
            notifyItemChanged(position + (mHeader != null ? 1 : 0));
        }
    }

    public interface CompareObject<T> {
        boolean isEqual(T left, T right);
    }

    //Parallax

    public interface OnParallaxScroll {
        /**
         * Event triggered when the parallax is being scrolled.
         *
         * @param percentage
         * @param offset
         * @param parallax
         */
        void onParallaxScroll(float percentage, float offset, View parallax);
    }

    private OnParallaxScroll mParallaxScroll;
    private RecyclerView mRecyclerView;
    private boolean mShouldClipView = true;

    /**
     * Translates the adapter in Y
     *
     * @param of offset in px
     */
    public void translateHeader(float of) {
        float ofCalculated = of * SCROLL_MULTIPLIER;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mHeader.setTranslationY(ofCalculated);
        } else {
            TranslateAnimation anim = new TranslateAnimation(0, 0, ofCalculated, ofCalculated);
            anim.setFillAfter(true);
            anim.setDuration(0);
            mHeader.startAnimation(anim);
        }
        ((CustomRelativeWrapper)mHeader).setClipY(Math.round(ofCalculated));
        if (mParallaxScroll != null) {
            float left = Math.min(1, ((ofCalculated) / (mHeader.getHeight() * SCROLL_MULTIPLIER)));
            mParallaxScroll.onParallaxScroll(left, of, mHeader);
        }
    }

    /**
     * Set the view as header.
     *
     * @param header The inflated header
     * @param view   The RecyclerView to set scroll listeners
     */
    public void setParallaxHeader(View header, final RecyclerView view) {
        mRecyclerView = view;
        mHeader = new CustomRelativeWrapper(header.getContext(), mShouldClipView);
        mHeader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        ((CustomRelativeWrapper)mHeader).addView(header, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mHeader != null) {
                    translateHeader(mRecyclerView.computeVerticalScrollOffset());
                }
            }
        });
    }

    static class CustomRelativeWrapper extends RelativeLayout {

        private int mOffset;
        private boolean mShouldClip;

        public CustomRelativeWrapper(Context context, boolean shouldClick) {
            super(context);
            mShouldClip = shouldClick;
        }

        @Override
        protected void dispatchDraw(Canvas canvas) {
            if (mShouldClip) {
                canvas.clipRect(new Rect(getLeft(), getTop(), getRight(), getBottom() + mOffset));
            }
            super.dispatchDraw(canvas);
        }

        public void setClipY(int offset) {
            mOffset = offset;
            invalidate();
        }
    }
}
