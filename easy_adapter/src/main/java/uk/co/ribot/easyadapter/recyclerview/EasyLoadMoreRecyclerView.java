package uk.co.ribot.easyadapter.recyclerview;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 11/13/15.
 */
public class EasyLoadMoreRecyclerView extends RecyclerView {

    private Boolean mLoadingMore = false;
    private Boolean mAddLoadMoreView = true;
    private LoadMoreOnScrollListener mLoadMoreOnScrollListener;

    public EasyLoadMoreRecyclerView(Context context) {
        super(context);
    }

    public EasyLoadMoreRecyclerView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EasyLoadMoreRecyclerView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    /**
     * enable list view auto load more
     *
     * @param loadMoreListener load more listener
     */
    public void enableAutoLoadMore(final LoadMoreListener loadMoreListener) {
        disableAutoLoadMore();
        mLoadMoreOnScrollListener = new LoadMoreOnScrollListener(loadMoreListener);
        addOnScrollListener(mLoadMoreOnScrollListener);
        mLoadingMore = true;
    }

    public void disableAutoLoadMore() {
        if (mLoadMoreOnScrollListener != null) {
            removeOnScrollListener(mLoadMoreOnScrollListener);
        }
        onLoadingMoreFinish();
    }

    private class LoadMoreOnScrollListener extends RecyclerView.OnScrollListener {

        private LoadMoreListener mLoadMoreListener;

        public LoadMoreOnScrollListener(LoadMoreListener loadMoreListener) {
            this.mLoadMoreListener = loadMoreListener;
        }

        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            if (!mLoadingMore) {
                if (null == getLayoutManager() || null == getAdapter()) {
                    return;
                }
                int mVisibleItemCount = getLayoutManager().getChildCount();
                int mTotalItemCount = getLayoutManager().getItemCount();
                int mFirstVisibleItemPosition = 0;
                if (getLayoutManager() instanceof LinearLayoutManager) {
                    mFirstVisibleItemPosition = ((LinearLayoutManager) getLayoutManager()).findFirstVisibleItemPosition();
                }

                if ((mVisibleItemCount + mFirstVisibleItemPosition) >= mTotalItemCount) {
                    callLoadMore();
                }
            }
        }
    }

    public void callLoadMore() {
        if (null != mLoadMoreOnScrollListener && null != mLoadMoreOnScrollListener.mLoadMoreListener) {
            mLoadingMore = true;
            mLoadMoreOnScrollListener.mLoadMoreListener.loadMore();
            if (getAdapter() instanceof EasyLoadMoreRecyclerAdapter && mAddLoadMoreView) {
                ((EasyLoadMoreRecyclerAdapter) getAdapter()).addLoadMore();
                if (getLayoutManager() instanceof GridLayoutManager) {
                    ((GridLayoutManager) getLayoutManager()).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                        @Override
                        public int getSpanSize(int position) {
                            int spanCount = ((GridLayoutManager) getLayoutManager()).getSpanCount();
                            if (position == getAdapter().getItemCount() - 1) {
                                return spanCount;
                            } else {
                                return 1;
                            }
                        }
                    });
                }
            }
        }
    }

    public void onLoadingMoreFinish() {
        mLoadingMore = false;
        if (getAdapter() instanceof EasyLoadMoreRecyclerAdapter && mAddLoadMoreView) {
            if (getLayoutManager() instanceof GridLayoutManager) {
                ((GridLayoutManager) getLayoutManager()).setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
                    @Override
                    public int getSpanSize(int position) {
                        return 1;
                    }
                });
            }
            ((EasyLoadMoreRecyclerAdapter) getAdapter()).removeLoadMore();
        }
    }

    public interface LoadMoreListener {
        void loadMore();
    }

    public Boolean isAddLoadMoreView() {
        return mAddLoadMoreView;
    }

    public void setAddLoadMoreView(Boolean mAddLoadMore) {
        this.mAddLoadMoreView = mAddLoadMore;
    }
}
