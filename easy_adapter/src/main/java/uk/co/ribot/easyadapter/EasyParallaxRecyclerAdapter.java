package uk.co.ribot.easyadapter;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class EasyParallaxRecyclerAdapter<T> extends EasyRecyclerAdapter<T> {

    private List<T> mListItems;

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, and list of items.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     */
    public EasyParallaxRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems) {
        super(context, itemViewHolderClass);
        setItems(listItems);
    }

    /**
     * Constructs and EasyAdapter with a Context and an {@link ItemViewHolder} class.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     */
    public EasyParallaxRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass) {
        super(context, itemViewHolderClass);
        mListItems = new ArrayList<T>();
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public EasyParallaxRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener) {
        super(context, itemViewHolderClass, listener);
        setItems(listItems);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public EasyParallaxRecyclerAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Object listener) {
        super(context, itemViewHolderClass, listener);
        mListItems = new ArrayList<T>();
    }

    /**
     * Set a new list of items into the Adapter and refresh the {@code RecyclerView} by calling
     * {@code notifyDataSetChanged()}.
     * Use {@link #setItemsWithoutNotifying(List)}()} if you don't want to refresh
     * the {@code RecyclerView} at this time.
     *
     * @param listItems new List of items to use as the underlying data structure
     */
    public void setItems(List<T> listItems) {
        mListItems = listItems;
        notifyDataSetChanged();
    }

    /**
     * Set a new list of items into the Adapter.
     *
     * @param listItems new List of items to use as the underlying data structure
     */
    public void setItemsWithoutNotifying(List<T> listItems) {
        mListItems = listItems;
    }

    /**
     * Retrieve the {@code List} of items. Changes to this {@code List} directly affect the data displayed on
     * the {@code RecyclerView}.
     *
     * @return the underlying data {@code List}
     */
    public List<T> getItems() {
        return mListItems;
    }

    /**
     * Add a single item and refresh the {@code RecyclerView} by calling
     * {@code notifyItemInserted()}.
     *
     * @param item item to add
     */
    public void addItem(T item) {
        mListItems.add(item);
        notifyItemInserted(mListItems.indexOf(item));
    }

    /**
     * Remove a single item and refresh the {@code RecyclerView} by calling
     * {@code notifyItemRemoved()}.
     *
     * @param item item to add
     * @return true if any data was modified by this operation, false otherwise.
     */
    public boolean removeItem(T item) {
        int index = mListItems.indexOf(item);
        if (index < 0) return false;
        mListItems.remove(index);
        notifyItemRemoved(index);
        return true;
    }

    public void removeAllItems() {
        this.removeItems(this.getItems());
    }

    /**
     * Append a collection of items and refresh the {@code RecyclerView} by calling
     * {@code notifyItemRangeInserted()}.
     *
     * @param items collection of items to append
     * @return true if any data was modified by this operation, false otherwise.
     */
    public boolean addItems(Collection<T> items) {
        if (items.isEmpty()) return false;
        int previousSize = getItemCount();
        if (mListItems.addAll(items)) {
            notifyItemRangeInserted(previousSize, items.size());
            return true;
        }
        return false;
    }

    /**
     * Remove a collection of items and refresh the {@code RecyclerView} by calling
     * {@code notifyDataSetChanged()}.
     *
     * @param items {@code Collection} of items to remove
     * @return true if any data was modified by this operation, false otherwise.
     */
    public boolean removeItems(Collection<? extends T> items) {
        if (mListItems.removeAll(items)) {
            notifyDataSetChanged();
            return true;
        }
        return false;
    }

    /**
     * Access a data item
     *
     * @param position the position of the item
     * @return the data item in the given position
     */
    @Override
    public T getItem(int position) {
        return mListItems.get(position);
    }

    /**
     * Access the size of the adapter
     *
     * @return the number of items in the adapter.
     */
    @Override
    public int getItemCount() {
        return mListItems.size()+ (mHeader == null ? 0 : 1);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 1)
            return VIEW_TYPES.FIRST_VIEW;
        return position == 0 ? VIEW_TYPES.HEADER : VIEW_TYPES.NORMAL;
    }

    @Override
    public void onBindViewHolder(EasyRecyclerAdapter.RecyclerViewHolder viewHolder, final int i) {
        if (i != 0 && mHeader != null) {
            super.onBindViewHolder(viewHolder, i - 1);
        } else if (i != 0) {
            super.onBindViewHolder(viewHolder, i);
        }
    }

    @Override
    public EasyRecyclerAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup viewGroup, final int i) {
        if (i == VIEW_TYPES.HEADER && mHeader != null) {
            return new EasyRecyclerAdapter.RecyclerViewHolder(new ViewHolder(mHeader));
        }

        if (i == VIEW_TYPES.FIRST_VIEW && mHeader != null && mRecyclerView != null) {
            final RecyclerView.ViewHolder holder = mRecyclerView.findViewHolderForAdapterPosition(0);
            if (holder != null) {
                translateHeader(-holder.itemView.getTop());
            }
        }
        return super.onCreateViewHolder(viewGroup, i);
    }

    public class ViewHolder extends ItemViewHolder<String> {

        public ViewHolder(View view) {
            super(view);
        }

        @Override
        public void onSetValues(String item, PositionInfo positionInfo) {
        }

        @Override
        public void onSetListeners() {
        }
    }

    private static final float SCROLL_MULTIPLIER = 0.5f;

    public static class VIEW_TYPES {
        public static final int NORMAL = 1;
        public static final int HEADER = 2;
        public static final int FIRST_VIEW = 3;
    }

    public interface OnParallaxScroll {
        /**
         * Event triggered when the parallax is being scrolled.
         *
         * @param percentage
         * @param offset
         * @param parallax
         */
        void onParallaxScroll(float percentage, float offset, View parallax);
    }

    private CustomRelativeWrapper mHeader;
    private OnParallaxScroll mParallaxScroll;
    private RecyclerView mRecyclerView;
    private boolean mShouldClipView = true;

    /**
     * Translates the adapter in Y
     *
     * @param of offset in px
     */
    public void translateHeader(float of) {
        float ofCalculated = of * SCROLL_MULTIPLIER;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            mHeader.setTranslationY(ofCalculated);
        } else {
            TranslateAnimation anim = new TranslateAnimation(0, 0, ofCalculated, ofCalculated);
            anim.setFillAfter(true);
            anim.setDuration(0);
            mHeader.startAnimation(anim);
        }
        mHeader.setClipY(Math.round(ofCalculated));
        if (mParallaxScroll != null) {
            float left = Math.min(1, ((ofCalculated) / (mHeader.getHeight() * SCROLL_MULTIPLIER)));
            mParallaxScroll.onParallaxScroll(left, of, mHeader);
        }
    }

    /**
     * Set the view as header.
     *
     * @param header The inflated header
     * @param view   The RecyclerView to set scroll listeners
     */
    public void setParallaxHeader(View header, final RecyclerView view) {
        mRecyclerView = view;
        mHeader = new CustomRelativeWrapper(header.getContext(), mShouldClipView);
        mHeader.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        mHeader.addView(header, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        view.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (mHeader != null) {
                    translateHeader(mRecyclerView.computeVerticalScrollOffset());
                }
            }
        });
    }

    /**
     * @return true if there is a header on this adapter, false otherwise
     */
    public boolean hasHeader() {
        return mHeader != null;
    }

    public boolean isShouldClipView() {
        return mShouldClipView;
    }

    /**
     * Defines if we will clip the layout or not. MUST BE CALLED BEFORE {@link #setParallaxHeader(android.view.View, android.support.v7.widget.RecyclerView)}
     *
     * @param shouldClickView
     */
    public void setShouldClipView(boolean shouldClickView) {
        mShouldClipView = shouldClickView;
    }

    public void setOnParallaxScroll(OnParallaxScroll parallaxScroll) {
        mParallaxScroll = parallaxScroll;
        mParallaxScroll.onParallaxScroll(0, 0, mHeader);
    }

    static class CustomRelativeWrapper extends RelativeLayout {

        private int mOffset;
        private boolean mShouldClip;

        public CustomRelativeWrapper(Context context, boolean shouldClick) {
            super(context);
            mShouldClip = shouldClick;
        }

        @Override
        protected void dispatchDraw(Canvas canvas) {
            if (mShouldClip) {
                canvas.clipRect(new Rect(getLeft(), getTop(), getRight(), getBottom() + mOffset));
            }
            super.dispatchDraw(canvas);
        }

        public void setClipY(int offset) {
            mOffset = offset;
            invalidate();
        }
    }
}
