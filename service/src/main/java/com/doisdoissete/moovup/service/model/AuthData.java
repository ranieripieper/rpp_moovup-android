package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmObject;

/**
 * Created by ranipieper on 11/12/15.
 */
public class AuthData extends RealmObject {

    @Expose
    @SerializedGsonName("auth_token")
    private String authToken;

    @Expose
    private String provider;

    @Expose
    @SerializedGsonName("expires_at")
    private Date expiresAt;

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public Date getExpiresAt() {
        return expiresAt;
    }

    public void setExpiresAt(Date expiresAt) {
        this.expiresAt = expiresAt;
    }
}
