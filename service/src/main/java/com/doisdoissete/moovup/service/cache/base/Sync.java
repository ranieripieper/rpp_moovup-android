package com.doisdoissete.moovup.service.cache.base;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by broto on 11/4/15.
 */
public class Sync extends RealmObject{

    @Ignore
    public static final int DEFAULT_CACHE_TIME_LIMIT = 1000 * 60 * 30; // 30 minutes

    @PrimaryKey
    @Required
    private String className;

    private long lastUpdated;

    public Sync() {
    }

    public Sync(String className, long lastUpdated) {
        this.className = className;
        this.lastUpdated = lastUpdated;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public long getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(long lastUpdated) {
        this.lastUpdated = lastUpdated;
    }
}
