package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.FriendsResponse;
import com.doisdoissete.moovup.service.model.response.FriendshipResponse;
import com.doisdoissete.moovup.service.model.response.UsersResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ranipieper on 2/12/16.
 */
public interface FriendsService {

    @GET("/users/me/friends")
    void getMyFriends(@Query("page") Integer page,
                      @Query("per_page") Integer perPage,
                      Callback<FriendsResponse> callback);

    @GET("/users/me/pending_requested_friends")
    void getPendingFriends(@Query("page") Integer page,
                      @Query("per_page") Integer perPage,
                      Callback<FriendsResponse> callback);

    @GET("/search/users")
    void searchFriends(
            @Query("q") String query,
            @Query("profile_types") String profileTypes,
            @Query("page") Integer page,
            @Query("per_page") Integer perPage,
            Callback<UsersResponse> callback);

    @POST("/friendships")
    @FormUrlEncoded
    void sendFriendship(
            @Field("friend_id") long friendId,
            Callback<FriendshipResponse> callback);

    @POST("/friends/{user_id}/accept")
    void acceptFriendship(
            @Path("user_id") long userId,
            @Body String emptyBody,
            Callback<FriendshipResponse> callback);

    @DELETE("/friends/{user_id}/decline")
    void declineFriendship(
            @Path("user_id") long userId,
            Callback<FriendshipResponse> callback);


}
