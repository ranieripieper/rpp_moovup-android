package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Answers;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class CommentsResponse extends BaseResponse {

    @Expose
    @SerializedName("comments")
    private List<Answers> comments;

    @Expose
    @SerializedName("linked_data")
    private LinkedData linkedData;

    public List<Answers> getComments() {
        return comments;
    }

    public void setComments(List<Answers> comments) {
        this.comments = comments;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
