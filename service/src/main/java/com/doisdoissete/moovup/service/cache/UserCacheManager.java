package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Configuration;
import com.doisdoissete.moovup.service.model.User;

import io.realm.Realm;

/**
 * Created by ranipieper on 11/19/15.
 */
public class UserCacheManager extends BaseCache<User> {

    private static UserCacheManager instance = new UserCacheManager();

    private UserCacheManager() {
    }

    public static UserCacheManager getInstance() {
        return instance;
    }

    public User getCurrentUser() {
        Configuration conf = ConfigurationManager.getInstance().get();
        if (conf != null) {
            return get(conf.getCurrentUserId());
        }
        return null;
    }

    public void updateUnreadNotificationsCount(long id, long unreadNotificationsCount) {
        Realm realm = getRealm();
        realm.beginTransaction();
        User obj = get(id);
        obj.setUnreadNotificationsCount(unreadNotificationsCount);
        realm.commitTransaction();
    }

    public User updateCurrentProfileImage(String profileImageUrl) {
        Realm realm = getRealm();
        realm.beginTransaction();
        User currentUser = getCurrentUser();
        currentUser.setProfileImageUrl(profileImageUrl);
        realm.commitTransaction();
        return currentUser;
    }

    @Override
    public Class<User> getReferenceClass() {
        return User.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
