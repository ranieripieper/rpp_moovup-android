package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 1/11/16.
 */
public class FriendsResponse extends BaseResponse {

    @Expose
    @SerializedName("friends")
    private List<User> friends;

    public List<User> getFriends() {
        return friends;
    }

    public void setFriends(List<User> friends) {
        this.friends = friends;
    }
}
