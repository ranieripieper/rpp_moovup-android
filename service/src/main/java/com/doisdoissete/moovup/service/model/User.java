package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ranipieper on 11/12/15.
 */
public class User extends RealmObject {

    public static final String PERSON_PROFILE_TYPE = "common_user";
    public static final String COMPANY_PROFILE_TYPE = "company";

    public static final String GENDER_MALE = "male";
    public static final String GENDER_FEMALE = "female";

    /*
       self_pending -> "Pedido enviado (pelo usuario serializado) porém pendente (usuario logado)"
       user_pending -> "Pedido enviado (usuario logado) porém pendente (pelo usuario serializado)"

       self_accepted || user_accepted -> Amigos (tanto faz quem aceitou)

       user_declined -> "Pedido enviado (usuario logado) porém não aceito (pelo usuario serializado)"

       self_declined -> "Pedido enviado (pelo usuario serializado) porém negado (usuario logado)"

       no_friendship -> Nenhum tipo de relacionamento de amizade
    */
    public static final String FRIENDSHIP_STATUS_SELF_PENDING = "self_pending";
    public static final String FRIENDSHIP_STATUS_USER_PENDING = "user_pending";
    public static final String FRIENDSHIP_STATUS_SELF_ACCEPTED = "self_accepted";
    public static final String FRIENDSHIP_STATUS_USER_DECLINED = "user_declined";
    public static final String FRIENDSHIP_STATUS_SELF_DECLINED = "self_declined";
    public static final String FRIENDSHIP_STATUS_NO_FRIENDSHIP = "no_friendship";
    public static final String FRIENDSHIP_STATUS_USER_ACCEPTED = "user_accepted";

    @PrimaryKey
    @Required
    @Expose
    private Long id;

    @Expose
    @SerializedGsonName("email")
    private String email;

    @Expose
    private String username;

    @Expose
    @SerializedGsonName("first_name")
    private String firstName;

    @Expose
    @SerializedGsonName("last_name")
    private String lastName;

    @Expose
    @SerializedGsonName("fullname")
    private String fullName;

    @Expose
    private String gender;

    @Expose
    @SerializedGsonName("profile_image_url")
    private String profileImageUrl;

    @Expose
    @SerializedGsonName("profile_images")
    private ProfileImage profileImages;

    @Expose
    @SerializedGsonName("profile_type")
    private String profileType;

    @Expose
    @SerializedGsonName("oauth_provider")
    private String oAuthProvider;

    @Expose
    @SerializedGsonName("oauth_provider_uid")
    private String oAuthProviderUid;

    @Expose
    @SerializedGsonName("birthday_date")
    private Date birthdayDate;

    @Expose
    @SerializedGsonName("carrer")
    private Carrer carrer;

    @Expose
    @SerializedGsonName("carrer_id")
    private Long carrerId;

    @Expose
    @SerializedGsonName("hobby_id")
    private Long hobbyId;

    @Expose
    @SerializedGsonName("hobby")
    private Category hobby;

    @Expose
    @SerializedGsonName("commercial_activity")
    private CommercialActivity commercialActivity;

    @Expose
    @SerializedGsonName("commercial_activity_id")
    private Long commercialActivityId;

    @Expose
    @SerializedGsonName("friendship_status")
    private String friendshipStatus;

    @Expose
    @SerializedGsonName("preferences")
    @Ignore
    private Preferences preferences;

    @Expose
    @SerializedGsonName("current_goal")
    @Ignore
    private Goal currentGoal;

    @Expose
    @SerializedGsonName("goals")
    @Ignore
    private List<Goal> goals;

    @Expose
    @SerializedGsonName("unread_notifications_count")
    private long unreadNotificationsCount;

    @Expose
    @SerializedGsonName("received_upvotes_count")
    private long receivedUpvotesCount;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFullName() {
        if (fullName == null) {
            return String.format("%s %s", this.firstName, this.lastName == null ? "" : this.lastName).trim();
        }
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getProfileImageUrl() {
        return profileImageUrl;
    }

    public void setProfileImageUrl(String profileImageUrl) {
        this.profileImageUrl = profileImageUrl;
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getoAuthProvider() {
        return oAuthProvider;
    }

    public void setoAuthProvider(String oAuthProvider) {
        this.oAuthProvider = oAuthProvider;
    }

    public String getoAuthProviderUid() {
        return oAuthProviderUid;
    }

    public void setoAuthProviderUid(String oAuthProviderUid) {
        this.oAuthProviderUid = oAuthProviderUid;
    }

    public ProfileImage getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(ProfileImage profileImages) {
        this.profileImages = profileImages;
    }

    public Date getBirthdayDate() {
        return birthdayDate;
    }

    public void setBirthdayDate(Date birthdayDate) {
        this.birthdayDate = birthdayDate;
    }

    public Carrer getCarrer() {
        return carrer;
    }

    public void setCarrer(Carrer carrer) {
        this.carrer = carrer;
    }

    /**
     * Gets the commercialActivity
     *
     * @return commercialActivity
     */
    public CommercialActivity getCommercialActivity() {
        return commercialActivity;
    }

    /**
     * Sets the commercialActivity
     *
     * @param commercialActivity
     */
    public void setCommercialActivity(CommercialActivity commercialActivity) {
        this.commercialActivity = commercialActivity;
    }

    public Category getHobby() {
        return hobby;
    }

    public void setHobby(Category hobby) {
        this.hobby = hobby;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFriendshipStatus() {
        return friendshipStatus;
    }

    public void setFriendshipStatus(String friendshipStatus) {
        this.friendshipStatus = friendshipStatus;
    }

    public Goal getCurrentGoal() {
        return currentGoal;
    }

    public void setCurrentGoal(Goal currentGoal) {
        this.currentGoal = currentGoal;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }

    public Preferences getPreferences() {
        return preferences;
    }

    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    public Long getCommercialActivityId() {
        return commercialActivityId;
    }

    public void setCommercialActivityId(Long commercialActivityId) {
        this.commercialActivityId = commercialActivityId;
    }

    public Long getCarrerId() {
        return carrerId;
    }

    public void setCarrerId(Long carrerId) {
        this.carrerId = carrerId;
    }

    public Long getHobbyId() {
        return hobbyId;
    }

    public void setHobbyId(Long hobbyId) {
        this.hobbyId = hobbyId;
    }

    /**
     * Gets the unreadNotificationsCount
     *
     * @return unreadNotificationsCount
     */
    public long getUnreadNotificationsCount() {
        return unreadNotificationsCount;
    }

    /**
     * Sets the unreadNotificationsCount
     *
     * @param unreadNotificationsCount
     */
    public void setUnreadNotificationsCount(long unreadNotificationsCount) {
        this.unreadNotificationsCount = unreadNotificationsCount;
    }

    public long getReceivedUpvotesCount() {
        return receivedUpvotesCount;
    }

    public void setReceivedUpvotesCount(long receivedUpvotesCount) {
        this.receivedUpvotesCount = receivedUpvotesCount;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", username='" + username + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", fullName='" + fullName + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
