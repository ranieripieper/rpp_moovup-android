package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.AnswerResponse;
import com.doisdoissete.moovup.service.model.response.AnswersResponse;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.QuestionResponse;
import com.doisdoissete.moovup.service.model.response.QuestionsResponse;
import com.doisdoissete.moovup.service.model.response.UpdateQuestionResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/11/15.
 */
public interface QuestionService {

    //Questions
    @GET("/questions/{question_id}")
    void getQuestionById(@Path("question_id") Long questionId, Callback<QuestionResponse> callback);


    @GET("/users/me/feed/questions")
    void getQuestionsFeed(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<QuestionsResponse> callback);

    @GET("/search/questions")
    void searchQuestions(@Query("q") String query, @Query("page") Integer page, @Query("per_page") Integer perPage, Callback<QuestionsResponse> callback);

    @GET("/users/me/feed/questions_to_reply")
    void getQuestionsToAnswer(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<QuestionsResponse> callback);

    @GET("/users/me/questions")
    void getMyQuestions(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<QuestionsResponse> callback);

    @GET("/users/me/favorite_questions")
    void getFavotiteQuestions(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<QuestionsResponse> callback);

    @POST("/questions/")
    @FormUrlEncoded
    void addNewQuestion(@Field("question[body_text]") String question, @Field("question[category_id]") Long categoryId, Callback<UpdateQuestionResponse> callback);

    @PUT("/questions/{question_id}")
    @FormUrlEncoded
    void updateQuestion(@Path("question_id") long questionId, @Field("question[body_text]") String question, @Field("question[category_id]") Long categoryId, Callback<UpdateQuestionResponse> callback);

    @DELETE("/questions/{question_id}")
    void deleteQuestion(@Path("question_id") long questionId, Callback<BaseResponse> callback);

    //favoritar
    @DELETE("/questions/{question_id}/favorite")
    void unfavoriteQuestion(@Path("question_id") long questionId, Callback<BaseResponse> callback);

    @POST("/questions/{question_id}/favorite")
    void favoriteQuestion(@Path("question_id") long questionId, @Body String emptyBody, Callback<BaseResponse> callback);

    //ignorar pergunta
    @POST("/questions/{question_id}/ignore")
    void ignoreQuestion(@Path("question_id") long questionId, @Body String emptyBody, Callback<BaseResponse> callback);

    //answers

    @POST("/questions/{question_id}/answers")
    @Multipart
    void answerQuestion(@Path("question_id") long questionId,
                        @Part("answer[body_text]") String answer,
                        @Part("answer[image]") TypedFile photoPath,
                        Callback<AnswerResponse> callback);

    @PUT("/questions/{question_id}/answers/{answer_id}")
    @Multipart
    void updateAnswer(@Path("question_id") long questionId,
                      @Path("answer_id") long answerId,
                      @Part("answer[body_text]") String answer,
                      @Part("answer[image]") TypedFile photoPath,
                      @Part("_clear_attrs") String clearAttrs,
                      Callback<AnswerResponse> callback);


    @GET("/questions/{question_id}/answers")
    void getAnswers(@Path("question_id") long questionId, @Query("page") Integer page, @Query("per_page") Integer perPage, Callback<AnswersResponse> callback);

    @DELETE("/questions/{question_id}/answers/{answer_id}")
    void deleteAnswer(@Path("question_id") long questionId, @Path("answer_id") long answerId, Callback<BaseResponse> callback);

    @DELETE("/questions/{question_id}/answers/{answer_id}/upvote")
    void downvoteQuestion(@Path("question_id") long questionId, @Path("answer_id") long answerId, Callback<BaseResponse> callback);

    @POST("/questions/{question_id}/answers/{answer_id}/upvote")
    void upvoteAnswer(@Path("question_id") long questionId, @Path("answer_id") long answerId, @Body String emptyBody, Callback<BaseResponse> callback);

}
