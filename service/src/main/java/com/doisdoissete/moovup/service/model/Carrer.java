package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by broto on 9/23/15.
 */
public class Carrer extends RealmObject {

    @PrimaryKey
    @Required
    @Expose
    private Long id;

    @Expose
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}
