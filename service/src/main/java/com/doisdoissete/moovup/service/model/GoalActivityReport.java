package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

/**
 * Created by ranipieper on 2/17/16.
 */
public class GoalActivityReport {

    @Expose
    @SerializedGsonName("total_count")
    private Long totalCount;

    @Expose
    @SerializedGsonName("total_by_interval")
    private Float totalByInterval;

    @Expose
    @SerializedGsonName("date_interval")
    private Date dateInterval;

    public Long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(Long totalCount) {
        this.totalCount = totalCount;
    }

    public Float getTotalByInterval() {
        return totalByInterval;
    }

    public void setTotalByInterval(Float totalByInterval) {
        this.totalByInterval = totalByInterval;
    }

    public Date getDateInterval() {
        return dateInterval;
    }

    public void setDateInterval(Date dateInterval) {
        this.dateInterval = dateInterval;
    }
}
