package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.BaseResponse;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.POST;
import retrofit.http.Path;

/**
 * Created by ranieripieper on 12/2/15.
 */
public interface ReportService {

    int REPORT_TYPE_INAPPROPRIATE_CONTENT = 1;
    int REPORT_TYPE_SPAM = 2;
    int REPORT_TYPE_INAPPROPRIATE_COMMENT = 3;

    @POST("/questions/{question_id}/report")
    @FormUrlEncoded
    void reportQuestion(@Path("question_id") long questionId,
                @Field("report_type") int reportType,
                Callback<BaseResponse> callback);

    @POST("/answers/{answer_id}/report")
    @FormUrlEncoded
    void reportAnswers(@Path("answer_id") long answerId,
                        @Field("report_type") int reportType,
                        Callback<BaseResponse> callback);
}
