package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Carrer;

import java.util.List;

import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class CarrerCacheManager extends BaseCache<Carrer> {

    private static CarrerCacheManager instance = new CarrerCacheManager();

    private CarrerCacheManager() {
    }

    public static CarrerCacheManager getInstance() {
        return instance;
    }

    @Override
    public List<Carrer> getAll() {
        return getRealm().where(getReferenceClass()).findAllSorted("title", Sort.ASCENDING);
    }

    @Override
    public Class<Carrer> getReferenceClass() {
        return Carrer.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
