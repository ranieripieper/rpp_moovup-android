package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.User;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class LinkedData {

    @Expose
    private List<User> users;

    @Expose
    private List<Category> categories;

    @Expose
    public List<Goal> goals;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }
}
