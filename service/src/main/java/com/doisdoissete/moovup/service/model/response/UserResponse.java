package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 11/25/15.
 */
public class UserResponse extends BaseResponse {

    @Expose
    @SerializedName("user")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
