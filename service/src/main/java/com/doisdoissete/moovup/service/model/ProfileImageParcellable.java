package com.doisdoissete.moovup.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by broto on 11/5/15.
 */
public class ProfileImageParcellable implements Parcelable {

    @Expose
    @SerializedGsonName("thumb")
    private String thumb;

    @Expose
    @SerializedGsonName("medium")
    private String medium;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.thumb);
        dest.writeString(this.medium);
    }

    public ProfileImageParcellable() {
    }

    protected ProfileImageParcellable(Parcel in) {
        this.thumb = in.readString();
        this.medium = in.readString();
    }

    public static final Parcelable.Creator<ProfileImageParcellable> CREATOR = new Parcelable.Creator<ProfileImageParcellable>() {
        @Override
        public ProfileImageParcellable createFromParcel(Parcel source) {
            return new ProfileImageParcellable(source);
        }

        @Override
        public ProfileImageParcellable[] newArray(int size) {
            return new ProfileImageParcellable[size];
        }
    };
}
