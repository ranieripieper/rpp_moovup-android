package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.CommercialActivityResponse;

import retrofit.Callback;
import retrofit.http.GET;

/**
 * Created by ranipieper on 11/11/15.
 */
public interface CommercialActivityService {

    @GET("/cached/commercial_activities")
    void getAllCommercialActivities(Callback<CommercialActivityResponse> callback);
}
