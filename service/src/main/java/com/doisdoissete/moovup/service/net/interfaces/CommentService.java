package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.AnswerResponse;
import com.doisdoissete.moovup.service.model.response.CommentsResponse;

import retrofit.Callback;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ranipieper on 12/14/15.
 */
public interface CommentService {

        //Get Comments
    @GET("/questions/{question_id}/answers/{answer_id}/comments")
    void getComments(@Path("question_id") Long questionId, @Path("answer_id") Long answerId, @Query("page") Integer page, @Query("per_page") Integer perPage, Callback<CommentsResponse> callback);

    //Post Comment
    @POST("/questions/{question_id}/answers/{answer_id}/comments")
    @FormUrlEncoded
    void addComment(@Path("question_id") Long questionId, @Path("answer_id") Long answerId, @Field("answer[body_text]") String comment, Callback<AnswerResponse> callback);

}
