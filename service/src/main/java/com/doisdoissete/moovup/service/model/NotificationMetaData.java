package com.doisdoissete.moovup.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 2/18/16.
 */
public class NotificationMetaData implements Parcelable {

    @Expose
    @SerializedGsonName("father_resource_id")
    private Long fatherResourceId;

    @Expose
    @SerializedGsonName("goal_id")
    private Long goalId;

    @Expose
    @SerializedGsonName("answer_id")
    private Long answerId;

    @Expose
    @SerializedGsonName("question_id")
    private Long questionId;

    public Long getFatherResourceId() {
        return fatherResourceId;
    }

    public void setFatherResourceId(Long fatherResourceId) {
        this.fatherResourceId = fatherResourceId;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public Long getAnswerId() {
        return answerId;
    }

    public void setAnswerId(Long answerId) {
        this.answerId = answerId;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.fatherResourceId);
        dest.writeValue(this.goalId);
        dest.writeValue(this.answerId);
        dest.writeValue(this.questionId);
    }

    public NotificationMetaData() {
    }

    protected NotificationMetaData(Parcel in) {
        this.fatherResourceId = (Long) in.readValue(Long.class.getClassLoader());
        this.goalId = (Long) in.readValue(Long.class.getClassLoader());
        this.answerId = (Long) in.readValue(Long.class.getClassLoader());
        this.questionId = (Long) in.readValue(Long.class.getClassLoader());
    }

    public static final Parcelable.Creator<NotificationMetaData> CREATOR = new Parcelable.Creator<NotificationMetaData>() {
        @Override
        public NotificationMetaData createFromParcel(Parcel source) {
            return new NotificationMetaData(source);
        }

        @Override
        public NotificationMetaData[] newArray(int size) {
            return new NotificationMetaData[size];
        }
    };
}


