package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/25/15.
 */
public class UsersResponse extends BaseResponse {

    @Expose
    @SerializedName("users")
    private List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }
}
