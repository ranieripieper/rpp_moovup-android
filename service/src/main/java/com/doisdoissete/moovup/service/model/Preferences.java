package com.doisdoissete.moovup.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 2/19/16.
 */
public class Preferences implements Parcelable {

    public static final String NOTIFY_WELCOME = "notify_welcome";
    public static final String NOTIFY_GOALS_OUTDATED = "notify_goals_outdated";
    public static final String NOTIFY_PASSWORD_UPDATED = "notify_password_updated";
    public static final String NOTIFY_NEW_LIKE_FOR_GOAL = "notify_new_like_for_goal";
    public static final String NOTIFY_OFFICIAL_STATEMENT = "notify_official_statement";
    public static final String NOTIFY_ACTIVITIES_OUTDATED = "notify_activities_outdated";
    public static final String NOTIFY_NEW_LIKE_FOR_ANSWER = "notify_new_like_for_answer";
    public static final String NOTIFY_NEW_LIKE_FOR_ACTIVITY = "notify_new_like_for_activity";
    public static final String NOTIFY_NEW_COMMENT_FOR_ANSWER = "notify_new_comment_for_answer";
    public static final String NOTIFY_NEW_FRIENDSHIP_REQUEST = "notify_new_friendship_request";
    public static final String NOTIFY_NEW_ANSWER_FOR_QUESTION = "notify_new_answer_for_question";
    public static final String NOTIFY_NEW_QUESTIONS_TO_ANSWER = "notify_new_questions_to_answer";
    public static final String NOTIFY_FRIENDSHIP_REQUEST_ACCEPTED = "notify_friendship_request_accepted";


    @Expose
    @SerializedGsonName(NOTIFY_WELCOME)
    private Boolean notifyWelcome;

    @Expose
    @SerializedGsonName(NOTIFY_GOALS_OUTDATED)
    private Boolean notifyGoalsOutdated;

    @Expose
    @SerializedGsonName(NOTIFY_PASSWORD_UPDATED)
    private Boolean notifyPasswordUpdated;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_LIKE_FOR_GOAL)
    private Boolean notifyNewLikeForGoal;

    @Expose
    @SerializedGsonName(NOTIFY_OFFICIAL_STATEMENT)
    private Boolean notifyOfficialStatement;

    @Expose
    @SerializedGsonName(NOTIFY_ACTIVITIES_OUTDATED)
    private Boolean notifyActivitiesOutdated;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_LIKE_FOR_ANSWER)
    private Boolean notifyNewLikeForAnswer;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_LIKE_FOR_ACTIVITY)
    private Boolean notifyNewLikeForActivity;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_COMMENT_FOR_ANSWER)
    private Boolean notifyNewCommentForAnswer;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_FRIENDSHIP_REQUEST)
    private Boolean notifyNewFriendshipRequest;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_ANSWER_FOR_QUESTION)
    private Boolean notifyNewAnswerForQuestion;

    @Expose
    @SerializedGsonName(NOTIFY_NEW_QUESTIONS_TO_ANSWER)
    private Boolean notifyNewQuestionsToAnswer;

    @Expose
    @SerializedGsonName(NOTIFY_FRIENDSHIP_REQUEST_ACCEPTED)
    private Boolean notifyFriendshipRequestAccepted;

    public Boolean isNotifyWelcome() {
        return notifyWelcome;
    }

    public void setNotifyWelcome(Boolean notifyWelcome) {
        this.notifyWelcome = notifyWelcome;
    }

    public Boolean isNotifyGoalsOutdated() {
        return notifyGoalsOutdated;
    }

    public void setNotifyGoalsOutdated(Boolean notifyGoalsOutdated) {
        this.notifyGoalsOutdated = notifyGoalsOutdated;
    }

    public Boolean isNotifyPasswordUpdated() {
        return notifyPasswordUpdated;
    }

    public void setNotifyPasswordUpdated(Boolean notifyPasswordUpdated) {
        this.notifyPasswordUpdated = notifyPasswordUpdated;
    }

    public Boolean isNotifyNewLikeForGoal() {
        return notifyNewLikeForGoal;
    }

    public void setNotifyNewLikeForGoal(Boolean notifyNewLikeForGoal) {
        this.notifyNewLikeForGoal = notifyNewLikeForGoal;
    }

    public Boolean isNotifyOfficialStatement() {
        return notifyOfficialStatement;
    }

    public void setNotifyOfficialStatement(Boolean notifyOfficialStatement) {
        this.notifyOfficialStatement = notifyOfficialStatement;
    }

    public Boolean isNotifyActivitiesOutdated() {
        return notifyActivitiesOutdated;
    }

    public void setNotifyActivitiesOutdated(Boolean notifyActivitiesOutdated) {
        this.notifyActivitiesOutdated = notifyActivitiesOutdated;
    }

    public Boolean isNotifyNewLikeForAnswer() {
        return notifyNewLikeForAnswer;
    }

    public void setNotifyNewLikeForAnswer(Boolean notifyNewLikeForAnswer) {
        this.notifyNewLikeForAnswer = notifyNewLikeForAnswer;
    }

    public Boolean isNotifyNewLikeForActivity() {
        return notifyNewLikeForActivity;
    }

    public void setNotifyNewLikeForActivity(Boolean notifyNewLikeForActivity) {
        this.notifyNewLikeForActivity = notifyNewLikeForActivity;
    }

    public Boolean isNotifyNewCommentForAnswer() {
        return notifyNewCommentForAnswer;
    }

    public void setNotifyNewCommentForAnswer(Boolean notifyNewCommentForAnswer) {
        this.notifyNewCommentForAnswer = notifyNewCommentForAnswer;
    }

    public Boolean isNotifyNewFriendshipRequest() {
        return notifyNewFriendshipRequest;
    }

    public void setNotifyNewFriendshipRequest(Boolean notifyNewFriendshipRequest) {
        this.notifyNewFriendshipRequest = notifyNewFriendshipRequest;
    }

    public Boolean isNotifyNewAnswerForQuestion() {
        return notifyNewAnswerForQuestion;
    }

    public void setNotifyNewAnswerForQuestion(Boolean notifyNewAnswerForQuestion) {
        this.notifyNewAnswerForQuestion = notifyNewAnswerForQuestion;
    }

    public Boolean isNotifyNewQuestionsToAnswer() {
        return notifyNewQuestionsToAnswer;
    }

    public void setNotifyNewQuestionsToAnswer(Boolean notifyNewQuestionsToAnswer) {
        this.notifyNewQuestionsToAnswer = notifyNewQuestionsToAnswer;
    }

    public Boolean isNotifyFriendshipRequestAccepted() {
        return notifyFriendshipRequestAccepted;
    }

    public void setNotifyFriendshipRequestAccepted(Boolean notifyFriendshipRequestAccepted) {
        this.notifyFriendshipRequestAccepted = notifyFriendshipRequestAccepted;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.notifyWelcome);
        dest.writeValue(this.notifyGoalsOutdated);
        dest.writeValue(this.notifyPasswordUpdated);
        dest.writeValue(this.notifyNewLikeForGoal);
        dest.writeValue(this.notifyOfficialStatement);
        dest.writeValue(this.notifyActivitiesOutdated);
        dest.writeValue(this.notifyNewLikeForAnswer);
        dest.writeValue(this.notifyNewLikeForActivity);
        dest.writeValue(this.notifyNewCommentForAnswer);
        dest.writeValue(this.notifyNewFriendshipRequest);
        dest.writeValue(this.notifyNewAnswerForQuestion);
        dest.writeValue(this.notifyNewQuestionsToAnswer);
        dest.writeValue(this.notifyFriendshipRequestAccepted);
    }

    public Preferences() {
    }

    protected Preferences(Parcel in) {
        this.notifyWelcome = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyGoalsOutdated = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyPasswordUpdated = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewLikeForGoal = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyOfficialStatement = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyActivitiesOutdated = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewLikeForAnswer = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewLikeForActivity = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewCommentForAnswer = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewFriendshipRequest = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewAnswerForQuestion = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyNewQuestionsToAnswer = (Boolean) in.readValue(Boolean.class.getClassLoader());
        this.notifyFriendshipRequestAccepted = (Boolean) in.readValue(Boolean.class.getClassLoader());
    }

    public static final Parcelable.Creator<Preferences> CREATOR = new Parcelable.Creator<Preferences>() {
        @Override
        public Preferences createFromParcel(Parcel source) {
            return new Preferences(source);
        }

        @Override
        public Preferences[] newArray(int size) {
            return new Preferences[size];
        }
    };
}
