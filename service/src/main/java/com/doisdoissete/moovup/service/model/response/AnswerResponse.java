package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Answers;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 11/13/15.
 */
public class AnswerResponse extends BaseResponse {

    @Expose
    @SerializedName("answer")
    private Answers answer;

    public Answers getAnswer() {
        return answer;
    }

    public void setAnswer(Answers answer) {
        this.answer = answer;
    }
}
