package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

/**
 * Created by broto on 11/5/15.
 */
public class ProfileImage extends RealmObject {

    @Expose
    @SerializedGsonName("thumb")
    private String thumb;

    @Expose
    @SerializedGsonName("medium")
    private String medium;

    public String getThumb() {
        return thumb;
    }

    public void setThumb(String thumb) {
        this.thumb = thumb;
    }

    public String getMedium() {
        return medium;
    }

    public void setMedium(String medium) {
        this.medium = medium;
    }
}
