package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.CarrerResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 11/11/15.
 */
public interface CarrerService {

    @GET("/cached/carrers")
    void getCareers(@Query("per_page") int perPage, Callback<CarrerResponse> callback);
}
