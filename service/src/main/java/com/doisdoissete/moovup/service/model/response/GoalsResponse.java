package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Goal;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalsResponse extends BaseResponse {

    @Expose
    @SerializedName("goals")
    private List<Goal> goals;

    public List<Goal> getGoals() {
        return goals;
    }

    public void setGoals(List<Goal> goals) {
        this.goals = goals;
    }
}
