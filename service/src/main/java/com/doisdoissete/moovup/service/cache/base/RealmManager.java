package com.doisdoissete.moovup.service.cache.base;

import android.content.Context;

import com.doisdoissete.moovup.service.BuildConfig;
import com.doisdoissete.moovup.service.net.RetrofitManager;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmObject;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by jeffcailteux on 7/29/15.
 */
public class RealmManager {

    private Realm realm;
    private static final long REALMVERSION = 3;

    public static final ThreadLocal<RealmManager> realmManager = new ThreadLocal<RealmManager>() {
        @Override
        protected RealmManager initialValue() {
            return new RealmManager();
        }
    };

    private RealmManager() {
    }

    private static Realm getRealm(Context context) {
        RealmConfiguration config;
        if (!BuildConfig.DEBUG) {
            config = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .migration(new MoovupRealmMigration())
                    .build();
        } else {
            config = new RealmConfiguration.Builder(context)
                    .schemaVersion(REALMVERSION)
                    .deleteRealmIfMigrationNeeded()
                    .build();
        }

        Realm.setDefaultConfiguration(config);
        return Realm.getInstance(config);
    }

    /**
     * Gets default instance in MainThread
     *
     * @return
     */
    public static RealmManager getInstance() {
        return getInstance(null);
    }

    /**
     * Gets instance in actual Thread
     *
     * @param context
     * @return
     */
    public static RealmManager getInstance(Context context) {
        RealmManager rm = realmManager.get();
        if (rm.realm == null && context != null) {
            rm.realm = getRealm(context);
        } else if (rm.realm == null && context == null) {
            rm.realm = Realm.getDefaultInstance();
        }
        return rm;
    }

    public <T extends RealmObject> void updateWithoutNulls(List<T> lst, Class<T> type) {
        if (lst != null) {
            RealmManager rm = realmManager.get();
            rm.realm .beginTransaction();
            String json = RetrofitManager.getGsonRealm().toJson(lst);
            rm.realm.createOrUpdateAllFromJson(type, json);

            rm.realm .commitTransaction();
        }
    }

    /**
     * Gets all objects
     *
     * @param type
     * @param sortField
     * @param sortAscending
     * @param <T>
     * @return
     */
    public <T extends RealmObject> List<T> getAll(Class<T> type, String sortField, boolean sortAscending) {
        return getAll(type, sortField, sortAscending ? Sort.ASCENDING : Sort.DESCENDING);
    }

    /**
     * Gets all objects
     *
     * @param type
     * @param sortField
     * @param sort
     * @param <T>
     * @return
     */
    public <T extends RealmObject> List<T> getAll(Class<T> type, String sortField, Sort sort) {
        RealmResults<T> allRealmObjects = realm.where(type).findAllSorted(sortField, sort);
        return allRealmObjects;
    }

    /**
     * Gets total items
     *
     * @param type
     * @param <T>
     * @return
     */
    public <T extends RealmObject> long count(Class<T> type) {
        return realm.where(type).count();
    }

    public void put(RealmObject t) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(t);
        realm.commitTransaction();
    }

    public <T extends RealmObject> void put(RealmObject obj, boolean updateExpireTime, Class<T> type) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(obj);
        if (updateExpireTime) {
            Sync sync = new Sync(type.getName(), System.currentTimeMillis());
            realm.copyToRealmOrUpdate(sync);
        }
        realm.commitTransaction();
    }

    public <T extends RealmObject> void insert(RealmObject obj, Class<T> type) {
        realm.beginTransaction();
        realm.copyToRealm(obj);
        realm.commitTransaction();
    }

    public void putAll(List<RealmObject> t) {
        realm.beginTransaction();

        for (RealmObject item : t) {
            realm.copyToRealmOrUpdate(item);
        }

        realm.commitTransaction();
    }


    public <T extends RealmObject> void delete(final long id, final Class<T> type) {
        realm.setAutoRefresh(true);
        realm.beginTransaction();
        realm.where(type).equalTo("id", id).findFirst().deleteFromRealm();
        realm.commitTransaction();
    }


    public <T extends RealmObject> void deleteAll(Class<T> type) {
        realm.setAutoRefresh(true);
        realm.beginTransaction();
        realm.where(type).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }

    public <T extends RealmObject> T get(int id, Class<T> type) {
        return realm.where(type).equalTo("id", id).findFirst();
    }

    public <T extends RealmObject> List<T> getAll(Class<T> type) {
        return realm.where(type).findAll();
    }

    public Realm getRealm() {
        return realm;
    }
}
