package com.doisdoissete.moovup.service.util;

import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by ranieripieper on 12/2/15.
 */
public class NumberUtil {

    public static String getNumberFormated(Long value) {
        if (value == null) {
            return "0";
        } else {
            return getNumberFormated(Double.valueOf(value)).trim();
        }
    }

    public static String getNumberFormated(Double value) {
        if (value == null || value == 0.0) {
            return "0";
        } else {
            int power;
            String suffix = " kmbt";
            String formattedNumber = "";

            NumberFormat formatter = new DecimalFormat("#,###.#");
            power = (int) StrictMath.log10(value);
            value = value / (Math.pow(10, (power / 3) * 3));
            formattedNumber = formatter.format(value);
            formattedNumber = formattedNumber + suffix.charAt(power / 3);
            return formattedNumber.length() > 4 ? formattedNumber.replaceAll("\\.[0-9]+", "") : formattedNumber;
        }
    }
}
