package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ranipieper on 1/20/16.
 */
public class GoalActivity extends RealmObject {

    @Expose
    @PrimaryKey
    @Required
    @SerializedGsonName("id")
    private Long id;

    @Expose
    @SerializedGsonName("body_text")
    private String bodyText;

    @Expose
    @SerializedGsonName("total")
    private Float total;

    @Expose
    @SerializedGsonName("date")
    private Date date;

    @Expose
    @SerializedGsonName("goal_id")
    private Long goalId;

    @Expose
    @SerializedGsonName("has_uploaded_image")
    private boolean hasUploadedImage;

    @Expose
    @SerializedGsonName("images_url")
    private ProfileImage imagesUrl;

    @Expose
    @SerializedGsonName("user_id")
    private Long userId;

    @Expose
    @SerializedGsonName("upvotes_count")
    private Long upvotesCount;

    @Expose
    @SerializedGsonName("liked")
    private boolean liked;

    private Goal goal;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Long getGoalId() {
        return goalId;
    }

    public void setGoalId(Long goalId) {
        this.goalId = goalId;
    }

    public boolean isHasUploadedImage() {
        return hasUploadedImage;
    }

    public void setHasUploadedImage(boolean hasUploadedImage) {
        this.hasUploadedImage = hasUploadedImage;
    }

    public ProfileImage getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(ProfileImage imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUpvotesCount() {
        return upvotesCount;
    }

    public void setUpvotesCount(Long upvotesCount) {
        this.upvotesCount = upvotesCount;
    }

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

}
