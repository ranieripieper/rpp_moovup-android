package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.SerializedGsonName;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalActivitiesResponse extends BaseResponse {

    @Expose
    @SerializedGsonName("activities")
    private List<GoalActivity> goalActivities;

    @Expose
    @SerializedGsonName("linked_data")
    private LinkedData linkedData;

    public List<GoalActivity> getGoalActivities() {
        return goalActivities;
    }

    public void setGoalActivities(List<GoalActivity> goalActivities) {
        this.goalActivities = goalActivities;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
