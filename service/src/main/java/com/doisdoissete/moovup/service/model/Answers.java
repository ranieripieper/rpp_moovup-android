package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by ranipieper on 11/16/15.
 */
public class Answers extends RealmObject {

    @PrimaryKey
    @Required
    @Expose
    private Long id;

    @Expose
    @SerializedGsonName("body_text")
    private String bodyText;

    @Expose
    @SerializedGsonName("user_id")
    private Long userId;

    @Expose
    @SerializedGsonName("upvotes_count")
    private Long upvotesCount;

    @Expose
    @SerializedGsonName("question_id")
    private Long questionId;

    @Expose
    @SerializedGsonName("parent_id")
    private Long parentId;

    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updated_at")
    private Date updatedAt;

    @Expose
    @SerializedGsonName("reported_at")
    private Date reportedAt;

    @Expose
    @SerializedGsonName("replies_count")
    private long repliesCount;

    @Expose
    @SerializedGsonName("images_url")
    private ProfileImage imagesUrl;

    @Expose
    @SerializedGsonName("has_uploaded_image")
    private boolean hasUploadedImage;

    @Expose
    @SerializedGsonName("liked")
    private boolean liked;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getUpvotesCount() {
        return upvotesCount;
    }

    public void setUpvotesCount(Long upvotesCount) {
        this.upvotesCount = upvotesCount;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Date getReportedAt() {
        return reportedAt;
    }

    public void setReportedAt(Date reportedAt) {
        this.reportedAt = reportedAt;
    }

    public long getRepliesCount() {
        return repliesCount;
    }

    public void setRepliesCount(long repliesCount) {
        this.repliesCount = repliesCount;
    }

    public ProfileImage getImagesUrl() {
        return imagesUrl;
    }

    public void setImagesUrl(ProfileImage imagesUrl) {
        this.imagesUrl = imagesUrl;
    }

    public boolean isHasUploadedImage() {
        return hasUploadedImage;
    }

    public void setHasUploadedImage(boolean hasUploadedImage) {
        this.hasUploadedImage = hasUploadedImage;
    }

    public boolean isLiked() {
        return liked;
    }

    public void setLiked(boolean liked) {
        this.liked = liked;
    }

}
