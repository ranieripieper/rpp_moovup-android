package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

/**
 * Created by ranipieper on 2/12/16.
 */
public class Friendship {

    @Expose
    @SerializedGsonName("id")
    private Long id;

    @Expose
    @SerializedGsonName("friend_id")
    private Long friendId;

    @Expose
    @SerializedGsonName("status")
    private String status;

    @Expose
    @SerializedGsonName("statusChangedAt")
    private Date statusChangedAt;

    @Expose
    @SerializedGsonName("createdAt")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updatedAt")
    private Date updatedAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getFriendId() {
        return friendId;
    }

    public void setFriendId(Long friendId) {
        this.friendId = friendId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getStatusChangedAt() {
        return statusChangedAt;
    }

    public void setStatusChangedAt(Date statusChangedAt) {
        this.statusChangedAt = statusChangedAt;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
