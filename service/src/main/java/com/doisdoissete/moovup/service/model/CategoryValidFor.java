package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

/**
 * Created by ranipieper on 2/19/16.
 */
public class CategoryValidFor extends RealmObject {

    @Expose
    @SerializedGsonName("goals")
    private boolean goals;

    @Expose
    @SerializedGsonName("questions")
    private boolean questions;

    @Expose
    @SerializedGsonName("interests")
    private boolean interests;

    public boolean isGoals() {
        return goals;
    }

    public void setGoals(boolean goals) {
        this.goals = goals;
    }

    public boolean isQuestions() {
        return questions;
    }

    public void setQuestions(boolean questions) {
        this.questions = questions;
    }

    public boolean isInterests() {
        return interests;
    }

    public void setInterests(boolean interests) {
        this.interests = interests;
    }

}
