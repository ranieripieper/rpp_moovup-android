package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.InterestResponse;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Query;

/**
 * Created by ranipieper on 1/8/16.
 */
public interface InterestsService {

    //adiciona interesse
    @POST("/users/me/interests")
    @FormUrlEncoded
    void addInterest(@Field("interest[category_id]") Long categoryId, @Field("interest[max_answers_count]") Long maxAnswerCount, Callback<BaseResponse> callback);

    //remove interesse
    @DELETE("/users/me/interests")
    void removeInterest(@Query("category_id") Long categoryId, Callback<BaseResponse> callback);

    @GET("/users/me/interests")
    void getMyInterests(Callback<InterestResponse> callback);

}
