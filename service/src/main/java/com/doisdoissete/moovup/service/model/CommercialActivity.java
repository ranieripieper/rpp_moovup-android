package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by broto on 10/9/15.
 */
public class CommercialActivity extends RealmObject {

    @PrimaryKey
    @Required
    @Expose
    private Long id;

    @Expose
    private String title;

    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updated_at")
    private Date updatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
}
