package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.AuthData;
import com.doisdoissete.moovup.service.model.User;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 9/20/15.
 */
public class ResponseResult extends BaseResponse {

    @Expose
    @SerializedName("user_exists")
    private boolean userExists;

    @Expose
    @SerializedName("changed_attributes")
    private List<String> changedAttributes;

    @Expose
    @SerializedName("user_data")
    private User user;

    @Expose
    @SerializedName("auth_data")
    private AuthData authData;

    @Expose
    @SerializedName("new_user")
    private boolean newUser;

    @Expose
    @SerializedName("reset_password_token")
    private String resetPasswordToken;

    public boolean isUserExists() {
        return userExists;
    }

    public void setUserExists(boolean userExists) {
        this.userExists = userExists;
    }

    public List<String> getChangedAttributes() {
        return changedAttributes;
    }

    public void setChangedAttributes(List<String> changedAttributes) {
        this.changedAttributes = changedAttributes;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public AuthData getAuthData() {
        return authData;
    }

    public void setAuthData(AuthData authData) {
        this.authData = authData;
    }

    public boolean isNewUser() {
        return newUser;
    }

    public void setNewUser(boolean newUser) {
        this.newUser = newUser;
    }

    public String getResetPasswordToken() {
        return resetPasswordToken;
    }

    public void setResetPasswordToken(String resetPasswordToken) {
        this.resetPasswordToken = resetPasswordToken;
    }
}
