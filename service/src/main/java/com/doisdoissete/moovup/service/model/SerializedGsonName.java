package com.doisdoissete.moovup.service.model;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Created by ranipieper on 12/16/15.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SerializedGsonName {
    /**
     * @return the desired name of the field when it is serialized
     */
    String value();
}
