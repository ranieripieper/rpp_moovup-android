package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Question;

import io.realm.Realm;

/**
 * Created by ranipieper on 11/19/15.
 */
public class AnswerCacheManager extends BaseCache<Answers> {

    private static AnswerCacheManager instance = new AnswerCacheManager();

    private AnswerCacheManager() {
    }

    public static AnswerCacheManager getInstance() {
        return instance;
    }

    public void delete(long questionId, long answerId) {
        getRealm().beginTransaction();
        getRealm().where(Answers.class).equalTo("id", answerId).findFirst().deleteFromRealm();
        Question question = QuestionCacheManager.getInstance().get(questionId);
        question.setAnswersCount(question.getAnswersCount() - 1);
        getRealm().commitTransaction();
    }

    public void put(long questionId, Answers answers) {
        getRealm().setAutoRefresh(true);
        getRealm().beginTransaction();
        getRealm().copyToRealmOrUpdate(answers);
        Question question = QuestionCacheManager.getInstance().get(questionId);
        question.setAnswersCount(question.getAnswersCount() + 1);
        getRealm().commitTransaction();
    }

    public void putComment(long answerId, Answers answers) {
        getRealm().beginTransaction();
        getRealm().copyToRealmOrUpdate(answers);
        Answers answer = get(answerId);
        answer.setRepliesCount(answer.getRepliesCount() + 1);
        getRealm().commitTransaction();
    }

    public void deleteComment(long answerId, long commentId) {
        getRealm().beginTransaction();
        Answers answerRemove = getRealm().where(Answers.class).equalTo("id", commentId).findFirst();
        if (answerRemove != null) {
            answerRemove.deleteFromRealm();
        }
        Answers answers = get(answerId);
        answers.setRepliesCount(answers.getRepliesCount() - 1);
        getRealm().commitTransaction();
    }

    public Answers updateUpvoute(long id, boolean upvote) {
        Realm realm = getRealm();
        realm.beginTransaction();
        Answers updateObj = get(id);
        updateObj.setLiked(upvote);
        updateObj.setUpvotesCount(updateObj.getUpvotesCount() + (upvote ? 1 : -1));
        realm.commitTransaction();
        return updateObj;
    }

    @Override
    public Class<Answers> getReferenceClass() {
        return Answers.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
