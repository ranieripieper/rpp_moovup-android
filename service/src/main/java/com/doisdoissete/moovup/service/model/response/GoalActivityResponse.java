package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.SerializedGsonName;
import com.google.gson.annotations.Expose;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalActivityResponse extends BaseResponse {

    @Expose
    @SerializedGsonName("activity")
    private GoalActivity activity;

    @Expose
    @SerializedGsonName("linked_data")
    private LinkedData linkedData;

    public GoalActivity getActivity() {
        return activity;
    }

    public void setActivity(GoalActivity activity) {
        this.activity = activity;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
