package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by broto on 10/16/15.
 */
public class Goal extends RealmObject {

    @Expose
    @PrimaryKey
    @Required
    @SerializedGsonName("id")
    private Long id;

    @Expose
    @SerializedGsonName("category_id")
    private long categoryId;

    @Expose
    @SerializedGsonName("periodicity_type")
    private String periodicityType;

    @Expose
    private float target;

    @Expose
    private float current;

    @Expose
    @SerializedGsonName("total")
    private Float total;

    @Expose
    @SerializedGsonName("target_date")
    private Date targetDate;

    @Expose
    @SerializedGsonName("public")
    private boolean shared;

    @Expose
    @SerializedGsonName("likes_count")
    private long likesCount;

    @Expose
    @SerializedGsonName("user")
    private User user;

    @Expose
    @SerializedGsonName("user_id")
    private Long userId;

    @Expose
    @SerializedGsonName("category")
    private Category category;

    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updated_at")
    private Date updatedAt;

    @Expose
    @SerializedGsonName("current_activity")
    private GoalActivity currentActivity;

    @Expose
    @SerializedGsonName("activities_reports")
    @Ignore
    private List<GoalActivityReport> goalActivitiesReport;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public String getPeriodicityType() {
        return periodicityType;
    }

    public void setPeriodicityType(String periodicityType) {
        this.periodicityType = periodicityType;
    }


    public Date getTargetDate() {
        return targetDate;
    }

    public void setTargetDate(Date targetDate) {
        this.targetDate = targetDate;
    }

    public boolean isShared() {
        return shared;
    }

    public void setShared(boolean shared) {
        this.shared = shared;
    }


    public long getLikesCount() {
        return likesCount;
    }

    public void setLikesCount(long likesCount) {
        this.likesCount = likesCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public GoalActivity getCurrentActivity() {
        return currentActivity;
    }

    public void setCurrentActivity(GoalActivity currentActivity) {
        this.currentActivity = currentActivity;
    }

    public List<GoalActivityReport> getGoalActivitiesReport() {
        return goalActivitiesReport;
    }

    public void setGoalActivitiesReport(List<GoalActivityReport> goalActivitiesReport) {
        this.goalActivitiesReport = goalActivitiesReport;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public float getTarget() {
        return target;
    }

    public void setTarget(float target) {
        this.target = target;
    }

    public float getCurrent() {
        return current;
    }

    public void setCurrent(float current) {
        this.current = current;
    }

    public Float getTotal() {
        return total;
    }

    public void setTotal(Float total) {
        this.total = total;
    }
}
