package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.CategoryResponse;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by ranipieper on 11/16/15.
 */
public interface CategoryService {

    @GET("/categories")
    void getCategories(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<CategoryResponse> callback);

}
