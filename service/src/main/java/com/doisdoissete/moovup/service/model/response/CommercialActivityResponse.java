package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.CommercialActivity;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 10/9/15.
 */
public class CommercialActivityResponse extends BaseResponse {

    @Expose
    @SerializedName("commercial_activities")
    private List<CommercialActivity> commercialActivities;

    public List<CommercialActivity> getCommercialActivities() {
        return commercialActivities;
    }

    public void setCommercialActivities(List<CommercialActivity> commercialActivities) {
        this.commercialActivities = commercialActivities;
    }
}
