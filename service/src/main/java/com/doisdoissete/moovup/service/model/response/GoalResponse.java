package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Goal;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalResponse extends BaseResponse {

    @Expose
    @SerializedName("goal")
    private Goal goal;

    public Goal getGoal() {
        return goal;
    }

    public void setGoal(Goal goal) {
        this.goal = goal;
    }
}
