package com.doisdoissete.moovup.service.model;

import io.realm.RealmObject;

/**
 * Created by ranipieper on 12/16/15.
 */
public class Configuration extends RealmObject {

    private Long currentUserId;
    private String currentAuthToken;

    public Long getCurrentUserId() {
        return currentUserId;
    }

    public void setCurrentUserId(Long currentUserId) {
        this.currentUserId = currentUserId;
    }

    public String getCurrentAuthToken() {
        return currentAuthToken;
    }

    public void setCurrentAuthToken(String currentAuthToken) {
        this.currentAuthToken = currentAuthToken;
    }
}
