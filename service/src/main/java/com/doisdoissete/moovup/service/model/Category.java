package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;


/**
 * Created by broto on 10/13/15.
 */
public class Category extends RealmObject {

    @Expose
    @PrimaryKey
    @Required
    private Long id;

    @Expose
    private int position;

    @Expose
    private String title;

    @Expose
    private boolean hobby;

    @Expose
    @SerializedGsonName("closed_goal")
    private boolean closedGoal;

    @Expose
    @SerializedGsonName("measurement_type")
    private String measurementType;

    @Expose
    @SerializedGsonName("measurement_text")
    private String measurementText;

    @Expose
    @SerializedGsonName("measurement_metric_texts")
    private MeasurementMetricTexts measurementMetricTexts;

    @Expose
    @SerializedGsonName("current_measurement_text")
    private String currentMeasurementText;

    @Expose
    @SerializedGsonName("target_measurement_text")
    private String targetMeasurementText;

    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;

    @Expose
    @SerializedGsonName("updated_at")
    private Date updatedAt;

    @Expose
    @SerializedGsonName("parent_id")
    private Long parentId;

    @Expose
    @SerializedGsonName("is_children")
    private boolean isChildren;

    @Expose
    @SerializedGsonName("children")
    private RealmList<Category> children;

    @Expose
    @SerializedGsonName("valid_for")
    private CategoryValidFor validFor;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHobby() {
        return hobby;
    }

    public void setHobby(boolean hobby) {
        this.hobby = hobby;
    }

    public String getMeasurementType() {
        return measurementType;
    }

    public void setMeasurementType(String measurementType) {
        this.measurementType = measurementType;
    }

    public String getMeasurementText() {
        return measurementText;
    }

    public void setMeasurementText(String measurementText) {
        this.measurementText = measurementText;
    }

    public MeasurementMetricTexts getMeasurementMetricTexts() {
        return measurementMetricTexts;
    }

    public void setMeasurementMetricTexts(MeasurementMetricTexts measurementMetricTexts) {
        this.measurementMetricTexts = measurementMetricTexts;
    }

    public String getCurrentMeasurementText() {
        return currentMeasurementText;
    }

    public void setCurrentMeasurementText(String currentMeasurementText) {
        this.currentMeasurementText = currentMeasurementText;
    }

    public String getTargetMeasurementText() {
        return targetMeasurementText;
    }

    public void setTargetMeasurementText(String targetMeasurementText) {
        this.targetMeasurementText = targetMeasurementText;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public RealmList<Category> getChildren() {
        return children;
    }

    public void setChildren(RealmList<Category> children) {
        this.children = children;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public boolean isClosedGoal() {
        return closedGoal;
    }

    public void setClosedGoal(boolean closedGoal) {
        this.closedGoal = closedGoal;
    }

    public CategoryValidFor getValidFor() {
        return validFor;
    }

    public void setValidFor(CategoryValidFor validFor) {
        this.validFor = validFor;
    }

    public boolean isChildren() {
        return isChildren;
    }

    public void setIsChildren(boolean isChildren) {
        this.isChildren = isChildren;
    }
}
