package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.cache.base.RealmManager;
import com.doisdoissete.moovup.service.model.Configuration;

/**
 * Created by ranipieper on 11/19/15.
 */
public class ConfigurationManager extends BaseCache<Configuration> {

    private static ConfigurationManager instance = new ConfigurationManager();

    private ConfigurationManager() {
    }

    public static ConfigurationManager getInstance() {
        return instance;
    }

    public Configuration get() {
        return getRealm().where(Configuration.class).findFirst();
    }

    @Override
    public void put(Configuration configuration) {
        RealmManager.getInstance().insert(configuration, Configuration.class);
    }

    @Override
    public Class<Configuration> getReferenceClass() {
        return Configuration.class;
    }

}
