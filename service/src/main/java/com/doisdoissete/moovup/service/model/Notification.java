package com.doisdoissete.moovup.service.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;

import java.util.Date;

/**
 * Created by ranipieper on 2/18/16.
 */
public class Notification implements Parcelable {

    /* basic */
    public static final String WELCOME = "welcome";
    public static final String PASSWORD_UPDATED = "password_updated";
    public static final String OFFICIAL_STATEMENT = "official_statement";

    /* goals & activities */
    public static final String GOALS_OUTDATED = "goals_outdated";
    public static final String ACTIVITIES_OUTDATED = "activities_outdated";
    public static final String NEW_LIKE_FOR_GOAL = "new_like_for_goal";
    public static final String NEW_LIKE_FOR_ACTIVITY = "new_like_for_activity";

    /* Q & A */
    public static final String NEW_QUESTIONS_TO_ANSWER = "new_questions_to_answer";
    public static final String NEW_ANSWER_FOR_QUESTION = "new_answer_for_question";
    public static final String NEW_COMMENT_FOR_ANSWER = "new_comment_for_answer";
    public static final String NEW_LIKE_FOR_QUESTION = "new_like_for_question";
    public static final String NEW_LIKE_FOR_ANSWER = "new_like_for_answer";

    /* Social Network */
    public static final String NEW_FRIENDSHIP_REQUEST = "new_friendship_request";
    public static final String FRIENDSHIP_REQUEST_ACCEPTED = "friendship_request_accepted";

    public static final String INVITE_FRIENDS = "invite_friends";

    //NOTIFICABLE
    public static final String NOTIFICABLE_TYPE_ACTIVITY = "Activity";
    public static final String NOTIFICABLE_TYPE_FRIENDSHIP = "Friendship";


    @Expose
    @SerializedGsonName("id")
    private Long id;
    @Expose
    @SerializedGsonName("receiver_user_id")
    private Long receiverUserId;
    @Expose
    @SerializedGsonName("sender_user_id")
    private Long senderUserId;
    @Expose
    @SerializedGsonName("receiver_user_image_url")
    private String receiverUserImageUrl;

    @Expose
    @SerializedGsonName("sender_user_image_url")
    private String senderUserImageUrl;

    @Expose
    @SerializedGsonName("notificable_image_url")
    private String notificableImageUrl;

    @Expose
    @SerializedGsonName("notification_type")
    private String notificationType;
    @Expose
    @SerializedGsonName("notificable_id")
    private Long notificableId;
    @Expose
    @SerializedGsonName("notificable_type")
    private String notificableType;
    @Expose
    @SerializedGsonName("read")
    private boolean read;
    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;
    @Expose
    @SerializedGsonName("updated_at")
    private Date updatedAt;
    @Expose
    @SerializedGsonName("message")
    private String message;
    @Expose
    @SerializedGsonName("metadata")
    private NotificationMetaData notificationMetaData;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getReceiverUserId() {
        return receiverUserId;
    }

    public void setReceiverUserId(Long receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    public Long getSenderUserId() {
        return senderUserId;
    }

    public void setSenderUserId(Long senderUserId) {
        this.senderUserId = senderUserId;
    }

    public String getReceiverUserImageUrl() {
        return receiverUserImageUrl;
    }

    public void setReceiverUserImageUrl(String receiverUserImageUrl) {
        this.receiverUserImageUrl = receiverUserImageUrl;
    }

    public String getSenderUserImageUrl() {
        return senderUserImageUrl;
    }

    public void setSenderUserImageUrl(String senderUserImageUrl) {
        this.senderUserImageUrl = senderUserImageUrl;
    }

    public String getNotificableImageUrl() {
        return notificableImageUrl;
    }

    public void setNotificableImageUrl(String notificableImageUrl) {
        this.notificableImageUrl = notificableImageUrl;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public Long getNotificableId() {
        return notificableId;
    }

    public void setNotificableId(Long notificableId) {
        this.notificableId = notificableId;
    }

    public String getNotificableType() {
        return notificableType;
    }

    public void setNotificableType(String notificableType) {
        this.notificableType = notificableType;
    }

    public boolean isRead() {
        return read;
    }

    public void setRead(boolean read) {
        this.read = read;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public NotificationMetaData getNotificationMetaData() {
        return notificationMetaData;
    }

    public void setNotificationMetaData(NotificationMetaData notificationMetaData) {
        this.notificationMetaData = notificationMetaData;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.id);
        dest.writeValue(this.receiverUserId);
        dest.writeValue(this.senderUserId);
        dest.writeString(this.receiverUserImageUrl);
        dest.writeString(this.senderUserImageUrl);
        dest.writeString(this.notificableImageUrl);
        dest.writeString(this.notificationType);
        dest.writeValue(this.notificableId);
        dest.writeString(this.notificableType);
        dest.writeByte(read ? (byte) 1 : (byte) 0);
        dest.writeLong(createdAt != null ? createdAt.getTime() : -1);
        dest.writeLong(updatedAt != null ? updatedAt.getTime() : -1);
        dest.writeString(this.message);
        dest.writeParcelable(this.notificationMetaData, flags);
    }

    public Notification() {
    }

    protected Notification(Parcel in) {
        this.id = (Long) in.readValue(Long.class.getClassLoader());
        this.receiverUserId = (Long) in.readValue(Long.class.getClassLoader());
        this.senderUserId = (Long) in.readValue(Long.class.getClassLoader());
        this.receiverUserImageUrl = in.readString();
        this.senderUserImageUrl = in.readString();
        this.notificableImageUrl = in.readString();
        this.notificationType = in.readString();
        this.notificableId = (Long) in.readValue(Long.class.getClassLoader());
        this.notificableType = in.readString();
        this.read = in.readByte() != 0;
        long tmpCreatedAt = in.readLong();
        this.createdAt = tmpCreatedAt == -1 ? null : new Date(tmpCreatedAt);
        long tmpUpdatedAt = in.readLong();
        this.updatedAt = tmpUpdatedAt == -1 ? null : new Date(tmpUpdatedAt);
        this.message = in.readString();
        this.notificationMetaData = in.readParcelable(NotificationMetaData.class.getClassLoader());
    }

    public static final Parcelable.Creator<Notification> CREATOR = new Parcelable.Creator<Notification>() {
        @Override
        public Notification createFromParcel(Parcel source) {
            return new Notification(source);
        }

        @Override
        public Notification[] newArray(int size) {
            return new Notification[size];
        }
    };
}
