package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Notification;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class NotificationsResponse extends BaseResponse {

    @Expose
    @SerializedName("notifications")
    private List<Notification> notifications;

    public List<Notification> getNotifications() {
        return notifications;
    }

    public void setNotifications(List<Notification> notifications) {
        this.notifications = notifications;
    }
}
