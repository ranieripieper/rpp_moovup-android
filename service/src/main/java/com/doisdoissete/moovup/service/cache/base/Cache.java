package com.doisdoissete.moovup.service.cache.base;

import java.util.List;

/**
 * Created by broto on 8/16/15.
 */
public interface Cache<T> {

    void put(T t, boolean updateExpireTime);

    void put(T t);

    void putAll(List<T> t);

    void delete(long id);

    void deleteAll();

    T get(long id);

    List<T> getAll();

    long count();

    void updateWithoutNulls(List<T> t);
}
