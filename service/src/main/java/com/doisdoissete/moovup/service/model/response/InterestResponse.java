package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Interest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class InterestResponse extends BaseResponse {

    @Expose
    @SerializedName("interests")
    private List<Interest> interests;

    public List<Interest> getInterests() {
        return interests;
    }

    public void setInterests(List<Interest> interests) {
        this.interests = interests;
    }
}
