package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.CarrerResponse;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.model.response.UserResponse;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FieldMap;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/10/15.
 */
public interface UserService {

    // GET
    @GET("/users/check_email")
    void checkEmail(@Query("email") String email, Callback<CarrerResponse> callback);

    // POST
    @POST("/users")
    @Multipart
    void signUpPerson(@Part("user[first_name]") String firstName,
                      @Part("user[last_name]") String lastName,
                      @Part("user[email]") String email,
                      @Part("user[password]") String password,
                      @Part("user[password_confirmation]") String passwordConfirmation,
                      @Part("user[gender]") String gender,
                      @Part("user[birthday_date]") String birthdate,
                      @Part("user[carrer_id]") Long carrerId,
                      @Part("user[hobby_id]") Long hobbyId,
                      @Part("user[profile_type]") String profileType,
                      @Part("user[device][token]") String deviceToken,
                      @Part("user[device][platform]") String devicePlatform,
                      @Part("user[device][installation_id]") String parseInstallationId,
                      @Part("provider") String provider,
                      @Part("user[profile_image]") TypedFile photoPath,
                      Callback<ResponseResult> callback);

    @POST("/users")
    @Multipart
    void signUpCompany(@Part("user[company_name]") String companyName,
                       @Part("user[email]") String email,
                       @Part("user[password]") String password,
                       @Part("user[password_confirmation]") String passwordConfirmation,
                       @Part("user[foundation_date]") String foundationDate,
                       @Part("user[commercial_activity_id]") Long commercialActivityId,
                       @Part("user[profile_type]") String profileType,
                       @Part("user[device][token]") String deviceToken,
                       @Part("user[device][platform]") String devicePlatform,
                       @Part("user[device][installation_id]") String parseInstallationId,
                       @Part("provider") String provider,
                       @Part("user[profile_image]") TypedFile photoPath,
                       Callback<ResponseResult> callback);

    @GET("/users/me")
    void getCurrentUserData(Callback<User> callback);

    @PUT("/users/me")
    @FormUrlEncoded
    void updatePerson(@Field("user[first_name]") String firstName,
                      @Field("user[last_name]") String lastName,
                      @Field("user[email]") String email,
                      @Field("user[gender]") String gender,
                      @Field("user[birthday_date]") String birthdate,
                      @Field("user[carrer_id]") Long carrerId,
                      @Field("user[hobby_id]") Long hobbyId,
                      @Field("user[device][token]") String deviceToken,
                      @Field("user[device][platform]") String devicePlatform,
                      @Field("user[device][installation_id]") String parseInstallationId,
                      Callback<ResponseResult> callback);

    @PUT("/users/me")
    @FormUrlEncoded
    void updateCompany(@Field("user[company_name]") String companyName,
                       @Field("user[email]") String email,
                       @Field("user[foundation_date]") String foundationDate,
                       @Field("user[commercial_activity_id]") Long commercialActivityId,
                       @Field("user[device][token]") String deviceToken,
                       @Field("user[device][platform]") String devicePlatform,
                       @Field("user[device][installation_id]") String parseInstallationId,
                       Callback<ResponseResult> callback);

    @PUT("/users/me")
    @FormUrlEncoded
    void updatePassword(@Field("user[password]") String password,
                        @Field("user[password_confirmation]") String passwordConfirmation,
                        Callback<ResponseResult> callback);

    @PUT("/users/me/picture")
    @Multipart
    void updatePhoto(@Part("user[profile_image]") TypedFile photoPath, Callback<UserResponse> callback);

    @POST("/users/auth/facebook")
    @FormUrlEncoded
    void loginFacebook(@Field("access_token") String token,
                       @Field("provider") String provider,
                       @Field("device[token]") String deviceToken,
                       @Field("device[platform]") String devicePlatform,
                       @Field("device[installation_id]") String parseInstallationId,
                       Callback<ResponseResult> callback);

    @POST("/users/auth/google_plus")
    @FormUrlEncoded
    void loginGoogle(@Field("access_token") String token,
                     @Field("provider") String provider,
                     @Field("device[token]") String deviceToken,
                     @Field("device[platform]") String devicePlatform,
                     @Field("device[installation_id]") String parseInstallationId,
                     Callback<ResponseResult> callback);

    @POST("/users/auth")
    @FormUrlEncoded
    void login(@Field("email") String email,
               @Field("password") String password,
               @Field("provider") String provider,
               @Field("device[token]") String deviceToken,
               @Field("device[platform]") String devicePlatform,
               @Field("device[installation_id]") String parseInstallationId,
               Callback<ResponseResult> callback);

    @POST("/users/password_reset")
    @FormUrlEncoded
    void requestRecover(@Field("user[email]") String email, Callback<ResponseResult> callback);

    @POST("/users/me/devices")
    @FormUrlEncoded
    void registerDevice(@Field("device[token]") String token,
                        @Field("device[platform]") String platform,
                        @Field("device[installation_id]") String parseInstallationId,
                        Callback<ResponseResult> callback);

    // DELETE

    @DELETE("/users/auth")
    void logout(@Header("X-Token") String token, Callback<BaseResponse> responseCallback);

    @DELETE("/users/me/account")
    void deleteAccount(Callback<BaseResponse> responseCallback);

    //Retorna dados do usuário
    @GET("/users/{user_id}")
    void getUserData(@Path("user_id") long userId, Callback<User> callback);

    //Preferences

    //Retorna dados do usuário
    @GET("/users/me/preferences")
    void getUserPreferences(Callback<User> callback);

    @PUT("/users/me/preferences")
    @FormUrlEncoded
    void updateUserPreferences(@FieldMap Map<String, String> params, Callback<BaseResponse> callback);

    @POST("/users/resend_activation_mail")
    @FormUrlEncoded
    void resendActivationMail(@Field("email") String email, Callback<BaseResponse> callback);

}
