package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

/**
 * Created by broto on 11/3/15.
 */
public class Interest {

    @Expose
    private long id;

    @Expose
    @SerializedGsonName("category_id")
    private long categoryId;

    @Expose
    @SerializedGsonName("max_answers_count")
    private Long maxAnswersCount;

    private boolean active;

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getMaxAnswersCount() {
        return maxAnswersCount;
    }

    public void setMaxAnswersCount(Long maxAnswersCount) {
        this.maxAnswersCount = maxAnswersCount;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }
}
