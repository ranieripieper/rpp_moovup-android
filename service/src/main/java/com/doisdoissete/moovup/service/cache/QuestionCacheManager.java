package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Question;

import io.realm.Realm;

/**
 * Created by ranipieper on 11/19/15.
 */
public class QuestionCacheManager extends BaseCache<Question> {

    private static QuestionCacheManager instance = new QuestionCacheManager();

    private QuestionCacheManager() {
    }

    public static QuestionCacheManager getInstance() {
        return instance;
    }

    public Question updateFavorited(long id, boolean favorited) {
        Realm realm = getRealm();
        realm.beginTransaction();
        Question updateQuestion = get(id);
        updateQuestion.setFavorited(favorited);
        realm.commitTransaction();
        return updateQuestion;
    }

    @Override
    public Class<Question> getReferenceClass() {
        return Question.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
