package com.doisdoissete.moovup.service.cache.base;

import io.realm.DynamicRealm;
import io.realm.RealmSchema;

/**
 * Created by ranipieper on 6/22/16.
 */
public class MoovupRealmMigration implements io.realm.RealmMigration {

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        RealmSchema schema = realm.getSchema();

        if (oldVersion == 1) {
            //migration to 1.0.0

            schema.get("Answers").setRequired("id", false);
            schema.get("Carrer").setRequired("id", false);
            schema.get("Category").setRequired("id", false);
            schema.get("CommercialActivity").setRequired("id", false);
            schema.get("Goal").setRequired("id", false);
            schema.get("GoalActivity").setRequired("id", false);
            schema.get("Question").setRequired("id", false);
            schema.get("User").setRequired("id", false);
            schema.get("Sync").setRequired("className", false);
            oldVersion++;
        }

        if (oldVersion == 2) {
            schema.get("Question").addField("coverImageUrl", String.class);
        }
    }
}
