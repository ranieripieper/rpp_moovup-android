package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.CommercialActivity;

import java.util.List;

import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class CommercialActivityCacheManager extends BaseCache<CommercialActivity> {

    private static CommercialActivityCacheManager instance = new CommercialActivityCacheManager();

    private CommercialActivityCacheManager() {
    }

    public static CommercialActivityCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<CommercialActivity> getReferenceClass() {
        return CommercialActivity.class;
    }

    @Override
    public List<CommercialActivity> getAll() {
        return getRealm().where(getReferenceClass()).findAllSorted("title", Sort.ASCENDING);
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
