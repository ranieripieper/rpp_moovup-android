package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Category;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import io.realm.RealmQuery;
import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class CategoryCacheManager extends BaseCache<Category> {

    private static CategoryCacheManager instance = new CategoryCacheManager();

    private CategoryCacheManager() {
    }

    public static CategoryCacheManager getInstance() {
        return instance;
    }

    public List<Category> getHobbies() {
        return copyFromRealm(getRealm().where(Category.class).equalTo("hobby", true).findAllSorted("title", Sort.ASCENDING));
    }


    public List<Category> getParentCategoryGoals() {
        List<Category> childrenCategorires = getRealm().where(Category.class).equalTo("validFor.goals", true).findAllSorted("title", Sort.ASCENDING);

        LinkedHashSet<Long> parentIds = new LinkedHashSet<>();
        if (childrenCategorires != null) {
            for (Category category : childrenCategorires) {
                if (category.getParentId() != null) {
                    parentIds.add(category.getParentId());
                }
            }
        }
        RealmQuery query = getRealm().where(Category.class).isNull("parentId");
        List<Long> idsToSearch = new ArrayList<>(parentIds);
        if (idsToSearch.size() > 0) {
            query = query.equalTo("id", idsToSearch.get(0));
            for (int i = 1; i < parentIds.size(); i++) {
                query = query.or().equalTo("id", idsToSearch.get(i));
            }
        }

        return copyFromRealm(query.findAllSorted("title", Sort.ASCENDING));
    }



    public List<Category> getCategoryForQuestions() {
        return copyFromRealm(getRealm().where(Category.class).equalTo("validFor.questions", true).findAllSorted("title", Sort.ASCENDING));
    }

    public List<Category> getCategoryForInterests() {
        return copyFromRealm(getRealm().where(Category.class).equalTo("validFor.interests", true).findAllSorted("title", Sort.ASCENDING));
    }

    @Override
    public List<Category> getAll() {
        return copyFromRealm(getRealm().where(getReferenceClass()).findAllSorted("title", Sort.ASCENDING));
    }

    @Override
    public Class<Category> getReferenceClass() {
        return Category.class;
    }

    @Override
    protected long getCacheLifeTime() {
        return 1000 * 60 * 60 * 24 * 15; // 15 dias
    }
}
