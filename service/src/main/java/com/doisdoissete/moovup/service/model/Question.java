package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import java.util.Date;
import java.util.List;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.Required;

/**
 * Created by broto on 11/5/15.
 */
public class Question extends RealmObject{

    @PrimaryKey
    @Required
    @Expose
    private Long id;

    @Expose
    @SerializedGsonName("user_id")
    private long userId;

    @Expose
    @SerializedGsonName("body_text")
    private String bodyText;

    @Expose
    @SerializedGsonName("upvotes_count")
    private String upvotesCount;

    @Expose
    @SerializedGsonName("created_at")
    private Date createdAt;

    @Expose
    @SerializedGsonName("favorited_count")
    private long favoritedCount;

    @Expose
    private int status;

    @Expose
    @SerializedGsonName("category_id")
    private long categoryId;

    @Expose
    @SerializedGsonName("answers_count")
    private long answersCount;

    @Expose
    @SerializedGsonName("answers")
    @Ignore
    private List<Answers> answers;

    @Expose
    @SerializedGsonName("cover_image_url")
    private String coverImageUrl;

    @Expose
    @SerializedGsonName("favorited")
    private boolean favorited;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getBodyText() {
        return bodyText;
    }

    public void setBodyText(String bodyText) {
        this.bodyText = bodyText;
    }

    public String getUpvotesCount() {
        return upvotesCount;
    }

    public void setUpvotesCount(String upvotesCount) {
        this.upvotesCount = upvotesCount;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public long getFavoritedCount() {
        return favoritedCount;
    }

    public void setFavoritedCount(long favoritedCount) {
        this.favoritedCount = favoritedCount;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public long getAnswersCount() {
        return answersCount;
    }

    public void setAnswersCount(long answersCount) {
        this.answersCount = answersCount;
    }

    public List<Answers> getAnswers() {
        return answers;
    }

    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

    public boolean isFavorited() {
        return favorited;
    }

    public void setFavorited(boolean favorited) {
        this.favorited = favorited;
    }

    public String getCoverImageUrl() {
        return coverImageUrl;
    }

    public void setCoverImageUrl(String coverImageUrl) {
        this.coverImageUrl = coverImageUrl;
    }
}
