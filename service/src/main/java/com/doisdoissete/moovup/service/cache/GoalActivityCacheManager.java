package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.GoalActivity;

/**
 * Created by ranipieper on 11/19/15.
 */
public class GoalActivityCacheManager extends BaseCache<GoalActivity> {

    private static GoalActivityCacheManager instance = new GoalActivityCacheManager();

    private GoalActivityCacheManager() {
    }

    public GoalActivity getCopy(long id) {
        GoalActivity obj = getRealm().where(getReferenceClass()).equalTo("id", id).findFirst();
        if (obj == null) {
            return null;
        }
        return getRealm().copyFromRealm(obj);
    }

    public static GoalActivityCacheManager getInstance() {
        return instance;
    }

    @Override
    public Class<GoalActivity> getReferenceClass() {
        return GoalActivity.class;
    }

}
