package com.doisdoissete.moovup.service.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by ranipieper on 11/12/15.
 */
public class DateUtil {

    public static final ThreadLocal<DateFormat> DATE_YEAR_MONTH_DAY = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH_YEAR = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM/yyyy");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_DAY_MONTH = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("dd/MM");
        }
    };

    public static final ThreadLocal<DateFormat> DATE_MONTH_CHART = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("MMM");
        }
    };
}
