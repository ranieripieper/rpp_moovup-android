package com.doisdoissete.moovup.service.cache;

import com.doisdoissete.moovup.service.cache.base.BaseCache;
import com.doisdoissete.moovup.service.model.Goal;

import java.util.List;

import io.realm.Sort;

/**
 * Created by ranipieper on 11/19/15.
 */
public class GoalCacheManager extends BaseCache<Goal> {

    private static GoalCacheManager instance = new GoalCacheManager();

    private GoalCacheManager() {
    }

    public static GoalCacheManager getInstance() {
        return instance;
    }


    public List<Goal> getAllByUserId(long userId) {
        List<Goal> result = getRealm().where(getReferenceClass())
                .equalTo("userId", userId)
                .findAllSorted("updatedAt", Sort.DESCENDING);
        if (result != null) {
            return getRealm().copyFromRealm(result);
        }
        return null;
    }

    @Override
    public Class<Goal> getReferenceClass() {
        return Goal.class;
    }

}
