package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class AnswersResponse extends BaseResponse {

    @Expose
    @SerializedName("answers")
    private List<Answers> answers;

    @Expose
    @SerializedName("linked_data")
    private LinkedData linkedData;

    /**
     * Gets the answers
     *
     * @return answers
     */
    public List<Answers> getAnswers() {
        return answers;
    }

    /**
     * Sets the answers
     *
     * @param answers
     */
    public void setAnswers(List<Answers> answers) {
        this.answers = answers;
    }

    /**
     * Gets the linkedData
     *
     * @return linkedData
     */
    public LinkedData getLinkedData() {
        return linkedData;
    }

    /**
     * Sets the linkedData
     *
     * @param linkedData
     */
    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
