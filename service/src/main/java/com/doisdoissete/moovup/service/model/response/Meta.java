package com.doisdoissete.moovup.service.model.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by broto on 10/13/15.
 */
public class Meta {


    @Expose
    private Pagination pagination;

    public Meta() {
    }

    public Meta(Pagination pagination) {
        this.pagination = pagination;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }


    public class Pagination {

        @Expose
        @SerializedName("total_count")
        private int totalCount;

        @Expose
        @SerializedName("total_pages")
        private int totalPages;

        @Expose
        @SerializedName("current_page")
        private int currentPage;

        @Expose
        @SerializedName("next_page")
        private int nextPage;

        @Expose
        @SerializedName("prev_page")
        private int prevPage;

        @Expose
        @SerializedName("per_page")
        private int perPage;

        public Pagination() {
        }

        public Pagination(int totalCount, int totalPages, int currentPage, int nextPage, int prevPage, int perPage) {
            this.totalCount = totalCount;
            this.totalPages = totalPages;
            this.currentPage = currentPage;
            this.nextPage = nextPage;
            this.prevPage = prevPage;
            this.perPage = perPage;
        }

        public int getTotalCount() {
            return totalCount;
        }

        public void setTotalCount(int totalCount) {
            this.totalCount = totalCount;
        }

        public int getTotalPages() {
            return totalPages;
        }

        public void setTotalPages(int totalPages) {
            this.totalPages = totalPages;
        }

        public int getCurrentPage() {
            return currentPage;
        }

        public void setCurrentPage(int currentPage) {
            this.currentPage = currentPage;
        }

        public int getNextPage() {
            return nextPage;
        }

        public void setNextPage(int nextPage) {
            this.nextPage = nextPage;
        }

        public int getPrevPage() {
            return prevPage;
        }

        public void setPrevPage(int prevPage) {
            this.prevPage = prevPage;
        }

        public int getPerPage() {
            return perPage;
        }

        public void setPerPage(int perPage) {
            this.perPage = perPage;
        }
    }
}
