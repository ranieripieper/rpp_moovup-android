package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 11/13/15.
 */
public class QuestionResponse extends BaseResponse {

    @Expose
    @SerializedName("question")
    private Question question;

    @Expose
    @SerializedName("linked_data")
    private LinkedData linkedData;

    /**
     * Gets the question
     *
     * @return question
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Sets the question
     *
     * @param question
     */
    public void setQuestion(Question question) {
        this.question = question;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
