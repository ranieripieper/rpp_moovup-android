package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by ranipieper on 11/13/15.
 */
public class QuestionsResponse extends BaseResponse {

    @Expose
    @SerializedName("questions")
    private List<Question> questions;

    @Expose
    @SerializedName("linked_data")
    private LinkedData linkedData;

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public LinkedData getLinkedData() {
        return linkedData;
    }

    public void setLinkedData(LinkedData linkedData) {
        this.linkedData = linkedData;
    }
}
