package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.NotificationsResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by ranipieper on 2/18/16.
 */
public interface NotificationService {

    @GET("/users/me/notifications")
    void getNotifications(@Query("page") Integer page, @Query("per_page") Integer perPage, Callback<NotificationsResponse> callback);

    @PUT("/users/me/notifications/{notification_id}")
    void readNotifications(@Path("notification_id") Long norificationId, @Body String emptyBody, Callback<BaseResponse> callback);

}
