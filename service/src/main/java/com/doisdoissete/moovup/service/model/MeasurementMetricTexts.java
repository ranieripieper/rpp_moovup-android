package com.doisdoissete.moovup.service.model;

import com.google.gson.annotations.Expose;

import io.realm.RealmObject;

/**
 * Created by ranipieper on 11/13/15.
 */
public class MeasurementMetricTexts extends RealmObject {

    @Expose
    @SerializedGsonName("singular")
    private String singular;

    @Expose
    @SerializedGsonName("plural")
    private String plural;

    public String getSingular() {
        return singular;
    }

    public void setSingular(String singular) {
        this.singular = singular;
    }

    public String getPlural() {
        return plural;
    }

    public void setPlural(String plural) {
        this.plural = plural;
    }

}
