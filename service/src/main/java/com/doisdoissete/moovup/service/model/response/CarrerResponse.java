package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Carrer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by broto on 9/21/15.
 */
public class CarrerResponse extends BaseResponse {

    @Expose
    @SerializedName("carrers")
    private List<Carrer> carrers;

    public CarrerResponse() {
    }

    public List<Carrer> getCarrers() {
        return carrers;
    }

    public void setCarrers(List<Carrer> carrers) {
        this.carrers = carrers;
    }

}
