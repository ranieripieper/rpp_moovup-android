package com.doisdoissete.moovup.service.net;

import com.doisdoissete.moovup.service.BuildConfig;
import com.doisdoissete.moovup.service.cache.ConfigurationManager;
import com.doisdoissete.moovup.service.model.Configuration;
import com.doisdoissete.moovup.service.model.SerializedGsonName;
import com.doisdoissete.moovup.service.net.interfaces.CarrerService;
import com.doisdoissete.moovup.service.net.interfaces.CategoryService;
import com.doisdoissete.moovup.service.net.interfaces.CommentService;
import com.doisdoissete.moovup.service.net.interfaces.CommercialActivityService;
import com.doisdoissete.moovup.service.net.interfaces.FriendsService;
import com.doisdoissete.moovup.service.net.interfaces.GoalService;
import com.doisdoissete.moovup.service.net.interfaces.InterestsService;
import com.doisdoissete.moovup.service.net.interfaces.NotificationService;
import com.doisdoissete.moovup.service.net.interfaces.QuestionService;
import com.doisdoissete.moovup.service.net.interfaces.ReportService;
import com.doisdoissete.moovup.service.net.interfaces.UserService;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.google.gson.FieldNamingStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.logging.HttpLoggingInterceptor;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.OkClient;
import retrofit.converter.GsonConverter;

/**
 * Created by ranieri on 8/15/15.
 */
public class RetrofitManager {

    public static final String PROVIDER = "android";
    public static final int MAX_PER_PAGE = 60;
    public static final int DEFAULT_PER_PAGE = 20;
    public static final int DEFAULT_PER_PAGE_SEARCH = 10;

    public RestAdapter restAdapter;
    final ConcurrentHashMap<Class, Object> services;
    private static RetrofitManager instance;

    private RetrofitManager() {
        services = new ConcurrentHashMap();
    }

    public static RetrofitManager getInstance() {
        if (instance == null) {
            instance = new RetrofitManager();
        }
        return instance;
    }

    public static Gson getGson() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer)
                .setFieldNamingStrategy(new FieldNamingStrategy() {
                    @Override
                    public String translateName(Field field) {
                        if (field.isAnnotationPresent(SerializedGsonName.class)) {
                            SerializedGsonName serializedName = field.getAnnotation(SerializedGsonName.class);
                            return serializedName.value();
                        }
                        return field.getName();
                    }
                })
                .create();
        return gson;
    }

    public static Gson getGsonRealm() {
        Gson gson = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .registerTypeAdapter(Date.class, dateDeserializer)
                .registerTypeAdapter(Date.class, dateSerializer)

                .create();
        return gson;
    }

    public void initialize(final String locale, final String apiBaseUrl) {

        RequestInterceptor requestInterceptor = new RequestInterceptor() {
            @Override
            public void intercept(RequestFacade request) {
                Configuration conf = ConfigurationManager.getInstance().get();
                if (conf != null) {
                    request.addHeader("X-Token", conf.getCurrentAuthToken());
                }
                request.addHeader("X-Provider", PROVIDER);
                request.addHeader("X-Locale", locale);
            }
        };

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient httpClient = new OkHttpClient();
        httpClient.interceptors().add(logging);

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(apiBaseUrl)
                .setConverter(new GsonConverter(getGson()))
                .setRequestInterceptor(requestInterceptor)
                .setClient(new OkClient(httpClient))
                .setLogLevel(BuildConfig.DEBUG ? RestAdapter.LogLevel.FULL : RestAdapter.LogLevel.FULL)/*.setLog(new RestAdapter.Log() {
                    public void log(String msg) {
                        Log.i("retrofit", msg);
                    }
                })*/.build();
    }

    //Services

    public NotificationService getNotificationService() {
        return (NotificationService)this.getService(NotificationService.class);
    }

    public UserService getUserService() {
        return (UserService)this.getService(UserService.class);
    }

    public FriendsService getFriendsService() {
        return (FriendsService)this.getService(FriendsService.class);
    }

    public GoalService getGoalService() {
        return (GoalService)this.getService(GoalService.class);
    }

    public InterestsService getInterestsService() {
        return (InterestsService)this.getService(InterestsService.class);
    }

    public ReportService getReportService() {
        return (ReportService)this.getService(ReportService.class);
    }

    public QuestionService getQuestionService() {
        return (QuestionService)this.getService(QuestionService.class);
    }

    public CommentService getCommentService() {
        return (CommentService)this.getService(CommentService.class);
    }

    public CategoryService getCategoryService() {
        return (CategoryService)this.getService(CategoryService.class);
    }

    public CarrerService getCarrerService() {
        return (CarrerService)this.getService(CarrerService.class);
    }

    public CommercialActivityService getCommercialActivityService() {
        return (CommercialActivityService)this.getService(CommercialActivityService.class);
    }

    protected <T> Object getService(Class<T> cls) {
        if(!this.services.contains(cls)) {
            this.services.putIfAbsent(cls, this.restAdapter.create(cls));
        }

        return this.services.get(cls);
    }

    static JsonDeserializer<Date> dateDeserializer = new JsonDeserializer<Date>() {
        @Override
        public Date deserialize(JsonElement json, Type typeOfT,
                                JsonDeserializationContext context) throws JsonParseException {
            if (json != null) {
                try {
                    Date dt = DATE_SERVICE_1.get().parse(json.getAsString());
                    if (dt == null) {
                        return tryOtherFormat(json);
                    }
                    return dt;
                } catch (ParseException e) {
                    return tryOtherFormat(json);
                }
            }

            return null;
        }

        private Date tryOtherFormat(JsonElement json) {
            try {
                return DATE_SERVICE_2.get().parse(json.getAsString());
            } catch (ParseException e1) {
                return tryOtherFormat2(json);
            }
        }

        private Date tryOtherFormat2(JsonElement json) {
            try {
                return DateUtil.DATE_YEAR_MONTH_DAY.get().parse(json.getAsString());
            } catch (ParseException e1) {
            }
            return null;
        }
    };


    static JsonSerializer<Date> dateSerializer = new JsonSerializer<Date>() {
        @Override
        public JsonElement serialize(Date src, Type typeOfSrc, JsonSerializationContext
                context) {
            return src == null ? null : new JsonPrimitive(src.getTime());
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_1 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ") {
                public StringBuffer format(Date date, StringBuffer toAppendTo, java.text.FieldPosition pos) {
                    StringBuffer toFix = super.format(date, toAppendTo, pos);
                    return toFix.insert(toFix.length()-2, ':');
                };

                public Date parse(String text, ParsePosition pos) {
                    int indexOf = text.indexOf(':', text.length() - 4);
                    if (indexOf > 0) {
                        text = text.substring(0, indexOf) + text.substring(indexOf+1, text.length());
                        return super.parse(text, pos);
                    } else {
                        return null;
                    }

                }

            };
        }
    };

    private static final ThreadLocal<DateFormat> DATE_SERVICE_2 = new ThreadLocal<DateFormat>() {
        @Override
        protected DateFormat initialValue() {
            return new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        }
    };

}
