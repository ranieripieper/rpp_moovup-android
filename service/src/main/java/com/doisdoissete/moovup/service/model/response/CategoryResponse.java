package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Category;
import com.google.gson.annotations.Expose;

import java.util.List;

/**
 * Created by ranipieper on 11/16/15.
 */
public class CategoryResponse extends BaseResponse {

    @Expose
    private List<Category> categories;

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }
}
