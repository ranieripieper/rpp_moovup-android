package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Question;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 11/25/15.
 */
public class UpdateQuestionResponse extends BaseResponse {

    @Expose
    @SerializedName("question")
    private Question question;

    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }
}
