package com.doisdoissete.moovup.service.cache.base;


import android.content.Context;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmObject;
import io.realm.RealmQuery;

/**
 * Created by broto on 11/3/15.
 */
public abstract class BaseCache<T extends RealmObject> implements Cache<T>{

    public BaseCache() {
    }

    public void updateWithoutNulls(List<T> lst) {
        RealmManager.getInstance().updateWithoutNulls(lst, getReferenceClass());
    }

    public void updateWithoutNulls(T obj) {
        updateWithoutNulls(Arrays.asList(obj));
    }


    @Override
    public void put(T t) {
        RealmManager.getInstance().put(t);
        updateExpireTime(System.currentTimeMillis());
    }

    @Override
    public void put(T t, boolean updateExpireTime) {
        RealmManager.getInstance().put(t, updateExpireTime, getReferenceClass());
    }

    @Override
    public void putAll(List<T> lst) {
        RealmManager.getInstance().putAll((List<RealmObject>) lst);
    }

    @Override
    public void delete(final long id) {
        RealmManager.getInstance().delete(id, getReferenceClass());
    }

    @Override
    public void deleteAll() {
        RealmManager.getInstance().deleteAll(getReferenceClass());
        getRealm().executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                Sync sync = getRealm().where(Sync.class).equalTo("className", getReferenceClass().getName()).findFirst();
                if (sync != null) {
                    sync.deleteFromRealm();
                }
            }
        });
    }

    @Override
    public T get(long id) {
        return getRealm().where(getReferenceClass()).equalTo("id", id).findFirst();
    }

    @Override
    public long count() {
        return RealmManager.getInstance().count(getReferenceClass());
    }

    @Override
    public List<T> getAll() {
        return copyFromRealm(getRealm().where(getReferenceClass()).findAll());
    }

    private void updateExpireTime(long timestamp) {
        Sync sync = new Sync(getReferenceClass().getName(), timestamp);
        RealmManager.getInstance().put(sync);
    }

    public boolean isExpired() {
        Realm realm = RealmManager.getInstance().getRealm();
        RealmQuery<Sync> query = realm.where(Sync.class);
        query.equalTo("className", getReferenceClass().getName());
        Sync result = query.findFirst();

        if (result == null) {
            return true;
        }

        Date expiredDate = new Date(result.getLastUpdated() + getCacheLifeTime());

        return new Date().after(expiredDate);
    }

    protected List<T> copyFromRealm(List<T> realmList) {
        if (realmList != null) {
            return getRealm().copyFromRealm(realmList);
        }
        return null;
    }

    protected long getCacheLifeTime() {
        return Sync.DEFAULT_CACHE_TIME_LIMIT;
    }

    public Realm getRealm(Context ctx) {
        return RealmManager.getInstance(ctx).getRealm();
    }

    public Realm getRealm() {
        return RealmManager.getInstance().getRealm();
    }

    public abstract Class<T> getReferenceClass();
}
