package com.doisdoissete.moovup.service.net.interfaces;

import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.GoalActivitiesResponse;
import com.doisdoissete.moovup.service.model.response.GoalActivityResponse;
import com.doisdoissete.moovup.service.model.response.GoalResponse;
import com.doisdoissete.moovup.service.model.response.GoalsResponse;

import java.util.Date;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/11/15.
 */
public interface GoalService {

    @GET("/users/me/goals/{goal_id}")
    void getMyGoalById(
            @Path("goal_id") long goalId,
            Callback<Goal> callback);

    @POST("/goals")
    @FormUrlEncoded
    void addNewCommonGoal(@Field("goal[periodicity_type]") String periodicityType,
                          @Field("goal[total]") String total,
                          @Field("goal[category_id]") long categoryId,
                          @Field("goal[public]") boolean shareFriends,
                          Callback<GoalResponse> callback);

    @PUT("/goals/{goal_id}")
    @FormUrlEncoded
    void updateNewCommonGoal(
            @Path("goal_id") long goalId,
            @Field("goal[periodicity_type]") String periodicityType,
            @Field("goal[total]") String total,
            @Field("goal[category_id]") long categoryId,
            @Field("goal[public]") boolean shareFriends,
            Callback<GoalResponse> callback);


    @POST("/goals")
    @FormUrlEncoded
    void addNewClosedGoal(@Field("goal[current]") float current,
                          @Field("goal[target]") float target,
                          @Field("goal[target_date]") Date targetDate,
                          @Field("goal[category_id]") long categoryId,
                          @Field("goal[public]") boolean shareFriends,
                          Callback<GoalResponse> callback);

    @PUT("/goals/{goal_id}")
    @FormUrlEncoded
    void updateNewClosedGoal(@Path("goal_id") long goalId,
                             @Field("goal[current]") float current,
                             @Field("goal[target]") float target,
                             @Field("goal[target_date]") Date targetDate,
                             @Field("goal[category_id]") long categoryId,
                             @Field("goal[public]") boolean shareFriends,
                             Callback<GoalResponse> callback);


    @GET("/users/me/goals")
    void getMyGoals(@Query("page") Integer page,
                    @Query("per_page") Integer perPage,
                    Callback<GoalsResponse> callback);

    @GET("/users/{user_id}/goals")
    void getGoals(
            @Path("user_id") Long userId,
            @Query("page") Integer page,
            @Query("per_page") Integer perPage,
            Callback<GoalsResponse> callback);

    @DELETE("/goals/{goal_id}")
    void deleteGoal(@Path("goal_id") long goalId, Callback<BaseResponse> responseCallback);

    //Activities

    @GET("/users/me/goals/{goal_id}/activities")
    void getMyGoalActivities(
            @Path("goal_id") long goalId,
            @Query("page") Integer page,
            @Query("per_page") Integer perPage,
            Callback<GoalActivitiesResponse> callback);

    @GET("/users/{user_id}/goals/{goal_id}/activities")
    void getGoalActivities(
            @Path("user_id") long userId,
            @Path("goal_id") long goalId,
            @Query("page") Integer page,
            @Query("per_page") Integer perPage,
            Callback<GoalActivitiesResponse> callback);

    @POST("/goals/{goal_id}/activities")
    @Multipart
    void addGoalActivities(
            @Path("goal_id") long goalId,
            @Part("activity[body_text]") String bodyText,
            @Part("activity[total]") String total,
            @Part("activity[date]") String date,
            @Part("activity[image]") TypedFile photoPath,
            Callback<GoalActivityResponse> callback);

    @DELETE("/activities/{activity_id}/upvote?_s=simple")
    void downvoteGoalActivity(@Path("activity_id") long activityId, Callback<BaseResponse> callback);

    @POST("/activities/{activity_id}/upvote?_s=simple")
    void upvoteGoalActivity(@Path("activity_id") long activityId, @Body String emptyBody, Callback<BaseResponse> callback);

    @GET("/users/me/goals/{goal_id}/reports")
    void getMyGoalToReport(@Path("goal_id") long goalId, Callback<Goal> callback);

    @GET("/users/{user_id}/goals/{goal_id}/reports")
    void getGoalToReport(@Path("user_id") long userId, @Path("goal_id") long goalId, Callback<Goal> callback);

}
