package com.doisdoissete.moovup.service.model.response;

import com.doisdoissete.moovup.service.model.Friendship;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ranipieper on 1/11/16.
 */
public class FriendshipResponse extends BaseResponse {

    @Expose
    @SerializedName("friendship")
    private Friendship friendship;

    public Friendship getFriendship() {
        return friendship;
    }

    public void setFriendship(Friendship friendship) {
        this.friendship = friendship;
    }
}
