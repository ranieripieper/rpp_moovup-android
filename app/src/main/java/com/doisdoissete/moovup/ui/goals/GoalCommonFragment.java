package com.doisdoissete.moovup.ui.goals;

import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.GoalCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.response.GoalResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/16/15.
 */
public class GoalCommonFragment extends AddGoalBaseFragment implements Validator.ValidationListener {

    @Bind(R.id.lbl_goal_schedule_title)
    public TextView lblTitle;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_goal_schedule_amount)
    public EditText edtTarget;

    private String mPeriodicityType = "day";

    public static GoalCommonFragment newInstance(Category category) {
        GoalCommonFragment fragment = new GoalCommonFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_CATEGORY, category.getId());

        fragment.setArguments(bundle);
        return fragment;
    }

    public static GoalCommonFragment newInstance(Goal goal, GoalDetailFragment goalDetailFragment) {
        GoalCommonFragment fragment = new GoalCommonFragment();
        fragment.mGoal = goal;
        fragment.mGoalDetailFragment = goalDetailFragment;
        fragment.setArguments(getBundleToNewInstance(goal));
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (TextUtils.isEmpty(mCategory.getMeasurementText())) {
            lblTitle.setText(getString(R.string.how_many_minutes_of) + " " + mCategory.getTitle() + getString(R.string.do_wanna_practice));
        } else {
            lblTitle.setText(Html.fromHtml(mCategory.getMeasurementText()));
        }
        if (mCategory.getMeasurementMetricTexts() != null && !TextUtils.isEmpty(mCategory.getMeasurementMetricTexts().getPlural())) {
            edtTarget.setHint(mCategory.getMeasurementMetricTexts().getPlural());
        } else {
            edtTarget.setHint(mCategory.getMeasurementText());
        }

        if (mGoal != null) {
            edtTarget.setText(GoalHelper.getValue(mGoal.getTotal()));
            mPeriodicityType = mGoal.getPeriodicityType();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_common;
    }

    @OnClick(R.id.radio_day)
    public void onRadioDayClick() {
        mPeriodicityType = "day";
    }

    @OnClick(R.id.radio_week)
    public void onRadioWeekClick() {
        mPeriodicityType = "week";
    }

    @OnClick(R.id.radio_month)
    public void onRadioMonthClick() {
        mPeriodicityType = "month";
    }

    @Override
    public void onValidationSucceeded() {
        showLoadingView();
        if (mGoal == null) {
            RetrofitManager.getInstance().getGoalService().addNewCommonGoal(mPeriodicityType,
                    edtTarget.getText().toString(),
                    mCategory.getId(),
                    lblShareGoalWithFriends.isChecked(),
                    new Callback<GoalResponse>() {
                        @Override
                        public void success(final GoalResponse baseResponse, Response response) {
                            hideLoadingView();
                            if (baseResponse != null && baseResponse.getGoal() != null) {
                                GoalCacheManager.getInstance().updateWithoutNulls(baseResponse.getGoal());

                                showSuccessDialog(getActivity(), R.string.goal_save, new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        Navigator.navigateToMyGoalsFragment(getActivity());//baseResponse.getGoal());
                                    }
                                });
                            } else {
                                showErrorDialog(R.string.error_generic);
                            }

                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showError(error);
                        }
                    }
            );
        } else {
            //Update
            RetrofitManager.getInstance().getGoalService().updateNewCommonGoal(mGoal.getId(),
                    mPeriodicityType,
                    edtTarget.getText().toString(),
                    mCategory.getId(),
                    lblShareGoalWithFriends.isChecked(),
                    new Callback<GoalResponse>() {
                        @Override
                        public void success(final GoalResponse baseResponse, Response response) {
                            hideLoadingView();
                            if (baseResponse != null && baseResponse.getGoal() != null) {
                                GoalCacheManager.getInstance().updateWithoutNulls(baseResponse.getGoal());
                            }
                            showSuccessDialog(getActivity(), R.string.goal_save);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showError(error);
                        }
                    }
            );
        }

    }
}
