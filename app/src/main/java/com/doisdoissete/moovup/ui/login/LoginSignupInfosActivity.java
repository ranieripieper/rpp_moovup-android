package com.doisdoissete.moovup.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.login.adapter.SignUpViewPagerAdapter;

import butterknife.Bind;

/**
 * Created by ranipieper on 11/11/15.
 */
public class LoginSignupInfosActivity extends LoginBaseActivity {

    private static final String EXTRA_EMAIL = "EXTRA_EMAIL";
    private static final String EXTRA_PASSWORD = "EXTRA_PASSWORD";

    private String email;
    private String password;

    @Bind(R.id.viewpager_profile)
    public ViewPager viewPager;

    @Bind(R.id.tablayout_profile)
    public TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup_infos);

        email = getExtraString(EXTRA_EMAIL);
        password = getExtraString(EXTRA_PASSWORD);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPager(ViewPager viewPager) {
        SignUpViewPagerAdapter adapter = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN_MR1) {
            adapter = new SignUpViewPagerAdapter(getSupportFragmentManager());
        } else {
            adapter = new SignUpViewPagerAdapter(getSupportFragmentManager());
        }

        adapter.addFrag(SignUpPersonFragment.newInstance(email, password), getString(R.string.person));
        adapter.addFrag(SignUpCompanyFragment.newInstance(email, password), getString(R.string.company));
        viewPager.setAdapter(adapter);
    }

    public static void startActivity(Context ctx, String email, String password) {
        Intent intent = new Intent(ctx, LoginSignupInfosActivity.class);
        intent.putExtra(EXTRA_EMAIL, email);
        intent.putExtra(EXTRA_PASSWORD, password);
        ctx.startActivity(intent);
    }
}
