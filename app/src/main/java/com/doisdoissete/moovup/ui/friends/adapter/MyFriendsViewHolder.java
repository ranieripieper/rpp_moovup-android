package com.doisdoissete.moovup.ui.friends.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.custom.CircleTextView;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.doisdoissete.moovup.ui.helper.UserHelper;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_my_friend)
public class MyFriendsViewHolder extends ItemViewHolder<User> {

    @ViewId(R.id.layout_row)
    View layoutRow;

    @ViewId(R.id.lbl_name)
    TextView lblName;

    @ViewId(R.id.lbl_goal_value)
    TextView lblGoalValue;

    @ViewId(R.id.lbl_goal_category)
    TextView lblGoalCategory;

    @ViewId(R.id.img_photo)
    ImageView imgPhoto;

    @ViewId(R.id.layout_activity)
    View layoutActivity;

    @ViewId(R.id.lbl_activity_date)
    TextView lblActivityDate;

    @ViewId(R.id.lbl_activity)
    TextView lblActivity;

    @ViewId(R.id.lbl_activity_comment)
    TextView lblActivityComment;

    @ViewId(R.id.img_activity)
    ImageView imgActivity;

    @ViewId(R.id.layout_upvote)
    View layoutUpvote;

    @ViewId(R.id.lbl_nr_goal_activity_upvote)
    CircleTextView lblNrGoalActivityUpvote;

    @ViewId(R.id.lbl_goal_activity_upvote)
    View lblGoalActivityUpvote;

    private int position;

    public MyFriendsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(User itemTmp, PositionInfo positionInfo) {
        position = positionInfo.getPosition();

        User item = UserCacheManager.getInstance().get(itemTmp.getId());
        if (item == null) {
            item = itemTmp;
        }

        lblName.setText(item.getFullName());
        String urlPhoto = UserHelper.getImageUrl(item);
        if (TextUtils.isEmpty(urlPhoto)) {
            imgPhoto.setImageResource(R.drawable.user_placeholder);
        } else {
            MoovUpApplication.imageLoaderUser.displayImage(item.getProfileImageUrl(), imgPhoto);
        }
        lblGoalValue.setText("");
        lblGoalValue.setVisibility(View.GONE);
        lblGoalCategory.setText(R.string.no_friend_goals);
        layoutActivity.setVisibility(View.GONE);

        if (itemTmp.getCurrentGoal() != null) {
            if (itemTmp.getCurrentGoal().getCategory() != null) {
                lblGoalCategory.setText(itemTmp.getCurrentGoal().getCategory().getTitle());
            }
            lblGoalValue.setVisibility(View.VISIBLE);
            lblGoalValue.setText(GoalHelper.getGoalDescription(getContext(), itemTmp.getCurrentGoal()));

            if (itemTmp.getCurrentGoal().getCurrentActivity() != null) {
                GoalActivity goalActivity = itemTmp.getCurrentGoal().getCurrentActivity();
                //render atividade
                layoutActivity.setVisibility(View.VISIBLE);

                lblActivityDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(goalActivity.getDate()));
                lblActivity.setText(GoalHelper.getActivityDescription(getContext(), goalActivity, itemTmp.getCurrentGoal()));
                lblActivityComment.setText(goalActivity.getBodyText());

                lblNrGoalActivityUpvote.setText(GoalHelper.getGoalActivityUpvote(goalActivity));
                if (goalActivity.isLiked()) {
                    lblNrGoalActivityUpvote.setBackgroundResource(R.drawable.circular_textview);
                    lblNrGoalActivityUpvote.setTextColor(getContext().getResources().getColor(android.R.color.white));
                    lblNrGoalActivityUpvote.setCircleText(true);
                    lblNrGoalActivityUpvote.setMinimumWidth(getContext().getResources().getDimensionPixelOffset(R.dimen.min_cirlce_withd));
                } else {
                    lblNrGoalActivityUpvote.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
                    lblNrGoalActivityUpvote.setTextColor(getContext().getResources().getColor(R.color.orange));
                    lblNrGoalActivityUpvote.setCircleText(false);
                    lblNrGoalActivityUpvote.setMinimumWidth(0);
                }

                String activityImage = GoalHelper.getImageGoalActivityUrl(goalActivity);
                if (TextUtils.isEmpty(activityImage)) {
                    imgActivity.setVisibility(View.GONE);
                } else {
                    MoovUpApplication.imageLoader.displayImage(activityImage, imgActivity);
                    imgActivity.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    @Override
    public void onSetListeners() {
        lblNrGoalActivityUpvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upvoteClicked();
            }
        });

        lblGoalActivityUpvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                upvoteClicked();
            }
        });

        layoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyFriendsHolderListener listener = getListener(MyFriendsHolderListener.class);
                if (listener != null) {
                    listener.onUserClicked(getItem());
                }
            }
        });

    }

    private void upvoteClicked() {
        MyFriendsHolderListener listener = getListener(MyFriendsHolderListener.class);
        if (listener != null && getItem().getCurrentGoal() != null && getItem().getCurrentGoal().getCurrentActivity() != null) {
            listener.onUpvotedClicked(getItem().getCurrentGoal().getCurrentActivity(), position);
        }
    }

    public interface MyFriendsHolderListener {
        void onUserClicked(User user);

        void onUpvotedClicked(GoalActivity goalActivity, int position);
    }
}