package com.doisdoissete.moovup.ui.questions;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.asynctask.ResizeImageAsyncTask;
import com.doisdoissete.moovup.service.cache.AnswerCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.AnswerResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.custom.OnKeyboardVisibilityListener;
import com.doisdoissete.moovup.ui.helper.AnswerHelper;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 12/8/15.
 */
@RuntimePermissions
public class AnswerQuestionFragment extends BaseFragment implements ImageChooserListener {

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.edt_answer)
    EditText edtAnswer;

    @Bind(R.id.txt_question)
    TextView txtQuestion;

    @Bind(R.id.img_photo)
    ImageView imgPhoto;

    @Bind(R.id.btn_answer_question)
    Button btnAnswerQuestion;

    @Bind(R.id.btn_remove_image)
    ImageView btnRemoveImage;

    @Bind(R.id.layout_content)
    RelativeLayout mRelativeLayout;

    private Question mQuestion;
    private Answers mEditAnswer;

    // Photo Picker Stuff
    private ImageChooserManager imageChooserManager;
    private int chooserType;
    protected String filePath;

    public static final String EXTRA_QUESTION_TO_ANSWER = "EXTRA_QUESTION_TO_ANSWER";
    public static final String EXTRA_EDIT_ANSWER = "EXTRA_EDIT_ANSWER";
    public static final String EXTRA_QUESTION_ANSWERED = "EXTRA_QUESTION_ANSWERED";

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_answer_question;
    }

    public static AnswerQuestionFragment newInstance(Question question) {
        return newInstance(question, null);
    }

    public static AnswerQuestionFragment newInstance(Question question, Answers editAnswer) {
        AnswerQuestionFragment fragment = new AnswerQuestionFragment();
        Bundle args = new Bundle();
        args.putLong(EXTRA_QUESTION_TO_ANSWER, question.getId());
        if (editAnswer != null) {
            args.putLong(EXTRA_EDIT_ANSWER, editAnswer.getId());
        }
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        if (getArguments() != null) {
            long questionId = getArguments().getLong(EXTRA_QUESTION_TO_ANSWER);
            mQuestion = QuestionCacheManager.getInstance().get(questionId);
            long answerId = getArguments().getLong(EXTRA_EDIT_ANSWER);
            mEditAnswer = AnswerCacheManager.getInstance().get(answerId);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mQuestion == null) {
            finish();
        } else {
            if (isAdded()) {
                txtQuestion.setText(mQuestion.getBodyText());
                setKeyboardListener(new OnKeyboardVisibilityListener() {
                    @Override
                    public void onVisibilityChanged(boolean isShowing) {
                        if (isAdded() && btnAnswerQuestion != null) {
                            if (isShowing) {
                                btnAnswerQuestion.setVisibility(View.GONE);
                            } else {
                                btnAnswerQuestion.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
                if (mEditAnswer != null) {
                    edtAnswer.setText(mEditAnswer.getBodyText());
                    String imageUrl = AnswerHelper.getImageUrl(mEditAnswer);
                    if (!TextUtils.isEmpty(imageUrl)) {
                        imgPhoto.setVisibility(View.VISIBLE);
                        btnRemoveImage.setVisibility(View.VISIBLE);
                        MoovUpApplication.imageLoader.displayImage(imageUrl, imgPhoto);
                    }
                    btnAnswerQuestion.setText(R.string.update_answer);
                }
            }
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.answer_question);
    }

    @OnClick(R.id.btn_answer_question)
    public void addAnswerClick(View v) {
        hideKeyboard();
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callServiceSendAnswer();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }

    private void callServiceSendAnswer() {
        showLoadingView();
        if (!TextUtils.isEmpty(filePath)) {
            new ResizeImageAsyncTask(getActivity(), filePath, new ResizeImageAsyncTask.ResizeImageListener() {
                @Override
                public void onCancelled() {
                    hideLoadingView();
                }

                @Override
                public void onPostExecute(String resultFilePath) {
                    callSaveUpdateService(new TypedFile("image/jpeg", new File(resultFilePath)));
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            callSaveUpdateService(null);
        }

    }

    private void callSaveUpdateService(TypedFile photo) {
        if (mEditAnswer != null) {
            String clearAttr = "";
            if (photo == null) {
                clearAttr = "image";
            }

            RetrofitManager.getInstance().getQuestionService().updateAnswer(mQuestion.getId(), mEditAnswer.getId(), edtAnswer.getText().toString(), photo, clearAttr, new Callback<AnswerResponse>() {
                @Override
                public void success(AnswerResponse answerResponse, Response response) {
                    hideLoadingView();
                    processSuccess(answerResponse);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getQuestionService().answerQuestion(mQuestion.getId(), edtAnswer.getText().toString(), photo, new Callback<AnswerResponse>() {
                @Override
                public void success(AnswerResponse answerResponse, Response response) {
                    processSuccess(answerResponse);
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }
    }

    private void processSuccess(AnswerResponse answerResponse) {
        hideLoadingView();
        if (answerResponse.isSuccess()) {

            if (mEditAnswer == null) {
                AnswerCacheManager.getInstance().put(mQuestion.getId(), answerResponse.getAnswer());
            } else {
                AnswerCacheManager.getInstance().put(answerResponse.getAnswer());
            }
            showSuccessDialog(getContext(), R.string.msg_answer_sent, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                    finish();
                    Intent i = new Intent();
                    i.putExtra(EXTRA_QUESTION_ANSWERED, mQuestion.getId());
                    getTargetFragment().onActivityResult(RequestCodeUtil.REQUEST_CODE_ANSWER_QUESTION, Activity.RESULT_OK, i);
                }
            });
        }
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    // Should be called if for some reason the ImageChooserManager is null (Due
    // to destroying of activity for low memory situations)
    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        filePath = chosenImage.getFileThumbnail();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Uri uriFile = Uri.fromFile(new File(filePath));
                MoovUpApplication.imageLoaderUser.displayImage(Uri.decode(uriFile.toString()), imgPhoto, new ImageLoadingListener() {
                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                        imgPhoto.setVisibility(View.GONE);
                        btnRemoveImage.setVisibility(View.GONE);
                        filePath = null;
                        showToastError(getActivity(), getString(R.string.error_generic));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        imgPhoto.setVisibility(View.VISIBLE);
                        btnRemoveImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                        imgPhoto.setVisibility(View.GONE);
                        btnRemoveImage.setVisibility(View.GONE);
                        filePath = null;
                        showToastError(getActivity(), getString(R.string.error_generic));
                    }

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }
                });
            }
        });
    }

    @Override
    public void onError(String s) {
        showToastError(getActivity(), getString(R.string.error_generic));
        filePath = null;
        imgPhoto.setVisibility(View.GONE);
        btnRemoveImage.setVisibility(View.GONE);
    }

    public void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    public void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    protected void showMediaTypeChooserChecked() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_photo_video_chooser);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.choose_photo).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.take_picture).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.btn_camera)
    protected void onPhotoClicked() {
        AnswerQuestionFragmentPermissionsDispatcher.showMediaTypeChooserWithCheck(this);
    }

    @OnClick(R.id.btn_remove_image)
    protected void onRemoveImageClicked() {
        filePath = null;
        imgPhoto.setVisibility(View.GONE);
        btnRemoveImage.setVisibility(View.GONE);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    protected void showMediaTypeChooser() {
        showMediaTypeChooserChecked();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        AnswerQuestionFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }
}


