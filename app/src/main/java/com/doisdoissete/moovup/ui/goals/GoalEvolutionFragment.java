package com.doisdoissete.moovup.ui.goals;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.GoalActivityReport;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.YAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;

import java.util.ArrayList;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalEvolutionFragment extends BaseFragment implements OnChartValueSelectedListener {

    private Goal mGoal;

    @Bind(R.id.chart_bar)
    BarChart mBarChart;

    @Bind(R.id.chart_line)
    LineChart mLineChart;

    @Bind(R.id.lbl_update)
    TextView lblUdate;

    @Bind(R.id.lbl_goal_value)
    TextView lblGoal;

    private boolean mIsGoalCurrentUser = false;
    protected GoalDetailFragment mGoalDetailFragment;

    public static GoalEvolutionFragment newInstance(Goal goal, GoalDetailFragment goalDetailFragment) {
        GoalEvolutionFragment fragment = new GoalEvolutionFragment();
        Bundle bundle = new Bundle();
        fragment.mGoal = goal;
        fragment.mGoalDetailFragment = goalDetailFragment;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        if  (mGoal != null) {
            User user = getCurrentUser();
            Crashlytics.log("GoalEvolutionFragment - goal é null " + user);
            if (user != null &&
                    ((mGoal.getUserId() != null && user.getId() == mGoal.getUserId()) ||
                            (mGoal.getUser() != null && user.getId() == mGoal.getUser().getId()))) {
                mIsGoalCurrentUser = true;
            } else {
                mIsGoalCurrentUser = false;
            }
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lblGoal.setText(GoalHelper.getGoalDescription(getActivity(), mGoal));
        lblUdate.setText(getString(R.string.goal_updated, DateUtil.DATE_DAY_MONTH_YEAR.get().format(mGoal.getUpdatedAt())));
        loadData();
    }

    private void processResult(Goal goal) {
        if (!isAdded()) {
            return;
        }
        if (goal != null) {
            initChart(goal);
        }
    }

    private void initChart(Goal goal) {
        Category category = CategoryCacheManager.getInstance().get(mGoal.getCategoryId());
        if (category != null) {
            if (category.isClosedGoal()) {
                initLineChart(goal);
            } else {
                initBarChart(goal);
            }
        }
    }

    private void initLineChart(Goal goal) {
        mLineChart.clear();
        mLineChart.setOnChartValueSelectedListener(this);
        mLineChart.setDrawGridBackground(false);

        // no description text
        mLineChart.setDescription("");
        mLineChart.setNoDataTextDescription("");

        mLineChart.setDragDecelerationFrictionCoef(0.9f);

        // enable scaling and dragging
        mLineChart.setDragEnabled(true);
        mLineChart.setScaleEnabled(true);
        mLineChart.setDrawGridBackground(false);
        mLineChart.setHighlightPerDragEnabled(true);
        mLineChart.setPinchZoom(true);

        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawAxisLine(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setAvoidFirstLastClipping(true);

        YAxis leftAxis = mLineChart.getAxisLeft();
        leftAxis.removeAllLimitLines();
        leftAxis.setDrawGridLines(false);
        leftAxis.setDrawAxisLine(false);

        // limit lines are drawn behind data (and not on top)
        leftAxis.setDrawLimitLinesBehindData(true);

        mLineChart.getAxisRight().setEnabled(false);

        Legend l = mLineChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_RIGHT);
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setFormSize(10f);
        l.setTextSize(12f);
        l.setXEntrySpace(4f);

        setDataLineChart(goal);
    }

    private void setDataLineChart(Goal goal) {

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> yVals = new ArrayList<>();
        ArrayList<Entry> yValsGoal = new ArrayList<>();

        xVals.add(DateUtil.DATE_DAY_MONTH_YEAR.get().format(goal.getCreatedAt()));
        yValsGoal.add(new Entry(goal.getCurrent(), 0));
        int i = 1;
        if (goal.getGoalActivitiesReport() != null) {
            for (GoalActivityReport report : goal.getGoalActivitiesReport()) {
                xVals.add(GoalHelper.getLineXVal(report));
                yVals.add(new Entry(report.getTotalByInterval(), i));
                i++;
            }
        }
        yValsGoal.add(new Entry(goal.getTarget(), i));
        xVals.add(DateUtil.DATE_DAY_MONTH_YEAR.get().format(goal.getTargetDate()));

        LineDataSet setActivities = new LineDataSet(yVals, getString(R.string.goal_activity_legend));
        setActivities.setAxisDependency(YAxis.AxisDependency.RIGHT);
        setActivities.setColor(getResources().getColor(R.color.chart_value));
        setActivities.setCircleColor(getResources().getColor(R.color.chart_value));
        setActivities.setLineWidth(5f);
        setActivities.setCircleRadius(2f);
        setActivities.setFillAlpha(65);
        setActivities.setFillColor(getResources().getColor(R.color.chart_value));
        setActivities.setDrawCircleHole(false);
        setActivities.setDrawValues(false);

        LineDataSet setGoal = new LineDataSet(yValsGoal, getString(R.string.goal_legend));
        setGoal.setAxisDependency(YAxis.AxisDependency.RIGHT);
        setGoal.setColor(getResources().getColor(R.color.chart_goal));
        setGoal.setCircleColor(getResources().getColor(R.color.chart_goal));
        setGoal.setLineWidth(5f);
        setGoal.setCircleRadius(2f);
        setGoal.setFillAlpha(65);
        setGoal.setFillColor(getResources().getColor(R.color.chart_goal));
        setGoal.setDrawCircleHole(false);
        setGoal.setDrawValues(false);

        ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
        dataSets.add(setActivities);
        dataSets.add(setGoal);

        // create a data object with the datasets
        LineData data = new LineData(xVals, dataSets);

        // set data
        mLineChart.setData(data);

        mLineChart.setData(data);
        mLineChart.animateX(1000);
        mLineChart.setVisibility(View.VISIBLE);
        mLineChart.invalidate();
    }

    public void refresh() {
        if (!isAdded()) {
            return;
        }
        loadData();
    }

    private void initBarChart(Goal goal) {
        mBarChart.clear();
        mBarChart.setOnChartValueSelectedListener(this);
        mBarChart.setPinchZoom(false);
        mBarChart.setDrawBarShadow(false);
        mBarChart.setDrawValueAboveBar(true);
        mBarChart.setDescription("");
        mBarChart.setVisibleXRangeMaximum(5);
        mBarChart.setDrawGridBackground(false);

        XAxis xAxis = mBarChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);
        xAxis.setSpaceBetweenLabels(2);
        xAxis.setAvoidFirstLastClipping(true);

        YAxis leftAxis = mBarChart.getAxisLeft();
        leftAxis.setLabelCount(8, false);
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART);
        leftAxis.setSpaceTop(5f);
        leftAxis.setAxisMinValue(0f);
        YAxisValueFormatter custom = new MyYAxisValueFormatter();

        YAxis rightAxis = mBarChart.getAxisRight();
        rightAxis.setDrawGridLines(false);
        rightAxis.setDrawAxisLine(false);
        rightAxis.setLabelCount(0, true);
        rightAxis.setShowOnlyMinMax(false);
        rightAxis.setSpaceTop(5f);
        rightAxis.setValueFormatter(custom);
        rightAxis.setAxisMinValue(0f);


        Legend l = mBarChart.getLegend();
        l.setPosition(Legend.LegendPosition.ABOVE_CHART_RIGHT);
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setFormSize(10f);
        l.setTextSize(12f);
        l.setXEntrySpace(4f);

        setDataClosedGoal(goal);
    }

    public class MyYAxisValueFormatter implements YAxisValueFormatter {

        public MyYAxisValueFormatter() {
        }

        @Override
        public String getFormattedValue(float value, YAxis yAxis) {
            return "";
        }
    }

    private void setDataClosedGoal(Goal goal) {

        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<BarEntry> yVals = new ArrayList<>();
        ArrayList<BarEntry> yValsGoal = new ArrayList<>();

        int i = 0;

        if (goal.getGoalActivitiesReport() != null) {
            for (GoalActivityReport report : goal.getGoalActivitiesReport()) {
                xVals.add(GoalHelper.getBarXVal(getActivity(), goal, report));
                yVals.add(new BarEntry(report.getTotalByInterval(), i));
                yValsGoal.add(new BarEntry(goal.getTotal(), i));
                i++;
            }
        }

        BarDataSet setValues = new BarDataSet(yVals, getString(R.string.goal_activity_legend));
        setValues.setColors(new int[]{R.color.chart_value}, getActivity());
        setValues.setDrawValues(false);

        BarDataSet setGoal = new BarDataSet(yValsGoal, getString(R.string.goal_legend));
        setGoal.setColors(new int[]{R.color.chart_goal}, getActivity());
        setGoal.setDrawValues(false);

        ArrayList<IBarDataSet> dataSets = new ArrayList<>();
        dataSets.add(setValues);
        dataSets.add(setGoal);

        BarData data = new BarData(xVals, dataSets);
        data.setValueTextSize(10f);

        mBarChart.setData(data);
        mBarChart.animateY(1000);
        mBarChart.setVisibility(View.VISIBLE);
        mBarChart.invalidate();

    }

    @SuppressLint("NewApi")
    @Override
    public void onValueSelected(Entry e, int dataSetIndex, Highlight h) {
    }

    public void onNothingSelected() {
    }

    private void loadData() {
        showLoadingView();

        if (mIsGoalCurrentUser) {
            RetrofitManager.getInstance().getGoalService().getMyGoalToReport(mGoal.getId(), new Callback<Goal>() {
                @Override
                public void success(Goal goal, Response response) {
                    processResult(goal);
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, new ErrorListener()  {
                        @Override
                        public void positiveButton() {
                            loadData();
                        }

                        @Override
                        public void negativeButton() {

                        }
                    });
                }
            });
        } else {
            RetrofitManager.getInstance().getGoalService().getGoalToReport(mGoal.getUserId(), mGoal.getId(), new Callback<Goal>() {
                @Override
                public void success(Goal goal, Response response) {
                    processResult(goal);
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, new ErrorListener()  {
                        @Override
                        public void positiveButton() {
                            loadData();
                        }

                        @Override
                        public void negativeButton() {
                        }
                    });
                }
            });
        }
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_evolution;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
