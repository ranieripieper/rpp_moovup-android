package com.doisdoissete.moovup.ui.friends;

import android.app.SearchManager;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.FriendsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseActivity;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.friends.adapter.MyFriendsViewHolder;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.search.SearchActivity;
import com.doisdoissete.moovup.util.FunctionsUtil;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 12/18/15.
 */
public class MyFriendsFragment extends BaseFragment implements MyFriendsViewHolder.MyFriendsHolderListener {

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_view)
    protected EasyLoadMoreRecyclerView mRecyclerView;

    @Bind(R.id.txt_no_results)
    protected TextView txtNoResults;

    protected EasyLoadMoreRecyclerAdapter<User> mFriendsAdapter;

    protected Integer mPage = 1;

    public static MyFriendsFragment newInstance() {
        return new MyFriendsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSwipeRefresh();
    }

    private void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });

        loadData();
    }

    private void loadData() {
        if (mPage == 1) {
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    mPage++;
                    loadData();
                }
            });
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
            showLoadingView();
            if (mFriendsAdapter != null) {
                mFriendsAdapter.removeAllItems();
            }
        }
        callService();
    }

    protected void callService() {
        RetrofitManager.getInstance().getFriendsService().getMyFriends(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<FriendsResponse>() {
            @Override
            public void success(FriendsResponse friendsResponse, Response response) {
                if (isAdded()) {
                    processResponse(friendsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void processResponse(FriendsResponse response) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if (response.getFriends() != null && response.getFriends().size() > 0) {
            disableLoadingMore(mRecyclerView, response.getMeta(), response.getFriends().size());
            UserCacheManager.getInstance().putAll(response.getFriends());
            renderUsers(response.getFriends());
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
        } else {
            showEmpty();
        }
    }

    public void renderUsers(List<User> list) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mFriendsAdapter == null) {
            createAdapter(list);
        } else {
            mFriendsAdapter.addItems(list);
        }
    }

    protected void createAdapter(List<User> list) {
        mFriendsAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                MyFriendsViewHolder.class,
                list,
                this,
                R.layout.loading_recycler_view);

        mFriendsAdapter.notifyDataSetChanged();

        mRecyclerView.setAdapter(mFriendsAdapter);
    }

    protected void showEmpty(boolean force) {
        disableLoadingMore(mRecyclerView);
        if (force || mPage <= 1) {
            txtNoResults.setVisibility(View.VISIBLE);
        }
        hideLoadingView();
    }

    protected void showEmpty() {
        showEmpty(false);
    }

    private MenuItem mSearchMenu;

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.menu_my_friends, menu);

        mSearchMenu = menu.findItem(R.id.menu_friend_search);

        Drawable drawable = mSearchMenu.getIcon();
        drawable = DrawableCompat.wrap(drawable);

        DrawableCompat.setTint(drawable, getResources().getColor(R.color.white));

        mSearchMenu.setIcon(drawable);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) mSearchMenu.getActionView();
        ViewUtil.configSearchView((BaseActivity) getActivity(), searchManager, searchView, SearchActivity.FRIENDS_SEARCH_TYPE);

        searchView.setQueryHint(getString(R.string.search_frirends_hint));

        Drawable drawableShare = menu.findItem(R.id.menu_friend_share).getIcon();

        drawableShare = DrawableCompat.wrap(drawableShare);

        DrawableCompat.setTint(drawableShare, getResources().getColor(R.color.white));

        menu.findItem(R.id.menu_friend_share).setIcon(drawableShare);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
        txtNoResults.setVisibility(View.GONE);
    }

    @Override
    public void onUserClicked(User user) {
        Navigator.navigateToUserGoalsFragment(getActivity(), this, user.getId());
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_friend_share) {
            FunctionsUtil.shareUrl(getContext(), getString(R.string.share_friends));
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onUpvotedClicked(final GoalActivity goalActivity, final int position) {
        super.showLoadingView();
        if (goalActivity.isLiked()) {
            RetrofitManager.getInstance().getGoalService().downvoteGoalActivity(goalActivity.getId(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateUpvotedGoalActivity(goalActivity, false, position);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getGoalService().upvoteGoalActivity(goalActivity.getId(), "", new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateUpvotedGoalActivity(goalActivity, true, position);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }
    }

    private void updateUpvotedGoalActivity(GoalActivity goalActivity, boolean upvouted, int position) {
        if (goalActivity.getUpvotesCount() == null) {
            goalActivity.setUpvotesCount(0l);
        }
        goalActivity.setLiked(upvouted);
        goalActivity.setUpvotesCount(goalActivity.getUpvotesCount() + (upvouted ? 1 : -1));
        if (mFriendsAdapter != null) {
            mFriendsAdapter.notifyItemChanged(position);
        }
    }

    @OnClick(R.id.fab_add_friend)
    void fabAddFriendClick() {
        if (isAdded() && mSearchMenu != null) {
            mSearchMenu.expandActionView();
            mSearchMenu.getActionView().requestFocus();
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        if (mFriendsAdapter != null) {
            mFriendsAdapter.notifyDataSetChanged();
        }
        verifyIfEmpty();
    }


    private void verifyIfEmpty() {
        if (mFriendsAdapter != null && mFriendsAdapter.getItemCount() <= 0) {
            showEmpty(true);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_friends;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.my_friends);
    }
}
