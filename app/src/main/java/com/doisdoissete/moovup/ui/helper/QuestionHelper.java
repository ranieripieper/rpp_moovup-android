package com.doisdoissete.moovup.ui.helper;

import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.util.NumberUtil;

/**
 * Created by ranipieper on 12/17/15.
 */
public class QuestionHelper {

    public static String getAnswersCount(Question question) {
        return NumberUtil.getNumberFormated(question.getAnswersCount());
    }
}
