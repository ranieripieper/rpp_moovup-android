package com.doisdoissete.moovup.ui.settings.adapter;

import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 1/8/16.
 */
@LayoutId(R.layout.row_about)
public class AboutMoovupViewHolder extends ItemViewHolder<String> {

    @ViewId(R.id.lbl_text)
    TextView lblText;


    public AboutMoovupViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(final String item, PositionInfo positionInfo) {
        lblText.setText(item);
    }


    @Override
    public void onSetListeners() {
    }
}