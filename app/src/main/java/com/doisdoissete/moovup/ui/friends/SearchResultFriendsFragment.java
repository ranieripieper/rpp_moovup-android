package com.doisdoissete.moovup.ui.friends;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.FriendshipResponse;
import com.doisdoissete.moovup.service.model.response.UsersResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.friends.adapter.SearchFriendsViewHolder;
import com.doisdoissete.moovup.util.Constants;

import java.util.Arrays;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 11/26/15.
 */
public class SearchResultFriendsFragment extends MyFriendsFragment implements SearchFriendsViewHolder.SearchFriendsHolderListener {

    private static final String ARG_QUERY = "ARG_QUERY";

    private String mQuery = "";


    public static SearchResultFriendsFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(ARG_QUERY, query);
        SearchResultFriendsFragment fragment = new SearchResultFriendsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        if (getArguments() != null) {
            mQuery = getArguments().getString(ARG_QUERY);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        txtNoResults.setText(R.string.no_result_search_friends);
        super.onViewCreated(view, savedInstanceState);
    }

    protected void callService() {
        if (mQuery == null || mQuery.length() < Constants.MIN_CHARACTER_TO_SEARCH) {
            txtNoResults.setText(getString(R.string.search_min_character, Constants.MIN_CHARACTER_TO_SEARCH));
            showEmpty();
        } else {
            RetrofitManager.getInstance().getFriendsService().searchFriends(mQuery, User.PERSON_PROFILE_TYPE, mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<UsersResponse>() {
                @Override
                public void success(UsersResponse friendsResponse, Response response) {
                    if (isAdded()) {
                        processResponse(friendsResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isAdded()) {
                        showError(error);
                        disableLoadingMore(mRecyclerView);
                    }
                }
            });
        }
    }

    protected void createAdapter(List<User> list) {
        mFriendsAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                SearchFriendsViewHolder.class,
                list,
                this,
                R.layout.loading_recycler_view);

        mFriendsAdapter.notifyDataSetChanged();

        mRecyclerView.setAdapter(mFriendsAdapter);
    }

    protected void processResponse(UsersResponse response) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        txtNoResults.setText(R.string.no_result_search_friends);
        if (response.getUsers() != null && response.getUsers().size() > 0) {
            disableLoadingMore(mRecyclerView, response.getMeta(), response.getUsers().size());
            UserCacheManager.getInstance().putAll(response.getUsers());
            renderUsers(response.getUsers());
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
        } else {
            showEmpty();
        }
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_friends;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    public void onUserClicked(User user) {
        super.onUserClicked(user);
    }

    @Override
    public void onAcceptClicked(final User user, final int position) {
        super.showLoadingView();

        if (User.FRIENDSHIP_STATUS_SELF_PENDING.equalsIgnoreCase(user.getFriendshipStatus())) {
           //accept friendship
            RetrofitManager.getInstance().getFriendsService().acceptFriendship(user.getId(), "", new Callback<FriendshipResponse>() {
                @Override
                public void success(FriendshipResponse friendshipResponse, Response response) {
                    acceptFriendship(user, friendshipResponse, position);
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            //send friendship
            RetrofitManager.getInstance().getFriendsService().sendFriendship(user.getId(), new Callback<FriendshipResponse>() {
                @Override
                public void success(FriendshipResponse friendshipResponse, Response response) {
                    friendshipSent(user, friendshipResponse, position);
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }

    }

    @Override
    public void onDeclineClicked(final User user, final int position) {
        super.showLoadingView();

        RetrofitManager.getInstance().getFriendsService().declineFriendship(user.getId(), new Callback<FriendshipResponse>() {
            @Override
            public void success(FriendshipResponse friendshipResponse, Response response) {
                declineFriendship(user, friendshipResponse, position);
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });

    }

    private void friendshipSent(User item, FriendshipResponse friendshipResponse, int position) {
        showSuccess(item, friendshipResponse, position, User.FRIENDSHIP_STATUS_USER_PENDING, R.string.msg_friendship_sent);
    }

    private void acceptFriendship(User item, FriendshipResponse friendshipResponse, int position) {
        showSuccess(item, friendshipResponse, position, User.FRIENDSHIP_STATUS_SELF_ACCEPTED, R.string.msg_friendship_accept);
    }

    private void declineFriendship(User item, FriendshipResponse friendshipResponse, int position) {
        showSuccess(item, friendshipResponse, position, User.FRIENDSHIP_STATUS_SELF_DECLINED, R.string.msg_friendship_declined);
    }

    private void showSuccess(User item, FriendshipResponse friendshipResponse, int position, String status, int msg) {
        if (!isAdded()) {
            return;
        }

        if (friendshipResponse.isSuccess()) {
            showSuccessDialog(getContext(), msg);
            item.setFriendshipStatus(status);
            UserCacheManager.getInstance().updateWithoutNulls(Arrays.asList(item));
            mFriendsAdapter.notifyItemChanged(position);
        } else {
            showErrorDialog(R.string.error_generic);
        }
    }
}
