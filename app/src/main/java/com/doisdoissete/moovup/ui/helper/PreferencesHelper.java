package com.doisdoissete.moovup.ui.helper;

import com.doisdoissete.moovup.service.model.Preferences;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by ranipieper on 2/19/16.
 */
public class PreferencesHelper {

    public static HashMap<String, Boolean> getPreferences(Preferences pref) {
        HashMap<String, Boolean> result = new HashMap<>();
        if (pref != null) {
            result.put(Preferences.NOTIFY_GOALS_OUTDATED, pref.isNotifyGoalsOutdated());
            result.put(Preferences.NOTIFY_PASSWORD_UPDATED, pref.isNotifyPasswordUpdated());
            result.put(Preferences.NOTIFY_NEW_LIKE_FOR_GOAL, pref.isNotifyNewLikeForGoal());
            result.put(Preferences.NOTIFY_OFFICIAL_STATEMENT, pref.isNotifyOfficialStatement());
            result.put(Preferences.NOTIFY_ACTIVITIES_OUTDATED, pref.isNotifyActivitiesOutdated());
            result.put(Preferences.NOTIFY_NEW_LIKE_FOR_ANSWER, pref.isNotifyNewLikeForAnswer());
            result.put(Preferences.NOTIFY_NEW_LIKE_FOR_ACTIVITY, pref.isNotifyNewLikeForActivity());
            result.put(Preferences.NOTIFY_NEW_COMMENT_FOR_ANSWER, pref.isNotifyNewCommentForAnswer());
            result.put(Preferences.NOTIFY_NEW_FRIENDSHIP_REQUEST, pref.isNotifyNewFriendshipRequest());
            result.put(Preferences.NOTIFY_NEW_ANSWER_FOR_QUESTION, pref.isNotifyNewAnswerForQuestion());
            result.put(Preferences.NOTIFY_NEW_QUESTIONS_TO_ANSWER, pref.isNotifyNewQuestionsToAnswer());
            result.put(Preferences.NOTIFY_FRIENDSHIP_REQUEST_ACCEPTED, pref.isNotifyFriendshipRequestAccepted());
        }

        return result;
    }

    public static HashMap<String, String> getPreferencesToSendService(Preferences pref) {
        HashMap<String, String> result = new HashMap<>();
        if (pref != null) {
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_GOALS_OUTDATED), String.valueOf(pref.isNotifyGoalsOutdated()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_PASSWORD_UPDATED), String.valueOf(pref.isNotifyPasswordUpdated()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_LIKE_FOR_GOAL), String.valueOf(pref.isNotifyNewLikeForGoal()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_OFFICIAL_STATEMENT), String.valueOf(pref.isNotifyOfficialStatement()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_ACTIVITIES_OUTDATED), String.valueOf(pref.isNotifyActivitiesOutdated()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_LIKE_FOR_ANSWER), String.valueOf(pref.isNotifyNewLikeForAnswer()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_LIKE_FOR_ACTIVITY), String.valueOf(pref.isNotifyNewLikeForActivity()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_COMMENT_FOR_ANSWER), String.valueOf(pref.isNotifyNewCommentForAnswer()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_FRIENDSHIP_REQUEST), String.valueOf(pref.isNotifyNewFriendshipRequest()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_ANSWER_FOR_QUESTION), String.valueOf(pref.isNotifyNewAnswerForQuestion()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_NEW_QUESTIONS_TO_ANSWER), String.valueOf(pref.isNotifyNewQuestionsToAnswer()));
            result.put(getPreferencesKeyToService(Preferences.NOTIFY_FRIENDSHIP_REQUEST_ACCEPTED), String.valueOf(pref.isNotifyFriendshipRequestAccepted()));
        }

        return result;
    }

    public static Preferences getPreferences(Map<String, Boolean> map) {
        Preferences preferences = new Preferences();

        preferences.setNotifyGoalsOutdated(map.get(Preferences.NOTIFY_GOALS_OUTDATED));
        preferences.setNotifyPasswordUpdated(map.get(Preferences.NOTIFY_PASSWORD_UPDATED));
        preferences.setNotifyNewLikeForGoal(map.get(Preferences.NOTIFY_NEW_LIKE_FOR_GOAL));
        preferences.setNotifyOfficialStatement(map.get(Preferences.NOTIFY_OFFICIAL_STATEMENT));
        preferences.setNotifyActivitiesOutdated(map.get(Preferences.NOTIFY_ACTIVITIES_OUTDATED));
        preferences.setNotifyNewLikeForAnswer(map.get(Preferences.NOTIFY_NEW_LIKE_FOR_ANSWER));
        preferences.setNotifyNewLikeForActivity(map.get(Preferences.NOTIFY_NEW_LIKE_FOR_ACTIVITY));
        preferences.setNotifyNewCommentForAnswer(map.get(Preferences.NOTIFY_NEW_COMMENT_FOR_ANSWER));
        preferences.setNotifyNewFriendshipRequest(map.get(Preferences.NOTIFY_NEW_FRIENDSHIP_REQUEST));
        preferences.setNotifyNewAnswerForQuestion(map.get(Preferences.NOTIFY_NEW_ANSWER_FOR_QUESTION));
        preferences.setNotifyNewQuestionsToAnswer(map.get(Preferences.NOTIFY_NEW_QUESTIONS_TO_ANSWER));
        preferences.setNotifyFriendshipRequestAccepted(map.get(Preferences.NOTIFY_FRIENDSHIP_REQUEST_ACCEPTED));

        return preferences;
    }

    public static String getPreferencesKeyToService(String key) {
        return String.format("preferences[%s]", key);
    }
}
