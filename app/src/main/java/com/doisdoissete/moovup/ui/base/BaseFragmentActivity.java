package com.doisdoissete.moovup.ui.base;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public abstract class BaseFragmentActivity extends BaseActivity {

    @Nullable
    @Bind(R.id.navigation_view)
    protected NavigationView mNavigationView;

    @Nullable
    @Bind(R.id.drawer_layout)
    protected DrawerLayout mDrawerLayout;

    protected ActionBarDrawerToggle mDrawerToggle;

    protected List<String> mFragments = new ArrayList<>();

    protected abstract int getLayoutFragmentContainer();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * Adds a {@link Fragment} to this activity's layout.
     *
     * @param fragment The fragment to be added.
     */
    public void addFragment(BaseFragment fragment) {
        addFragment(null, fragment, null, false);
    }

    public void addFragment(BaseFragment fragment, boolean clearBackStack) {
        addFragment(null, fragment, null, clearBackStack);
    }

    public void addFragment(BaseFragment fragment, BaseFragment fragment2, boolean clearBackStack) {
        addFragment(null, fragment, fragment2, clearBackStack);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment) {
        addFragment(actualFragment, fragment, null, false);
    }

    public void addFragment(BaseFragment actualFragment, BaseFragment fragment, BaseFragment fragment2, boolean clearBackStack) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        if (clearBackStack) {
            clearBackStack(transaction);
        }

        transaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_left,
                R.anim.slide_in_left, R.anim.slide_out_right);

        String tag = fragment.getClass().toString();
        transaction.add(getLayoutFragmentContainer(), fragment, tag).addToBackStack(tag);
        transaction.commitAllowingStateLoss();

        if (actualFragment != null) {
            fragment.setTargetFragment(actualFragment, -1);
        }

        mFragments.add(tag);
        //call executePendingTransactions() else findFragmentByTag() will return null
        getSupportFragmentManager().executePendingTransactions();

        if (fragment2 != null) {
            addFragment(fragment, fragment2, null, false);
        }
    }

    private Fragment getActualFragment() {
        if (mFragments.size() > 0) {
            FragmentManager manager = getSupportFragmentManager();
            return manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
        }

        return null;
    }


    public void clearBackStack(FragmentTransaction transaction) {
        FragmentManager manager = getSupportFragmentManager();
        for (String fragmentTag : mFragments) {
            manager.findFragmentByTag(fragmentTag);
            transaction.remove(getActualFragment());
        }
        mFragments = new ArrayList<>();
    }

    @Override
    public void onBackPressed() {
        User user = UserCacheManager.getInstance().getCurrentUser();
        if (mFragments.size() <= 1) {
            if (user != null && mNavBackMode) {
                Navigator.navigateToHome(getContext());
            } else {
                finish();
            }
        } else {
            callFragmentBackPressed();
            super.onBackPressed();
        }
    }

    protected void callFragmentBackPressed() {
        FragmentManager manager = getSupportFragmentManager();

        if (manager != null) {
            if (mFragments.size() > 0) {
                Fragment lastFragment = manager.findFragmentByTag(mFragments.get(mFragments.size() - 1));
                if (lastFragment != null && lastFragment instanceof BaseFragment) {
                    ((BaseFragment) lastFragment).onBackPressed();
                }

                //previous fragment
                if (mFragments.size() > 1) {
                    Fragment prevFragment = manager.findFragmentByTag(mFragments.get(mFragments.size()-2));
                    if (prevFragment != null && prevFragment instanceof BaseFragment) {
                        ((BaseFragment) prevFragment).onResumeFromBackStack();
                    }
                }

                //remove o fragment
                mFragments.remove(mFragments.size()-1);
            }
        }
    }

    /*
    private FragmentManager.OnBackStackChangedListener getListener() {
        FragmentManager.OnBackStackChangedListener result = new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                FragmentManager manager = getSupportFragmentManager();

                if (manager != null) {
                    int backStackEntryCount = manager.getBackStackEntryCount();
                    if (backStackEntryCount > 0) {

                        for (Fragment fragment : manager.getFragments()) {
                            if (fragment.isResumed()) {
                                if (fragment instanceof BaseFragment) {
                                    //((BaseFragment) fragment).onResumeFromBackStack();
                                }
                            }
                        }
                        //Fragment fragment = manager.getFragments().get(manager.getFragments().size()-1);
                        //fragment.isResumed()

                    }
                }
            }
        };

        return result;
    }
    */

    private boolean mNavBackMode = false;

    public void setNavBackMode() {
        mNavBackMode = true;
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (mDrawerToggle != null) {
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            Drawable backButton = getDrawerToggleDelegate().getThemeUpIndicator();
            backButton = DrawableCompat.wrap(backButton);
            DrawableCompat.setTint(backButton, getResources().getColor(android.R.color.white));
            mDrawerToggle.setHomeAsUpIndicator(backButton);
            mDrawerToggle.syncState();
        }
    }

    public void resetBackMode() {
        mNavBackMode = false;
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mDrawerLayout != null) {
                        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                            mDrawerLayout.closeDrawer(GravityCompat.START);
                        } else {
                            beforeOpenDrawer();
                            mDrawerLayout.openDrawer(GravityCompat.START);
                        }
                    }
                }
            });
        }

        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        }
        if (mDrawerToggle != null) {
            User user = UserCacheManager.getInstance().getCurrentUser();
            mDrawerToggle.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_navigation_menu_default));
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            if (user != null) {
                if (user.getUnreadNotificationsCount() > 0) {
                    mDrawerToggle.setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_navigation_menu));
                    mDrawerToggle.setDrawerIndicatorEnabled(false);
                }
            }


            mDrawerToggle.syncState();
        }
    }

    public void hideDrawerAndBackButton() {
        mNavBackMode = false;
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            mToolbar.setNavigationIcon(android.R.color.transparent);
        }

        if (mDrawerLayout != null) {
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
        if (mDrawerToggle != null) {
            mDrawerToggle.setHomeAsUpIndicator(android.R.color.transparent);
            mDrawerToggle.setDrawerIndicatorEnabled(false);
            mDrawerToggle.syncState();
        }
    }

    protected void beforeOpenDrawer() {

    }

}
