package com.doisdoissete.moovup.ui.login;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.CommercialActivityCacheManager;
import com.doisdoissete.moovup.service.model.CommercialActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.CommercialActivityResponse;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.login.adapter.CommercialActivityViewHolder;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.parse.ParseInstallation;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import fr.ganfra.materialspinner.MaterialSpinner;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import uk.co.ribot.easyadapter.EasyAdapter;

/**
 * Created by broto on 9/18/15.
 */
@RuntimePermissions
public class SignUpCompanyFragment extends SignUpFragment {

    @Select(defaultSelection = 0, messageResId = R.string.required_field)
    @Bind(R.id.spinner_profile_job)
    public MaterialSpinner spinnerCommercialActivity;

    @Bind(R.id.txt_profile_foudation_date)
    public EditText txtFoundationDate;

    public SignUpCompanyFragment() {
    }

    public static SignUpCompanyFragment newInstance(String email, String password) {
        SignUpCompanyFragment fragment = new SignUpCompanyFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_EMAIL, email);
        bundle.putString(EXTRA_PASSWORD, password);

        fragment.setArguments(bundle);
        return fragment;
    }

    public static SignUpCompanyFragment newInstance(boolean editProfile, boolean profileIncomplete) {
        SignUpCompanyFragment fragment = new SignUpCompanyFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_EDIT_MODE, editProfile);
        bundle.putBoolean(EXTRA_PROFILE_INCOMPLETE, profileIncomplete);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile_input_company;
    }

    @Override
    protected void onPhotoClicked() {
        SignUpCompanyFragmentPermissionsDispatcher.showMediaTypeChooserWithCheck(this);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    protected void showMediaTypeChooser() {
        super.showMediaTypeChooserChecked();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDateField();
        initCommercialActivities();
    }

    @Override
    protected void populateUserInfoForEdit(User user) {
        txtName.setText(user.getFirstName());
        txtEmail.setVisibility(View.GONE);
        if (user.getBirthdayDate() != null) {
            txtFoundationDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(user.getBirthdayDate()));
        }

        if (user.getCommercialActivity() != null) {
            //find position
            int pos = 0;
            for (int i = 1; i < spinnerCommercialActivity.getAdapter().getCount(); i++) {
                CommercialActivity CommercialActivity = (CommercialActivity) spinnerCommercialActivity.getAdapter().getItem(i);
                if (CommercialActivity.getId() == user.getCommercialActivity().getId()) {
                    pos = i;
                    break;
                }
            }
            spinnerCommercialActivity.setSelection(pos);
        }
    }

    private void initDateField() {
        super.initDateField(txtFoundationDate, Calendar.getInstance().getTime());
    }

    @Override
    public void signUpUpdate() {
        showLoadingView();
        String email = txtEmail.getText().toString();
        String name = txtName.getText().toString();
        long commercialActivityId = -1;
        if (spinnerCommercialActivity.getSelectedItemPosition() > 0) {
            CommercialActivity commercialActivity = (CommercialActivity) spinnerCommercialActivity.getAdapter().getItem(spinnerCommercialActivity.getSelectedItemPosition());
            commercialActivityId = commercialActivity.getId();
        }
        String foundationDate = txtFoundationDate.getText().toString();

        if (mEdit) {
            update(email, name, foundationDate, commercialActivityId, filePath);
        } else {
            signUp(email, password, name, foundationDate, commercialActivityId, filePath);
        }
    }

    public void update(String email, String name, String foundationDate, long commercialActivityId, String photoPath) {
        RetrofitManager.getInstance().getUserService().updateCompany(
                name,
                email,
                foundationDate,
                commercialActivityId,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processResult(responseResult);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
    }

    public void signUp(String email, String password, String name, String foundationDate, long commercialActivityId, String photoPath) {
        TypedFile photo = null;
        if (!TextUtils.isEmpty(photoPath)) {
            photo = new TypedFile("image/jpeg", new File(photoPath));
        }
        RetrofitManager.getInstance().getUserService().signUpCompany(
                name,
                email,
                password,
                password,
                foundationDate,
                commercialActivityId,
                User.COMPANY_PROFILE_TYPE,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                RetrofitManager.PROVIDER,
                photo,
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processResult(responseResult);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
    }

    private void initCommercialActivities() {

        if (CategoryCacheManager.getInstance().count() <= 0) {
            spinnerCommercialActivity.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    renderCommercialActivities();
                    hideKeyboard();
                    return false;
                }
            });
        } else {
            spinnerCommercialActivity.setOnTouchListener(focusListener);
            renderCommercialActivities();
        }
    }

    private void renderCommercialActivities() {
        if (spinnerCommercialActivity.getAdapter().getCount() <= 1) {
            if (CommercialActivityCacheManager.getInstance().count() <= 0) {
                spinnerCommercialActivity.setEnabled(false);
                refreshCommercialActivities();
            } else {
                renderCommercialActivities(CommercialActivityCacheManager.getInstance().getAll());
            }
        }
    }

    private void refreshCommercialActivities() {
        showLoadingView();

        RetrofitManager.getInstance().getCommercialActivityService().getAllCommercialActivities(new Callback<CommercialActivityResponse>() {
            @Override
            public void success(CommercialActivityResponse commercialActivityResponse, Response response) {
                List<CommercialActivity> result = new ArrayList<>();
                if (commercialActivityResponse != null && commercialActivityResponse.getCommercialActivities() != null) {
                    CommercialActivityCacheManager.getInstance().putAll(commercialActivityResponse.getCommercialActivities());
                }
                renderCommercialActivities(result);

                spinnerCommercialActivity.setEnabled(true);
                spinnerCommercialActivity.performClick();
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, true);
                spinnerCommercialActivity.setEnabled(true);
            }
        });

    }

    public void renderCommercialActivities(List<CommercialActivity> commercialActivities) {
        configSpinnerError(spinnerCommercialActivity);

        spinnerCommercialActivity.setAdapter(new EasyAdapter(getActivity(),
                CommercialActivityViewHolder.class,
                commercialActivities,
                this));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        // delegate the permission handling to generated method
        SignUpCompanyFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

}