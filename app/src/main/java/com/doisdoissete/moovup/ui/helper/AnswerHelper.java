package com.doisdoissete.moovup.ui.helper;

import android.text.TextUtils;

import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.util.NumberUtil;

/**
 * Created by ranipieper on 12/17/15.
 */
public class AnswerHelper {

    public static String getImageUrl(Answers answer) {
        String imageUrl = null;
        if (answer.isHasUploadedImage() && answer.getImagesUrl() != null) {
            if (!TextUtils.isEmpty(answer.getImagesUrl().getMedium())) {
                imageUrl = answer.getImagesUrl().getMedium();
            } else if (!TextUtils.isEmpty(answer.getImagesUrl().getThumb())) {
                imageUrl = answer.getImagesUrl().getThumb();
            }
        }
        return imageUrl;
    }

    public static String getAnswersHelped(Answers question) {
        return NumberUtil.getNumberFormated(question.getUpvotesCount());
    }
}
