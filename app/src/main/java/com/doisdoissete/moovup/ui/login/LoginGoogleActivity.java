package com.doisdoissete.moovup.ui.login;

import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.R;
import com.google.android.gms.auth.GoogleAuthException;
import com.google.android.gms.auth.GoogleAuthUtil;
import com.google.android.gms.auth.GooglePlayServicesAvailabilityException;
import com.google.android.gms.auth.UserRecoverableAuthException;
import com.google.android.gms.common.AccountPicker;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;

import java.io.IOException;

/**
 * Created by broto on 9/29/15.
 */
public class LoginGoogleActivity extends Activity {

    public static final int GOOGLE_PLUS_RESULT = 4569;
    public static final String PARAM_GOOGLE_PLUS_TOKEN = "param_google_plus_token";

    static final int REQUEST_CODE_PICK_ACCOUNT = 1000;
    static final int REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR = 1001;

    String mEmail; // Received from newChooseAccountIntent(); passed to getToken()

//    private final static String mScopes
//            = "oauth2:" + Scopes.PLUS_LOGIN + " " + Scopes.PROFILE + " " + Scopes.EMAIL;

    private final static String mScopes
            = "oauth2:" + Scopes.PLUS_LOGIN + " " + "https://www.googleapis.com/auth/plus.profile.emails.read";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_plus_login);

        getUsername();
    }

    private void pickUserAccount() {
        String[] accountTypes = new String[]{"com.google"};
        Intent intent = AccountPicker.newChooseAccountIntent(null, null,
                accountTypes, false, null, null, null, null);
        startActivityForResult(intent, REQUEST_CODE_PICK_ACCOUNT);
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    /**
     * Attempts to retrieve the username.
     * If the account is not yet known, invoke the picker. Once the account is known,
     * start an instance of the AsyncTask to get the auth token and do work with it.
     */
    private void getUsername() {
        if (mEmail == null) {
            pickUserAccount();
        } else {
            if (isNetworkConnected()) {
                new GetUsernameTask(LoginGoogleActivity.this, mEmail, mScopes).execute();
            } else {
                Toast.makeText(this, "No Internet Connection Available!", Toast.LENGTH_LONG).show();
            }
        }
    }

    public class GetUsernameTask extends AsyncTask<Void, Void, String> {
        LoginGoogleActivity mActivity;
        String mScope;
        String mEmail;

        GetUsernameTask(LoginGoogleActivity activity, String name, String scope) {
            this.mActivity = activity;
            this.mScope = scope;
            this.mEmail = name;
        }

        /**
         * Executes the asynchronous job. This runs when you call execute()
         * on the AsyncTask instance.
         */
        @Override
        protected String doInBackground(Void[] params) {
            try {
                final String token = fetchToken();
                if (token != null) {
                    Intent intent = new Intent();
                    intent.putExtra(PARAM_GOOGLE_PLUS_TOKEN, token);
                    setResult(GOOGLE_PLUS_RESULT, intent);

                    return token;
                }
            } catch (IOException e) {
                setResult(GOOGLE_PLUS_RESULT, null);
            }
            return null;
        }

        @Override
        protected void onPostExecute(String token) {
            super.onPostExecute(token);

            if (token != null && !token.isEmpty())
                finish();
        }

        /**
         * Gets an authentication token from Google and handles any
         * GoogleAuthException that may occur.
         */
        protected String fetchToken() throws IOException {
            try {
                return GoogleAuthUtil.getToken(mActivity, mEmail, mScope);
            } catch (UserRecoverableAuthException userRecoverableException) {
                // GooglePlayServices.apk is either old, disabled, or not present
                // so we need to show the user some UI in the activity to recover.
                mActivity.handleException(userRecoverableException);
            } catch (GoogleAuthException fatalException) {
                // Some other type of unrecoverable exception has occurred.
                // Report and log the error as appropriate for your app.

                Crashlytics.log(Log.ERROR, "Google+ Login", fatalException.getMessage());
                setResult(GOOGLE_PLUS_RESULT, null);
            }
            return null;
        }
    }

    /**
     * This method is a hook for background threads and async tasks that need to
     * provide the user a response UI when an exception occurs.
     */
    public void handleException(final Exception e) {
        // Because this call comes from the AsyncTask, we must ensure that the following
        // code instead executes on the UI thread.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (e instanceof GooglePlayServicesAvailabilityException) {
                    // The Google Play services APK is old, disabled, or not present.
                    // Show a dialog created by Google Play services that allows
                    // the user to update the APK
                    int statusCode = ((GooglePlayServicesAvailabilityException) e)
                            .getConnectionStatusCode();
                    Dialog dialog = GooglePlayServicesUtil.getErrorDialog(statusCode,
                            LoginGoogleActivity.this,
                            REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                    dialog.show();
                } else if (e instanceof UserRecoverableAuthException) {
                    // Unable to authenticate, such as when the user has not yet granted
                    // the app access to the account, but the user can fix this.
                    // Forward the user to an activity in Google Play services.
                    Intent intent = ((UserRecoverableAuthException) e).getIntent();
                    startActivityForResult(intent,
                            REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case REQUEST_CODE_PICK_ACCOUNT:
                // Receiving a result from the AccountPicker
                if (resultCode == RESULT_OK) {
                    mEmail = data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    // With the account name acquired, go get the auth token
                    getUsername();
                } else if (resultCode == RESULT_CANCELED) {
                    setResult(GOOGLE_PLUS_RESULT, null);
                    finish();
                }

                break;

            case REQUEST_CODE_RECOVER_FROM_PLAY_SERVICES_ERROR:
                if (resultCode == RESULT_CANCELED) {
                    setResult(GOOGLE_PLUS_RESULT, null);
                    finish();
                } else {
                    getUsername();
                }

                break;

            default:
                break;
        }
    }
}
