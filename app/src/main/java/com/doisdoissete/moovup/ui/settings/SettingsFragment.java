package com.doisdoissete.moovup.ui.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.background.SavePreferencesService;
import com.doisdoissete.moovup.service.model.Preferences;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.helper.PreferencesHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.settings.adapter.SettingsAdapter;
import com.doisdoissete.moovup.ui.settings.adapter.SettingsViewHolder;
import com.doisdoissete.moovup.util.FunctionsUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderDecoration;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/16/15.
 */
public class SettingsFragment extends BaseFragment implements SettingsViewHolder.SettingsViewHolderListener {

    @Bind(R.id.recycler_settings)
    public RecyclerView mRecyclerView;

    private SettingsAdapter mAdapter;

    private boolean changeSettigns;

    public static SettingsFragment newInstance() {
        Bundle args = new Bundle();
        SettingsFragment fragment = new SettingsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadSettings();
    }

    private void loadSettings() {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().getUserPreferences(new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                renderSettings(user);
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        loadSettings();
                    }

                    @Override
                    public void negativeButton() {
                    }
                });
            }
        });
    }

    private void renderSettings(User user) {
        if (!isAdded()) {
            return;
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        List<SettingsViewHolder.SettingsModel> lstSettigns = new ArrayList<>();
        SettingsViewHolder.SettingsModel editProfile = new SettingsViewHolder.SettingsModel(getString(R.string.edit_profile), new SettingsViewHolder.SettingsListener() {
            @Override
            public void onSettingsListener(SettingsViewHolder.SettingsModel settings) {
                Navigator.navigateToEditProfileFragment(getContext());
            }
        });

        SettingsViewHolder.SettingsModel answersOptions = new SettingsViewHolder.SettingsModel(getString(R.string.answers_options), new SettingsViewHolder.SettingsListener() {
            @Override
            public void onSettingsListener(SettingsViewHolder.SettingsModel settings) {
                Navigator.navigateToInterestFragment(getContext());
            }
        });

        //lstSettigns.add(editProfile);
        //lstSettigns.add(answersOptions);

        HashMap<String, Boolean> hashMapPref = PreferencesHelper.getPreferences(user.getPreferences());

        if (hashMapPref != null) {
            for (String key : hashMapPref.keySet()) {
                SettingsViewHolder.SettingsModel notificationPref = new SettingsViewHolder.SettingsModel(key, FunctionsUtil.getStringResourceByName(getActivity(), key), hashMapPref.get(key), SettingsAdapter.NOTIFICATIONS_HEADER);
                lstSettigns.add(notificationPref);
            }
        }

        SettingsViewHolder.SettingsModel about = new SettingsViewHolder.SettingsModel(getString(R.string.about_moovup), new SettingsViewHolder.SettingsListener() {
            @Override
            public void onSettingsListener(SettingsViewHolder.SettingsModel settings) {
                Navigator.navigateToAboutMoovupFragment(getContext());
            }
        }, SettingsAdapter.EMPTY_HEADER);

        SettingsViewHolder.SettingsModel terms = new SettingsViewHolder.SettingsModel(getString(R.string.terms_moovup), new SettingsViewHolder.SettingsListener() {
            @Override
            public void onSettingsListener(SettingsViewHolder.SettingsModel settings) {
                Navigator.navigateToTermsActivity(getContext(), false);
            }
        }, SettingsAdapter.EMPTY_HEADER);

        final SettingsViewHolder.SettingsModel deleteAccount = new SettingsViewHolder.SettingsModel(getString(R.string.delete_account), new SettingsViewHolder.SettingsListener() {
            @Override
            public void onSettingsListener(SettingsViewHolder.SettingsModel settings) {
                ViewUtil.showConfirmDialog(getContext(), R.string.title_delete_account, R.string.confirm_delete_account, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        deleteAccount();
                    }
                });
            }
        }, SettingsAdapter.EMPTY_HEADER);

        lstSettigns.add(about);
        lstSettigns.add(terms);
        lstSettigns.add(deleteAccount);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mAdapter = new SettingsAdapter(
                getActivity(),
                SettingsViewHolder.class,
                lstSettigns,
                this);

        mRecyclerView.setAdapter(mAdapter);

        StickyHeaderDecoration mStickyHeaderDecoration = new StickyHeaderDecoration(mAdapter);
        mRecyclerView.addItemDecoration(mStickyHeaderDecoration);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onSettingsCheckedChange(SettingsViewHolder.SettingsModel settings, boolean checked) {
        changeSettigns = true;
        settings.setValueSwitch(checked);
    }

    @Override
    public void onSettingsSelected(SettingsViewHolder.SettingsModel settings) {
        if (settings != null && settings.getListener() != null) {
            settings.getListener().onSettingsListener(settings);
        }
    }

    @Override
    public void onDestroy() {
        if (changeSettigns) {
            saveChanges();
        }
        super.onDestroy();
    }

    private void saveChanges() {
        Map<String, Boolean> hashSettings = new HashMap<>();

        for(int i =0; i < mAdapter.getItems().size(); i++) {
            SettingsViewHolder.SettingsModel settingsModel = (SettingsViewHolder.SettingsModel)mAdapter.getItem(i);
            if (!TextUtils.isEmpty(settingsModel.getSettingsId())) {
                hashSettings.put(settingsModel.getSettingsId(), settingsModel.isValueSwitch());
            }
        }

        Preferences preferences = PreferencesHelper.getPreferences(hashSettings);

        SavePreferencesService.callService(getActivity(), preferences);

    }

    private void deleteAccount() {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().deleteAccount(new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                logout(false);
                showSuccessDialog(getContext(), R.string.account_deleted, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        logout(true);
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_settings;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.settings);
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
