package com.doisdoissete.moovup.ui.custom;

import com.nostra13.universalimageloader.core.ImageLoader;

public class UserImageLoader extends ImageLoader {

    private volatile static UserImageLoader instance;

    /** Returns singletone class instance */
    public static UserImageLoader getInstance() {
        if (instance == null) {
            synchronized (ImageLoader.class) {
                if (instance == null) {
                    instance = new UserImageLoader();
                }
            }
        }
        return instance;
    }
}