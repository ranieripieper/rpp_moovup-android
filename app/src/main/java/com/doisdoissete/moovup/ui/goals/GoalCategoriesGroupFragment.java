package com.doisdoissete.moovup.ui.goals;

import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.goals.adapter.CategoryViewHolder;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.List;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyParallaxRecyclerAdapter;

/**
 * Created by ranipieper on 11/17/15.
 */
public class GoalCategoriesGroupFragment extends BaseFragment implements CategoryViewHolder.CategoryHolderListener {

    @Bind(R.id.layout_goal_categories_recycler_view)
    public RecyclerView mRecyclerView;

    private List<Category> mCategories;

    public static GoalCategoriesGroupFragment newInstance() {
        return newInstance(false);
    }

    public static GoalCategoriesGroupFragment newInstance(boolean skip) {
        Bundle args = new Bundle();

        GoalCategoriesGroupFragment fragment = new GoalCategoriesGroupFragment();
        fragment.setArguments(args);
        //TODO skip
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mCategories = CategoryCacheManager.getInstance().getParentCategoryGoals();
        if (mCategories == null) {
            getCategories();
        } else {
            renderCategories(mCategories);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_categories_group;
    }

    private void getCategories() {
        showLoadingView();
        RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE, new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse categoryResponse, Response response) {
                if (categoryResponse != null) {
                    mCategories = categoryResponse.getCategories();
                    CategoryCacheManager.getInstance().updateWithoutNulls(categoryResponse.getCategories());
                    renderCategories(CategoryCacheManager.getInstance().getParentCategoryGoals());
                }
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void renderCategories(List<Category> categoryList) {

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        EasyParallaxRecyclerAdapter adapter = new EasyParallaxRecyclerAdapter(
        getActivity(),
                CategoryViewHolder.class,
                categoryList,
                this);

        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.include_header_goal_categories, mRecyclerView, false);

        TextView txtView = (TextView)header.findViewById(R.id.bt_skip_add_goal);
        txtView.setPaintFlags(txtView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        txtView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipAddGoal();
            }
        });
        adapter.setParallaxHeader(header, mRecyclerView);

        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void skipAddGoal() {
    }

    @Override
    public void onCategoryClicked(Category category) {
        Navigator.navigateToGoalCategoriesFragmentFragment(getActivity(), category, category.getChildren());
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
