package com.doisdoissete.moovup.ui.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.settings.adapter.AboutMoovupViewHolder;

import java.util.Arrays;

import butterknife.Bind;
import uk.co.ribot.easyadapter.EasyParallaxRecyclerAdapter;

/**
 * Created by ranipieper on 1/8/16.
 */
public class AboutMoovupFragment extends BaseFragment {

    @Bind(R.id.recycler_about)
    public RecyclerView mRecyclerView;

    public static AboutMoovupFragment newInstance() {
        Bundle args = new Bundle();

        AboutMoovupFragment fragment = new AboutMoovupFragment();
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        EasyParallaxRecyclerAdapter adapter = new EasyParallaxRecyclerAdapter<>(
                getActivity(),
                AboutMoovupViewHolder.class,
                Arrays.asList(getString(R.string.about_text)),
                this);

        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.include_header_about, mRecyclerView, false);

        TextView txtVersion = (TextView)header.findViewById(R.id.txt_header);
        txtVersion.setText(getString(R.string.txt_version, BuildConfig.VERSION_NAME));
        adapter.setParallaxHeader(header, mRecyclerView);

        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_config_about;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }


    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
