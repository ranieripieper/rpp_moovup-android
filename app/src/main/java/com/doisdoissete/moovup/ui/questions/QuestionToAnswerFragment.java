package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.QuestionsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.questions.adapter.QuestionToAnswerViewHolder;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 12/8/15.
 */
public class QuestionToAnswerFragment extends FeedQuestionFragment implements QuestionToAnswerViewHolder.QuestionToAnswerListener {

    public static QuestionToAnswerFragment newInstance() {
        return new QuestionToAnswerFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtNoResults.setText(R.string.no_question_to_answer);
        mFabAddQuestion.setVisibility(View.GONE);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.to_answer);
    }

    protected void callService() {

        RetrofitManager.getInstance().getQuestionService().getQuestionsToAnswer(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<QuestionsResponse>() {
            @Override
            public void success(QuestionsResponse questionsResponse, Response response) {
                if (isAdded()) {
                    processResponse(questionsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void createAdapter(List<Question> questionList) {
        questionAdapter = new EasyLoadMoreRecyclerAdapter<>(
                getActivity(),
                QuestionToAnswerViewHolder.class,
                questionList,
                this,
                R.layout.loading_recycler_view);
        mRecyclerView.setAdapter(questionAdapter);
    }

    @Override
    public void onAnswerClick(Question question) {
        hideLoadingView();
        Navigator.navigateToAnswerQuestionFragment(getActivity(), this, question);
    }

    @Override
    public void onNoAnswerClick(final Question question) {
        ViewUtil.showConfirmDialog(getActivity(), R.string.title_dialog_default, R.string.msg_no_answer_question, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                showLoadingView();
                RetrofitManager.getInstance().getQuestionService().ignoreQuestion(question.getId(), "", new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                        hideLoadingView();
                        if (baseResponse != null && baseResponse.isSuccess()) {
                            questionAdapter.removeItem(question);
                        } else {
                            showErrorDialog(R.string.error_generic);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });

            }
        });
    }

    protected boolean showAddQuestion() {
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_ANSWER_QUESTION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AnswerQuestionFragment.EXTRA_QUESTION_ANSWERED)) {
                    long questionId = data.getLongExtra(AnswerQuestionFragment.EXTRA_QUESTION_ANSWERED, -1);
                    if (questionAdapter != null && questionAdapter.getItemCount() > 0 && questionId > 0) {
                        removeQuestion(questionId);
                    }

                    if (questionAdapter.getItemCount() <= 0) {
                        showEmptyQuestions();
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
