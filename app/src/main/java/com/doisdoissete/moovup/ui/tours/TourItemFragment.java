package com.doisdoissete.moovup.ui.tours;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.util.SharedPrefManager;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 11/12/15.
 */
public class TourItemFragment extends BaseFragment {

    @Bind(R.id.bt_skip_tour)
    public View mViewSkipTour;

    @Bind(R.id.img_tour)
    public ImageView mImgTour;

    @Bind(R.id.txt_info_screen_1)
    public TextView mLblInfoScreen1;

    @Bind(R.id.txt_info_screen_2)
    public TextView mLblInfoScreen2;

    @Bind(R.id.img_arrow_left)
    public ImageView mImgArrowLeft;

    @Bind(R.id.img_arrow_right)
    public ImageView mImgArrowRight;

    public int mResImage;
    public boolean mSkipTour;
    public String mInfoScreen1;
    public String mInfoScreen2;
    public boolean mArrowLeft;
    public boolean mArrowRight;


    private PageController mPageController;

    public TourItemFragment() {
    }

    public static TourItemFragment newInstance(int resImage, boolean skipTour, String infoScreen1, String infoScreen2, PageController pageController) {
        return newInstance(resImage, skipTour, infoScreen1, infoScreen2, pageController, true, true);
    }

    public static TourItemFragment newInstance(int resImage, boolean skipTour, String infoScreen1, String infoScreen2, PageController pageController, boolean arrowLeft, boolean arrowRight) {
        TourItemFragment fragment = new TourItemFragment();
        Bundle bundle = new Bundle();
        fragment.mResImage = resImage;
        fragment.mSkipTour = skipTour;
        fragment.mInfoScreen1 = infoScreen1;
        fragment.mInfoScreen2 = infoScreen2;
        fragment.mPageController = pageController;
        fragment.mArrowLeft = arrowLeft;
        fragment.mArrowRight = arrowRight;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mImgTour.setImageResource(mResImage);
        if (mSkipTour) {
            mViewSkipTour.setVisibility(View.VISIBLE);
        } else {
            mViewSkipTour.setVisibility(View.GONE);
        }

        if (TextUtils.isEmpty(mInfoScreen1)) {
            mLblInfoScreen1.setVisibility(View.GONE);
        } else {
            mLblInfoScreen1.setVisibility(View.VISIBLE);
            mLblInfoScreen1.setText(mInfoScreen1);
        }

        if (TextUtils.isEmpty(mInfoScreen2)) {
            mLblInfoScreen2.setVisibility(View.GONE);
        } else {
            mLblInfoScreen2.setVisibility(View.VISIBLE);
            mLblInfoScreen2.setText(mInfoScreen2);
        }

        if (mArrowLeft) {
            mImgArrowLeft.setVisibility(View.VISIBLE);
        } else {
            mImgArrowLeft.setVisibility(View.INVISIBLE);
        }

        if (mArrowRight) {
            mImgArrowRight.setVisibility(View.VISIBLE);
        } else {
            mImgArrowRight.setVisibility(View.INVISIBLE);
        }
    }


    @OnClick(R.id.bt_skip_tour)
    public void onSkipTourClicked(View v) {
        User user = UserCacheManager.getInstance().getCurrentUser();
        SharedPrefManager.getInstance().setShowTour(user.getEmail(), false);
        Navigator.navigateToHome(getContext());
    }

    @OnClick(R.id.img_arrow_right)
    void arrowRightClick() {
        if (mPageController != null) {
            mPageController.nextPage();
        }
    }

    @OnClick(R.id.img_arrow_left)
    void arrowLeftClick() {
        if (mPageController != null) {
            mPageController.previousPage();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_item_tour;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.NOTHING;
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }
}
