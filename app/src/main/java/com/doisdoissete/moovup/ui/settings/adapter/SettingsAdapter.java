package com.doisdoissete.moovup.ui.settings.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.doisdoissete.moovup.R;

import java.util.List;

import ca.barrenechea.widget.recyclerview.decoration.StickyHeaderAdapter;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.ItemViewHolder;

/**
 * Created by ranipieper on 2/19/16.
 */
public class SettingsAdapter<T> extends EasyLoadMoreRecyclerAdapter<T> implements StickyHeaderAdapter<SettingsAdapter.HeaderHolder> {

    public static int NO_HEADER = 0;
    public static int NOTIFICATIONS_HEADER = 1;
    public static int EMPTY_HEADER = 2;

    @Override
    public SettingsAdapter.HeaderHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
        final View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.header_row_settings, viewGroup, false);
        return new HeaderHolder(view);
    }

    @Override
    public void onBindHeaderViewHolder(SettingsAdapter.HeaderHolder headerHolder, int position) {
        SettingsViewHolder.SettingsModel obj = (SettingsViewHolder.SettingsModel) getItem(position);

        long headerId = getHeaderId(position);
        if (headerId == NO_HEADER) {
            headerHolder.mLblHeader.setVisibility(View.GONE);
        } else if (headerId == NOTIFICATIONS_HEADER) {
            headerHolder.mLblHeader.setText(R.string.notifications);
            headerHolder.mLblHeader.setVisibility(View.VISIBLE);
        } else {
            headerHolder.mLblHeader.setVisibility(View.VISIBLE);
            headerHolder.mLblHeader.setText("");
        }
    }

    @Override
    public long getHeaderId(int position) {
        SettingsViewHolder.SettingsModel obj = (SettingsViewHolder.SettingsModel) getItem(position);
        return obj.getHeaderId();
    }

    /**
     * Header View Holder.
     */
    protected class HeaderHolder extends RecyclerView.ViewHolder {

        public TextView mLblHeader;

        /**
         * Constructor with the View.
         *
         * @param view view object
         */
        public HeaderHolder(View view) {
            super(view);
            mLblHeader = (TextView) view.findViewById(R.id.lbl_header);
        }
    }

    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener) {
        super(context, itemViewHolderClass, listItems, listener);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, and list of items.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     */
    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context and an {@link ItemViewHolder} class.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     */
    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listItems           the list of items to load in the Adapter
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class, list of items and a generic listener.
     *
     * @param context
     * @param itemViewHolderClass
     * @param listItems
     * @param listener
     * @param itemLayoutLoadMoreId
     * @param header
     */
    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, List<T> listItems, Object listener, Integer itemLayoutLoadMoreId, View header) {
        super(context, itemViewHolderClass, listItems, listener, itemLayoutLoadMoreId, header);
    }

    /**
     * Constructs and EasyAdapter with a Context, an {@link ItemViewHolder} class and a generic listener.
     *
     * @param context             a valid Context
     * @param itemViewHolderClass your {@link ItemViewHolder} implementation class
     * @param listener            a generic object that you can access from your {@link ItemViewHolder} by calling
     *                            {@link ItemViewHolder#getListener()}, This can be used to pass a listener to the view holder that then you
     *                            can cast and use as a callback.
     */
    public SettingsAdapter(Context context, Class<? extends ItemViewHolder> itemViewHolderClass, Object listener, Integer itemLayoutLoadMoreId) {
        super(context, itemViewHolderClass, listener, itemLayoutLoadMoreId);
    }

}
