package com.doisdoissete.moovup.ui.friends;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.FriendsResponse;
import com.doisdoissete.moovup.service.model.response.FriendshipResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.friends.adapter.PendingFriendsViewHolder;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;

/**
 * Created by ranipieper on 11/26/15.
 */
public class PendingFriendsFragment extends MyFriendsFragment implements PendingFriendsViewHolder.PendingFriendsHolderListener {

    public static PendingFriendsFragment newInstance() {
        Bundle args = new Bundle();
        PendingFriendsFragment fragment = new PendingFriendsFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtNoResults.setText(R.string.no_friends_request_yet);
    }

    protected void callService() {
        RetrofitManager.getInstance().getFriendsService().getPendingFriends(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<FriendsResponse>() {
            @Override
            public void success(FriendsResponse friendsResponse, Response response) {
                if (isAdded()) {
                    processResponse(friendsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void createAdapter(List<User> list) {
        mFriendsAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                PendingFriendsViewHolder.class,
                list,
                this,
                R.layout.loading_recycler_view);

        mFriendsAdapter.notifyDataSetChanged();

        mRecyclerView.setAdapter(mFriendsAdapter);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.pending_friendship);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    @Override
    public void onUserClicked(User user) {
        super.onUserClicked(user);
    }

    @Override
    public void onAcceptClicked(final User user) {
        showLoadingView();
        RetrofitManager.getInstance().getFriendsService().acceptFriendship(user.getId(), "", new Callback<FriendshipResponse>() {
            @Override
            public void success(FriendshipResponse friendshipResponse, Response response) {
                mFriendsAdapter.removeItem(user);
                hideLoadingView();
                Navigator.navigateToUserGoalsFragment(getActivity(), PendingFriendsFragment.this, user.getId());
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    @Override
    public void onRecuseClicked(final User user) {
        showLoadingView();
        RetrofitManager.getInstance().getFriendsService().declineFriendship(user.getId(), new Callback<FriendshipResponse>() {
            @Override
            public void success(FriendshipResponse friendshipResponse, Response response) {
                mFriendsAdapter.removeItem(user);
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }
}
