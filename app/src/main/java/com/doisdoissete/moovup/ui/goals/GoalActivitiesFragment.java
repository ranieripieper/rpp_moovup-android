package com.doisdoissete.moovup.ui.goals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.GoalActivityCacheManager;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.GoalActivitiesResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.goals.adapter.GoalActivitiesViewHolder;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalActivitiesFragment extends BaseFragment implements GoalActivitiesViewHolder.GoalActivitiesViewHolderListener {

    private Goal mGoal;

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_goal_activities)
    protected EasyLoadMoreRecyclerView mRecyclerView;

    @Bind(R.id.txt_no_results)
    protected TextView txtNoResults;

    protected EasyLoadMoreRecyclerAdapter<GoalActivity> mGoalActivitiesAdapter;

    protected Integer mPage = 1;

    @Bind(R.id.fab_add_goal_activity)
    protected FloatingActionButton fabAddGoalactivity;

    private boolean mIsGoalCurrentUser = false;

    protected GoalDetailFragment mGoalDetailFragment;

    public static GoalActivitiesFragment newInstance(Goal goal, GoalDetailFragment goalDetailFragment) {
        GoalActivitiesFragment fragment = new GoalActivitiesFragment();
        Bundle bundle = new Bundle();
        fragment.mGoal = goal;
        fragment.mGoalDetailFragment = goalDetailFragment;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
        User user = getCurrentUser();
        if (mGoal == null) {
            Crashlytics.log("GoalActivitiesFragment - goal é null " + user);
            finish();
        } else {
            if (user != null &&
                    ((mGoal.getUserId() != null && user.getId() == mGoal.getUserId()) ||
                            (mGoal.getUser() != null && user.getId() == mGoal.getUser().getId()))) {
                mIsGoalCurrentUser = true;
            } else {
                mIsGoalCurrentUser = false;
            }
        }

    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initSwipeRefresh();

        if (mIsGoalCurrentUser) {
            fabAddGoalactivity.setVisibility(View.VISIBLE);
            txtNoResults.setText(R.string.no_activites_yet);
        } else {
            fabAddGoalactivity.setVisibility(View.GONE);
            txtNoResults.setText(R.string.no_activites_yet_friend);
        }
    }

    private void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });

        loadData();
    }

    private void loadData() {
        if (mPage == 1) {
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    mPage++;
                    loadData();
                }
            });
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
            showLoadingView();
            if (mGoalActivitiesAdapter != null) {
                mGoalActivitiesAdapter.removeAllItems();
            }
        }
        callService();
    }

    protected void callService() {
        if (mIsGoalCurrentUser) {
            RetrofitManager.getInstance().getGoalService().getMyGoalActivities(mGoal.getId(), mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<GoalActivitiesResponse>() {
                @Override
                public void success(GoalActivitiesResponse goalActivitiesResponse, Response response) {
                    if (isAdded()) {
                        processResponse(goalActivitiesResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isAdded()) {
                        showError(error);
                        disableLoadingMore(mRecyclerView);
                    }
                }
            });
        } else {
            Long userId = null;
            if (mGoal.getUserId() != null) {
                userId = mGoal.getUserId();
            } else if (mGoal.getUser() != null && mGoal.getUser().getId() > 0) {
                userId =mGoal.getUser().getId();
            }

            RetrofitManager.getInstance().getGoalService().getGoalActivities(userId, mGoal.getId(), mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<GoalActivitiesResponse>() {
                @Override
                public void success(GoalActivitiesResponse goalActivitiesResponse, Response response) {
                    if (isAdded()) {
                        processResponse(goalActivitiesResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isAdded()) {
                        showError(error);
                        disableLoadingMore(mRecyclerView);
                    }
                }
            });
        }

    }

    protected void processResponse(GoalActivitiesResponse goalActivitiesResponse) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if (goalActivitiesResponse.getGoalActivities() != null && goalActivitiesResponse.getGoalActivities().size() > 0) {
            disableLoadingMore(mRecyclerView, goalActivitiesResponse.getMeta(), goalActivitiesResponse.getGoalActivities().size());
            renderGoalActivites(goalActivitiesResponse.getGoalActivities());
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
        } else {
            showEmptyQuestions();
        }
    }

    public void renderGoalActivites(List<GoalActivity> goalActivityList) {
        if (!isAdded()) {
            return;
        }
        if (mSwipeRefreshLayout != null && mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        //seta a goal
        if (goalActivityList != null) {
            for (GoalActivity goalActivity : goalActivityList) {
                goalActivity.setGoal(mGoal);
            }
        }

        if (mGoalActivitiesAdapter == null) {
            createAdapter(goalActivityList);
        } else {
            mGoalActivitiesAdapter.addItems(goalActivityList);
        }
    }

    protected void createAdapter(List<GoalActivity> goalActivityList) {
        mGoalActivitiesAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                GoalActivitiesViewHolder.class,
                goalActivityList,
                this,
                R.layout.loading_recycler_view);


        mRecyclerView.setAdapter(mGoalActivitiesAdapter);
    }

    @OnClick(R.id.fab_add_goal_activity)
    public void addGoalActivityClick() {
        Navigator.navigateToAddGoalActivityFragment(getContext(), this, mGoal);
    }

    private void showEmptyQuestions() {
        disableLoadingMore(mRecyclerView);
        if (mPage <= 1) {
            txtNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
        txtNoResults.setVisibility(View.GONE);
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    private void addGoalActivity(GoalActivity goalActivity) {
        txtNoResults.setVisibility(View.GONE);
        goalActivity.setGoal(mGoal);
        if (mGoalActivitiesAdapter == null) {
            createAdapter(new ArrayList(Arrays.asList(goalActivity)));
        } else {
            mGoalActivitiesAdapter.addItem(goalActivity);
        }
        if (mGoalDetailFragment != null) {
            mGoalDetailFragment.goalActivityAdded(goalActivity);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_ADD_GOAL_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AddGoalActivityFragment.EXTRA_GOAL_ACTIVITY)) {
                    Long golaActivityId = data.getLongExtra(AddGoalActivityFragment.EXTRA_GOAL_ACTIVITY, -1);
                    if (golaActivityId != null && golaActivityId >= 0) {
                        GoalActivity goalActivity = GoalActivityCacheManager.getInstance().getCopy(golaActivityId);
                        if (goalActivity != null) {
                            addGoalActivity(goalActivity);
                        }
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onUpvotedClicked(final GoalActivity goalActivity) {
        super.showLoadingView();
        if (goalActivity.isLiked()) {
            RetrofitManager.getInstance().getGoalService().downvoteGoalActivity(goalActivity.getId(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateUpvotedGoalActivity(goalActivity, false);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getGoalService().upvoteGoalActivity(goalActivity.getId(), "", new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateUpvotedGoalActivity(goalActivity, true);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }
    }

    private void updateUpvotedGoalActivity(GoalActivity goalActivity, boolean upvouted) {
        if (goalActivity.getUpvotesCount() == null) {
            goalActivity.setUpvotesCount(0l);
        }
        goalActivity.setLiked(upvouted);
        goalActivity.setUpvotesCount(goalActivity.getUpvotesCount() + (upvouted ? 1 : -1));
        if (mGoalActivitiesAdapter != null) {
            mGoalActivitiesAdapter.notifyItemChanged(goalActivity, new EasyLoadMoreRecyclerAdapter.CompareObject<GoalActivity>() {
                @Override
                public boolean isEqual(GoalActivity left, GoalActivity right) {
                    if (left.getId().equals(right.getId())) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    @Override
    public void onGoalActivityClicked(GoalActivity goalActivity) {
        ViewUtil.showDialogViewImage(getActivity(), GoalHelper.getImageGoalActivityUrl(goalActivity), MoovUpApplication.imageLoader);
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_activities;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
