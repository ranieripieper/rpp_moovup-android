package com.doisdoissete.moovup.ui.base;

/**
 * Created by ranieripieper on 11/30/15.
 */
public interface ErrorListener {

    void positiveButton();
    void negativeButton();
}
