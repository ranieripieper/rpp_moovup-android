package com.doisdoissete.moovup.ui.login.adapter;

import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.service.model.Category;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(android.R.layout.simple_spinner_dropdown_item)
public class CategoryViewHolder extends ItemViewHolder<Category> {

    @ViewId(android.R.id.text1)
    TextView text;

    public CategoryViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Category item, PositionInfo positionInfo) {
        text.setText(item.getTitle());
    }

    @Override
    public void onSetListeners() {
    }
}