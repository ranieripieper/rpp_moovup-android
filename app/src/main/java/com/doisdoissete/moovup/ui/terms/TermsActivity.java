package com.doisdoissete.moovup.ui.terms;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.base.BaseActivity;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import butterknife.Bind;
import butterknife.OnClick;

/**
 * Created by ranipieper on 11/12/15.
 */
public class TermsActivity extends BaseActivity {

    private static final String EXTRA_NEED_ACCEPT = "EXTRA_NEED_ACCEPT";

    @Bind(R.id.webview_terms_conditions)
    WebView webView;

    @Bind(R.id.layout_buttons)
    View layoutButtons;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_conditions);

        boolean needAccept = getExtraBoolean(EXTRA_NEED_ACCEPT);

        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.loadUrl("file:///android_asset/terms.html");

        setToolbarTitle(R.string.terms_moovup);

        if (needAccept) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            layoutButtons.setVisibility(View.VISIBLE);
        } else {
            layoutButtons.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_accept)
    void acceptClick() {
        Navigator.navigateToMainActivity(getContext());
        finish();
    }

    @OnClick(R.id.btn_recuse)
    void recuseClick() {
        logout(true);
    }


    public static void startActivity(Context ctx, boolean needAccept) {
        Intent it = new Intent(ctx, TermsActivity.class);
        it.putExtra(EXTRA_NEED_ACCEPT, needAccept);
        ctx.startActivity(it);
    }
}
