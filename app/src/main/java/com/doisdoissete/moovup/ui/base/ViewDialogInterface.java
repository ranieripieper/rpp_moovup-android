package com.doisdoissete.moovup.ui.base;

import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;

import java.util.List;

import retrofit.RetrofitError;

/**
 * Created by ranipieper on 11/12/15.
 */
public interface ViewDialogInterface {


    void showError(RetrofitError error, int defaultError, boolean ignoreUnauthorized);

    void showError(RetrofitError error, int defaultError, ErrorListener errorListener);

    void showError(RetrofitError error, ErrorListener errorListener);

    void showError(RetrofitError error);

    void showError(RetrofitError error, int defaultError);

    void showErrorDialog(List<String> lstMessage);

    void showErrorDialog(String message);

    void showErrorDialog(int message);

    void showSuccessDialog(Context ctx, int message);

    void showSuccessDialog(Context ctx, int message, boolean finish);

    void showSuccessDialog(Context ctx, int message, final MaterialDialog.SingleButtonCallback callback);

    void showSuccessDialog(Context ctx, String message);

    void showToastError(Context ctx, String message);

    void showToastError(Context ctx, RetrofitError error);
}

