package com.doisdoissete.moovup.ui.settings.adapter;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 1/8/16.
 */
@LayoutId(R.layout.row_settings)
public class SettingsViewHolder extends ItemViewHolder<SettingsViewHolder.SettingsModel> {

    @ViewId(R.id.lbl_title)
    TextView lblTitle;

    @ViewId(R.id.swc_check)
    SwitchCompat chkEnabled;

    public SettingsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(final SettingsModel item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitle());
        chkEnabled.setOnCheckedChangeListener(null);
        if (item.hasSwitch()) {
            chkEnabled.setVisibility(View.VISIBLE);
            chkEnabled.setChecked(item.getValueSwitch());
        } else {
            chkEnabled.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSetListeners() {
        getView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsModel item = getItem();
                if (item != null && item.getListener() != null && !item.hasSwitch()) {
                    SettingsViewHolderListener listener = getListener(SettingsViewHolderListener.class);
                    if (listener != null) {
                        listener.onSettingsSelected(item);
                    }
                }
            }
        });

        chkEnabled.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SettingsModel item = getItem();
                if (item != null && item.hasSwitch()) {
                    SettingsViewHolderListener listener = getListener(SettingsViewHolderListener.class);
                    if (listener != null) {
                        listener.onSettingsCheckedChange(item, chkEnabled.isChecked());
                    }
                }
            }
        });
    }


    public interface SettingsViewHolderListener {
        void onSettingsSelected(SettingsModel settings);

        void onSettingsCheckedChange(SettingsModel settings, boolean checked);
    }

    public interface SettingsListener {
        void onSettingsListener(SettingsModel settings);
    }

    public static class SettingsModel {

        private String title;
        private String settingsId;
        private int headerId = SettingsAdapter.NO_HEADER;
        private boolean hasSwitch;
        private boolean valueSwitch;
        private SettingsListener listener;

        public SettingsModel() {
        }

        public SettingsModel(String title, SettingsListener listener) {
            this.title = title;
            this.listener = listener;
            this.hasSwitch = false;
        }

        public SettingsModel(String title, SettingsListener listener, int headerId) {
            this.title = title;
            this.listener = listener;
            this.hasSwitch = false;
            this.headerId = headerId;
        }

        public SettingsModel(String settingsId, String title, SettingsListener listener, boolean valueSwitch, int headerId) {
            this.settingsId = settingsId;
            this.title = title;
            this.listener = listener;
            this.valueSwitch = valueSwitch;
            this.hasSwitch = true;
            this.headerId = headerId;
        }

        public SettingsModel(String settingsId, String title, boolean valueSwitch, int headerId) {
            this.settingsId = settingsId;
            this.title = title;
            this.valueSwitch = valueSwitch;
            this.hasSwitch = true;
            this.headerId = headerId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public boolean hasSwitch() {
            return hasSwitch;
        }

        public void setHasSwitch(boolean hasSwitch) {
            this.hasSwitch = hasSwitch;
        }

        public boolean getValueSwitch() {
            return valueSwitch;
        }

        public void setValueSwitch(boolean valueSwitch) {
            this.valueSwitch = valueSwitch;
        }

        public SettingsListener getListener() {
            return listener;
        }

        public void setListener(SettingsListener listener) {
            this.listener = listener;
        }

        public int getHeaderId() {
            return headerId;
        }

        public String getSettingsId() {
            return settingsId;
        }

        public void setSettingsId(String settingsId) {
            this.settingsId = settingsId;
        }

        public boolean isHasSwitch() {
            return hasSwitch;
        }

        public boolean isValueSwitch() {
            return valueSwitch;
        }

        public void setHeaderId(int headerId) {
            this.headerId = headerId;
        }
    }
}