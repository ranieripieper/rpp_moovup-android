package com.doisdoissete.moovup.ui.notification;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.background.ReadNotificationService;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Notification;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.NotificationsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.notification.adapter.NotificationViewHolder;

import java.util.List;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by broto on 10/27/15.
 */
public class NotificationsFragment extends BaseFragment implements NotificationViewHolder.NotificationViewHolderListener {

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_view)
    protected EasyLoadMoreRecyclerView mRecyclerView;

    @Bind(R.id.txt_no_results)
    protected TextView txtNoResults;

    protected Integer mPage = 1;

    protected EasyLoadMoreRecyclerAdapter<Notification> mAdapter;


    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_notifications;
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSwipeRefresh();
    }

    private void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });

        loadData();
    }

    public void renderQuestion(List<Notification> lst) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mAdapter == null) {
            createAdapter(lst);
        } else {
            mAdapter.addItems(lst);
        }
    }

    protected void createAdapter(List<Notification> lst) {
        mAdapter = new EasyLoadMoreRecyclerAdapter<>(
                getActivity(),
                NotificationViewHolder.class,
                lst,
                this,
                R.layout.loading_recycler_view);
        mRecyclerView.setAdapter(mAdapter);
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
        txtNoResults.setVisibility(View.GONE);
    }

    private void loadData() {
        if (mPage == 1) {
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    mPage++;
                    loadData();
                }
            });
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
            showLoadingView();
            if (mAdapter != null) {
                mAdapter.removeAllItems();
            }
        }
        callService();
    }

    protected void callService() {
        RetrofitManager.getInstance().getNotificationService().getNotifications(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<NotificationsResponse>() {
            @Override
            public void success(NotificationsResponse notificationsResponse, Response response) {
                if (isAdded()) {
                    processResponse(notificationsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void processResponse(NotificationsResponse notificationsResponse) {
        if (!isAdded()) {
            return;
        }
        hideLoadingView();
        hideKeyboard();
        if (notificationsResponse.getNotifications() != null && notificationsResponse.getNotifications().size() > 0) {
            disableLoadingMore(mRecyclerView, notificationsResponse.getMeta(), notificationsResponse.getNotifications().size());
            renderQuestion(notificationsResponse.getNotifications());
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
        } else {
            showEmptyQuestions();
        }
    }

    protected void showEmptyQuestions() {
        disableLoadingMore(mRecyclerView);
        if (mPage <= 1) {
            txtNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onNotificationSelected(Notification notification) {
        if (!notification.isRead()) {
            User user = getCurrentUser(true);
            UserCacheManager.getInstance().updateUnreadNotificationsCount(user.getId(), user.getUnreadNotificationsCount() - 1);
            ReadNotificationService.callService(getActivity(), notification.getId());
            notification.setRead(true);
            mAdapter.notifyItemChanged(notification, new EasyLoadMoreRecyclerAdapter.CompareObject<Notification>() {
                @Override
                public boolean isEqual(Notification left, Notification right) {
                    return left.getId().equals(right.getId());
                }
            });
        }
        Navigator.navigateToFragment(getContext(), notification, false);
        hideKeyboard();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.notifications);
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();

        if (mAdapter != null) {
            if (mAdapter.getItemCount() <= 0) {
                mPage = 1;
                showEmptyQuestions();
            }
            mAdapter.notifyDataSetChanged();
        }
    }
}
