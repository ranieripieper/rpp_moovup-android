package com.doisdoissete.moovup.ui.goals.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleTextView;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_goal_activity)
public class GoalActivitiesViewHolder extends ItemViewHolder<GoalActivity> {

    @ViewId(R.id.card_view)
    CardView card;

    @ViewId(R.id.lbl_date)
    TextView lblDate;

    @ViewId(R.id.lbl_activity)
    TextView lblActivity;

    @ViewId(R.id.lbl_comment)
    TextView lblComment;

    @ViewId(R.id.layout_upvote)
    View layoutUpvote;

    @ViewId(R.id.lbl_nr_goal_activity_upvote)
    CircleTextView lblNrGoalActivityUpvote;

    @ViewId(R.id.img_background)
    ImageView imgBackground;

    @ViewId(R.id.overlay_image)
    View overlayImage;

    public GoalActivitiesViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(GoalActivity item, PositionInfo positionInfo) {
        lblDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(item.getDate()));
        lblActivity.setText(GoalHelper.getActivityDescription(getContext(), item, item.getGoal()));
        lblComment.setText(item.getBodyText());

        lblNrGoalActivityUpvote.setText(GoalHelper.getGoalActivityUpvote(item));
        if (item.isLiked()) {
            lblNrGoalActivityUpvote.setBackgroundResource(R.drawable.circular_textview);
            lblNrGoalActivityUpvote.setTextColor(getContext().getResources().getColor(android.R.color.white));
            lblNrGoalActivityUpvote.setCircleText(true);
            lblNrGoalActivityUpvote.setMinimumWidth(getContext().getResources().getDimensionPixelOffset(R.dimen.min_cirlce_withd));
        } else {
            lblNrGoalActivityUpvote.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
            lblNrGoalActivityUpvote.setTextColor(getContext().getResources().getColor(R.color.orange));
            lblNrGoalActivityUpvote.setCircleText(false);
            lblNrGoalActivityUpvote.setMinimumWidth(0);
        }

        imgBackground.setVisibility(View.GONE);
        imgBackground.setMaxHeight(1);
        imgBackground.getLayoutParams().height = 1;
        overlayImage.setVisibility(View.GONE);

        lblActivity.setTextColor(getContext().getResources().getColor(R.color.black));
        lblDate.setTextColor(getContext().getResources().getColor(R.color.black));
        lblComment.setTextColor(getContext().getResources().getColor(R.color.black));

        ViewUtil.loadGoalActivityImage(item, imgBackground, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
                card.post(new Runnable() {
                    @Override
                    public void run() {
                        imgBackground.setMaxHeight(card.getHeight());
                        imgBackground.getLayoutParams().height = card.getHeight();
                        //imgBackground.setImageBitmap(loadedImage);
                        imgBackground.setVisibility(View.VISIBLE);
                        overlayImage.setVisibility(View.VISIBLE);
                        overlayImage.setMinimumHeight(card.getHeight());
                        overlayImage.getLayoutParams().height = card.getHeight();
                        overlayImage.bringToFront();
                        lblActivity.setTextColor(getContext().getResources().getColor(R.color.white));
                        lblDate.setTextColor(getContext().getResources().getColor(R.color.white));
                        lblComment.setTextColor(getContext().getResources().getColor(R.color.white));
                    }
                });
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }

    @Override
    public void onSetListeners() {

        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoalActivitiesViewHolderListener listener = getListener(GoalActivitiesViewHolderListener.class);
                if (listener != null) {
                    listener.onGoalActivityClicked(getItem());
                }
            }
        });

        layoutUpvote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoalActivitiesViewHolderListener listener = getListener(GoalActivitiesViewHolderListener.class);
                if (listener != null) {
                    listener.onUpvotedClicked(getItem());
                }
            }
        });
    }

    public interface GoalActivitiesViewHolderListener {
        void onUpvotedClicked(GoalActivity goalActivity);
        void onGoalActivityClicked(GoalActivity goalActivity);
    }
}