package com.doisdoissete.moovup.ui.search;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.inputmethod.InputMethodManager;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.ui.base.BaseFragmentActivity;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.navigation.Navigator;

/**
 * Created by ranipieper on 1/13/16.
 */
public class SearchActivity extends BaseFragmentActivity {

    private SearchView mSearchView;

    public static final String EXTRA_SEARCH_TYPE = "EXTRA_SEARCH_TYPE";
    public static final int QUESTION_SEARCH_TYPE = 1;
    public static final int FRIENDS_SEARCH_TYPE = 2;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setToolbarTitle("");
        handleIntent(getIntent());
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
    }

    private void handleIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            loadData(query);
        }
        hideKeyboard();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        handleIntent(intent);
    }

    private void loadData(String query) {
        Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
        if (appData != null && appData.getInt(EXTRA_SEARCH_TYPE) == FRIENDS_SEARCH_TYPE) {
            Navigator.navigateToSearchResultFriendsFragment(this, query);
        } else {
            Navigator.navigateToSearchQuestionsFragment(this, query);
        }

        if (mSearchView != null) {
            mSearchView.clearFocus();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        Drawable drawable = menu.findItem(R.id.menu_feed_search).getIcon();
        drawable = DrawableCompat.wrap(drawable);

        DrawableCompat.setTint(drawable, getResources().getColor(R.color.white));
        menu.findItem(R.id.menu_feed_search).setIcon(drawable);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView = (SearchView) menu.findItem(R.id.menu_feed_search).getActionView();
        Bundle appData = getIntent().getBundleExtra(SearchManager.APP_DATA);
        ViewUtil.configSearchView(SearchActivity.this, searchManager, mSearchView, appData.getInt(EXTRA_SEARCH_TYPE));
        mSearchView.setIconified(false);

        //searchView.setIconifiedByDefault(true); // Do not iconify the widget; expand it by default
        if (Intent.ACTION_SEARCH.equals(getIntent().getAction())) {
            String query = getIntent().getStringExtra(SearchManager.QUERY);
            MenuItemCompat.expandActionView(menu.findItem(R.id.menu_feed_search));
            mSearchView.setQuery(query, false);
            mSearchView.clearFocus();
            final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(mSearchView.getWindowToken(), 0);
        }

        if (appData != null && appData.getInt(EXTRA_SEARCH_TYPE) == QUESTION_SEARCH_TYPE) {
            mSearchView.setQueryHint(getString(R.string.search_question_hint));
        } else {
            mSearchView.setQueryHint(getString(R.string.search_frirends_hint));
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (mFragments.size() <= 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }
}
