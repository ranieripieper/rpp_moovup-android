package com.doisdoissete.moovup.ui.navigation;

import android.content.Context;

import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.Notification;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.MainActivity;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.BaseFragmentActivity;
import com.doisdoissete.moovup.ui.friends.MyFriendsFragment;
import com.doisdoissete.moovup.ui.friends.PendingFriendsFragment;
import com.doisdoissete.moovup.ui.friends.SearchResultFriendsFragment;
import com.doisdoissete.moovup.ui.goals.AddGoalActivityFragment;
import com.doisdoissete.moovup.ui.goals.GoalCategoriesFragment;
import com.doisdoissete.moovup.ui.goals.GoalCategoriesGroupFragment;
import com.doisdoissete.moovup.ui.goals.GoalClosedFragment;
import com.doisdoissete.moovup.ui.goals.GoalCommonFragment;
import com.doisdoissete.moovup.ui.goals.GoalDetailFragment;
import com.doisdoissete.moovup.ui.goals.MyGoalsFragment;
import com.doisdoissete.moovup.ui.goals.UserGoalsFragment;
import com.doisdoissete.moovup.ui.login.LoginActivity;
import com.doisdoissete.moovup.ui.login.LoginIntroActivity;
import com.doisdoissete.moovup.ui.login.SignUpCompanyFragment;
import com.doisdoissete.moovup.ui.login.SignUpPersonFragment;
import com.doisdoissete.moovup.ui.notification.NotificationsFragment;
import com.doisdoissete.moovup.ui.questions.AddQuestionFragment;
import com.doisdoissete.moovup.ui.questions.AnswerQuestionFragment;
import com.doisdoissete.moovup.ui.questions.FavoritedQuestionFragment;
import com.doisdoissete.moovup.ui.questions.FeedQuestionFragment;
import com.doisdoissete.moovup.ui.questions.MyQuestionFragment;
import com.doisdoissete.moovup.ui.questions.QuestionDetailFragment;
import com.doisdoissete.moovup.ui.questions.QuestionToAnswerFragment;
import com.doisdoissete.moovup.ui.questions.SearchResultQuestionFragment;
import com.doisdoissete.moovup.ui.questions.comments.CommentsFragment;
import com.doisdoissete.moovup.ui.settings.AboutMoovupFragment;
import com.doisdoissete.moovup.ui.settings.InterestFragment;
import com.doisdoissete.moovup.ui.settings.SettingsFragment;
import com.doisdoissete.moovup.ui.terms.TermsActivity;
import com.doisdoissete.moovup.ui.tours.ToursFragment;

import java.util.List;

/**
 * Created by broto on 9/11/15.
 */
public class Navigator {

    //Activities

    public static void navigateToLoginIntroActivity(Context context) {
        if (context != null) {
            LoginIntroActivity.startActivity(context);
        }
    }

    public static void navigateToLoginActivity(Context context, boolean enabledBackButton) {
        if (context != null) {
            LoginActivity.startActivity(context, enabledBackButton);
        }
    }

    public static void navigateToMainActivity(Context context) {
        MainActivity.startActivity(context);
    }

    public static void navigateToTermsActivity(Context context, boolean needAccept) {
        TermsActivity.startActivity(context, needAccept);
    }

    public static void navigateToNotificationsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(NotificationsFragment.newInstance(), true);
        }
    }

    //Fragments

    public static void navigateToFragment(Context context, Notification notification, boolean forceNavigate) {
        if (context != null && notification != null) {

            if (Notification.NEW_QUESTIONS_TO_ANSWER.equalsIgnoreCase(notification.getNotificationType())) {
                navigateToQuestionsToAnswerFragment(context);
            } else if (Notification.NEW_FRIENDSHIP_REQUEST.equalsIgnoreCase(notification.getNotificationType())) {
                navigateToPendingFriendsFragment(context);
            } else if (Notification.NEW_LIKE_FOR_QUESTION.equalsIgnoreCase(notification.getNotificationType())
                    && notification.getNotificableId() != null) {
                Long questionId = notification.getNotificableId();
                if (questionId == null && notification.getNotificationMetaData() != null) {
                    questionId = notification.getNotificationMetaData().getQuestionId();
                }
                navigateToQuestionDetailsFragment(context, null, questionId);

            } else if (Notification.NEW_LIKE_FOR_QUESTION.equalsIgnoreCase(notification.getNotificationType()) && notification.getNotificableId() != null) {
                navigateToQuestionDetailsFragment(context, null, notification.getNotificableId());
            } else if (Notification.NEW_LIKE_FOR_GOAL.equalsIgnoreCase(notification.getNotificationType()) && notification.getNotificableId() != null) {
                navigateToGoalDetailFragment(context, notification.getNotificableId());

            } else if (Notification.NEW_LIKE_FOR_ANSWER.equalsIgnoreCase(notification.getNotificationType())
                    && notification.getNotificationMetaData() != null
                    && notification.getNotificationMetaData().getQuestionId() != null
                    ) {
                navigateToQuestionDetailsFragment(context, null, notification.getNotificationMetaData().getQuestionId());

            } else if (Notification.NEW_LIKE_FOR_ACTIVITY.equalsIgnoreCase(notification.getNotificationType())
                    && notification.getNotificationMetaData() != null) {

                navigateToGoalDetailFragment(context, notification.getNotificationMetaData().getGoalId());

            } else if (Notification.NEW_COMMENT_FOR_ANSWER.equalsIgnoreCase(notification.getNotificationType())
                    && notification.getNotificationMetaData() != null
                    && notification.getNotificationMetaData().getQuestionId() != null) {
                navigateToQuestionDetailsFragment(context, null, notification.getNotificationMetaData().getQuestionId());

            } else if (Notification.NEW_ANSWER_FOR_QUESTION.equalsIgnoreCase(notification.getNotificationType())
                    && notification.getNotificableId() != null) {
                navigateToQuestionDetailsFragment(context, null, notification.getNotificableId());
            } else if (Notification.GOALS_OUTDATED.equalsIgnoreCase(notification.getNotificationType())) {
                navigateToMyGoalsFragment(context);
            } else if (Notification.FRIENDSHIP_REQUEST_ACCEPTED.equalsIgnoreCase(notification.getNotificationType())) {
                if (notification.getSenderUserId() != null) {
                    //navigateToMyFriendsFragment(context);
                    navigateToUserGoalsFragment(context, null, notification.getSenderUserId());
                } else {
                    navigateToMyFriendsFragment(context);
                }
            } else if (Notification.INVITE_FRIENDS.equalsIgnoreCase(notification.getNotificationType())) {
                navigateToMyFriendsFragment(context);
            } else if (Notification.ACTIVITIES_OUTDATED.equalsIgnoreCase(notification.getNotificationType())) {
                navigateToMyGoalsFragment(context);
            } else if (forceNavigate
                    || (Notification.WELCOME.equalsIgnoreCase(notification.getNotificationType())
                    || Notification.PASSWORD_UPDATED.equalsIgnoreCase(notification.getNotificationType()))) {
                navigateToHome(context);
            }
        }

    }

    public static void navigateToSettingsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(SettingsFragment.newInstance(), false);
        }
    }

    public static void navigateToInterestFragment(Context context) {
        navigateToInterestFragment(context, false);
    }

    public static void navigateToInterestFragment(Context context, boolean mainMenu) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(InterestFragment.newInstance(mainMenu), mainMenu);
        }
    }

    public static void navigateToEditProfileFragment(Context context) {
        navigateToEditProfileFragment(context, false);
    }

    public static void navigateToEditProfileFragment(Context context, boolean profileIncomplete) {
        User user = UserCacheManager.getInstance().getCurrentUser();
        if (context != null && user != null) {
            if (User.COMPANY_PROFILE_TYPE.equals(user.getProfileType())) {
                ((BaseFragmentActivity) context).addFragment(SignUpCompanyFragment.newInstance(true, profileIncomplete));
            } else {
                ((BaseFragmentActivity) context).addFragment(SignUpPersonFragment.newInstance(true, profileIncomplete));
            }
        }
    }

    public static void navigateToFragment(Context context, BaseFragment fragment, boolean clearBackStack) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(fragment, clearBackStack);
        }
    }

    public static void navigateToHome(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(FeedQuestionFragment.newInstance(), true);
        }
    }

    public static void navigateToFeedQuestionsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(FeedQuestionFragment.newInstance(), true);
        }
    }

    public static void navigateToMyQuestionsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(MyQuestionFragment.newInstance(), true);
        }
    }

    public static void navigateToFavoritedQuestionFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(FavoritedQuestionFragment.newInstance(), true);
        }
    }

    public static void navigateToMyGoalsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(MyGoalsFragment.newInstance(), true);
        }
    }

    public static void navigateToQuestionsToAnswerFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(QuestionToAnswerFragment.newInstance(), true);
        }
    }

    public static void navigateToAddGoalFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(GoalCategoriesGroupFragment.newInstance());
        }
    }

    public static void navigateToGoalCategoriesFragmentFragment(Context context, Category category, List<Category> categories) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(GoalCategoriesFragment.newInstance(category, categories));
        }
    }

    public static void navigateToAddGoalFragment(Context context, Category category) {
        if (context != null) {
            if (category.isClosedGoal()) {
                ((BaseFragmentActivity) context).addFragment(GoalClosedFragment.newInstance(category));
            } else {
                ((BaseFragmentActivity) context).addFragment(GoalCommonFragment.newInstance(category));
            }
        }
    }

    public static void navigateToGoalDetailFragment(Context context, BaseFragment actualFragment, Goal goal) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, GoalDetailFragment.newInstance(goal));
        }
    }

    public static void navigateToGoalDetailFragment(Context context, Long goalId) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(GoalDetailFragment.newInstance(goalId));
        }
    }

    public static void navigateToSearchQuestionsFragment(Context context, String mQuery) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(SearchResultQuestionFragment.newInstance(mQuery), true);
        }
    }

    public static void navigateToAnswerQuestionFragment(Context context, BaseFragment actualFragment, Question question) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, AnswerQuestionFragment.newInstance(question));
        }
    }

    public static void navigateToEditAnswerFragment(Context context, BaseFragment actualFragment, Question question, Answers answers) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, AnswerQuestionFragment.newInstance(question, answers));
        }
    }

    public static void navigateToQuestionDetailsFragment(Context context, BaseFragment actualFragment, Question question) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, QuestionDetailFragment.newInstance(question));
        }
    }

    public static void navigateToQuestionDetailsFragment(Context context, BaseFragment actualFragment, Long questionId) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, QuestionDetailFragment.newInstance(questionId));
        }
    }

    public static void navigateToAddQuestion(Context context, BaseFragment actualFragment) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, AddQuestionFragment.newInstance());
        }
    }

    public static void navigateToAddGoalActivityFragment(Context context, BaseFragment actualFragment, Goal goal) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, AddGoalActivityFragment.newInstance(goal));
        }
    }

    public static void navigateToEditQuestion(Context context, BaseFragment actualFragment, Question question) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, AddQuestionFragment.newInstance(question));
        }
    }

    public static void navigateToCommentsFragment(Context context, BaseFragment actualFragment, Question question, Answers answer) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, CommentsFragment.newInstance(question, answer));
        }
    }


    public static void navigateToAboutMoovupFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(AboutMoovupFragment.newInstance(), false);
        }
    }

    //friends
    public static void navigateToMyFriendsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(MyFriendsFragment.newInstance(), true);
        }
    }


    public static void navigateToSearchResultFriendsFragment(Context context, String mQuery) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(SearchResultFriendsFragment.newInstance(mQuery), true);
        }
    }

    public static void navigateToPendingFriendsFragment(Context context) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(PendingFriendsFragment.newInstance(), true);
        }
    }

    public static void navigateToUserGoalsFragment(Context context, BaseFragment actualFragment, Long userId) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(actualFragment, UserGoalsFragment.newInstance(userId));
        }
    }

    public static void navigateToTourFragment(Context context, boolean enableSkip) {
        navigateToTourFragment(context, enableSkip, false);
    }

    public static void navigateToTourFragment(Context context, boolean enableSkip, boolean mainMenu) {
        if (context != null) {
            ((BaseFragmentActivity) context).addFragment(ToursFragment.newInstance(enableSkip, mainMenu), mainMenu);
        }
    }
}
