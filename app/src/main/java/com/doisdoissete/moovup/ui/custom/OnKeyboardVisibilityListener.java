package com.doisdoissete.moovup.ui.custom;

/**
 * Created by ranieripieper on 5/17/16.
 */
public interface OnKeyboardVisibilityListener {

    void onVisibilityChanged(boolean visible);
}
