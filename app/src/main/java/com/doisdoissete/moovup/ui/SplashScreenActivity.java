package com.doisdoissete.moovup.ui;

import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.login.LoginIntroActivity;
import com.doisdoissete.moovup.ui.navigation.Navigator;

public class SplashScreenActivity extends AppCompatActivity {

    private static final Integer SPLASH_TIME_OUT = BuildConfig.DEBUG ? 1 : 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        //LoginSignupInfosActivity.startActivity(SplashScreenActivity.this, "xxxxxx@2sasdf.com", "123456");

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                User userCache = UserCacheManager.getInstance().getCurrentUser();
                if (userCache != null) {
                    showMainActivity();
                } else {
                    showFirstAccess();
                }

                finish();
            }
        }, SPLASH_TIME_OUT);

    }

    private void showMainActivity() {
        Navigator.navigateToMainActivity(SplashScreenActivity.this);
    }

    private void showFirstAccess() {
        LoginIntroActivity.startActivity(SplashScreenActivity.this);
    }

}
