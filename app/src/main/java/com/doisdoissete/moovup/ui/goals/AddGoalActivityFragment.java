package com.doisdoissete.moovup.ui.goals;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.GoalActivityCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.response.GoalActivityResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.service.asynctask.ResizeImageAsyncTask;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.io.File;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 1/20/16.
 */
@RuntimePermissions
public class AddGoalActivityFragment extends BaseFragment implements ImageChooserListener {

    private Goal mGoal;

    public static String EXTRA_GOAL_ACTIVITY = "EXTRA_GOAL_ACTIVITY";

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.edt_total)
    EditText edtTotal;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.edt_date)
    EditText edtDate;

    @Bind(R.id.edt_comment)
    EditText edtComment;

    @Bind(R.id.img_photo)
    ImageView imgPhoto;

    @Bind(R.id.btn_remove_image)
    ImageView btnRemoveImage;

    @Bind(R.id.txt_input_layout_total)
    TextInputLayout txtInputLayoutTotal;


    // Photo Picker Stuff
    private ImageChooserManager imageChooserManager;
    private int chooserType;
    protected String filePath;

    public static AddGoalActivityFragment newInstance(Goal goal) {
        AddGoalActivityFragment fragment = new AddGoalActivityFragment();
        Bundle bundle = new Bundle();
        fragment.mGoal = goal;
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initDateField();

        Category category = CategoryCacheManager.getInstance().get(mGoal.getCategoryId());
        if (category.getMeasurementMetricTexts() != null && !TextUtils.isEmpty(category.getMeasurementMetricTexts().getPlural())) {
            txtInputLayoutTotal.setHint(category.getMeasurementMetricTexts().getPlural());
        }
    }

    protected void initDateField() {
        edtDate.setOnKeyListener(null);
        edtDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ViewUtil.buildDatePicker(getContext(), edtDate, new Date(), mGoal.getCreatedAt(), null).show();
                }

                return true;
            }
        });
    }


    @OnClick(R.id.btn_add_goal_activity)
    public void addGoalActivity(View v) {
        hideKeyboard();
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callServiceResizeImage();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }


    private void callServiceResizeImage() {
        showLoadingView();
        if (!TextUtils.isEmpty(filePath)) {
            new ResizeImageAsyncTask(getActivity(), filePath, new ResizeImageAsyncTask.ResizeImageListener() {
                @Override
                public void onCancelled() {
                    hideLoadingView();
                }

                @Override
                public void onPostExecute(String resultFilePath) {
                    callSaveGoalActivityService(new TypedFile("image/jpeg", new File(resultFilePath)));
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            callSaveGoalActivityService(null);
        }

    }

    private void callSaveGoalActivityService(TypedFile photo) {
        Date date = null;
        try {
            date = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(edtDate.getText().toString());
        } catch (ParseException e) {
        }
        RetrofitManager.getInstance().getGoalService().addGoalActivities(
                mGoal.getId(),
                edtComment.getText().toString().trim(),
                //getFloatValue(edtTotal),
                edtTotal.getText().toString(),
                DateUtil.DATE_YEAR_MONTH_DAY.get().format(date),
                photo,
                new Callback<GoalActivityResponse>() {
                    @Override
                    public void success(GoalActivityResponse goalActivityResponse, Response response) {
                        hideLoadingView();
                        processSuccess(goalActivityResponse);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
    }

    private void processSuccess(final GoalActivityResponse goalActivitiesResponse) {
        if (goalActivitiesResponse.isSuccess()) {
            if (goalActivitiesResponse != null && goalActivitiesResponse.getActivity() != null) {
                GoalActivityCacheManager.getInstance().updateWithoutNulls(goalActivitiesResponse.getActivity());
            }

            showSuccessDialog(getContext(), R.string.msg_goal_activity_added, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(@NonNull MaterialDialog materialDialog, @NonNull DialogAction dialogAction) {
                    hideLoadingView();
                    finish();
                    Intent i = new Intent();
                    i.putExtra(EXTRA_GOAL_ACTIVITY, goalActivitiesResponse.getActivity().getId());
                    getTargetFragment().onActivityResult(RequestCodeUtil.REQUEST_CODE_ADD_GOAL_ACTIVITY, Activity.RESULT_OK, i);
                }
            });
        }
        hideLoadingView();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    // Should be called if for some reason the ImageChooserManager is null (Due
    // to destroying of activity for low memory situations)
    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    @Override
    public void onImageChosen(ChosenImage chosenImage) {
        filePath = chosenImage.getFileThumbnail();
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Uri uriFile = Uri.fromFile(new File(filePath));
                MoovUpApplication.imageLoaderUser.displayImage(Uri.decode(uriFile.toString()), imgPhoto, new ImageLoadingListener() {
                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                        imgPhoto.setVisibility(View.GONE);
                        btnRemoveImage.setVisibility(View.GONE);
                        filePath = null;
                        showToastError(getActivity(), getString(R.string.error_generic));
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        imgPhoto.setVisibility(View.VISIBLE);
                        btnRemoveImage.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                        imgPhoto.setVisibility(View.GONE);
                        btnRemoveImage.setVisibility(View.GONE);
                        filePath = null;
                        showToastError(getActivity(), getString(R.string.error_generic));
                    }

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }
                });
            }
        });
    }

    @Override
    public void onError(String s) {
        showToastError(getActivity(), getString(R.string.error_generic));
        filePath = null;
        imgPhoto.setVisibility(View.GONE);
        btnRemoveImage.setVisibility(View.GONE);
    }

    public void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    public void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    protected void showMediaTypeChooserChecked() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_photo_video_chooser);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.choose_photo).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.take_picture).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @OnClick(R.id.btn_camera)
    protected void onPhotoClicked() {
        AddGoalActivityFragmentPermissionsDispatcher.showMediaTypeChooserWithCheck(this);
    }

    @OnClick(R.id.btn_remove_image)
    protected void onRemoveImageClicked() {
        filePath = null;
        imgPhoto.setVisibility(View.GONE);
        btnRemoveImage.setVisibility(View.GONE);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    protected void showMediaTypeChooser() {
        showMediaTypeChooserChecked();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        AddGoalActivityFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_add_goal_activity;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}