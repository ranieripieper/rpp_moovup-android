package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.QuestionsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/26/15.
 */
public class MyQuestionFragment extends FeedQuestionFragment {

    public static MyQuestionFragment newInstance() {
        return new MyQuestionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        txtNoResults.setText(R.string.no_my_question_yet);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.my_questions);
    }

    protected void callService() {
        RetrofitManager.getInstance().getQuestionService().getMyQuestions(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<QuestionsResponse>() {
            @Override
            public void success(QuestionsResponse questionsResponse, Response response) {
                if (isAdded()) {
                    txtNoResults.setText(R.string.no_my_question_yet);
                    processResponse(questionsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_ADD_QUESTION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED)) {
                    long questionId = data.getLongExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED, -1);
                    Question question = QuestionCacheManager.getInstance().get(questionId);
                    if (questionAdapter == null) {
                        createAdapter(new ArrayList<Question>());
                    }
                    if (question != null) {
                        questionAdapter.addItem(0, question);
                        mRecyclerView.smoothScrollToPosition(0);
                        onQuestionSelected(question);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        txtNoResults.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
