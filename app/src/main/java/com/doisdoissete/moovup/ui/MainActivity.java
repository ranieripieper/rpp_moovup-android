package com.doisdoissete.moovup.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.push.PushNotificationMessage;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.BaseFragmentActivity;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.helper.UserHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.util.SharedPrefManager;

import java.util.Arrays;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/10/15.
 */
public class MainActivity extends BaseFragmentActivity {

    private User mUserCache;

    private MenuItem lastMenuItem = null;

    private static final String EXTRA_NOTIFICATION = "EXTRA_NOTIFICATION";
    private PushNotificationMessage mPushNotificationMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (hasExtra(EXTRA_NOTIFICATION)) {
            mPushNotificationMessage = getIntent().getExtras().getParcelable(EXTRA_NOTIFICATION);
        }

        mUserCache = UserCacheManager.getInstance().getCurrentUser();

        if (mUserCache == null) {
            Navigator.navigateToLoginActivity(getContext(), false);
            finish();
        } else {
            initDrawerToggle();
            initNavigationView();
            showFirstScreen();
        }

        //recupera os dados do usuário
        getUserInfo();
    }

    private void getUserInfo() {
        RetrofitManager.getInstance().getUserService().getCurrentUserData(new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                UserCacheManager.getInstance().updateWithoutNulls(Arrays.asList(user));
                updateNotificationIcons();
            }

            @Override
            public void failure(RetrofitError error) {
            }
        });
    }

    private void updateNotificationIcons() {
        User user = UserCacheManager.getInstance().getCurrentUser();
        MenuItem menuNotification = mNavigationView.getMenu().findItem(R.id.drawer_notifications);
        if (menuNotification != null) {
            if (user != null && user.getUnreadNotificationsCount() > 0) {
                menuNotification.setIcon(R.drawable.ic_notifications_unread);
            } else {
                menuNotification.setIcon(R.drawable.ic_notifications);
            }

            mNavigationView.invalidate();
            mDrawerToggle.syncState();
        }
    }

    private void showFirstScreen() {
        //verifica se usuário tem o perfil completo
        if (UserHelper.profileIsComplete(mUserCache)) {
            if (SharedPrefManager.getInstance().showTour(mUserCache.getEmail())) {
                Navigator.navigateToTourFragment(getContext(), true);
            } else {
/*
                mPushNotificationMessage = new PushNotificationMessage();
                mPushNotificationMessage.setData(new Notification());
                mPushNotificationMessage.getData().setNotificationType(Notification.NEW_COMMENT_FOR_ANSWER);
                mPushNotificationMessage.getData().setNotificableId(809l);
                mPushNotificationMessage.getData().setNotificationMetaData(new NotificationMetaData());
                mPushNotificationMessage.getData().getNotificationMetaData().setAnswerId(1422l);
                mPushNotificationMessage.getData().getNotificationMetaData().setQuestionId(809l);
*/
                if (mPushNotificationMessage != null && mPushNotificationMessage.getData() != null) {
                    Navigator.navigateToFragment(getContext(), mPushNotificationMessage.getData(), true);
                } else {
                    Navigator.navigateToHome(getContext());
                    //Navigator.navigateToQuestionDetailsFragment(getContext(), null, 1466l);
                }
            }
        } else {
            //edit
            showSuccessDialog(getContext(), R.string.msg_complete_profile, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    Navigator.navigateToEditProfileFragment(MainActivity.this, true);
                }
            });

        }
    }

    @Override
    protected int getLayoutFragmentContainer() {
        return R.id.layout_fragment_container;
    }

    public static void startActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, MainActivity.class));
    }

    public static Intent getActivityIntent(Context ctx, PushNotificationMessage pushNotificationMessage) {
        Intent it = new Intent(ctx, MainActivity.class);
        it.putExtra(EXTRA_NOTIFICATION, pushNotificationMessage);
        return it;
    }

    private void initDrawerToggle() {
        if (mDrawerLayout != null) {

            mDrawerToggle = new ActionBarDrawerToggle(
                    this,
                    mDrawerLayout,
                    R.string.empty,
                    R.string.empty
            );

            mDrawerLayout.setDrawerListener(mDrawerToggle); // REQUIRED FOR ARROW ANIMATION. Fml.
            mDrawerToggle.syncState();
        }
    }

    private void initNavigationView() {
        if (mNavigationView != null) {
            mNavigationView.setItemIconTintList(null);
            User user = UserCacheManager.getInstance().getCurrentUser();
            if (user.getProfileType().equalsIgnoreCase(User.COMPANY_PROFILE_TYPE)) {
                mNavigationView.inflateMenu(R.menu.menu_drawer_company);
            } else {
                mNavigationView.inflateMenu(R.menu.menu_drawer);
            }
            initNavigationHeader();
            lastMenuItem = mNavigationView.getMenu().findItem(R.id.drawer_home);
            mNavigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(MenuItem item) {
                    //if (item.isChecked()) {
                    //    item.setChecked(false);
                    //} else {

                    if (item.getItemId() == R.id.drawer_logout) {
                        closeDrawers(false);
                    } else {
                        closeDrawers(item.getItemId() != R.id.drawer_logout);
                        lastMenuItem = item;
                        item.setChecked(true);
                    }

                    switch (item.getItemId()) {
                        case R.id.drawer_home:
                            Navigator.navigateToFeedQuestionsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_my_questions:
                            Navigator.navigateToMyQuestionsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_to_answer:
                            Navigator.navigateToQuestionsToAnswerFragment(MainActivity.this);
                            break;
                        case R.id.drawer_favorited_questions:
                            Navigator.navigateToFavoritedQuestionFragment(MainActivity.this);
                            break;
                        case R.id.drawer_options_awnser:
                            Navigator.navigateToInterestFragment(MainActivity.this, true);
                            break;
                        case R.id.drawer_my_friends:
                            Navigator.navigateToMyFriendsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_my_goals:
                            Navigator.navigateToMyGoalsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_profile:
                            Navigator.navigateToEditProfileFragment(MainActivity.this);
                            break;
                        case R.id.drawer_pending_friends:
                            Navigator.navigateToPendingFriendsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_notifications:
                            Navigator.navigateToNotificationsFragment(MainActivity.this);
                            break;
                        case R.id.drawer_help:
                            Navigator.navigateToTourFragment(MainActivity.this, false, true);
                            break;
                        case R.id.drawer_logout:
                            showLogoutDialog();
                            break;
                        default:
                            break;
                    }

                    //}

                    return true;
                }
            });
        }
    }

    private void showLogoutDialog() {
        ViewUtil.showConfirmDialog(getContext(), R.string.dialog_title_logout, R.string.dialog_msg_logout, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                logout();
            }
        });
    }

    TextView lblNavigationHeaderName;
    CircleImageView imgHeaderMenuPhoto;

    private void initNavigationHeader() {
        View headerView = mNavigationView.inflateHeaderView(R.layout.drawer_header);
        lblNavigationHeaderName = (TextView) headerView.findViewById(R.id.lbl_drawer_name);
        imgHeaderMenuPhoto = (CircleImageView) headerView.findViewById(R.id.img_drawer_photo);
        ImageView imgSettings = (ImageView) headerView.findViewById(R.id.img_drawer_settings);

        imgHeaderMenuPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.navigateToEditProfileFragment(MainActivity.this);
                closeDrawers();
            }
        });

        imgSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Navigator.navigateToSettingsFragment(MainActivity.this);
                closeDrawers();
            }
        });

        updateHeaderMenu();
    }

    private void updateMenu() {
        mUserCache = UserCacheManager.getInstance().getCurrentUser();
        updateHeaderMenu();
        updateNotificationIcons();
    }

    private void updateHeaderMenu() {
        if (mUserCache != null) {
            if (imgHeaderMenuPhoto != null) {
                MoovUpApplication.imageLoaderUser.displayImage(mUserCache.getProfileImageUrl(), imgHeaderMenuPhoto);
            }

            if (lblNavigationHeaderName != null) {
                lblNavigationHeaderName.setText(mUserCache.getFullName());
            }
        }
    }

    private void closeDrawers() {
        closeDrawers(true);
    }

    private void closeDrawers(boolean uncheked) {
        if (mDrawerLayout != null) {
            mDrawerLayout.closeDrawers();
        }
        if (lastMenuItem != null && uncheked) {
            lastMenuItem.setChecked(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateMenu();
        if (mFragments != null && !mFragments.isEmpty()) {
            Fragment fragment = getSupportFragmentManager().findFragmentByTag(mFragments.get(mFragments.size() - 1));
            if (fragment != null && fragment.isVisible()) {
                ((BaseFragment) fragment).onResumeFromBackStack();
            }
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout != null && mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeDrawers();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void beforeOpenDrawer() {
        super.beforeOpenDrawer();
        updateMenu();
    }
}
