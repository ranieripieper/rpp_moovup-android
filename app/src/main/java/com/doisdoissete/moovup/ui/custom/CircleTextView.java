package com.doisdoissete.moovup.ui.custom;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by ranipieper on 12/17/15.
 */
public class CircleTextView extends TextView {

    private boolean mCircleText = true;

    public CircleTextView(Context context) {
        super(context);
    }

    public CircleTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        if (mCircleText) {
            int width = getMeasuredWidth();
            int height = getMeasuredHeight();
            int size = width;
            if (height > width) {
                size = height;
            }
            setMeasuredDimension(size, size);
        }
    }

    public void setCircleText(boolean circleText) {
        this.mCircleText = circleText;
        invalidate();
    }
}
