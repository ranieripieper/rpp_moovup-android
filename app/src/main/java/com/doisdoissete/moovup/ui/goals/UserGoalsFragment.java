package com.doisdoissete.moovup.ui.goals;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.FriendshipResponse;
import com.doisdoissete.moovup.service.model.response.GoalsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.goals.adapter.MyGoalsViewHolder;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.OnClick;
import io.realm.exceptions.RealmError;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 2/18/16.
 */
public class UserGoalsFragment extends MyGoalsFragment implements MyGoalsViewHolder.MyGoalsHolderListener {

    protected Long mUserId;
    protected User mUser;

    public static UserGoalsFragment newInstance(Long userId) {
        UserGoalsFragment fragment = new UserGoalsFragment();
        fragment.mUserId = userId;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected void loadData() {
        if (mUser != null) {
            super.loadData();
        } else {
            callServiceLoadUserData();
        }
    }

    @Override
    protected void callService() {
        RetrofitManager.getInstance().getGoalService().getGoals(mUserId, mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<GoalsResponse>() {
            @Override
            public void success(GoalsResponse goalsResponse, Response response) {
                if (isAdded()) {
                    processResponse(goalsResponse);
                }
                invalidateOptionsMenu();
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showEmpty();
                }
                invalidateOptionsMenu();
            }
        });
    }

    protected void processResponse(GoalsResponse goalsResponse) {
        if (goalsResponse == null) {
            goalsResponse = new GoalsResponse();
        }
        if (goalsResponse.getGoals() == null) {
            goalsResponse.setGoals(new ArrayList<Goal>());
        }
        super.processResponse(goalsResponse);
    }

    protected void callServiceLoadUserData() {
        if (mUserId == null) {
            finish();
            return;
        }
        showLoadingView();
        RetrofitManager.getInstance().getUserService().getUserData(mUserId, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                if (isAdded()) {
                    processResponse(user);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error, new ErrorListener() {
                        @Override
                        public void positiveButton() {
                            loadData();
                        }

                        @Override
                        public void negativeButton() {
                            finish();
                        }
                    });
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void processResponse(User user) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        mUser = user;
        try {
            UserCacheManager.getInstance().updateWithoutNulls(Arrays.asList(mUser));
        } catch (RealmError e) {
            Crashlytics.logException(e);
        }
        if (mUser != null) {
            loadData();
        } else {
            showEmptyQuestions();
        }
    }

    protected User getUser() {
        return mUser;
    }

    protected boolean isCurrentUser() {
        return false;
    }


    private void showEmptyQuestions() {
        disableLoadingMore(mRecyclerView);
        if (mPage <= 1) {
            txtNoResults.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onGoalClicked(Goal goal) {
        Navigator.navigateToGoalDetailFragment(getContext(), this, goal);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();

        if (mUser != null) {
            User currentUser = getCurrentUser();
            if (currentUser.getId() != mUserId) {
                inflater.inflate(R.menu.menu_user_goals, menu);
                MenuItem mReqFriendMenu = menu.findItem(R.id.menu_req_friend);
                MenuItem mAcceptFriendMenu = menu.findItem(R.id.menu_accept_friend);
                if (User.FRIENDSHIP_STATUS_NO_FRIENDSHIP.equalsIgnoreCase(mUser.getFriendshipStatus())) {
                    mAcceptFriendMenu.setVisible(false);
                    mReqFriendMenu.setVisible(true);
                } else if (User.FRIENDSHIP_STATUS_SELF_PENDING.equalsIgnoreCase(mUser.getFriendshipStatus())) {
                    mAcceptFriendMenu.setVisible(true);
                    mReqFriendMenu.setVisible(false);
                } else {
                    mAcceptFriendMenu.setVisible(false);
                    mReqFriendMenu.setVisible(false);
                }
            }
        }
    }

    @Override
    protected String getEmptyGoalsText() {
        if (isCurrentUser()) {
            return getString(R.string.no_goals_yet);
        } else {
            return getString(R.string.no_goals_yet_friend);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_req_friend) {
            sendFriendshipRequest(mUser);
            return true;
        } else if (item.getItemId() == R.id.menu_accept_friend) {
            acceptFriendshipRequest(mUser);
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.fab_add_goal)
    public void addQuestionClick() {
        Navigator.navigateToAddGoalFragment(getContext());
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_goals;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    public void acceptFriendshipRequest(final User user) {
        showLoadingView();
        RetrofitManager.getInstance().getFriendsService().acceptFriendship(user.getId(), "", new Callback<FriendshipResponse>() {
            @Override
            public void success(FriendshipResponse friendshipResponse, Response response) {
                showSuccessDialog(getContext(), R.string.msg_friendship_accept);
                user.setFriendshipStatus(User.FRIENDSHIP_STATUS_SELF_ACCEPTED);
                UserCacheManager.getInstance().updateWithoutNulls(Arrays.asList(user));
                hideLoadingView();
                invalidateOptionsMenu();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void sendFriendshipRequest(final User user) {
        showLoadingView();
        //send friendship
        RetrofitManager.getInstance().getFriendsService().sendFriendship(user.getId(), new Callback<FriendshipResponse>() {
            @Override
            public void success(FriendshipResponse friendshipResponse, Response response) {
                showSuccessDialog(getContext(), R.string.msg_friendship_sent);
                user.setFriendshipStatus(User.FRIENDSHIP_STATUS_USER_PENDING);
                UserCacheManager.getInstance().updateWithoutNulls(Arrays.asList(user));
                invalidateOptionsMenu();
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }
}