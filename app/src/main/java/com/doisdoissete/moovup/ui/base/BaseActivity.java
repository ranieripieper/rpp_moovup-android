package com.doisdoissete.moovup.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.background.LogoutService;
import com.doisdoissete.moovup.service.model.response.Meta;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.listeners.OnLogoutListener;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.RetrofitError;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

public abstract class BaseActivity extends AppCompatActivity implements ViewDialogInterface {

    @Nullable
    @Bind(R.id.toolbar)
    protected Toolbar mToolbar;

    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
        initToolbar();
    }

    protected boolean hasExtra(String key) {
        if (getIntent() != null && getIntent().getExtras() != null) {
            return getIntent().getExtras().containsKey(key);
        }

        return false;
    }

    protected boolean getExtraBoolean(String key) {
        if (hasExtra(key)) {
            return getIntent().getExtras().getBoolean(key);
        }
        return false;
    }

    protected String getExtraString(String key) {
        if (hasExtra(key)) {
            return getIntent().getExtras().getString(key);
        }
        return "";
    }

    public void showLoadingView() {
        hideKeyboard();
        if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(this, getString(R.string.wait),
                    getString(R.string.loading), true);
            hideKeyboard();
        }
    }

    public void hideLoadingView() {
        if (progress != null && progress.isShowing()) {
            try {
                progress.dismiss();
            } catch (IllegalArgumentException e) {
                Crashlytics.logException(e);
            }
            progress = null;
        }
    }

    public void hideKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getWindow().getDecorView().getRootView().getWindowToken(), 0);
    }

    protected void logout() {
        logout(true);
    }

    protected void logout(boolean redirect) {
        LogoutService.callService(getContext());
        SimpleFacebook simpleFacebook = SimpleFacebook.getInstance();
        if (simpleFacebook != null) {

            if (simpleFacebook.isLogin()) {
                // logout listener
                OnLogoutListener onLogoutListener = new OnLogoutListener() {

                    @Override
                    public void onLogout() {
                    }

                };
                simpleFacebook.logout(onLogoutListener);
            }
            simpleFacebook.clean();
        }

        if (redirect) {
            Navigator.navigateToLoginIntroActivity(getContext());
            finish();
        }
    }

    /**
     * Inicializa toolbar
     */
    private void initToolbar() {
        if (mToolbar != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mToolbar.setElevation(getResources().getDimensionPixelSize(R.dimen.action_bar_elevation));
            }
            setSupportActionBar(mToolbar);

            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeButtonEnabled(false);
                getSupportActionBar().setTitle(getString(R.string.app_name));
            }
        }
    }

    /**
     * Calligraphy
     *
     * @param newBase
     */
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    protected Context getContext() {
        return BaseActivity.this;
    }

    //ViewDialogInterface

    @Override
    public void showError(RetrofitError error) {
        showError(error, R.string.error_generic);
    }

    @Override
    public void showError(RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError, ignoreUnauthorized);
    }

    @Override
    public void showError(RetrofitError error, ErrorListener errorListener) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, R.string.error_generic, false, errorListener);
    }

    @Override
    public void showError(RetrofitError error, int defaultError, ErrorListener errorListener) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError, false, errorListener);
    }

    @Override
    public void showError(RetrofitError error, int defaultError) {
        hideLoadingView();
        ViewUtil.showError(getContext(), error, defaultError);
    }

    @Override
    public void showErrorDialog(List<String> lstMessage) {
        hideLoadingView();
        ViewUtil.showErrorDialog(getContext(), lstMessage, null);
    }

    @Override
    public void showErrorDialog(String message) {
        hideLoadingView();
        ViewUtil.showErrorDialog(getContext(), message);
    }

    @Override
    public void showErrorDialog(int message) {
        hideLoadingView();
        showErrorDialog(getString(message));
    }

    @Override
    public void showToastError(Context ctx, String message) {
        hideLoadingView();
        ViewUtil.showToastError(ctx, message);
    }

    @Override
    public void showToastError(Context ctx, RetrofitError error) {
        hideLoadingView();
        ViewUtil.showToastError(ctx, error);
    }

    @Override
    public void showSuccessDialog(Context ctx, int message) {
        showSuccessDialog(ctx, ctx.getString(message));
    }

    @Override
    public void showSuccessDialog(Context ctx, String message) {
        ViewUtil.showSuccessDialog(ctx, getWindow().getDecorView(), message);
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final boolean finish) {
        ViewUtil.showSuccessDialog(ctx, getWindow().getDecorView(), message, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                if (finish) {
                    finish();
                }
            }
        });
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final MaterialDialog.SingleButtonCallback callback) {
        ViewUtil.showSuccessDialog(ctx, getWindow().getDecorView(), message, callback);
    }

    //Toolbar
    public void setToolbarTitle(String s) {
        if (mToolbar != null) {
            mToolbar.setTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (mToolbar != null) {
            mToolbar.setTitle(stringId);
        }
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, Meta meta, int totalItems) {
        ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, meta, totalItems, true);
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView) {
        ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, true);
    }

    @Override
    public void finish() {
        hideKeyboard();
        super.finish();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }
}
