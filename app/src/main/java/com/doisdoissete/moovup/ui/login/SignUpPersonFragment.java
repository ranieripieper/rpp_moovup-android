package com.doisdoissete.moovup.ui.login;

import android.Manifest;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CarrerCacheManager;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Carrer;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.CarrerResponse;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.login.adapter.CarrerViewHolder;
import com.doisdoissete.moovup.ui.login.adapter.CategoryViewHolder;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.mobsandgeeks.saripaar.annotation.Select;
import com.parse.ParseInstallation;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.Bind;
import fr.ganfra.materialspinner.MaterialSpinner;
import permissions.dispatcher.NeedsPermission;
import permissions.dispatcher.RuntimePermissions;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;
import uk.co.ribot.easyadapter.EasyAdapter;

/**
 * Created by broto on 9/18/15.
 */
@RuntimePermissions
public class SignUpPersonFragment extends SignUpFragment {

    @Bind(R.id.txt_profile_family_name)
    public EditText txtFamilyName;

    @Bind(R.id.txt_profile_birthday)
    public EditText txtBirthday;

    @Select(defaultSelection = 0, messageResId = R.string.required_field)
    @Bind(R.id.spinner_profile_job)
    public MaterialSpinner spinnerCarrer;

    @Select(defaultSelection = 0, messageResId = R.string.required_field)
    @Bind(R.id.spinner_profile_hobby)
    public MaterialSpinner spinnerHobby;

    @Select(defaultSelection = 0, messageResId = R.string.required_field)
    @Bind(R.id.spinner_profile_gender)
    public MaterialSpinner spinnerGender;

    public SignUpPersonFragment() {
    }

    public static SignUpPersonFragment newInstance(String email, String password) {
        SignUpPersonFragment fragment = new SignUpPersonFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EXTRA_EMAIL, email);
        bundle.putString(EXTRA_PASSWORD, password);

        fragment.setArguments(bundle);
        return fragment;
    }

    public static SignUpPersonFragment newInstance(boolean editProfile, boolean profileIncomplete) {
        SignUpPersonFragment fragment = new SignUpPersonFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_EDIT_MODE, editProfile);
        bundle.putBoolean(EXTRA_PROFILE_INCOMPLETE, profileIncomplete);

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_profile_input;
    }


    @Override
    protected void onPhotoClicked() {
        SignUpPersonFragmentPermissionsDispatcher.showMediaTypeChooserWithCheck(this);
    }

    @NeedsPermission({Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE})
    protected void showMediaTypeChooser() {
        super.showMediaTypeChooserChecked();
    }


    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDateField();
        initFocusFields();

        initCarrers();
        initHobbies();
        renderGenders();

        if (userCache != null) {
            txtFamilyName.setText(userCache.getLastName());
        } else if (BuildConfig.DEBUG) {
            txtName.setText("nome");
            txtFamilyName.setText("last name");
            txtBirthday.setText("10/10/1979");
        }
    }

    private void initDateField() {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, -18);
        super.initDateField(txtBirthday, cal.getTime());
    }

    @Override
    protected void populateUserInfoForEdit(User user) {
        txtFamilyName.setText(user.getLastName());
        txtEmail.setVisibility(View.GONE);
        if (user.getBirthdayDate() != null) {
            txtBirthday.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(user.getBirthdayDate()));
        }
        if (!TextUtils.isEmpty(user.getGender())) {
            if (user.getGender().equals(User.GENDER_MALE)) {
                spinnerGender.setSelection(1);
            } else if (user.getGender().equals(User.GENDER_FEMALE)) {
                spinnerGender.setSelection(2);
            }
        }

        if (user.getHobby() != null) {
            //find position
            int pos = 0;
            for (int i = 1; i < spinnerHobby.getAdapter().getCount(); i++) {
                Category Category = (Category) spinnerHobby.getAdapter().getItem(i);
                if (Category.getId() == user.getHobby().getId()) {
                    pos = i;
                    break;
                }
            }
            spinnerHobby.setSelection(pos);
        }

        if (user.getCarrer() != null) {
            //find position
            int pos = 0;
            for (int i = 1; i < spinnerCarrer.getAdapter().getCount(); i++) {
                Carrer carrerCache = (Carrer) spinnerCarrer.getAdapter().getItem(i);
                if (carrerCache.getId() == user.getCarrer().getId()) {
                    pos = i;
                    break;
                }
            }
            spinnerCarrer.setSelection(pos);
        }
    }

    @Override
    protected void signUpUpdate() {
        showLoadingView();
        String email = txtEmail.getText().toString();
        String name = txtName.getText().toString();
        String lastName = txtFamilyName.getText().toString();
        String birthdate = txtBirthday.getText().toString();
        String gender = User.GENDER_MALE;
        if (spinnerGender.getSelectedItemId() == 1) {
            gender = User.GENDER_FEMALE;
        }
        long carrerId = -1;
        if (spinnerCarrer.getSelectedItemPosition() > 0) {
            Carrer carrer = (Carrer) spinnerCarrer.getAdapter().getItem(spinnerCarrer.getSelectedItemPosition());
            carrerId = carrer.getId();
        }
        long hobbyId = -1;
        if (spinnerHobby.getSelectedItemPosition() > 0) {
            Category hobby = (Category) spinnerHobby.getAdapter().getItem(spinnerHobby.getSelectedItemPosition());
            hobbyId = hobby.getId();
        }


        if (mEdit) {
            update(email, name, lastName, birthdate, gender, carrerId, hobbyId, filePath);
        } else {
            signUp(email, password, name, lastName, birthdate, gender, carrerId, hobbyId, filePath);
        }
    }

    public void signUp(String email, String password, String name, String lastName, String birthdate, String gender, Long carrerId, Long hobbyId, String photoPath) {
        TypedFile photo = null;
        if (!TextUtils.isEmpty(photoPath)) {
            photo = new TypedFile("image/jpeg", new File(photoPath));
        }

        RetrofitManager.getInstance().getUserService().signUpPerson(
                name,
                lastName,
                email,
                password,
                password,
                gender,
                birthdate,
                carrerId,
                hobbyId,
                User.PERSON_PROFILE_TYPE,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                RetrofitManager.PROVIDER,
                photo,
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processResult(responseResult);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
    }

    protected void update(String email, String name, String lastName, String birthdate, String gender, long carrerId, long hobbyId, String photoPath) {
        RetrofitManager.getInstance().getUserService().updatePerson(
                name,
                lastName,
                email,
                gender,
                birthdate,
                carrerId,
                hobbyId,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processResult(responseResult);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
    }

    public void renderGenders() {

        configSpinnerError(spinnerGender);

        String[] genders = new String[2];
        genders[0] = getString(R.string.male);
        genders[1] = getString(R.string.female);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_dropdown_item, genders);
        spinnerGender.setAdapter(spinnerArrayAdapter);
    }

    public void renderCarrers(List<Carrer> carrers) {
        configSpinnerError(spinnerCarrer);

        spinnerCarrer.setAdapter(new EasyAdapter(getActivity(),
                CarrerViewHolder.class,
                carrers,
                this));
    }

    public void renderHobbies(List<Category> categories) {
        configSpinnerError(spinnerHobby);

        spinnerHobby.setAdapter(new EasyAdapter(getActivity(),
                CategoryViewHolder.class,
                getHobbies(categories),
                this));
    }

    private List<Category> getHobbies(List<Category> categories) {
        List<Category> hobbies = new ArrayList<>();
        if (categories != null) {
            for (Category category : categories) {
                if (category.getChildren() != null && !category.getChildren().isEmpty()) {
                    hobbies.addAll(getHobbies(category.getChildren()));
                }
                if (category.isHobby()) {
                    hobbies.add(category);
                }
            }
        }
        return hobbies;
    }

    private void initFocusFields() {
        spinnerHobby.setOnTouchListener(focusListener);
    }

    private void initCarrers() {

        if (CarrerCacheManager.getInstance().count() <= 0) {
            spinnerCarrer.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    renderCarrers();
                    hideKeyboard();
                    return false;
                }
            });
        } else {
            spinnerCarrer.setOnTouchListener(focusListener);
            renderCarrers();
        }
    }

    private void renderCarrers() {
        if (spinnerCarrer.getAdapter().getCount() <= 1) {
            if (CarrerCacheManager.getInstance().count() <= 0) {
                spinnerCarrer.setEnabled(false);
                refreshCarrers();
            } else {
                renderCarrers(CarrerCacheManager.getInstance().getAll());
            }
        }
    }

    private void refreshCarrers() {
        showLoadingView();
        RetrofitManager.getInstance().getCarrerService().getCareers(RetrofitManager.MAX_PER_PAGE, new Callback<CarrerResponse>() {
            @Override
            public void success(CarrerResponse carrerResponse, Response response) {

                List<Carrer> result = new ArrayList<>();
                if (carrerResponse != null && carrerResponse.getCarrers() != null) {
                    CarrerCacheManager.getInstance().putAll(carrerResponse.getCarrers());
                }
                renderCarrers(result);

                spinnerCarrer.setEnabled(true);
                spinnerCarrer.performClick();
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, true);
                spinnerCarrer.setEnabled(true);
            }
        });

    }

    private void initHobbies() {

        if (CategoryCacheManager.getInstance().count() <= 0) {
            spinnerHobby.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    renderHobbies();
                    hideKeyboard();
                    return false;
                }
            });
        } else {
            spinnerHobby.setOnTouchListener(focusListener);
            renderHobbies();
        }
    }

    private void renderHobbies() {
        if (spinnerHobby.getAdapter().getCount() <= 1) {
            if (CategoryCacheManager.getInstance().count() <= 0) {
                spinnerHobby.setEnabled(false);
                refreshCategories();
            } else {
                renderHobbies(CategoryCacheManager.getInstance().getHobbies());
            }
        }
    }

    private void refreshCategories() {
        showLoadingView();
        RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE,new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse categoryResponse, Response response) {

                if (categoryResponse != null && categoryResponse.getCategories() != null) {
                    CategoryCacheManager.getInstance().putAll(categoryResponse.getCategories());
                }
                renderHobbies(CategoryCacheManager.getInstance().getHobbies());

                spinnerHobby.setEnabled(true);
                spinnerHobby.performClick();
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, true);
                spinnerHobby.setEnabled(true);
            }
        });

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        SignUpPersonFragmentPermissionsDispatcher.onRequestPermissionsResult(this, requestCode, grantResults);
    }
}