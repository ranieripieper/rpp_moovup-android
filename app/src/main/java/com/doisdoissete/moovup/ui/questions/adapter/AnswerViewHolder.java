package com.doisdoissete.moovup.ui.questions.adapter;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.AnswerCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.service.util.NumberUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.custom.CircleTextView;
import com.doisdoissete.moovup.ui.helper.AnswerHelper;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_answer)
public class AnswerViewHolder extends ItemViewHolder<Answers> {

    @ViewId(R.id.layout_row)
    CardView card;

    @ViewId(R.id.img_answer_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_answer_name)
    TextView lblName;

    @ViewId(R.id.lbl_answer_date)
    TextView lblDate;

    @ViewId(R.id.lbl_answer_content)
    TextView lblContent;

    @ViewId(R.id.lbl_answer_nr_comments)
    TextView lblNrComments;

    @ViewId(R.id.lbl_answer_comments)
    TextView lblComments;

    @ViewId(R.id.lbl_answer_nr_helped)
    CircleTextView lblAnswerNrHelp;

    @ViewId(R.id.img_answer_more)
    ImageView btMore;

    @ViewId(R.id.layout_row)
    View layoutRow;

    @ViewId(R.id.layout_helped)
    View layoutHelped;

    @ViewId(R.id.layout_comments)
    View layoutComments;

    @ViewId(R.id.img_answer)
    ImageView imgAnswer;

    public AnswerViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(final Answers itemTmp, PositionInfo positionInfo) {

        Answers item = AnswerCacheManager.getInstance().get(itemTmp.getId());

        if (item == null) {
            item = itemTmp;
        }

        Long userId = null;
        if (item.getUserId()!= null) {
            userId = item.getUserId();
        } else if (itemTmp.getUserId()!= null) {
            userId = itemTmp.getUserId();

        }
        if (userId != null) {
            User user = UserCacheManager.getInstance().get(item.getUserId());
            if (user != null) {
                ViewUtil.displayUserImage(user, imgPhoto);
                lblName.setText(user.getFullName());
            }
        }

        lblDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(item.getCreatedAt()));
        lblContent.setText(item.getBodyText());
        lblNrComments.setText(NumberUtil.getNumberFormated(item.getRepliesCount()));
        if (item.getRepliesCount() != 1) {
            lblComments.setText(R.string.comments);
        } else {
            lblComments.setText(R.string.comment);
        }
        lblAnswerNrHelp.setText(AnswerHelper.getAnswersHelped(item));
        if (item.isLiked()) {
            lblAnswerNrHelp.setBackgroundResource(R.drawable.circular_textview);
            lblAnswerNrHelp.setTextColor(getContext().getResources().getColor(android.R.color.white));
            lblAnswerNrHelp.setCircleText(true);
            lblAnswerNrHelp.setMinimumWidth(getContext().getResources().getDimensionPixelOffset(R.dimen.min_cirlce_withd));
        } else {
            lblAnswerNrHelp.setBackgroundColor(getContext().getResources().getColor(android.R.color.transparent));
            lblAnswerNrHelp.setTextColor(getContext().getResources().getColor(R.color.orange));
            lblAnswerNrHelp.setCircleText(false);
            lblAnswerNrHelp.setMinimumWidth(0);
        }
        imgAnswer.setVisibility(View.GONE);
        lblName.setTextColor(getContext().getResources().getColor(R.color.black));
        lblContent.setTextColor(getContext().getResources().getColor(R.color.black));
        lblDate.setTextColor(getContext().getResources().getColor(R.color.black));

        Drawable normalDrawable = getContext().getResources().getDrawable(R.drawable.ic_more_vert_24dp);
        final Drawable wrapDrawable = DrawableCompat.wrap(normalDrawable);
        DrawableCompat.setTint(wrapDrawable,  getContext().getResources().getColor(R.color.black));
        btMore.setImageDrawable(wrapDrawable);

        ViewUtil.loadAnswerImage(item, imgAnswer, new ImageLoadingListener() {
            @Override
            public void onLoadingStarted(String imageUri, View view) {
            }

            @Override
            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
            }

            @Override
            public void onLoadingComplete(String imageUri, View view, final Bitmap loadedImage) {
                card.post(new Runnable() {
                    @Override
                    public void run() {
                        imgAnswer.setVisibility(View.VISIBLE);
                        //lblName.setTextColor(getContext().getResources().getColor(R.color.white));
                       // lblContent.setTextColor(getContext().getResources().getColor(R.color.white));
                       // lblDate.setTextColor(getContext().getResources().getColor(R.color.white));

                        //DrawableCompat.setTint(wrapDrawable, getContext().getResources().getColor(R.color.white));
                        //btMore.setImageDrawable(wrapDrawable);

                    }
                });
            }

            @Override
            public void onLoadingCancelled(String imageUri, View view) {
            }
        });
    }

    @Override
    public void onSetListeners() {

        btMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    final Answers item = AnswerCacheManager.getInstance().get(getItem().getId());
                    listener.onBtMoreClicked(item);
                }
            }
        });

        layoutHelped.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    final Answers item = AnswerCacheManager.getInstance().get(getItem().getId());
                    listener.onHelpedClicked(item);
                }
            }
        });

        layoutComments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    final Answers item = AnswerCacheManager.getInstance().get(getItem().getId());
                    listener.onCommentsClicked(item);
                }
            }
        });

        layoutRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    final Answers item = AnswerCacheManager.getInstance().get(getItem().getId());
                    listener.onAnswerSelected(item);
                }
            }
        });


        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    listener.onPhotoUserClicked(getItem().getUserId());
                }
            }
        });

        imgAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AnswerHolderListener listener = getListener(AnswerHolderListener.class);
                if (listener != null) {
                    listener.onAnswerPhotoClicked(getItem());
                }
            }
        });
    }

    public interface AnswerHolderListener {
        void onPhotoUserClicked(Long userId);
        void onAnswerSelected(Answers answer);
        void onBtMoreClicked(Answers answer);
        void onAnswerPhotoClicked(Answers answer);
        void onCommentsClicked(Answers answer);
        void onHelpedClicked(Answers answer);
    }
}