package com.doisdoissete.moovup.ui.tours;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.login.adapter.SignUpViewPagerAdapter;
import com.doisdoissete.moovup.util.SharedPrefManager;

import butterknife.Bind;

/**
 * Created by ranipieper on 2/24/16.
 */
public class ToursFragment extends BaseFragment {

    private static final String EXTRA_SKIP_TOUR = "EXTRA_SKIP_TOUR";
    private static final String EXTRA_MAIN_MENU = "EXTRA_MAIN_MENU";

    @Bind(R.id.viewpager_tour)
    public ViewPager viewPager;

    private boolean mEnableSkipTour = false;
    private boolean mMainMenu = false;

    public ToursFragment() {
    }

    public static ToursFragment newInstance(boolean enableSkipTour, boolean mainMenu) {
        ToursFragment fragment = new ToursFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(EXTRA_SKIP_TOUR, enableSkipTour);
        bundle.putBoolean(EXTRA_MAIN_MENU, mainMenu);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mEnableSkipTour = getArguments().getBoolean(EXTRA_SKIP_TOUR);
            mMainMenu = getArguments().getBoolean(EXTRA_MAIN_MENU);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViewPager(viewPager);
    }

    private void setupViewPager(final ViewPager viewPager) {
        final SignUpViewPagerAdapter adapter = new SignUpViewPagerAdapter(getChildFragmentManager());

        PageController pageController = new PageController() {
            @Override
            public void nextPage() {
                if (viewPager != null) {
                    if (viewPager.getCurrentItem() + 1 >= adapter.getCount()) {
                        return;
                    }
                    viewPager.setCurrentItem(viewPager.getCurrentItem() + 1, true);
                }
            }

            @Override
            public void previousPage() {
                if (viewPager != null) {
                    if (viewPager.getCurrentItem() - 1 < 0) {
                        return;
                    }
                    viewPager.setCurrentItem(viewPager.getCurrentItem() - 1, true);
                }
            }
        };
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour1, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_1), getString(R.string.tutorial_info_2_screen_1), pageController, false, true), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour2, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_2), getString(R.string.tutorial_info_2_screen_2), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour3, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_3), getString(R.string.tutorial_info_2_screen_3), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour4, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_4), getString(R.string.tutorial_info_2_screen_4), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour5, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_5), getString(R.string.tutorial_info_2_screen_5), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour6, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_6), getString(R.string.tutorial_info_2_screen_6), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour7, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_7), getString(R.string.tutorial_info_2_screen_7), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour8, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_8), getString(R.string.tutorial_info_2_screen_8), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour9, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_9), getString(R.string.tutorial_info_2_screen_9), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour10, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_10), getString(R.string.tutorial_info_2_screen_10), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour11, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_11), getString(R.string.tutorial_info_2_screen_11), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour12, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_12), getString(R.string.tutorial_info_2_screen_12), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour13, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_13), getString(R.string.tutorial_info_2_screen_13), pageController), "");
        adapter.addFrag(TourItemFragment.newInstance(R.drawable.img_tour14, mEnableSkipTour, getString(R.string.tutorial_info_1_screen_14), getString(R.string.tutorial_info_2_screen_14), pageController, true, false), "");

        viewPager.setOffscreenPageLimit(0);
        viewPager.setAdapter(adapter);
    }

    @Override
    public void finish() {
        User user = UserCacheManager.getInstance().getCurrentUser();
        SharedPrefManager.getInstance().setShowTour(user.getEmail(), false);
        super.finish();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_tour;
    }

    @Override
    protected String getToolbarTitle() {
        if (mMainMenu) {
            return getString(R.string.help);
        }
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        if (mEnableSkipTour) {
            return TOOLBAR_TYPE.NOTHING;
        } else if (mMainMenu) {
            return TOOLBAR_TYPE.DRAWER;
        } else {
            return TOOLBAR_TYPE.BACK_BUTTON;
        }
    }


}
