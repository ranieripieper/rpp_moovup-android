package com.doisdoissete.moovup.ui.login;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.model.response.UserResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.asynctask.ResizeImageAsyncTask;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.kbeanie.imagechooser.api.ChooserType;
import com.kbeanie.imagechooser.api.ChosenImage;
import com.kbeanie.imagechooser.api.ImageChooserListener;
import com.kbeanie.imagechooser.api.ImageChooserManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.OnClick;
import fr.ganfra.materialspinner.MaterialSpinner;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

/**
 * Created by ranipieper on 11/12/15.
 */
public abstract class SignUpFragment extends BaseFragment implements ImageChooserListener {

    protected static final String EXTRA_EMAIL = "param_email";
    protected static final String EXTRA_PASSWORD = "param_password";
    protected static final String EXTRA_EDIT_MODE = "EXTRA_EDIT_MODE";
    protected static final String EXTRA_PROFILE_INCOMPLETE = "EXTRA_PROFILE_INCOMPLETE";

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_profile_email)
    public EditText txtEmail;

    @Bind(R.id.img_profile_photo)
    public CircleImageView imgPhoto;

    @Bind(R.id.btn_profile_done)
    public Button btSave;

    @NotEmpty(messageResId = R.string.required_field)
    @Bind(R.id.txt_profile_name)
    public EditText txtName;

    protected String email;
    protected String password;
    protected User userCache;

    // Photo Picker Stuff
    private ImageChooserManager imageChooserManager;
    private int chooserType;
    protected String filePath;

    protected boolean mEdit = false;
    protected boolean mProfileIncomplete = false;

    protected Long mUserId = null;

    private Map<View, TextView> spinnerSelections = new HashMap<View, TextView>();

    public SignUpFragment() {
    }

    protected abstract void onPhotoClicked();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        userCache = UserCacheManager.getInstance().getCurrentUser();

        if (getArguments() != null && getArguments().containsKey(EXTRA_PROFILE_INCOMPLETE)) {
            mProfileIncomplete = getArguments().getBoolean(EXTRA_PROFILE_INCOMPLETE);
        }

        if (userCache != null) {
            email = userCache.getEmail();
            mEdit = getArguments().getBoolean(EXTRA_EDIT_MODE);
        } else if (getArguments() != null) {
            email = getArguments().getString(EXTRA_EMAIL);
            password = getArguments().getString(EXTRA_PASSWORD);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        txtEmail.setText(email);
        if (TextUtils.isEmpty(email)) {
            txtEmail.setVisibility(View.VISIBLE);
        }

        if (userCache != null) {
            txtName.setText(userCache.getFirstName());
            MoovUpApplication.imageLoaderUser.displayImage(userCache.getProfileImageUrl(), imgPhoto);
        } else if (BuildConfig.DEBUG) {
            txtName.setText("nome");
        }

        if (mEdit) {
            btSave.setText(R.string.save);
            loadUser();
        }
    }

    private void loadUser() {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().getCurrentUserData(new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                populateUserInfo(user);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        loadUser();
                    }

                    @Override
                    public void negativeButton() {
                        finish();
                    }
                });
            }
        });
    }

    private void populateUserInfo(User user) {
        hideLoadingView();
        if (user != null) {
            txtName.setText(user.getFirstName());
            mUserId = user.getId();
            MoovUpApplication.imageLoaderUser.displayImage(user.getProfileImageUrl(), imgPhoto);
        }

        populateUserInfoForEdit(user);
    }

    @OnClick(R.id.btn_profile_done)
    public void onDoneClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                if (!mEdit && !TextUtils.isEmpty(filePath)) {
                    showLoadingView();
                    resizeImage(new ResizeImageAsyncTask.ResizeImageListener() {
                        @Override
                        public void onCancelled() {
                            hideLoadingView();
                        }

                        @Override
                        public void onPostExecute(String resultFilePath) {
                            filePath = resultFilePath;
                            signUpUpdate();
                        }
                    });
                } else {
                    signUpUpdate();
                }

            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    } else if (view instanceof Spinner) {
                        if (spinnerSelections.get(view) != null) {
                            spinnerSelections.get(view).setError(message);
                        }
                    }
                }
            }
        });
        validator.validate();
    }

    protected abstract void signUpUpdate();
    protected abstract void populateUserInfoForEdit(User user);

    protected void processResult(ResponseResult responseResult) {
        hideLoadingView();
        if (mEdit) {
            if (responseResult.isSuccess()) {
                showSuccessDialog(getContext(), R.string.msg_user_updated, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (mProfileIncomplete && SharedPrefManager.getInstance().showTour(getCurrentUser().getEmail())) {
                            Navigator.navigateToTourFragment(getContext(), true);
                        } else {
                            Navigator.navigateToHome(getContext());
                        }
                    }
                });
            } else {
                showErrorDialog(R.string.error_generic);
            }
        } else {
            if (isAdded()) {
                ViewUtil.showSuccessDialog(getContext(), getString(R.string.msg_confirm_register), new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        Navigator.navigateToLoginActivity(getActivity(), false);
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            getActivity().finishAffinity();
                        } else {
                            getActivity().finish();
                        }
                    }
                });
            }
        }

        if (responseResult != null && responseResult.getUser() != null) {
            UserCacheManager.getInstance().put(responseResult.getUser());
        }
    }

    protected void configSpinnerError(MaterialSpinner spinner) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                synchronized (spinnerSelections) {
                    spinnerSelections.put(parent, (TextView) view);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    @OnClick(R.id.img_profile_photo)
    public void onPhotoClick() {
        onPhotoClicked();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK
                && (requestCode == ChooserType.REQUEST_PICK_PICTURE || requestCode == ChooserType.REQUEST_CAPTURE_PICTURE)) {
            if (imageChooserManager == null) {
                reinitializeImageChooser();
            }
            imageChooserManager.submit(requestCode, data);
        }
    }

    protected void showMediaTypeChooserChecked() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_photo_video_chooser);
        dialog.getWindow().setBackgroundDrawable(
                new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.findViewById(R.id.choose_photo).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                chooseImage();
                dialog.dismiss();
            }
        });

        dialog.findViewById(R.id.take_picture).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                takePicture();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void chooseImage() {
        chooserType = ChooserType.REQUEST_PICK_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_PICK_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.clearOldFiles();
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    public void takePicture() {
        chooserType = ChooserType.REQUEST_CAPTURE_PICTURE;
        imageChooserManager = new ImageChooserManager(this,
                ChooserType.REQUEST_CAPTURE_PICTURE, true);
        imageChooserManager.setImageChooserListener(this);
        try {
            filePath = imageChooserManager.choose();
        } catch (IllegalArgumentException e) {
        } catch (Exception e) {
        }
    }

    // Should be called if for some reason the ImageChooserManager is null (Due
    // to destroying of activity for low memory situations)
    private void reinitializeImageChooser() {
        imageChooserManager = new ImageChooserManager(this, chooserType, true);
        imageChooserManager.setImageChooserListener(this);
        imageChooserManager.reinitialize(filePath);
    }

    protected void initDateField(final TextView txtDateField, final Date maxDate) {
        txtDateField.setOnKeyListener(null);
        txtDateField.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    Calendar cal = Calendar.getInstance();
                    cal.add(Calendar.YEAR, -18);
                    ViewUtil.buildDatePicker(getContext(), txtDateField, maxDate, null, cal.getTime()).show();
                }

                return true;
            }
        });
    }


    protected View.OnTouchListener focusListener = new View.OnTouchListener() {

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            hideKeyboard();
            return false;
        }
    };

    @Override
    public void onImageChosen(final ChosenImage chosenImage) {
        filePath = chosenImage.getFileThumbnail();
        if (mEdit) {
            //faz update da imagem
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!TextUtils.isEmpty(filePath)) {
                        showLoadingView();

                        resizeImage(new ResizeImageAsyncTask.ResizeImageListener() {
                            @Override
                            public void onCancelled() {
                                hideLoadingView();
                            }

                            @Override
                            public void onPostExecute(String resultFilePath) {
                                if (!TextUtils.isEmpty(resultFilePath)) {
                                    TypedFile photo = new TypedFile("image/jpeg", new File(resultFilePath));
                                    RetrofitManager.getInstance().getUserService().updatePhoto(photo, new Callback<UserResponse>() {
                                        @Override
                                        public void success(UserResponse result, Response response) {
                                            if (result.isSuccess()) {
                                                showPicture();
                                                hideLoadingView();
                                                UserCacheManager.getInstance().updateCurrentProfileImage(result.getUser().getProfileImageUrl());
                                            } else {
                                                showErrorDialog(R.string.error_generic);
                                            }
                                        }

                                        @Override
                                        public void failure(RetrofitError error) {
                                            showError(error, R.string.error_generic);
                                        }
                                    });
                                }

                            }
                        });
                    }
                }});

        } else {
            showPicture();
        }
    }

    private void resizeImage(ResizeImageAsyncTask.ResizeImageListener resizeImageListener) {
        new ResizeImageAsyncTask(getActivity(), filePath, resizeImageListener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private void showPicture() {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                Uri uriFile = Uri.fromFile(new File(filePath));
                MoovUpApplication.imageLoaderUser.displayImage(Uri.decode(uriFile.toString()), imgPhoto, new ImageLoadingListener() {
                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                        imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
                    }

                    @Override
                    public void onLoadingStarted(String imageUri, View view) {

                    }
                });
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (mEdit) {
            inflater.inflate(R.menu.menu_edit_profile, menu);
        } else {
            inflater.inflate(R.menu.menu_clear, menu);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_change_password) {
            showDialogUpdatePassword("", "");
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    private void showDialogUpdatePassword(final String password, String confPassword) {

        LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        View v = inflater.inflate(R.layout.dialog_update_password, null);

        final MaterialEditText edtPassword = (MaterialEditText)v.findViewById(R.id.txt_update_password);
        final MaterialEditText edtPasswordconf = (MaterialEditText)v.findViewById(R.id.txt_update_confirm_password);

        if (!TextUtils.isEmpty(password)) {
            edtPassword.setText(password);
        }
        if (!TextUtils.isEmpty(confPassword)) {
            edtPasswordconf.setText(confPassword);
        }

        Drawable icon = getActivity().getResources().getDrawable(R.drawable.ic_update_password);
        DrawableCompat.setTint(icon, getResources().getColor(R.color.colorAccent));

        new MaterialDialog.Builder(getActivity())
                .title(R.string.change_password)
                .customView(v, false)
                .positiveText(R.string.save)
                .negativeText(R.string.cancel)
                .cancelable(false)
                .autoDismiss(false)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (passwordIsValid(edtPassword, edtPasswordconf)) {
                            materialDialog.dismiss();
                            updatePassword(edtPassword.getText().toString(), edtPasswordconf.getText().toString());
                        }
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        materialDialog.dismiss();
                    }
                })
                .icon(icon)
                .show();
    }

    private boolean passwordIsValid(MaterialEditText edtPassword, MaterialEditText edtConfPassword) {
        boolean result = true;
        String regex = "(?=.*[a-zA-Z])(?=.*[\\d]).+";
        if (TextUtils.isEmpty(edtPassword.getText())) {
            edtPassword.setError(getString(R.string.required_field));
            result = false;
        } else if (!edtPassword.getText().toString().matches(regex)) {
            result = false;
            edtPassword.setError(getString(R.string.invalid_password));
        }

        if (TextUtils.isEmpty(edtConfPassword.getText())) {
            edtConfPassword.setError(getString(R.string.required_field));
            result = false;
        } else if (!edtPassword.getText().toString().equals(edtConfPassword.getText().toString())) {
            edtConfPassword.setError(getString(R.string.passwords_dont_match));
            result = false;
        }
        return result;
    }

    private void updatePassword(final String password, final String confPassword) {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().updatePassword(password, confPassword, new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                hideLoadingView();
                showSuccessDialog(getActivity(), R.string.msg_user_updated);
            }

            @Override
            public void failure(RetrofitError error) {
                showToastError(getActivity(), error);
                showDialogUpdatePassword(password, confPassword);
            }
        });
    }

    @Override
    public void onError(String s) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                imgPhoto.setImageResource(R.drawable.icn_photo_placeholder);
            }
        });
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        if (mEdit && !mProfileIncomplete) {
            return TOOLBAR_TYPE.DRAWER;
        } else {
            return TOOLBAR_TYPE.NOTHING;
        }
    }

    @Override
    protected String getToolbarTitle() {
        if (mEdit) {
            return getString(R.string.profile);
        } else {
            return "";
        }
    }
}