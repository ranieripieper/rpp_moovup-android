package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.model.response.UpdateQuestionResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/25/15.
 */
public class AddQuestionFragment extends BaseFragment {

    @Bind(R.id.rg_category)
    RadioGroup rgCategory;

    @NotEmpty(messageResId = R.string.required_field)
    @Length(min = 10, messageResId = R.string.error_question_nr_characters)
    @Bind(R.id.edt_question)
    EditText edtQuestion;

    @Bind(R.id.btn_add_question)
    Button btnAddQuestion;

    private Long mCategoryId;

    private Question mQuestionEdit;

    public static final String EXTRA_QUESTION_ADDED = "EXTRA_QUESTION_ADDED";
    public static final String EXTRA_QUESTION_UPDATED = "EXTRA_QUESTION_UPDATED";

    private static final String ARG_QUESTION_EDIT = "ARG_QUESTION_EDIT";

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_add_question;
    }

    public static AddQuestionFragment newInstance() {
        AddQuestionFragment fragment = new AddQuestionFragment();
        return fragment;
    }

    public static AddQuestionFragment newInstance(Question questionEdit) {
        AddQuestionFragment fragment = new AddQuestionFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_QUESTION_EDIT, questionEdit.getId());
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        if (getArguments() != null) {
            long questionId = getArguments().getLong(ARG_QUESTION_EDIT);
            mQuestionEdit = QuestionCacheManager.getInstance().get(questionId);
        }
    }

    private void initCategories() {
        if (CategoryCacheManager.getInstance().count() > 0) {
            renderCategories(CategoryCacheManager.getInstance().getCategoryForQuestions());
        } else {
            refreshCategories();
        }
    }

    private void refreshCategories() {
        showLoadingView();
        RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE, new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse categoryResponse, Response response) {

                if (categoryResponse != null && categoryResponse.getCategories() != null) {
                    CategoryCacheManager.getInstance().putAll(categoryResponse.getCategories());
                }
                renderCategories(CategoryCacheManager.getInstance().getCategoryForQuestions());
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, true);
            }
        });

    }
    private void renderCategories(List<Category> categories) {
        if (categories != null) {
            boolean select = true;
            for (Category category : categories) {
                RadioButton radioButton = createRadioButton(category);
                if (mQuestionEdit != null) {
                     if (mQuestionEdit.getCategoryId() == category.getId()) {
                         radioButton.setChecked(true);
                         select = false;
                         mCategoryId = category.getId();
                     }
                }
                rgCategory.addView(radioButton);
            }
        }
        rgCategory.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                mCategoryId = Long.valueOf(checkedId);
            }
        });
    }

    private RadioButton createRadioButton(Category category) {
        RadioButton radioButton = new RadioButton(getActivity());
        radioButton.setText(category.getTitle());
        radioButton.setTextAppearance(getActivity(), android.R.style.TextAppearance_Medium);
        radioButton.setTextColor(getResources().getColor(R.color.dark_gray));
        radioButton.setId(Long.valueOf(category.getId()).intValue());
        return radioButton;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initCategories();

        if (mQuestionEdit != null) {
            edtQuestion.setText(mQuestionEdit.getBodyText());
            rgCategory.setEnabled(false);
            btnAddQuestion.setText(R.string.update);
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.new_question);
    }

    @OnClick(R.id.btn_add_question)
    public void addQuestionClick(View v) {
        hideKeyboard();
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                if (mCategoryId == null) {
                    showToastError(getContext(), getString(R.string.msg_select_category));
                } else {
                    callServiceAddQuestion();
                }
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }

    private void callServiceAddQuestion() {
        showLoadingView();

        if (mQuestionEdit != null) {
            RetrofitManager.getInstance().getQuestionService().updateQuestion(mQuestionEdit.getId(), edtQuestion.getText().toString(), mCategoryId, new Callback<UpdateQuestionResponse>() {
                @Override
                public void success(UpdateQuestionResponse updateQuestionResponse, Response response) {
                    hideLoadingView();
                    if (updateQuestionResponse != null && updateQuestionResponse.isSuccess()) {
                        questionUpdated(updateQuestionResponse.getQuestion());
                    } else {
                        showErrorDialog(R.string.error_generic);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, R.string.error_generic, true);
                }
            });
        } else {
            RetrofitManager.getInstance().getQuestionService().addNewQuestion(edtQuestion.getText().toString(), mCategoryId, new Callback<UpdateQuestionResponse>() {
                @Override
                public void success(UpdateQuestionResponse updateQuestionResponse, Response response) {
                    hideLoadingView();
                    if (updateQuestionResponse != null && updateQuestionResponse.isSuccess()) {
                        questionAdded(updateQuestionResponse.getQuestion());
                    } else {
                        showErrorDialog(R.string.error_generic);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, R.string.error_generic, true);
                }
            });
        }
    }

    private void questionAdded(Question question) {
        QuestionCacheManager.getInstance().put(question);
        finish();
        Intent i = new Intent();
        i.putExtra(EXTRA_QUESTION_ADDED, question.getId());
        getTargetFragment().onActivityResult(RequestCodeUtil.REQUEST_CODE_ADD_QUESTION, Activity.RESULT_OK, i);

    }

    private void questionUpdated(Question question) {
        QuestionCacheManager.getInstance().put(question);
        finish();
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
