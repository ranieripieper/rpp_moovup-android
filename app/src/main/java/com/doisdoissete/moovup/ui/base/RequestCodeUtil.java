package com.doisdoissete.moovup.ui.base;

/**
 * Created by ranieripieper on 12/2/15.
 */
public class RequestCodeUtil {

    public static final int REQUEST_CODE_ADD_QUESTION = 100;
    public static final int REQUEST_CODE_EDIT_QUESTION = 101;
    public static final int REQUEST_CODE_QUESTION_REMOVED = 102;
    public static final int REQUEST_CODE_ANSWER_QUESTION = 103;
    public static final int REQUEST_CODE_ADD_GOAL_ACTIVITY = 104;
    public static final int REQUEST_CODE_REMOVE_GOAL = 105;
    public static final int REQUEST_CODE_ADD_GOAL = 106;

}
