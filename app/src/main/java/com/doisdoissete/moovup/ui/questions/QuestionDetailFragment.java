package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.AnswerCacheManager;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.AnswersResponse;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.QuestionResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.net.interfaces.ReportService;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.helper.AnswerHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.questions.adapter.AnswerViewHolder;
import com.doisdoissete.moovup.util.Constants;
import com.doisdoissete.moovup.util.FunctionsUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 11/13/15.
 */
public class QuestionDetailFragment extends BaseFragment implements AnswerViewHolder.AnswerHolderListener {

    private static final String ARG_QUESTION = "arg_question";
    private Question mQuestion;
    private long mQuestionId;

    @Bind(R.id.recycler_answer)
    public EasyLoadMoreRecyclerView mRecyclerView;

    private View mQuestionView;

    protected Integer mPage = 0;

    protected EasyLoadMoreRecyclerAdapter<Answers> answerAdapter;

    public static final String EXTRA_QUESTION_REMOVED = "EXTRA_QUESTION_REMOVED";

    public static QuestionDetailFragment newInstance(Question question) {
        Bundle args = new Bundle();
        args.putLong(ARG_QUESTION, question.getId());

        QuestionDetailFragment fragment = new QuestionDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public static QuestionDetailFragment newInstance(Long questionId) {
        Bundle args = new Bundle();
        args.putLong(ARG_QUESTION, questionId);
        QuestionDetailFragment fragment = new QuestionDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);

        if (getArguments() != null) {
            mQuestionId = getArguments().getLong(ARG_QUESTION);
            mQuestion = QuestionCacheManager.getInstance().get(mQuestionId);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        if (view != null) {
            view.setOnTouchListener(new View.OnTouchListener() {
                public boolean onTouch(View v, MotionEvent event) {
                    return true;
                }
            });
        }
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       // if (mQuestion == null) {
            getQuestion();
       // } else {
          //  renderQuestion();
        //}
    }

    private void getQuestion() {
        showLoadingView();
        RetrofitManager.getInstance().getQuestionService().getQuestionById(mQuestionId, new Callback<QuestionResponse>() {
            @Override
            public void success(QuestionResponse questionResponse, Response response) {
                processResponse(questionResponse);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        getQuestion();
                    }

                    @Override
                    public void negativeButton() {
                        finish();
                    }
                });
            }
        });
    }

    protected void processResponse(QuestionResponse questionsResponse) {
        if (!isAdded()) {
            return;
        }

        hideLoadingView();
        hideKeyboard();
        if (questionsResponse.getQuestion() != null) {
            QuestionCacheManager.getInstance().put(questionsResponse.getQuestion());

            if (questionsResponse.getLinkedData() != null) {
                UserCacheManager.getInstance().updateWithoutNulls(questionsResponse.getLinkedData().getUsers());
                CategoryCacheManager.getInstance().updateWithoutNulls(questionsResponse.getLinkedData().getCategories());
            }
            mQuestion = questionsResponse.getQuestion();
            renderQuestion();
        }
    }

    @Override
    protected String getToolbarTitle() {
        return "";
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (mQuestion != null) {
            inflater.inflate(R.menu.menu_question_details, menu);

            Drawable drawableFavorite = null;
            Drawable drawableShare = menu.findItem(R.id.menu_question_share).getIcon();

            drawableShare = DrawableCompat.wrap(drawableShare);

            DrawableCompat.setTint(drawableShare, getResources().getColor(R.color.white));

            menu.findItem(R.id.menu_question_share).setIcon(drawableShare);

            if (mQuestion.isFavorited()) {
                drawableFavorite = getResources().getDrawable(R.drawable.ic_favorite_24dp);
            } else {
                drawableFavorite = getResources().getDrawable(R.drawable.ic_favorite);
            }
            DrawableCompat.setTint(drawableFavorite, getResources().getColor(R.color.white));
            menu.findItem(R.id.menu_question_favorite).setIcon(drawableFavorite);
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_question_favorite) {
            callFavoriteQuestion();
            return true;
        } else if (item.getItemId() == R.id.menu_question_share) {
            FunctionsUtil.shareUrl(getContext(), getShareTextUrl());
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private String getShareTextUrl() {
        if (mQuestion != null && mQuestion.getId() > 0 && mQuestion.getBodyText() != null) {
            return String.format("%s %s", mQuestion.getBodyText(), String.format(BuildConfig.SHARE_QUESTION_URL, String.format("%d-%s", mQuestion.getId(), FunctionsUtil.getSlugString(mQuestion.getBodyText()))));
        } else {
            return Constants.SITE_MOOVUP_URL;
        }
    }

    private void callFavoriteQuestion() {
        showLoadingView();
        if (!mQuestion.isFavorited()) {
            RetrofitManager.getInstance().getQuestionService().favoriteQuestion(mQuestion.getId(), "", new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    if (baseResponse.isSuccess()) {
                        showSuccessDialog(getContext(), R.string.msg_favorite_success);
                        mQuestion = QuestionCacheManager.getInstance().updateFavorited(mQuestion.getId(), true);
                    }
                    getActivity().invalidateOptionsMenu();
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getQuestionService().unfavoriteQuestion(mQuestion.getId(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    if (baseResponse.isSuccess()) {
                        showSuccessDialog(getContext(), R.string.msg_unfavorite_success);
                        mQuestion = QuestionCacheManager.getInstance().updateFavorited(mQuestion.getId(), false);
                    }
                    getActivity().invalidateOptionsMenu();
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_question_details;
    }

    private TextView lblNrAnswer;

    public void renderQuestion() {

        if (isAdded()) {
            mQuestionView = LayoutInflater.from(getContext()).inflate(R.layout.include_header_question, null);

            updateQuestionView();

            initLoadAnswers(mQuestionView, mQuestion.getAnswersCount());

            mQuestionView.findViewById(R.id.img_question_more).setOnClickListener(onBtMoreQuestionClicked);

            if (getActivity() != null) {
                getActivity().invalidateOptionsMenu();
            }
        }

    }

    private void updateQuestionView() {
        if (!isAdded()) {
            return;
        }
        mQuestion = QuestionCacheManager.getInstance().get(mQuestionId);

        if (mQuestion != null) {
            if (mQuestionView == null) {
                renderQuestion();
            }
            CircleImageView imgPhoto = (CircleImageView) mQuestionView.findViewById(R.id.img_photo);
            TextView lblName = (TextView) mQuestionView.findViewById(R.id.lbl_question_name);
            TextView lblDate = (TextView) mQuestionView.findViewById(R.id.lbl_question_date);
            TextView lblCategory = (TextView) mQuestionView.findViewById(R.id.lbl_question_category);
            TextView lblContent = (TextView) mQuestionView.findViewById(R.id.lbl_question_content);

            lblNrAnswer = (TextView) mQuestionView.findViewById(R.id.lbl_question_answer_count);
            TextView lblNoAnswer = (TextView) mQuestionView.findViewById(R.id.lbl_no_answer);

            if (mQuestion != null) {
                final User user = UserCacheManager.getInstance().get(mQuestion.getUserId());
                ViewUtil.displayUserImage(user, imgPhoto);
                if (user != null) {
                    lblName.setText(user.getFullName());
                } else {
                    lblName.setText("");
                }

                Category category = CategoryCacheManager.getInstance().get(mQuestion.getCategoryId());
                if (category != null) {
                    lblCategory.setText(category.getTitle());
                }

                lblContent.setText(mQuestion.getBodyText());

                lblDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(mQuestion.getCreatedAt()));

                updateNrAnswers(mQuestion.getAnswersCount());

                if (mQuestion.getAnswersCount() == 0) {
                    lblNoAnswer.setVisibility(View.VISIBLE);
                } else {
                    lblNoAnswer.setVisibility(View.GONE);
                }

                imgPhoto.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (user != null && user.getId() > 0) {
                            Navigator.navigateToUserGoalsFragment(getContext(), QuestionDetailFragment.this, user.getId());
                        }
                    }
                });
            }
        }
    }

    private void updateNrAnswers(long count) {
        if (isAdded()) {
            if (count == 1) {
                lblNrAnswer.setText(getString(R.string.one_answer));
            } else {
                lblNrAnswer.setText(getString(R.string.nr_answer, count));
            }
        }
    }

    private void initLoadAnswers(View header, long count) {
        mPage = 0;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        answerAdapter = new EasyLoadMoreRecyclerAdapter<>(
                getActivity(),
                AnswerViewHolder.class,
                new ArrayList<Answers>(),
                this,
                R.layout.loading_recycler_view,
                header);
        mRecyclerView.setAdapter(answerAdapter);
        mRecyclerView.setVisibility(View.VISIBLE);

        //if (count > 0) {
        mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void loadMore() {
                mPage++;
                loadAnswers();
            }
        });
        mRecyclerView.callLoadMore();
        //}
        answerAdapter.notifyDataSetChanged();
    }

    private void loadAnswers() {
        RetrofitManager.getInstance().getQuestionService().getAnswers(mQuestion.getId(), mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<AnswersResponse>() {
            @Override
            public void success(AnswersResponse answersResponse, Response response) {
                if (isAdded()) {
                    processResponse(answersResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
                disableLoadingMore(mRecyclerView);
            }
        });
    }

    protected void processResponse(AnswersResponse answersResponse) {
        hideLoadingView();
        if (answersResponse.getAnswers() != null) {

            if (answersResponse.getLinkedData() != null) {
                UserCacheManager.getInstance().updateWithoutNulls(answersResponse.getLinkedData().getUsers());
            }

            AnswerCacheManager.getInstance().putAll(answersResponse.getAnswers());

            disableLoadingMore(mRecyclerView, answersResponse.getMeta(), answersResponse.getAnswers().size());

            if (answersResponse.getMeta() != null && answersResponse.getMeta().getPagination() != null) {
                updateNrAnswers(answersResponse.getMeta().getPagination().getTotalCount());
            }
            renderAnswers(answersResponse.getAnswers());

        } else {
            disableLoadingMore(mRecyclerView);
        }
    }

    public void renderAnswers(List<Answers> answerList) {
        answerAdapter.addItems(answerList);
    }

    @Override
    public void onAnswerSelected(Answers question) {
    }

    @Override
    public void onPhotoUserClicked(Long userId) {
        Navigator.navigateToUserGoalsFragment(getContext(), this, userId);
    }

    @Override
    public void onBtMoreClicked(final Answers answer) {

        String[] options = getResources().getStringArray(R.array.answer_menu_not_current_user_without_image);

        String imageUrl = AnswerHelper.getImageUrl(answer);

        if (answer.getUserId() != null && answer.getUserId().equals(getCurrentUser().getId())) {
            if (!TextUtils.isEmpty(imageUrl)) {
                options = getResources().getStringArray(R.array.answer_menu_current_user_with_image);
            } else {
                options = getResources().getStringArray(R.array.answer_menu_current_user_without_image);
            }
        } else if (!TextUtils.isEmpty(imageUrl)) {
            options = getResources().getStringArray(R.array.answer_menu_not_current_user_with_image);
        }

        ViewUtil.showOptionsDialog(getContext(), options, new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                processMenuSelect(mQuestion, answer, charSequence.toString());
            }
        });
    }

    @Override
    public void onAnswerPhotoClicked(Answers answer) {
        viewImage(answer);
    }

    @Override
    public void onCommentsClicked(Answers answer) {
        Navigator.navigateToCommentsFragment(getContext(), this, mQuestion, answer);
    }

    @Override
    public void onHelpedClicked(final Answers answer) {
        showLoadingView();
        if (answer.isLiked()) {
            RetrofitManager.getInstance().getQuestionService().downvoteQuestion(mQuestion.getId(), answer.getId(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateHelpedAnswer(answer, false);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getQuestionService().upvoteAnswer(mQuestion.getId(), answer.getId(), "", new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                    updateHelpedAnswer(answer, true);
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error);
                }
            });
        }
    }

    private void updateHelpedAnswer(Answers answer, boolean upvoted) {
        AnswerCacheManager.getInstance().updateUpvoute(answer.getId(), upvoted);
        if (answerAdapter != null) {
            answerAdapter.notifyItemChanged(answer, new EasyLoadMoreRecyclerAdapter.CompareObject<Answers>() {
                @Override
                public boolean isEqual(Answers left, Answers right) {
                    if (left.getId().equals(right.getId())) {
                        return true;
                    }
                    return false;
                }
            });
        }
    }

    private View.OnClickListener onBtMoreQuestionClicked = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            String[] options = getResources().getStringArray(R.array.question_menu_not_current_user);
            if (mQuestion.getUserId() == getCurrentUser().getId()) {
                options = getResources().getStringArray(R.array.question_menu_current_user);
            }

            ViewUtil.showOptionsDialog(getContext(), options, new MaterialDialog.ListCallback() {
                @Override
                public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                    processMenuSelect(mQuestion, charSequence.toString());
                }
            });
        }
    };

    private void processMenuSelect(final Question question, final Answers answer, String menuSelected) {
        if (getString(R.string.delete_answer).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_delete_answer, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    deleteAnswer(question, answer);
                }
            });
        } else if (getString(R.string.report_answer).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_report_answer, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    reportAnswer(answer);
                }
            });
        } else if (getString(R.string.edit_answer).equalsIgnoreCase(menuSelected)) {
            editAnswer(answer);
        } else if (getString(R.string.view_image).equalsIgnoreCase(menuSelected)) {
            viewImage(answer);
        }
    }

    private void processMenuSelect(final Question question, String menuSelected) {
        if (getString(R.string.delete_question).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_delete_question, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    deleteQuestion(question);
                }
            });
        } else if (getString(R.string.report_question).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_report_question, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    reportQuestion(question);
                }
            });
        } else if (getString(R.string.edit_question).equalsIgnoreCase(menuSelected)) {
            editQuestion(question);
        } else if (getString(R.string.answer_question).equalsIgnoreCase(menuSelected)) {
            answerQuestion(question);
        }
    }

    private void answerQuestion(Question question) {
        if (question.getAnswersCount() > 0) {
            showErrorDialog(R.string.error_question_cant_answer);
        } else {
           Navigator.navigateToAnswerQuestionFragment(getContext(), this, question);
        }
    }

    private void viewImage(Answers answer) {
        if (isAdded()) {
            ViewUtil.showDialogViewImage(getActivity(), AnswerHelper.getImageUrl(answer), MoovUpApplication.imageLoader);
        }
    }

    private void deleteQuestion(final Question question) {
        showLoadingView();
        RetrofitManager.getInstance().getQuestionService().deleteQuestion(question.getId(), new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_delete_question_success, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        finish();
                        Intent i = new Intent();
                        i.putExtra(EXTRA_QUESTION_REMOVED, mQuestion.getId());
                        QuestionCacheManager.getInstance().delete(mQuestion.getId());
                        if (getTargetFragment() != null) {
                            getTargetFragment().onActivityResult(RequestCodeUtil.REQUEST_CODE_QUESTION_REMOVED, Activity.RESULT_OK, i);
                        }
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void reportQuestion(final Question question) {
        showLoadingView();
        RetrofitManager.getInstance().getReportService().reportQuestion(question.getId(), ReportService.REPORT_TYPE_INAPPROPRIATE_CONTENT, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_report_question_success);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void editQuestion(final Question question) {
        Navigator.navigateToEditQuestion(getContext(), this, question);
    }

    //answer actions
    private void deleteAnswer(final Question question, final Answers answer) {
        showLoadingView();
        RetrofitManager.getInstance().getQuestionService().deleteAnswer(question.getId(), answer.getId(), new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_delete_answer_success, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (answerAdapter != null) {
                            answerAdapter.removeItem(answer);
                            answerAdapter.notifyDataSetChanged();
                        }

                        AnswerCacheManager.getInstance().delete(question.getId(), answer.getId());
                        updateQuestionView();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void reportAnswer(final Answers answer) {
        showLoadingView();
        RetrofitManager.getInstance().getReportService().reportAnswers(answer.getId(), ReportService.REPORT_TYPE_INAPPROPRIATE_CONTENT, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_report_answer_success);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void editAnswer(final Answers answer) {
        Navigator.navigateToEditAnswerFragment(getActivity(), this, mQuestion, answer);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_EDIT_QUESTION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AddQuestionFragment.EXTRA_QUESTION_UPDATED)) {
                    Question questionUpdated = data.getParcelableExtra(AddQuestionFragment.EXTRA_QUESTION_UPDATED);
                    mQuestion = questionUpdated;
                    updateQuestionView();
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        mQuestion = QuestionCacheManager.getInstance().get(mQuestionId);
        updateQuestionView();
        if (isAdded() && answerAdapter != null) {
            answerAdapter.notifyDataSetChanged();
        }
        if (isAdded()) {
            if (mQuestion == null) {
                getQuestion();
            } else if (mQuestion.getAnswersCount() == 1 && (answerAdapter == null || (answerAdapter != null && answerAdapter.getItemCount() <= 1))) {
                mPage = 0;
                mRecyclerView.callLoadMore();
            }
        }
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
