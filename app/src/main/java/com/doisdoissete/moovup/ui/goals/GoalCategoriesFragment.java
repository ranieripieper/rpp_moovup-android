package com.doisdoissete.moovup.ui.goals;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.goals.adapter.CategoryViewHolder;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import uk.co.ribot.easyadapter.EasyRecyclerAdapter;

/**
 * Created by ranipieper on 11/17/15.
 */
public class GoalCategoriesFragment extends BaseFragment implements CategoryViewHolder.CategoryHolderListener {

    @Bind(R.id.layout_goal_categories_recycler_view)
    public RecyclerView mRecyclerView;

    @Bind(R.id.txt_title)
    public TextView mTxtTitle;

    private List<Category> mCategories;
    private Category mCategory;

    public static GoalCategoriesFragment newInstance(Category category, List<Category> categories) {
        Bundle args = new Bundle();

        GoalCategoriesFragment fragment = new GoalCategoriesFragment();
        fragment.setArguments(args);
        fragment.mCategories = categories;
        fragment.mCategory = category;
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mCategories == null || mCategory == null) {
            finish();
        } else {
            mTxtTitle.setText(mCategory.getTitle());
            renderCategories(mCategories);
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_categories;
    }

    private void renderCategories(List<Category> categoryList) {
        if (isAdded()) {
            if (categoryList == null || categoryList.isEmpty()) {
                getView().post(new Runnable() {
                    @Override
                    public void run() {
                        onCategoryClicked(mCategory);
                    }
                });
            } else {
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

                mRecyclerView.setLayoutManager(layoutManager);
                mRecyclerView.setHasFixedSize(true);

                EasyRecyclerAdapter adapter = new EasyRecyclerAdapter(
                        getActivity(),
                        CategoryViewHolder.class,
                        categoryList,
                        this);

                adapter.notifyDataSetChanged();
                mRecyclerView.setAdapter(adapter);

                mRecyclerView.setVisibility(View.VISIBLE);
            }
        }
    }

    @Override
    public void onCategoryClicked(Category category) {
        if (isAdded()) {
            Navigator.navigateToAddGoalFragment(getContext(), category);
        }
    }

    @OnClick(R.id.txt_skip_goal)
    public void onSkipGoalClick(View v) {
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

}
