package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.QuestionsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseActivity;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.questions.adapter.QuestionHolderListener;
import com.doisdoissete.moovup.ui.questions.adapter.QuestionViewHolder;
import com.doisdoissete.moovup.ui.search.SearchActivity;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by broto on 10/27/15.
 */
public class FeedQuestionFragment extends BaseFragment implements QuestionHolderListener {

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_feed)
    protected EasyLoadMoreRecyclerView mRecyclerView;

    @Bind(R.id.txt_no_results)
    protected TextView txtNoResults;

    @Bind(R.id.fab_add_question)
    protected FloatingActionButton mFabAddQuestion;

    protected Integer mPage = 1;

    protected EasyLoadMoreRecyclerAdapter<Question> questionAdapter;

    private MenuItem mSearchMenu;

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_questions_feed;
    }

    public static FeedQuestionFragment newInstance() {
        return new FeedQuestionFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSwipeRefresh();
        txtNoResults.setText(R.string.no_result_search_questions);
        if (showAddQuestion()) {
            mFabAddQuestion.setVisibility(View.VISIBLE);
        } else {
            mFabAddQuestion.setVisibility(View.GONE);
        }
    }


    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.menu_feed, menu);

        mSearchMenu = menu.findItem(R.id.menu_feed_search);

        Drawable drawable = mSearchMenu.getIcon();
        drawable = DrawableCompat.wrap(drawable);

        DrawableCompat.setTint(drawable, getResources().getColor(R.color.white));

        mSearchMenu.setIcon(drawable);

        // Get the SearchView and set the searchable configuration
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);

        final SearchView searchView = (SearchView) mSearchMenu.getActionView();
        ViewUtil.configSearchView((BaseActivity) getActivity(), searchManager, searchView, SearchActivity.QUESTION_SEARCH_TYPE);
        searchView.setQueryHint(getString(R.string.search_question_hint));

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    private void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });

        loadData();
    }

    public void renderQuestion(List<Question> questionList) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (questionAdapter == null) {
            createAdapter(questionList);
        } else {
            questionAdapter.addItems(questionList);
        }
    }

    protected void createAdapter(List<Question> questionList) {
        questionAdapter = new EasyLoadMoreRecyclerAdapter<>(
                getActivity(),
                QuestionViewHolder.class,
                questionList,
                this,
                R.layout.loading_recycler_view);
        mRecyclerView.setAdapter(questionAdapter);
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
        txtNoResults.setVisibility(View.GONE);
    }

    private void loadData() {
        if (mPage == 1) {
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    mPage++;
                    loadData();
                }
            });
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
            showLoadingView();
            if (questionAdapter != null) {
                questionAdapter.removeAllItems();
            }
        }
        callService();
    }

    protected void callService() {
        RetrofitManager.getInstance().getQuestionService().getQuestionsFeed(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<QuestionsResponse>() {
            @Override
            public void success(QuestionsResponse questionsResponse, Response response) {
                if (isAdded()) {
                    processResponse(questionsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void processResponse(QuestionsResponse questionsResponse) {
        if (!isAdded()) {
            return;
        }
        hideLoadingView();
        hideKeyboard();
        if (questionsResponse.getQuestions() != null && questionsResponse.getQuestions().size() > 0) {
            QuestionCacheManager.getInstance().putAll(questionsResponse.getQuestions());

            if (questionsResponse.getLinkedData() != null) {
                UserCacheManager.getInstance().updateWithoutNulls(questionsResponse.getLinkedData().getUsers());
                CategoryCacheManager.getInstance().updateWithoutNulls(questionsResponse.getLinkedData().getCategories());
            }

            disableLoadingMore(mRecyclerView, questionsResponse.getMeta(), questionsResponse.getQuestions().size());
            renderQuestion(questionsResponse.getQuestions());
            mRecyclerView.setVisibility(View.VISIBLE);
            txtNoResults.setVisibility(View.GONE);
        } else {
            showEmptyQuestions();
        }
    }

    protected void showEmptyQuestions() {
        disableLoadingMore(mRecyclerView);
        if (mPage <= 1) {
            txtNoResults.setVisibility(View.VISIBLE);
        }
        hideLoadingView();
    }

    @OnClick(R.id.fab_add_question)
    public void addQuestionClick() {
        Navigator.navigateToAddQuestion(getContext(), this);
    }

    @Override
    public void onQuestionSelected(Question question) {
        if (question != null) {
            Navigator.navigateToQuestionDetailsFragment(getContext(), this, question);
        }
        hideKeyboard();
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.app_name);
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_ADD_QUESTION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED)) {
                    long questionId = data.getLongExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED, -1);
                    onQuestionSelected(QuestionCacheManager.getInstance().get(questionId));
                }
            }
        } else if (requestCode == RequestCodeUtil.REQUEST_CODE_QUESTION_REMOVED) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(QuestionDetailFragment.EXTRA_QUESTION_REMOVED)) {
                    long questionId = data.getLongExtra(QuestionDetailFragment.EXTRA_QUESTION_REMOVED, -1);
                    removeQuestion(questionId);
                }
                if (questionAdapter != null && questionAdapter.getItemCount() <= 0) {
                    mRecyclerView.setVisibility(View.INVISIBLE);
                    txtNoResults.setVisibility(View.VISIBLE);
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void removeQuestion(long questionId) {
        if (questionAdapter != null && questionId > 0 && questionAdapter.getItemCount() > 0) {
            Question questionToRemove = null;
            int position = -1;
            for (int i = 0 ; i < questionAdapter.getItemCount(); i++) {
                Question questionTmp = questionAdapter.getItem(i);
                if (questionId == questionTmp.getId()) {
                    questionToRemove = questionTmp;
                    position = i;
                    break;
                }
            }
            if (questionToRemove != null) {
                questionAdapter.removeItem(questionToRemove);
            }
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();

        if (mSearchMenu != null) {
            MenuItemCompat.collapseActionView(mSearchMenu);
        }

        if (questionAdapter != null) {
            if (questionAdapter.getItemCount() <= 0) {
                mPage = 1;
                showEmptyQuestions();
            }
            questionAdapter.notifyDataSetChanged();
        }
    }

    protected boolean showAddQuestion() {
        return true;
    }
}
