package com.doisdoissete.moovup.ui.helper;

import android.text.TextUtils;

import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.util.Constants;

/**
 * Created by ranipieper on 12/17/15.
 */
public class UserHelper {

    public static String getImageUrl(User user) {
        String imageUrl = null;

        if (user != null) {
            if (!TextUtils.isEmpty(user.getProfileImageUrl())) {
                imageUrl = user.getProfileImageUrl();
            } else if (user.getProfileImages() != null && !TextUtils.isEmpty(user.getProfileImages().getThumb())) {
                imageUrl = user.getProfileImages().getThumb();
            } else if (user.getProfileImages() != null && !TextUtils.isEmpty(user.getProfileImages().getMedium())) {
                imageUrl = user.getProfileImages().getMedium();
            }
        }
        return imageUrl;
    }

    public static boolean profileIsComplete(User user) {
        if (user.getBirthdayDate() == null) {
            return false;
        }
        if (User.PERSON_PROFILE_TYPE.equalsIgnoreCase(user.getProfileType())) {
            if (user.getHobby() == null && user.getHobbyId() == null) {
                return false;
            } else if (user.getCarrer() == null && user.getCarrerId() == null) {
                return false;
            }
        } else if (User.COMPANY_PROFILE_TYPE.equalsIgnoreCase(user.getProfileType())) {
            if (user.getCommercialActivity() == null && user.getCommercialActivityId() == null) {
                return false;
            }
        }

        return true;
    }

    public static String getHobbyImageUrl(User user) {
        if (user.getHobbyId() != null) {
            return String.format(Constants.URL_CATEGORY_PHOTOS, user.getHobbyId());
        } else if (user.getHobby() != null && user.getHobby().getId() > 0) {
            return String.format(Constants.URL_CATEGORY_PHOTOS, user.getHobby().getId());
        }

        return "";
    }
}
