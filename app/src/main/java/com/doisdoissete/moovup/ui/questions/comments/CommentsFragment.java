package com.doisdoissete.moovup.ui.questions.comments;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.AnswerCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.AnswerResponse;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.CommentsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.net.interfaces.ReportService;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.questions.comments.adapter.CommentViewHolder;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 12/14/15.
 */
public class CommentsFragment extends BaseFragment implements CommentViewHolder.CommentViewHolderListener {

    private static final String ARG_QUESTION = "ARG_QUESTION";
    private static final String ARG_ANSWER = "ARG_ANSWER";

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_comments)
    EasyLoadMoreRecyclerView mRecyclerView;

    @Bind(R.id.edt_comment)
    @NotEmpty(messageResId = R.string.required_field)
    EditText edtComment;

    protected Integer mPage = 1;

    protected EasyLoadMoreRecyclerAdapter<Answers> mCommentsAdapter;

    private Question mQuestion;
    private Answers mAnswer;

    long mQuestionId = -1;
    long mAnswerId = -1;

    public static CommentsFragment newInstance(Question question, Answers answer) {

        Bundle args = new Bundle();
        args.putLong(ARG_QUESTION, question.getId());
        args.putLong(ARG_ANSWER, answer.getId());
        CommentsFragment fragment = new CommentsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(false);

        if (getArguments() != null) {
            mQuestionId = getArguments().getLong(ARG_QUESTION);
            mQuestion = QuestionCacheManager.getInstance().get(mQuestionId);
            mAnswerId = getArguments().getLong(ARG_ANSWER);
            mAnswer = AnswerCacheManager.getInstance().get(mAnswerId);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initSwipeRefresh();
    }

    private void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });

        loadData();

        mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
            @Override
            public void loadMore() {
                mPage++;
                loadData();
            }
        });
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
    }

    private void loadData() {
        if (mPage == 1) {
            showLoadingView();
            if (mCommentsAdapter != null) {
                mCommentsAdapter.removeAllItems();
            }
            mRecyclerView.setVisibility(View.GONE);
        }
        if (mQuestion == null) {
            mQuestion = QuestionCacheManager.getInstance().get(mQuestionId);

        }
        if (mAnswer == null) {
            mAnswer = AnswerCacheManager.getInstance().get(mAnswerId);
        }

        if (mQuestion != null && mAnswer != null){
            RetrofitManager.getInstance().getCommentService().getComments(mQuestion.getId(), mAnswer.getId(), mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<CommentsResponse>() {
                @Override
                public void success(CommentsResponse commentsResponse, Response response) {
                    if (isAdded()) {
                        processResponse(commentsResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isAdded()) {
                        showError(error);
                        disableLoadingMore(mRecyclerView);
                    }
                }
            });
        } else {
            finish();
        }
    }

    private void processResponse(CommentsResponse commentsResponse) {
        hideLoadingView();
        if (commentsResponse.getComments() != null && commentsResponse.getComments().size() > 0) {
            if (commentsResponse.getLinkedData() != null) {
                if (commentsResponse.getLinkedData() != null) {
                    UserCacheManager.getInstance().updateWithoutNulls(commentsResponse.getLinkedData().getUsers());
                }
            }

            AnswerCacheManager.getInstance().putAll(commentsResponse.getComments());

            disableLoadingMore(mRecyclerView, commentsResponse.getMeta(), commentsResponse.getComments().size());
            renderComments(commentsResponse.getComments());
        } else {
            disableLoadingMore(mRecyclerView);
            if (mPage <= 1) {
                if (isAdded()) {
                    mRecyclerView.setVisibility(View.GONE);
                }
            }
        }
    }

    private void renderComments(List<Answers> commentList) {
        if (isAdded()) {
            mRecyclerView.setVisibility(View.VISIBLE);
            if (mSwipeRefreshLayout.isRefreshing()) {
                mSwipeRefreshLayout.setRefreshing(false);
            }

            if (mCommentsAdapter == null) {
                createAdapter(commentList);
            } else {
                mCommentsAdapter.addItems(commentList);
            }
        }
    }

    private void createAdapter(List<Answers> commentList) {
        mCommentsAdapter = new EasyLoadMoreRecyclerAdapter<>(
                getActivity(),
                CommentViewHolder.class,
                commentList,
                this,
                R.layout.loading_recycler_view);
        if (isAdded() && mRecyclerView != null) {
            mRecyclerView.setAdapter(mCommentsAdapter);
        }

    }

    @Override
    public void onBtMoreClicked(final Answers comment) {
        String[] options = getResources().getStringArray(R.array.comment_menu_not_current_user);

        if (comment.getUserId() != null && comment.getUserId().equals(getCurrentUser().getId())) {
            options = getResources().getStringArray(R.array.comment_menu_current_user);
        }

        ViewUtil.showOptionsDialog(getContext(), options, new MaterialDialog.ListCallback() {
            @Override
            public void onSelection(MaterialDialog materialDialog, View view, int i, CharSequence charSequence) {
                processMenuSelect(comment, charSequence.toString());
            }
        });
    }

    private void processMenuSelect(final Answers comment, String menuSelected) {
        if (getString(R.string.remove_comment).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_delete_comment, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    deleteComment(comment);
                }
            });
        } else if (getString(R.string.report_comment).equalsIgnoreCase(menuSelected)) {
            ViewUtil.showConfirmDialog(getContext(), menuSelected, R.string.msg_report_comment, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    reportComment(comment);
                }
            });
        }
    }

    private void deleteComment(final Answers comment) {
        showLoadingView();
        RetrofitManager.getInstance().getQuestionService().deleteAnswer(mQuestion.getId(), comment.getId(), new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_delete_comment_success, new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        if (mCommentsAdapter != null) {
                            mCommentsAdapter.removeItem(comment);
                            mCommentsAdapter.notifyDataSetChanged();

                            AnswerCacheManager.getInstance().deleteComment(mAnswer.getId(), comment.getId());
                        }
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    private void reportComment(final Answers comment) {
        showLoadingView();
        RetrofitManager.getInstance().getReportService().reportAnswers(comment.getId(), ReportService.REPORT_TYPE_INAPPROPRIATE_CONTENT, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.msg_report_comment_success);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    @OnClick(R.id.bt_send)
    void onSendCommentClick() {

        hideKeyboard();
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                callServiceAddComment();
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getActivity());

                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }

    private void callServiceAddComment() {
        showLoadingView();
        RetrofitManager.getInstance().getCommentService().addComment(mQuestion.getId(), mAnswer.getId(), edtComment.getText().toString(), new Callback<AnswerResponse>() {
            @Override
            public void success(AnswerResponse answerResponse, Response response) {
                if (mCommentsAdapter == null) {
                    createAdapter(new ArrayList<Answers>());
                }
                answerResponse.getAnswer().setUserId(getCurrentUser().getId());
                mCommentsAdapter.addItem(answerResponse.getAnswer());
                hideLoadingView();
                edtComment.setText("");
                mRecyclerView.setVisibility(View.VISIBLE);

                //update answer
                AnswerCacheManager.getInstance().putComment(mAnswer.getId(), answerResponse.getAnswer());
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error);
            }
        });
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_comments;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.comments);
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
