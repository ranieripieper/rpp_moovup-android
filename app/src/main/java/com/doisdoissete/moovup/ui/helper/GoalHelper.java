package com.doisdoissete.moovup.ui.helper;

import android.content.Context;
import android.text.TextUtils;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.GoalActivityReport;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.service.util.NumberUtil;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalHelper {

    public static final String PERIODICITY_TYPE_DAY = "day";
    public static final String PERIODICITY_TYPE_WEEK = "week";
    public static final String PERIODICITY_TYPE_MONTH = "month";

    public static String getValue(Float value) {
        if (value == null) {
            return "0";
        }
        Float tmp = Float.valueOf(value.intValue());

        if (value.floatValue() != tmp.floatValue()) {
            return String.valueOf(value);
        } else {
            return String.valueOf(value.intValue());
        }
    }

    public static String getBarXVal(Context ctx, Goal goal, GoalActivityReport goalReport) {
        if (PERIODICITY_TYPE_DAY.equalsIgnoreCase(goal.getPeriodicityType())) {
            return DateUtil.DATE_DAY_MONTH_YEAR.get().format(goalReport.getDateInterval());
        } else if (PERIODICITY_TYPE_WEEK.equalsIgnoreCase(goal.getPeriodicityType())) {
            return ctx.getString(R.string.week_x, DateUtil.DATE_DAY_MONTH_YEAR.get().format(goalReport.getDateInterval()));
        } else if (PERIODICITY_TYPE_MONTH.equalsIgnoreCase(goal.getPeriodicityType())) {
            return DateUtil.DATE_MONTH_CHART.get().format(goalReport.getDateInterval());
        }

        return DateUtil.DATE_DAY_MONTH_YEAR.get().format(goalReport.getDateInterval());
    }

    public static String getLineXVal(GoalActivityReport goalReport) {
        return DateUtil.DATE_DAY_MONTH_YEAR.get().format(goalReport.getDateInterval());
    }

    public static String getGoalDescription(Context ctx, Goal goal) {
        String desc = "";
        Category category = null;
        if (goal.getCategory() != null) {
            category = goal.getCategory();
        } else if (goal.getCategoryId() > 0) {
            category = CategoryCacheManager.getInstance().get(goal.getCategoryId());
        }

        //goal common Ex.: Corrida 10km por dia
        //goal closed Ex.: Corrida 10km até 10/10/2015

        if (category != null) {
            String metric = category.getMeasurementType();
            if (category.getMeasurementMetricTexts() != null && !TextUtils.isEmpty(category.getMeasurementMetricTexts().getPlural())) {
                metric = category.getMeasurementMetricTexts().getPlural();
            }
            if (category.isClosedGoal()) {
                desc = ctx.getResources().getString(R.string.goal_closed,
                        GoalHelper.getValue(goal.getTarget()),
                        category == null ? "" : metric,
                        DateUtil.DATE_DAY_MONTH_YEAR.get().format(goal.getTargetDate()));
            } else {
                desc = ctx.getResources().getString(R.string.goal_common,
                        GoalHelper.getValue(goal.getTotal()),
                        category == null ? "" : metric,
                        gePeriodicityType(ctx, goal.getPeriodicityType()));
            }
        }

        return desc;

    }

    public static String getActivityDescription(Context ctx, GoalActivity goalActivity, Goal goal) {
        String desc = "";
        Category category = null;
        if (goal.getCategory() != null) {
            category = goal.getCategory();
        } else if (goal.getCategoryId() > 0) {
            category = CategoryCacheManager.getInstance().get(goal.getCategoryId());
        }

        //Ex.: 10 min no dia
        desc = ctx.getResources().getString(R.string.activity_desc,
                GoalHelper.getValue(goalActivity.getTotal()),
                category == null ? "" : category.getMeasurementMetricTexts().getPlural());

        return desc;

    }

    public static String gePeriodicityType(Context ctx, String periodicityType) {
        if (PERIODICITY_TYPE_DAY.equalsIgnoreCase(periodicityType)) {
            return ctx.getString(R.string.periodicity_type_goal_desc_day);
        } else if (PERIODICITY_TYPE_WEEK.equalsIgnoreCase(periodicityType)) {
            return ctx.getString(R.string.periodicity_type_goal_desc_week);
        } else if (PERIODICITY_TYPE_MONTH.equalsIgnoreCase(periodicityType)) {
            return ctx.getString(R.string.periodicity_type_goal_desc_month);
        }
        return "";
    }

    public static String getGoalActivityUpvote(GoalActivity goalActivity) {
        return NumberUtil.getNumberFormated(goalActivity.getUpvotesCount());
    }


    public static String getImageGoalActivityUrl(GoalActivity goalActivity) {
        String imageUrl = null;
        if (goalActivity.isHasUploadedImage() && goalActivity.getImagesUrl() != null) {
            if (!TextUtils.isEmpty(goalActivity.getImagesUrl().getMedium())) {
                imageUrl = goalActivity.getImagesUrl().getMedium();
            } else if (!TextUtils.isEmpty(goalActivity.getImagesUrl().getThumb())) {
                imageUrl = goalActivity.getImagesUrl().getThumb();
            }
        }
        return imageUrl;
    }

}
