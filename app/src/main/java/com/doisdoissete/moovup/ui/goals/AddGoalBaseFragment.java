package com.doisdoissete.moovup.ui.goals;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 2/18/16.
 */
public abstract class AddGoalBaseFragment extends BaseFragment implements Validator.ValidationListener {

    protected static final String ARG_CATEGORY = "arg_category";


    @Bind(R.id.lbl_share_with_friends)
    protected CheckedTextView lblShareGoalWithFriends;

    @Bind(R.id.btn_goal_delete)
    protected Button btnDeleteGoal;

    protected Category mCategory;

    protected GoalDetailFragment mGoalDetailFragment;

    //Usada na edição
    protected Goal mGoal;


    protected static Bundle getBundleToNewInstance(Goal goal) {
        Bundle bundle = new Bundle();
        if (goal.getCategory() != null) {
            bundle.putLong(ARG_CATEGORY, goal.getCategory().getId());
        } else if (goal.getCategoryId() > 0) {
            bundle.putLong(ARG_CATEGORY, goal.getCategoryId());
        }
        return bundle;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            long categoryId = getArguments().getLong(ARG_CATEGORY);
            mCategory = CategoryCacheManager.getInstance().get(categoryId);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mGoal != null) {
            if (mGoal.isShared()) {
                lblShareGoalWithFriends.setChecked(true);
            } else {
                lblShareGoalWithFriends.setChecked(false);
            }
            btnDeleteGoal.setVisibility(View.VISIBLE);
        } else {
            btnDeleteGoal.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_goal_submit)
    public void onSubmitClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(this);
        validator.validate();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(getActivity());
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            }
        }
    }

    @OnClick(R.id.lbl_share_with_friends)
    public void onShareGoalWithFriendsClick() {
        lblShareGoalWithFriends.setChecked(!lblShareGoalWithFriends.isChecked());
    }

    @OnClick(R.id.btn_goal_delete)
    public void onDeleteGoalClick() {
        ViewUtil.showConfirmDialog(getActivity(), R.string.dialog_title_delete_goal, R.string.dialog_msg_delete_goal, new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                showLoadingView();

                RetrofitManager.getInstance().getGoalService().deleteGoal(mGoal.getId(), new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                        hideLoadingView();
                        if (baseResponse.isSuccess()) {
                            showSuccessDialog(getActivity(), R.string.msg_goal_deleted);

                            if (mGoalDetailFragment != null) {
                                mGoalDetailFragment.deleteGoal();
                            }
                        } else {
                            showErrorDialog(R.string.error_generic);
                        }
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error);
                    }
                });
            }
        });
    }


    @Override
    protected String getToolbarTitle() {
        return getString(R.string.add_goal);
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    protected boolean needConfigToolbar() {
        if (mGoal != null) {
            return false;
        }
        return true;
    }
}
