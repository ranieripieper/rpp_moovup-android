package com.doisdoissete.moovup.ui.settings;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Interest;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.model.response.InterestResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.settings.adapter.InterestViewHolder;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyParallaxRecyclerAdapter;

/**
 * Created by broto on 10/30/15.
 */
public class InterestFragment extends BaseFragment implements InterestViewHolder.InterestHolderListener {

    @Bind(R.id.recycler_interest)
    public RecyclerView mRecyclerView;

    @Bind(R.id.btn_submit)
    public Button btSubmit;

    private boolean mMainMenu = false;

    private static final String EXTRA_MAIN_MENU = "EXTRA_MAIN_MENU";

    public static InterestFragment newInstance(boolean mainMenu) {
        Bundle args = new Bundle();
        args.putBoolean(EXTRA_MAIN_MENU, mainMenu);
        InterestFragment fragment = new InterestFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mMainMenu = getArguments().getBoolean(EXTRA_MAIN_MENU);
        }
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getCategories();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_config_interests;
    }

    private void getCategories() {
        showLoadingView();
        if (CategoryCacheManager.getInstance().count() <= 0) {
            RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE, new Callback<CategoryResponse>() {
                @Override
                public void success(CategoryResponse categoryResponse, Response response) {
                    if (isAdded()) {
                        CategoryCacheManager.getInstance().putAll(categoryResponse.getCategories());
                        hideLoadingView();
                        getMyInterest();
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    showError(error, R.string.error_generic, new ErrorListener() {
                        @Override
                        public void positiveButton() {
                            getCategories();
                        }

                        @Override
                        public void negativeButton() {
                            finish();
                        }
                    });
                }
            });
        } else {
            getMyInterest();
        }


    }

    private void getMyInterest() {
        showLoadingView();
        RetrofitManager.getInstance().getInterestsService().getMyInterests(new Callback<InterestResponse>() {
            @Override
            public void success(InterestResponse interestResponse, Response response) {
                if (isAdded()) {
                    renderScreen(interestResponse);
                    hideLoadingView();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        getCategories();
                    }

                    @Override
                    public void negativeButton() {
                        finish();
                    }
                });
            }
        });

    }

    private void renderScreen(InterestResponse interestResponse) {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        //recupera as categorias
        List<Category> categories = CategoryCacheManager.getInstance().getCategoryForInterests();

        List<Interest> lstInterest = new ArrayList<>();

        for (Category category : categories) {
            //busca categoria nos interesses
            Interest newInterest = null;
            if (interestResponse != null) {
                for (Interest it : interestResponse.getInterests()) {
                    if (it.getCategoryId() == category.getId()) {
                        newInterest = it;
                        newInterest.setActive(true);
                        break;
                    }
                }
            }

            if (newInterest == null) {
                newInterest = new Interest();
                newInterest.setActive(false);
                //interest.setQuestionCount(0);
                newInterest.setCategoryId(category.getId());
            }

            lstInterest.add(newInterest);
        }

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        EasyParallaxRecyclerAdapter adapter = new EasyParallaxRecyclerAdapter<>(
                getActivity(),
                InterestViewHolder.class,
                lstInterest,
                this);


        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.include_header_interested, mRecyclerView, false);

        adapter.setParallaxHeader(header, mRecyclerView);

        adapter.notifyDataSetChanged();
        mRecyclerView.setAdapter(adapter);

        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onInterestChanged(final Interest interest) {
        showLoadingView();
        if (interest.isActive()) {
            RetrofitManager.getInstance().getInterestsService().addInterest(interest.getCategoryId(), interest.getMaxAnswersCount(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    interest.setActive(!interest.isActive());
                    showError(error);
                }
            });
        } else {
            RetrofitManager.getInstance().getInterestsService().removeInterest(interest.getCategoryId(), new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                    hideLoadingView();
                }

                @Override
                public void failure(RetrofitError error) {
                    interest.setActive(!interest.isActive());
                    showError(error);
                }
            });
        }
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        if (mMainMenu) {
            return TOOLBAR_TYPE.DRAWER;
        } else {
            return TOOLBAR_TYPE.BACK_BUTTON;
        }
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.answers_options);
    }
}
