package com.doisdoissete.moovup.ui.questions.comments.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.ProfileImage;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_comment)
public class CommentViewHolder extends ItemViewHolder<Answers> {

    @ViewId(R.id.img_user_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_user_name)
    TextView lblName;

    @ViewId(R.id.lbl_content)
    TextView lblContent;

    @ViewId(R.id.bt_more)
    View btMore;

    private User mUser;

    public CommentViewHolder(View view) {
        super(view);
        mUser = UserCacheManager.getInstance().getCurrentUser();
    }

    @Override
    public void onSetValues(final Answers item, PositionInfo positionInfo) {

        Long userId = null;
        if (item.getUserId()!= null) {
            userId = item.getUserId();
        }
        if (userId != null) {
            User user = UserCacheManager.getInstance().get(item.getUserId());
            if (user != null) {
                ViewUtil.displayUserImage(user, imgPhoto);
                lblName.setText(user.getFullName());
            }
        }
        lblContent.setText(item.getBodyText());
    }

    private void displayImage(String profileImg, ProfileImage profileImage, ImageView imgPhoto) {
        if (!TextUtils.isEmpty(profileImg)) {
            MoovUpApplication.imageLoaderUser.displayImage(profileImg, imgPhoto);
        } else if (profileImage != null && !TextUtils.isEmpty(profileImage.getThumb())) {
            MoovUpApplication.imageLoaderUser.displayImage(profileImage.getThumb(), imgPhoto);
        } else if (profileImage != null && !TextUtils.isEmpty(profileImage.getMedium())) {
            MoovUpApplication.imageLoaderUser.displayImage(profileImage.getMedium(), imgPhoto);
        } else {
            imgPhoto.setImageResource(R.drawable.user_placeholder);
        }
    }

    @Override
    public void onSetListeners() {
        btMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommentViewHolderListener listener = getListener(CommentViewHolderListener.class);
                if (listener != null) {
                    listener.onBtMoreClicked(getItem());
                }
            }
        });


    }

    public interface CommentViewHolderListener {
        void onBtMoreClicked(Answers question);
    }
}