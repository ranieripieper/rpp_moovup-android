package com.doisdoissete.moovup.ui.login;

import android.content.Intent;
import android.os.Bundle;

import com.crashlytics.android.Crashlytics;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.ConfigurationManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Configuration;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseActivity;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.parse.ParseInstallation;
import com.sromku.simple.fb.Permission;
import com.sromku.simple.fb.SimpleFacebook;
import com.sromku.simple.fb.SimpleFacebookConfiguration;
import com.sromku.simple.fb.listeners.OnLoginListener;

import java.net.HttpURLConnection;
import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class LoginBaseActivity extends BaseActivity {

    protected SimpleFacebook mSimpleFacebook;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initFacebook();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideKeyboard();
        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (mSimpleFacebook != null) {
            try {
                mSimpleFacebook.onActivityResult(requestCode, resultCode, data);
            } catch(NullPointerException e) {
                Crashlytics.logException(e);
                logout(false);
                showErrorDialog(getString(R.string.error_login_in_with_facebook));
            }
        }

        if (requestCode == LoginGoogleActivity.GOOGLE_PLUS_RESULT) {
            if (data != null) {
                String token = data.getStringExtra(LoginGoogleActivity.PARAM_GOOGLE_PLUS_TOKEN);
                loginGoogle(token);
            } else {
                showErrorDialog(getString(R.string.error_login_in_with_google_plus));
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void initFacebook() {
        // Facebook Permissions
        Permission[] permissions = new Permission[]{
                Permission.EMAIL};

        SimpleFacebookConfiguration configuration = new SimpleFacebookConfiguration.Builder()
                .setAppId(getString(R.string.facebook_app_id))
                .setPermissions(permissions)
                .setNamespace("moovup")
                .build();
        SimpleFacebook.setConfiguration(configuration);

        mSimpleFacebook = SimpleFacebook.getInstance(this);
    }

    public void callGoogleLogin() {
        showLoadingView();
        Intent intentToLaunch = new Intent(getContext(), LoginGoogleActivity.class);
        startActivityForResult(intentToLaunch, LoginGoogleActivity.GOOGLE_PLUS_RESULT);
    }

    public void callFacebookLogin() {
        showLoadingView();
        mSimpleFacebook = SimpleFacebook.getInstance();

        if (mSimpleFacebook.isLogin()) {
            loginFacebook(mSimpleFacebook.getAccessToken().getToken());
        } else {
            mSimpleFacebook.login(new OnLoginListener() {
                @Override
                public void onLogin(String accessToken, List<Permission> acceptedPermissions, List<Permission> declinedPermissions) {
                    loginFacebook(accessToken);
                }

                @Override
                public void onCancel() {
                    showErrorDialog(getString(R.string.error_login_in_with_facebook));
                }

                @Override
                public void onFail(String reason) {
                    showErrorDialog(getString(R.string.error_login_in_with_facebook));
                }

                @Override
                public void onException(Throwable throwable) {
                    showErrorDialog(getString(R.string.error_login_in_with_facebook));
                }
            });
        }
    }


    private void loginFacebook(String accessToken) {
        RetrofitManager.getInstance().getUserService().loginFacebook(accessToken,
                RetrofitManager.PROVIDER,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processLoginResult(responseResult, R.string.error_login_in_with_facebook, true);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error, R.string.error_login_in_with_facebook);
                    }
                });
    }

    private void loginGoogle(String googleToken) {
        RetrofitManager.getInstance().getUserService().loginGoogle(googleToken,
                RetrofitManager.PROVIDER,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processLoginResult(responseResult, R.string.error_login_in_with_google_plus, true);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        showError(error, R.string.error_login_in_with_google_plus);
                    }
                });
    }

    public void processLoginError(RetrofitError error, final String email) {
        hideLoadingView();
        ResponseResult response = ViewUtil.getErrorResponse(error);
        if (response != null && response.getStatusCode() == HttpURLConnection.HTTP_FORBIDDEN) {
            ViewUtil.showErrorDialog(getContext(), getString(R.string.msg_verify_account), getString(R.string.ok), getString(R.string.resend_email), new ErrorListener() {
                @Override
                public void positiveButton() {
                }

                @Override
                public void negativeButton() {
                    resentActivationMail(email);
                }
            });
        } else {
            showError(error, R.string.error_generic, true);
        }
    }

    private void resentActivationMail(String email) {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().resendActivationMail(email, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                showSuccessDialog(getContext(), R.string.resend_email_success);
                hideLoadingView();
            }

            @Override
            public void failure(RetrofitError error) {
                if (error != null && error.getResponse() != null && error.getResponse().getStatus() == 422) {
                    showSuccessDialog(getContext(), R.string.resend_email_success);
                } else {
                    showError(error);
                }
                hideLoadingView();
            }
        });
    }

    public void processLoginResult(ResponseResult responseResult, int resError, boolean socialNetwork) {

        if (responseResult != null && responseResult.isSuccess() && responseResult.getUser() != null && responseResult.getAuthData() != null) {

            UserCacheManager.getInstance().put(responseResult.getUser());
            ConfigurationManager.getInstance().deleteAll();
            Configuration configuration = new Configuration();
            configuration.setCurrentAuthToken(responseResult.getAuthData().getAuthToken());
            configuration.setCurrentUserId(responseResult.getUser().getId());
            ConfigurationManager.getInstance().put(configuration);

            if (responseResult.isNewUser()) {
                SharedPrefManager.getInstance().setShowTour(responseResult.getUser().getEmail(), true);
                Navigator.navigateToTermsActivity(getContext(), true);
            } else {
                Navigator.navigateToMainActivity(getContext());
            }

            hideLoadingView();
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                finishAffinity();
            } else {
                finish();
            }
        } else {
            showErrorDialog(resError);
        }
    }

}
