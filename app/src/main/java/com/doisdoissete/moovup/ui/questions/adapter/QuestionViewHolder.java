package com.doisdoissete.moovup.ui.questions.adapter;

import android.graphics.Bitmap;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.helper.QuestionHelper;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_question_feed)
public class QuestionViewHolder extends ItemViewHolder<Question> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.img_feed_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_feed_name)
    TextView lblName;

    @ViewId(R.id.lbl_feed_date)
    TextView lblDate;

    @ViewId(R.id.lbl_feed_category)
    TextView lblCategory;

    @ViewId(R.id.lbl_feed_content)
    TextView lblContent;

    @ViewId(R.id.img_cover)
    ImageView imgCover;

    @ViewId(R.id.lbl_feed_comments)
    TextView lblComments;

    public QuestionViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Question itemTmp, PositionInfo positionInfo) {
        Question item = QuestionCacheManager.getInstance().get(itemTmp.getId());

        User user = UserCacheManager.getInstance().get(item.getUserId());
        if (user != null) {
            ViewUtil.displayUserImage(user, imgPhoto);
            lblName.setText(user.getFullName());
        } else {
            imgPhoto.setImageResource(R.drawable.user_placeholder);
            lblName.setText("");
        }

        Category category = CategoryCacheManager.getInstance().get(item.getCategoryId());
        if (category != null) {
            lblCategory.setText(category.getTitle());
        } else {
            lblCategory.setText("");
        }

        lblContent.setText(item.getBodyText());
        lblComments.setText(QuestionHelper.getAnswersCount(item));

        lblDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(item.getCreatedAt()));
        imgCover.setVisibility(View.GONE);
        if (!TextUtils.isEmpty(item.getCoverImageUrl())) {
            MoovUpApplication.imageLoader.displayImage(item.getCoverImageUrl(), imgCover, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    imgCover.setVisibility(View.VISIBLE);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

        }
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionHolderListener listener = getListener(QuestionHolderListener.class);
                if (listener != null) {
                    listener.onQuestionSelected(getItem());
                }
            }
        });

        /*
        imgPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //User user = UserCacheManager.getInstance().get(getItem().getUserId());
                //ViewUtil.showDialogViewImage(getContext(), UserHelper.getImageUrl(user), MoovUpApplication.imageLoaderUser);
                //TODO
            }
        });
        */
    }
}