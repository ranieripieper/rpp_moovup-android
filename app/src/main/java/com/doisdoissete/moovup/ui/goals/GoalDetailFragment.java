package com.doisdoissete.moovup.ui.goals;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.GoalCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.goals.adapter.GoalDetailViewPagerAdapter;
import com.doisdoissete.moovup.ui.helper.GoalHelper;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 1/11/16.
 */
public class GoalDetailFragment extends BaseFragment {

    private static final String ARG_GOAL = "arg_goal";
    public static final String EXTRA_GOAL_REMOVED = "EXTRA_GOAL_REMOVED";

    @Bind(R.id.tablayout_goal_detail)
    TabLayout mTabLayoutGoal;

    @Bind(R.id.viewpager_goal_detail)
    public ViewPager mViewPager;

    private GoalDetailViewPagerAdapter mAdapter;
    private Goal mGoal;
    private Long mGoalId;

    public static GoalDetailFragment newInstance(Goal goal) {
        GoalDetailFragment fragment = new GoalDetailFragment();
        Bundle bundle = new Bundle();
        fragment.mGoal = goal;
        fragment.mGoalId = goal.getId();
        fragment.setArguments(bundle);
        return fragment;
    }

    public static GoalDetailFragment newInstance(Long goalID) {
        GoalDetailFragment fragment = new GoalDetailFragment();
        Bundle bundle = new Bundle();
        fragment.mGoalId = goalID;

        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if (mGoal == null && mGoalId != null) {
            loadGoal();
        } else if (mGoal != null) {
            renderGoal();
        } else {
            finish();
        }
    }

    private void loadGoal() {
        showLoadingView();
        RetrofitManager.getInstance().getGoalService().getMyGoalById(mGoalId, new Callback<Goal>() {
            @Override
            public void success(Goal goal, Response response) {
                mGoal = goal;
                if (isAdded()) {
                    renderGoal();
                    configToolbar();
                    hideLoadingView();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        loadGoal();
                    }

                    @Override
                    public void negativeButton() {
                        finish();
                    }
                });
            }
        });
    }

    private void renderGoal() {
        if (isAdded()) {
            setupViewPager(mViewPager);
            mTabLayoutGoal.setupWithViewPager(mViewPager);
        }
    }

    private void setupViewPager(ViewPager viewPager) {

        mAdapter = new GoalDetailViewPagerAdapter(getChildFragmentManager());

        mAdapter.addFrag(GoalActivitiesFragment.newInstance(mGoal, this), getString(R.string.activities));
        mAdapter.addFrag(GoalEvolutionFragment.newInstance(mGoal, this), getString(R.string.evolution));

        User currentUser = getCurrentUser();
        if (mGoal.getUser() != null && mGoal.getUser().getId() == currentUser.getId()
                || mGoal.getUserId() != null && mGoal.getUserId() == currentUser.getId()) {

            Category category = CategoryCacheManager.getInstance().get(mGoal.getCategoryId());
            if (category != null) {
                if (category.isClosedGoal()) {
                    mAdapter.addFrag(GoalClosedFragment.newInstance(mGoal, this), getString(R.string.edit_goal));
                } else {
                    mAdapter.addFrag(GoalCommonFragment.newInstance(mGoal, this), getString(R.string.edit_goal));
                }
            }
        }

        viewPager.setAdapter(mAdapter);
        viewPager.setOffscreenPageLimit(3);
    }

    public void deleteGoal() {
        GoalCacheManager.getInstance().delete(mGoalId);
        finish();
        Intent i = new Intent();
        i.putExtra(EXTRA_GOAL_REMOVED, mGoal.getId());
        if (getTargetFragment() != null) {
            getTargetFragment().onActivityResult(RequestCodeUtil.REQUEST_CODE_REMOVE_GOAL, Activity.RESULT_OK, i);
        }
    }

    public void goalActivityAdded(GoalActivity goalActivity) {
        if (goalActivity != null) {
            //fragment GoalEvolutionFragment
            if (mAdapter != null
                    && mAdapter.getCount() > 1
                    && mAdapter.getItem(1) instanceof GoalEvolutionFragment) {
                ((GoalEvolutionFragment)mAdapter.getItem(1)).refresh();
            }
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_detail;
    }

    @Override
    protected String getToolbarTitle() {
        if (mGoal != null) {

            Category category = mGoal.getCategory();
            if (category == null) {
                category = CategoryCacheManager.getInstance().get(mGoal.getCategoryId());
            }

            if (category != null) {
                return String.format("%s %s", category.getTitle(), GoalHelper.getGoalDescription(getContext(), mGoal));
            } else {
                return GoalHelper.getGoalDescription(getContext(), mGoal);
            }

        }
        return "";
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }
}
