package com.doisdoissete.moovup.ui.base;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.SearchView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.Answers;
import com.doisdoissete.moovup.service.model.GoalActivity;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.Meta;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.helper.AnswerHelper;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.doisdoissete.moovup.ui.helper.UserHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.ui.search.SearchActivity;
import com.doisdoissete.moovup.util.Constants;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import java.net.HttpURLConnection;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import retrofit.RetrofitError;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 11/12/15.
 */
public class ViewUtil {

    public static void configSearchView(final BaseActivity ctx, SearchManager searchManager, SearchView searchView, int searchType) {
        Bundle bundle = new Bundle();
        bundle.putInt(SearchActivity.EXTRA_SEARCH_TYPE, searchType);
        searchView.setAppSearchData(bundle);

        searchView.setImeOptions(EditorInfo.IME_ACTION_SEARCH);

        searchView.setSearchableInfo(searchManager.getSearchableInfo(ctx.getComponentName()));
        searchView.setIconifiedByDefault(true);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });

    }

    public static void showDialogViewImage(Context ctx, String imageUrl, ImageLoader imgLoader) {
        if (TextUtils.isEmpty(imageUrl)) {
            return;
        }
        final Dialog builder = new Dialog(ctx, android.R.style.Theme_Material_NoActionBar_Fullscreen);
        builder.requestWindowFeature(Window.FEATURE_NO_TITLE);
        builder.getWindow().getAttributes().width = ViewGroup.LayoutParams.MATCH_PARENT;
        builder.getWindow().setBackgroundDrawable(
                new ColorDrawable(ctx.getResources().getColor(R.color.black_alpha_80)));
        builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialogInterface) {
            }
        });

        LayoutInflater inflater = LayoutInflater.from(ctx);
        View dialogView = inflater.inflate(R.layout.dialog_view_image, null);

        ImageView imageView = (ImageView)dialogView.findViewById(R.id.img_photo);

        dialogView.findViewById(R.id.img_close).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.dismiss();
            }
        });

        imgLoader.displayImage(imageUrl, imageView);


        builder.addContentView(dialogView, new RelativeLayout.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        builder.show();
    }

    public static void displayUserImage(User user, ImageView imgView) {
        imgView.setImageResource(R.drawable.user_placeholder);
        String imageUrl = UserHelper.getImageUrl(user);
        if (!TextUtils.isEmpty(imageUrl)) {
            MoovUpApplication.imageLoaderUser.displayImage(imageUrl, imgView);
        }
    }

    public static void loadAnswerImage(Answers answer, ImageView imgView, ImageLoadingListener imageLoadingListener) {
        String imageUrl = AnswerHelper.getImageUrl(answer);
        if (!TextUtils.isEmpty(imageUrl)) {
            MoovUpApplication.imageLoader.displayImage(imageUrl, imgView, imageLoadingListener);
        }
    }

    public static void loadGoalActivityImage(GoalActivity goalActivity, ImageView imgView, ImageLoadingListener imageLoadingListener) {
        String imageUrl = GoalHelper.getImageGoalActivityUrl(goalActivity);
        if (!TextUtils.isEmpty(imageUrl)) {
            MoovUpApplication.imageLoader.displayImage(imageUrl, imgView, imageLoadingListener);
        }
    }

    public static void showError(Context context, RetrofitError error, int defaultError, ErrorListener errorListener) {
        showError(context, error, defaultError, false, errorListener);
    }

    public static void showError(Context context, RetrofitError error, int defaultError) {
        showError(context, error, defaultError, false, null);
    }

    public static void showError(Context context, RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        showError(context, error, defaultError, ignoreUnauthorized, null);
    }

    public static void showError(Context context, RetrofitError error, int defaultError, boolean ignoreUnauthorized, ErrorListener errorListener) {
        if (error != null) {
            if (BuildConfig.DEBUG) {
                error.printStackTrace();
            }
            try {
                ResponseResult errorObject = getErrorResponse(error);

                if (!ignoreUnauthorized && errorObject != null && errorObject.getStatusCode() == HttpURLConnection.HTTP_UNAUTHORIZED) {
                    if (errorObject.getErrors() != null && !errorObject.getErrors().isEmpty()) {
                        showToastError(context, errorObject.getErrors());
                    }
                    Navigator.navigateToLoginActivity(context, false);

                } else if (errorObject != null && errorObject.getErrors() != null && !errorObject.getErrors().isEmpty()) {
                    showErrorDialog(context, errorObject.getErrors(), errorListener);
                } else {
                    showErrorDialog(context, defaultError, errorListener);
                }

            } catch (Exception e) {
                showErrorDialog(context, defaultError, errorListener);
            }
        }
    }

    public static ResponseResult getErrorResponse(RetrofitError error) {
        try {
            ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);
            return errorObject;
        } catch (Exception e) {
        }
        return null;
    }

    public static void showErrorDialog(Context context, List<String> lstMessage, ErrorListener errorListener) {
        showErrorDialog(context, getMessage(lstMessage), errorListener);
    }

    public static void showToastError(Context context, List<String> lstMessage) {
        showToastError(context, getMessage(lstMessage));
    }

    public static void showToastError(Context context, String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    public static void showToastError(Context context, RetrofitError error) {
        Toast.makeText(context, getErrorMessage(context, error), Toast.LENGTH_SHORT).show();
    }

    public static String getMessage(List<String> lstMessage) {
        String message = "";
        for (String msg : lstMessage) {
            message += msg;
            message += "\r\n";
        }
        return message;
    }

    public static void showErrorDialog(Context ctx, int message, ErrorListener errorListener) {
        if (ctx != null) {
            showErrorDialog(ctx, ctx.getString(message), errorListener);
        }
    }

    public static void showErrorDialog(Context ctx, String message) {
        showErrorDialog(ctx, message, null);
    }

    public static void showErrorDialog(Context ctx, String message, final ErrorListener errorListener) {

        if (errorListener == null) {
            showErrorDialog(ctx, message, ctx.getString(R.string.ok), "", errorListener);
        } else {
            showErrorDialog(ctx, message, ctx.getString(R.string.retry), ctx.getString(R.string.cancel), errorListener);
        }
    }

    public static void showErrorDialog(Context ctx, String message, String positiveText, String negativeText, final ErrorListener errorListener) {
        if (errorListener == null) {
            new MaterialDialog.Builder(ctx)
                    .title(R.string.error)
                    .content(message)
                    .positiveText(positiveText)
                    .icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                    .show();
        } else {
            new MaterialDialog.Builder(ctx)
                    .title(R.string.error)
                    .content(message)
                    .positiveText(positiveText)
                    .negativeText(negativeText)
                    .cancelable(false)
                    .onPositive(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (errorListener != null) {
                                errorListener.positiveButton();
                            }
                        }
                    })
                    .onNegative(new MaterialDialog.SingleButtonCallback() {
                        @Override
                        public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                            if (errorListener != null) {
                                errorListener.negativeButton();
                            }
                        }
                    })
                    .icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                    .show();
        }
    }

    public static void showOptionsDialog(Context ctx, String[] options, MaterialDialog.ListCallback callback) {
        showOptionsDialog(ctx, null, options, callback);
    }

    public static void showOptionsDialog(Context ctx, String title, String[] options, MaterialDialog.ListCallback callback) {
        MaterialDialog.Builder bulder = new MaterialDialog.Builder(ctx)
                .items(options)
                .itemsCallback(callback);

        if (!TextUtils.isEmpty(title)) {
            bulder.title(title);
        }
        bulder.show();
    }

    public static void showSuccessDialog(Context ctx, View view, int message) {
        showSuccessDialog(ctx, view, ctx.getString(message), null);
    }

    public static void showSuccessDialog(Context ctx, View view, int message, MaterialDialog.SingleButtonCallback positiveCallback) {
        showSuccessDialog(ctx, view, ctx.getString(message), positiveCallback);
    }

    public static void showSuccessDialog(Context ctx, View view, String message) {
        showSuccessDialog(ctx, view, message, null);
    }

    public static void showSuccessDialog(Context ctx, View view, String message, MaterialDialog.SingleButtonCallback positiveCallback) {

        if (view != null) {
            Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_SHORT);

            snackbar.show();
            if (positiveCallback != null) {
                positiveCallback.onClick(null, null);
            }
        } else {
            Toast.makeText(ctx, message, Toast.LENGTH_SHORT).show();
        }

        /*
        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .title(R.string.success)
                .content(message)
                .positiveText(R.string.agree)
                .onPositive(positiveCallback)
                .icon(ctx.getResources().getDrawable(R.drawable.icn_success));
        if (positiveCallback != null) {
            builder.onPositive(positiveCallback);
        }
        builder.show();
        */
    }

    public static void showSuccessDialog(Context ctx, String message, MaterialDialog.SingleButtonCallback positiveCallback) {

        MaterialDialog.Builder builder = new MaterialDialog.Builder(ctx)
                .title(R.string.success)
                .content(message)
                .positiveText(R.string.agree)
                .onPositive(positiveCallback)
                .icon(ctx.getResources().getDrawable(R.drawable.icn_success));
        if (positiveCallback != null) {
            builder.onPositive(positiveCallback);
        }
        builder.show();

    }

    public static void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, boolean isAdded) {
        if (isAdded && easyLoadMoreRecyclerView != null) {
            easyLoadMoreRecyclerView.disableAutoLoadMore();
            easyLoadMoreRecyclerView.onLoadingMoreFinish();
        }
    }

    public static void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, Meta meta, int totalItems, boolean isAdded) {
        if (!isAdded) {
            return;
        }
        if (easyLoadMoreRecyclerView != null) {
            easyLoadMoreRecyclerView.onLoadingMoreFinish();
        }

        int maxItemsPerPage = Constants.DEFAULT_ITEMS_PER_PAGE;
        if (meta != null && meta.getPagination() != null && meta.getPagination().getPerPage() > 0) {
            maxItemsPerPage = meta.getPagination().getPerPage();
        }

        if (totalItems < maxItemsPerPage) {
            disableLoadingMore(easyLoadMoreRecyclerView, isAdded);
        }
    }

    private static String getErrorMessage(Context context, RetrofitError error) {
        try {
            ResponseResult errorObject = (ResponseResult) error.getBodyAs(ResponseResult.class);

            return getMessage(errorObject.getErrors());

        } catch (Exception e) {
            return context.getString(R.string.error_generic);
        }
    }


    public static void showConfirmDialog(Context ctx, int title, int message, MaterialDialog.SingleButtonCallback positiveButton) {
        showConfirmDialog(ctx, ctx.getString(title), ctx.getString(message), positiveButton);
    }

    public static void showConfirmDialog(Context ctx, String title, int message, MaterialDialog.SingleButtonCallback positiveButton) {
        showConfirmDialog(ctx, title, ctx.getString(message), positiveButton);
    }

    public static void showConfirmDialog(Context ctx, String title, String message, MaterialDialog.SingleButtonCallback positiveButton) {
        new MaterialDialog.Builder(ctx)
                .title(title)
                .content(message)
                .positiveText(R.string.yes)
                .negativeText(R.string.no)
                .cancelable(false)
                .onPositive(positiveButton)
                .icon(ctx.getResources().getDrawable(R.drawable.icn_error_alert))
                .show();

    }

    public static DatePickerDialog buildDatePicker(Context context, final TextView txtDateField, Date maxDate, Date minDate, Date preSelectDate) {

        DatePickerDialog datePicker;

        DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {

            // when dialog box is closed, below method will be called.
            public void onDateSet(DatePicker view, int selectedYear,
                                  int selectedMonth, int selectedDay) {
                String year1 = String.valueOf(selectedYear);
                String month1 = (selectedMonth < 9 ? "0" : "") + String.valueOf(selectedMonth + 1);
                String day1 = String.valueOf(selectedDay);
                txtDateField.setText(day1 + "/" + month1 + "/" + year1);
            }
        };

        Calendar cal = Calendar.getInstance(TimeZone.getDefault());

        if (!TextUtils.isEmpty(txtDateField.getText().toString())) {
            try {
                Date dt = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(txtDateField.getText().toString());
                cal.setTime(dt);
            } catch (Exception e) {
            }
        } else if (preSelectDate != null) {
            cal.setTime(preSelectDate);
        } else {
            cal.setTime(new Date());
        }

        if (isBrokenSamsungDevice()) {
            context = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog);
        }
        datePicker = new DatePickerDialog(context,
                R.style.DialogTheme,
                datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));

        if (isBrokenSamsungDevice()) {
            datePicker.setTitle("");
            datePicker.updateDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        }

        /*
        datePicker = new DatePickerDialog(context,
                R.style.DialogTheme,
                datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
*/
        datePicker.setCancelable(false);
        datePicker.getDatePicker().setCalendarViewShown(false);
        datePicker.getDatePicker().setSpinnersShown(true);

        if (maxDate != null) {
            Calendar calMaxDate = Calendar.getInstance();
            calMaxDate.setTime(maxDate);
            calMaxDate.set(Calendar.HOUR_OF_DAY, 23);
            calMaxDate.set(Calendar.MINUTE, 59);
            calMaxDate.set(Calendar.SECOND, 59);
            datePicker.getDatePicker().setMaxDate(calMaxDate.getTimeInMillis());
        }

        if (minDate != null) {
            Calendar calMinDate = Calendar.getInstance();
            calMinDate.setTime(minDate);
            calMinDate.set(Calendar.HOUR_OF_DAY, 0);
            calMinDate.set(Calendar.MINUTE, 0);
            calMinDate.set(Calendar.SECOND, 0);
            datePicker.getDatePicker().setMinDate(calMinDate.getTimeInMillis());
        }
        return datePicker;
    }


    private static boolean isBrokenSamsungDevice() {
        return (Build.MANUFACTURER.equalsIgnoreCase("samsung")
                && isBetweenAndroidVersions(
                Build.VERSION_CODES.LOLLIPOP,
                Build.VERSION_CODES.LOLLIPOP_MR1));
    }

    private static boolean isBetweenAndroidVersions(int min, int max) {
        return Build.VERSION.SDK_INT >= min && Build.VERSION.SDK_INT <= max;
    }


}
