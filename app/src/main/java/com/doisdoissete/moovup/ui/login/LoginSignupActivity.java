package com.doisdoissete.moovup.ui.login;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.response.CarrerResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.doisdoissete.moovup.util.ValidationUtil;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/11/15.
 */
public class LoginSignupActivity extends LoginBaseActivity {

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_signup_email)
    public AppCompatAutoCompleteTextView emailEditText;

    @Password(messageResId = R.string.invalid_password, scheme = Password.Scheme.ALPHA_NUMERIC)
    @Bind(R.id.txt_signup_password)
    public EditText passwordEditText;

    @ConfirmPassword(messageResId = R.string.passwords_dont_match)
    @Bind(R.id.txt_signup_confirm_password)
    public EditText confirmPasswordEditText;

    @Bind(R.id.progress_signup_email)
    public ProgressBar progressBar;

    @Bind(R.id.btn_signup)
    public Button btnSignUp;

    @Bind(R.id.lbl_signup_terms)
    public TextView txtTerms;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_signup);

        initEmailField();

        txtTerms.setPaintFlags(txtTerms.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        if (BuildConfig.DEBUG) {
            emailEditText.setText("xxx@xxx.com");
            passwordEditText.setText("qwerty1");
            confirmPasswordEditText.setText("qwerty1");
        }
    }

    private void initEmailField() {

        btnSignUp.setEnabled(false);
        btnSignUp.setBackgroundResource(R.drawable.btn_gray_rounded);

        // AutoComplete Edittext with device emails
        List<String> emails = getSuggestedEmails();
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(getContext(), android.R.layout.simple_list_item_1, emails);
        emailEditText.setAdapter(adapter);

        emailEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    String email = emailEditText.getText().toString();
                    if (!TextUtils.isEmpty(email)) {
                        if (ValidationUtil.isValidEmail(email)) {
                            checkEmail(email);
                        } else {
                            setInvalidForm(R.string.invalid_email);
                        }
                    } else {
                        btnSignUp.setEnabled(false);
                        btnSignUp.setBackgroundResource(R.drawable.btn_gray_rounded);
                    }
                }
            }
        });
    }

    private List<String> getSuggestedEmails() {
        List<String> emails = new ArrayList<>();
        Account[] accounts = AccountManager.get(getContext()).getAccounts();

        for (Account account : accounts) {
            if (ValidationUtil.isValidEmail(account.name) && !emails.contains(account.name))
                emails.add(account.name);
        }

        return emails;
    }

    public void showEmailLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    public void hideEmailLoading() {
        progressBar.setVisibility(View.GONE);
    }

    public void setInvalidForm() {
        setInvalidForm(R.string.email_already_taken);
    }

    public void setInvalidForm(int resMessage) {
        btnSignUp.setEnabled(false);
        btnSignUp.setBackgroundResource(R.drawable.btn_gray_rounded);
        emailEditText.setError(getString(resMessage));
    }

    public void setValidForm() {
        btnSignUp.setEnabled(true);
        btnSignUp.setBackgroundResource(R.drawable.button_orange_selector);
    }

    public void checkEmail(String email) {
        showEmailLoading();
        RetrofitManager.getInstance().getUserService().checkEmail(email, new Callback<CarrerResponse>() {
            @Override
            public void success(CarrerResponse careerResponse, Response response) {
                setInvalidForm();
                hideEmailLoading();
            }

            @Override
            public void failure(RetrofitError error) {
                setValidForm();
                hideEmailLoading();
            }
        });
    }

    //clicks
    @OnClick(R.id.btn_signup)
    public void onSignUpClick() {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                LoginSignupInfosActivity.startActivity(getContext(), emailEditText.getText().toString(), passwordEditText.getText().toString());
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getContext());

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }

    @OnClick(R.id.btn_socialnetwork_facebook)
    public void btnLoginFacebookClick(View view) {
        callFacebookLogin();
    }

    @OnClick(R.id.btn_socialnetwork_google)
    public void btnLoginGoogleClick(View view) {
        callGoogleLogin();
    }

    @OnClick(R.id.lbl_signup_terms)
    public void onSignupTermsClick(View view) {
        Navigator.navigateToTermsActivity(getContext(), false);
    }

    public static void startActivity(Context ctx) {
        Intent intent = new Intent(ctx, LoginSignupActivity.class);
        ctx.startActivity(intent);
    }
}
