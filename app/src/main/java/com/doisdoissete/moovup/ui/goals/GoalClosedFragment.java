package com.doisdoissete.moovup.ui.goals;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.GoalCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.response.GoalResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.helper.GoalHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import java.text.ParseException;
import java.util.Date;

import butterknife.Bind;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by broto on 10/16/15.
 */
public class GoalClosedFragment extends AddGoalBaseFragment implements Validator.ValidationListener {

    @Bind(R.id.lbl_question)
    public TextView lblQuestion;

    @Bind(R.id.lbl_question_target)
    public TextView lblQuestionTarget;

    @Bind(R.id.edt_goal_current)
    @NotEmpty(messageResId = R.string.required_field)
    public EditText edtCurrent;

    @Bind(R.id.edt_goal_target)
    @NotEmpty(messageResId = R.string.required_field)
    public EditText edtTarget;

    @Bind(R.id.lbl_goal_target_date)
    @NotEmpty(messageResId = R.string.required_field)
    public EditText edtTargetDate;


    public static GoalClosedFragment newInstance(Category category) {
        GoalClosedFragment fragment = new GoalClosedFragment();
        Bundle bundle = new Bundle();
        bundle.putLong(ARG_CATEGORY, category.getId());

        fragment.setArguments(bundle);
        return fragment;
    }

    public static GoalClosedFragment newInstance(Goal goal, GoalDetailFragment goalDetailFragment) {
        GoalClosedFragment fragment = new GoalClosedFragment();
        fragment.mGoal = goal;
        fragment.mGoalDetailFragment = goalDetailFragment;
        fragment.setArguments(getBundleToNewInstance(goal));
        return fragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initDateField();

        if (mGoal != null) {
            edtCurrent.setText(String.valueOf(GoalHelper.getValue(mGoal.getCurrent())));
            edtTarget.setText(String.valueOf(GoalHelper.getValue(mGoal.getTarget())));
            edtTargetDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(mGoal.getTargetDate()));
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_goal_closed;
    }

    protected void initDateField() {
        edtTargetDate.setOnKeyListener(null);
        edtTargetDate.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard();

                if (MotionEvent.ACTION_UP == event.getAction()) {
                    ViewUtil.buildDatePicker(getContext(), edtTargetDate, null, new Date(), null).show();
                }

                return true;
            }
        });
    }

    @Override
    public void onValidationSucceeded() {
        showLoadingView();

        Date targetDate = null;
        if (!TextUtils.isEmpty(edtTargetDate.getText())) {
            try {
                targetDate = DateUtil.DATE_DAY_MONTH_YEAR.get().parse(edtTargetDate.getText().toString());
            } catch (ParseException e) {
            }
        }

        if (mGoal == null) {
            RetrofitManager.getInstance().getGoalService().addNewClosedGoal(
                    getFloat(edtCurrent),
                    getFloat(edtTarget),
                    targetDate,
                    mCategory.getId(),
                    lblShareGoalWithFriends.isChecked(),
                    new Callback<GoalResponse>() {
                        @Override
                        public void success(final GoalResponse baseResponse, Response response) {
                            hideLoadingView();
                            if (baseResponse != null && baseResponse.getGoal() != null) {
                                GoalCacheManager.getInstance().updateWithoutNulls(baseResponse.getGoal());

                                showSuccessDialog(getActivity(), R.string.goal_save, new MaterialDialog.SingleButtonCallback() {
                                    @Override
                                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                                        Navigator.navigateToMyGoalsFragment(getActivity());//, baseResponse.getGoal());
                                    }
                                });
                            } else {
                                showErrorDialog(R.string.error_generic);
                            }
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showError(error);
                        }
                    }
            );
        } else {
            RetrofitManager.getInstance().getGoalService().updateNewClosedGoal(
                    mGoal.getId(),
                    getFloat(edtCurrent),
                    getFloat(edtTarget),
                    targetDate,
                    mCategory.getId(),
                    lblShareGoalWithFriends.isChecked(),
                    new Callback<GoalResponse>() {
                        @Override
                        public void success(final GoalResponse baseResponse, Response response) {
                            hideLoadingView();
                            if (baseResponse != null && baseResponse.getGoal() != null) {
                                GoalCacheManager.getInstance().updateWithoutNulls(baseResponse.getGoal());
                            }
                            showSuccessDialog(getActivity(), R.string.goal_save);
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            showError(error);
                        }
                    }
            );
        }

    }

    private Float getFloat(EditText edtText) {
        return Float.valueOf(edtText.getText().toString());
    }

}
