package com.doisdoissete.moovup.ui.tours;

/**
 * Created by ranipieper on 4/7/16.
 */
public interface PageController {

    void nextPage();

    void previousPage();
}
