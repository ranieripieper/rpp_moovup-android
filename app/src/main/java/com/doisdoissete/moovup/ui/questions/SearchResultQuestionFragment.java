package com.doisdoissete.moovup.ui.questions;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.response.QuestionsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.util.Constants;

import java.util.ArrayList;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/26/15.
 */
public class SearchResultQuestionFragment extends FeedQuestionFragment {

    private static final String ARG_QUERY = "ARG_QUERY";

    private String mQuery = "";

    public static SearchResultQuestionFragment newInstance(String query) {
        Bundle args = new Bundle();
        args.putString(ARG_QUERY, query);
        SearchResultQuestionFragment fragment = new SearchResultQuestionFragment();
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mQuery = getArguments().getString(ARG_QUERY);
        }
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        txtNoResults.setText(R.string.no_result_search_questions);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.my_questions);
    }

    protected void callService() {
        if (mQuery == null || mQuery.length() < Constants.MIN_CHARACTER_TO_SEARCH) {
            txtNoResults.setText(getString(R.string.search_min_character, Constants.MIN_CHARACTER_TO_SEARCH));
            showEmptyQuestions();
            hideLoadingView();
        } else {
            RetrofitManager.getInstance().getQuestionService().searchQuestions(mQuery, mPage, RetrofitManager.DEFAULT_PER_PAGE_SEARCH, new Callback<QuestionsResponse>() {
                @Override
                public void success(QuestionsResponse questionsResponse, Response response) {
                    if (isAdded()) {
                        processResponse(questionsResponse);
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                    if (isAdded()) {
                        showError(error);
                        disableLoadingMore(mRecyclerView);
                    }
                }
            });
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_ADD_QUESTION) {
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED)) {
                    long questionId = data.getLongExtra(AddQuestionFragment.EXTRA_QUESTION_ADDED, -1);
                    Question question = QuestionCacheManager.getInstance().get(questionId);
                    if (questionAdapter == null) {
                        createAdapter(new ArrayList<Question>());
                    }
                    if (question != null) {
                        questionAdapter.addItem(0, question);
                        mRecyclerView.smoothScrollToPosition(0);
                        onQuestionSelected(question);
                        mRecyclerView.setVisibility(View.VISIBLE);
                        txtNoResults.setVisibility(View.GONE);
                    }
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.BACK_BUTTON;
    }

    @Override
    protected boolean needConfigToolbar() {
        return false;
    }
}
