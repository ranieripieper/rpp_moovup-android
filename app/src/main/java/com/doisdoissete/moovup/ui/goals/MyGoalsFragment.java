package com.doisdoissete.moovup.ui.goals;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CarrerCacheManager;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.GoalCacheManager;
import com.doisdoissete.moovup.service.model.Carrer;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.model.response.GoalsResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.base.BaseFragment;
import com.doisdoissete.moovup.ui.base.ErrorListener;
import com.doisdoissete.moovup.ui.base.RequestCodeUtil;
import com.doisdoissete.moovup.ui.goals.adapter.MyGoalsViewHolder;
import com.doisdoissete.moovup.ui.helper.UserHelper;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.ribot.easyadapter.EasyLoadMoreRecyclerAdapter;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 12/18/15.
 */
public class MyGoalsFragment extends BaseFragment implements MyGoalsViewHolder.MyGoalsHolderListener {

    @Bind(R.id.swipe_refresh_layout)
    protected SwipeRefreshLayout mSwipeRefreshLayout;

    @Bind(R.id.recycler_my_goals)
    protected EasyLoadMoreRecyclerView mRecyclerView;

    protected TextView txtNoResults;

    protected EasyLoadMoreRecyclerAdapter<Goal> mGoalsAdapter;

    protected Integer mPage = 1;

    @Bind(R.id.fab_add_goal)
    protected FloatingActionButton mFabAddGoal;

    public static MyGoalsFragment newInstance() {
        return new MyGoalsFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        GoalCacheManager.getInstance().deleteAll();
        setHasOptionsMenu(false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (isCurrentUser()) {
            mFabAddGoal.setVisibility(View.VISIBLE);
        } else {
            mFabAddGoal.setVisibility(View.GONE);
        }

        initSwipeRefresh();
    }

    protected void initSwipeRefresh() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setHasFixedSize(true);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPage = 1;
                loadData();
            }
        });


        loadData();
    }

    protected void loadData() {
        if (mPage == 1) {
            mRecyclerView.enableAutoLoadMore(new EasyLoadMoreRecyclerView.LoadMoreListener() {
                @Override
                public void loadMore() {
                    mPage++;
                    loadData();
                }
            });
            mRecyclerView.setVisibility(View.VISIBLE);

            if (txtNoResults != null) {
                txtNoResults.setVisibility(View.GONE);
            }

            showLoadingView();
            if (mGoalsAdapter != null) {
                mGoalsAdapter.removeAllItems();
            }
            if (CategoryCacheManager.getInstance().count() < 0) {
                callServiceGetCategories();
            } else {
                callService();
            }
        }
    }

    private void callServiceGetCategories() {
        showLoadingView();
        RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE, new Callback<CategoryResponse>() {
            @Override
            public void success(CategoryResponse categoryResponse, Response response) {
                if (isAdded()) {
                    CategoryCacheManager.getInstance().putAll(categoryResponse.getCategories());
                    hideLoadingView();
                    loadData();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_generic, new ErrorListener() {
                    @Override
                    public void positiveButton() {
                        callServiceGetCategories();
                    }

                    @Override
                    public void negativeButton() {

                    }
                });
            }
        });
    }

    protected void callService() {
        RetrofitManager.getInstance().getGoalService().getMyGoals(mPage, RetrofitManager.DEFAULT_PER_PAGE, new Callback<GoalsResponse>() {
            @Override
            public void success(GoalsResponse goalsResponse, Response response) {
                if (isAdded()) {
                    processResponse(goalsResponse);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (isAdded()) {
                    showError(error);
                    disableLoadingMore(mRecyclerView);
                }
            }
        });
    }

    protected void processResponse(GoalsResponse goalsResponse) {
        hideLoadingView();
        if (!isAdded()) {
            return;
        }
        if (goalsResponse.getGoals() != null && goalsResponse.getGoals().size() > 0) {
            GoalCacheManager.getInstance().updateWithoutNulls(goalsResponse.getGoals());
            disableLoadingMore(mRecyclerView, goalsResponse.getMeta(), goalsResponse.getGoals().size());
            renderGoals(goalsResponse.getGoals());
            mRecyclerView.setVisibility(View.VISIBLE);
            if (txtNoResults != null) {
                txtNoResults.setVisibility(View.GONE);
            }
        } else {
            showEmpty();
        }
    }

    protected void renderGoals(List<Goal> goalList) {
        if (mSwipeRefreshLayout.isRefreshing()) {
            mSwipeRefreshLayout.setRefreshing(false);
        }

        if (mGoalsAdapter == null) {
            createAdapter(goalList);
        } else {
            mGoalsAdapter.addItems(goalList);
        }
    }

    protected void createAdapter(List<Goal> goalList) {
        mGoalsAdapter = new EasyLoadMoreRecyclerAdapter(
                getActivity(),
                MyGoalsViewHolder.class,
                goalList,
                this,
                R.layout.loading_recycler_view);

        mGoalsAdapter.setParallaxHeader(getHeader(getUser()), mRecyclerView);

        mGoalsAdapter.notifyDataSetChanged();

        mRecyclerView.setAdapter(mGoalsAdapter);
    }

    protected User getUser() {
        return getCurrentUser();
    }

    protected boolean isCurrentUser() {
        return true;
    }

    private View getHeader(User user) {
        View header = LayoutInflater.from(getActivity()).inflate(
                R.layout.include_header_my_goals, mRecyclerView, false);


        final ImageView imgCategory = (ImageView) header.findViewById(R.id.img_category);
        ImageView imgPhoto = (ImageView) header.findViewById(R.id.img_profile_photo);
        TextView lblName = (TextView) header.findViewById(R.id.lbl_name);
        TextView lblTitle = (TextView) header.findViewById(R.id.lbl_title);
        TextView lblHelped = (TextView) header.findViewById(R.id.lbl_answers_upvotes_count);
        txtNoResults = (TextView) header.findViewById(R.id.txt_no_results);

        TextView lblOccupation = (TextView) header.findViewById(R.id.lbl_occupation);
        TextView lblHobby = (TextView) header.findViewById(R.id.lbl_hobby);

        if (User.FRIENDSHIP_STATUS_SELF_ACCEPTED.equalsIgnoreCase(user.getFriendshipStatus())
                || User.FRIENDSHIP_STATUS_USER_ACCEPTED.equalsIgnoreCase(user.getFriendshipStatus())) {
            lblTitle.setVisibility(View.VISIBLE);
        } else {
            lblTitle.setVisibility(View.INVISIBLE);
        }
        if (user.getReceivedUpvotesCount() == 1) {
            lblHelped.setText(getString(R.string.helped_1_friends));
        } else {
            lblHelped.setText(getString(R.string.helped_x_friends, user.getReceivedUpvotesCount()));
        }

        MoovUpApplication.imageLoaderUser.displayImage(user.getProfileImageUrl(), imgPhoto);
        lblName.setText(user.getFullName());

        lblOccupation.setVisibility(View.GONE);
        if (user.getCarrer() != null) {
            lblOccupation.setText(getString(R.string.occupation_desc, user.getCarrer().getTitle()));
            lblOccupation.setVisibility(View.VISIBLE);
        } else if (user.getCarrerId() != null) {
            Carrer carrer = CarrerCacheManager.getInstance().get(user.getCarrerId());
            if (carrer != null) {
                lblOccupation.setText(getString(R.string.occupation_desc, carrer.getTitle()));
                lblOccupation.setVisibility(View.VISIBLE);
            }
        }

        lblHobby.setVisibility(View.GONE);
        if (user.getHobby() != null) {
            lblHobby.setText(getString(R.string.hobby_desc, user.getHobby().getTitle()));
            lblHobby.setVisibility(View.VISIBLE);
        } else if (user.getHobbyId() != null) {
            Category hobby = CategoryCacheManager.getInstance().get(user.getHobbyId());
            if (hobby != null) {
                lblHobby.setText(getString(R.string.hobby_desc, hobby.getTitle()));
                lblHobby.setVisibility(View.VISIBLE);
            }
        }

        String urlCategory = UserHelper.getHobbyImageUrl(user);
        if (!TextUtils.isEmpty(urlCategory)) {
            MoovUpApplication.imageLoader.displayImage(urlCategory, imgCategory);
        }

        return header;
    }

    protected void showEmpty() {
        disableLoadingMore(mRecyclerView);
        hideLoadingView();
        if (mPage <= 1) {
            if (isAdded()) {
                if (mGoalsAdapter == null) {
                    createAdapter(new ArrayList<Goal>());
                }
                mRecyclerView.setVisibility(View.VISIBLE);
                if (txtNoResults != null) {
                    txtNoResults.setVisibility(View.VISIBLE);
                    txtNoResults.setText(getEmptyGoalsText());
                }
            }
        }
    }

    protected String getEmptyGoalsText() {
        return getString(R.string.no_goals_yet);
    }

    @Override
    public void onGoalClicked(Goal goal) {
        Navigator.navigateToGoalDetailFragment(getContext(), this, goal);
    }

    @Override
    public void showLoadingView() {
        showLoadingView(mSwipeRefreshLayout);
        if (txtNoResults != null) {
            txtNoResults.setVisibility(View.GONE);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RequestCodeUtil.REQUEST_CODE_REMOVE_GOAL) {
            /*
            if (resultCode == Activity.RESULT_OK) {
                if (data.hasExtra(GoalDetailFragment.EXTRA_GOAL_REMOVED)) {
                    long goalId = data.getLongExtra(GoalDetailFragment.EXTRA_GOAL_REMOVED, -1);
                    removeGoal(goalId);
                }
                if (mGoalsAdapter ==  null
                        || (mGoalsAdapter != null
                            && (mGoalsAdapter.getItems() == null || mGoalsAdapter.getItems().size() <= 0))) {
                    if (isAdded() && mRecyclerView != null) {
                        mRecyclerView.setVisibility(View.INVISIBLE);
                        txtNoResults.setVisibility(View.VISIBLE);
                    }
                }
            }
            */
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    protected void removeGoal(long goalId) {
        if (mGoalsAdapter != null
                && goalId > 0
                && mGoalsAdapter.getItems() != null
                && mGoalsAdapter.getItems().size() == 1) {
            createAdapter(new ArrayList<Goal>());
        } else if (mGoalsAdapter != null
                && goalId > 0
                && mGoalsAdapter.getItems() != null
                && mGoalsAdapter.getItems().size() > 1) {
            Goal goalToRemove = null;
            int position = -1;
            for (int i = 0; i < mGoalsAdapter.getItems().size(); i++) {
                Goal goalTmp = mGoalsAdapter.getItem(i);
                if (goalId == goalTmp.getId()) {
                    goalToRemove = goalTmp;
                    position = i;
                    break;
                }
            }
            if (goalToRemove != null) {
                mGoalsAdapter.removeItem(goalToRemove);
            }
        }
    }

    @Override
    public void onResumeFromBackStack() {
        super.onResumeFromBackStack();
        if (isCurrentUser()) {
            if (mGoalsAdapter != null) {
                mGoalsAdapter.removeAllItems();
                List<Goal> lst = GoalCacheManager.getInstance().getAllByUserId(getCurrentUser().getId());
                if (lst != null && !lst.isEmpty()) {
                    mGoalsAdapter.addItems(lst);
                } else {
                    showEmpty();
                }
            }
        }
    }

    @OnClick(R.id.fab_add_goal)
    public void addQuestionClick() {
        Navigator.navigateToAddGoalFragment(getContext());
    }

    @Override
    public void hideLoadingView() {
        hideLoadingView(mSwipeRefreshLayout);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_my_goals;
    }

    @Override
    protected TOOLBAR_TYPE getToolbarType() {
        return TOOLBAR_TYPE.DRAWER;
    }

    @Override
    protected String getToolbarTitle() {
        return getString(R.string.my_goals);
    }
}
