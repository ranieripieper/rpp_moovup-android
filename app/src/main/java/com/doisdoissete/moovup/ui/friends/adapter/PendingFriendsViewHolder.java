package com.doisdoissete.moovup.ui.friends.adapter;

import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.helper.UserHelper;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_pending_friend)
public class PendingFriendsViewHolder extends ItemViewHolder<User> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.lbl_name)
    TextView lblName;

    @ViewId(R.id.lbl_status)
    TextView lblStatus;

    @ViewId(R.id.img_photo)
    ImageView imgPhoto;

    @ViewId(R.id.fab_accept_friend)
    FloatingActionButton fabAcceptFriend;

    @ViewId(R.id.fab_recuse_friend)
    FloatingActionButton fabRecuseFriend;

    public PendingFriendsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(User itemTmp, PositionInfo positionInfo) {
        User item = UserCacheManager.getInstance().get(itemTmp.getId());
        if (item == null) {
            item = itemTmp;
        }

        lblName.setText(item.getFullName());
        String urlPhoto = UserHelper.getImageUrl(item);
        if (TextUtils.isEmpty(urlPhoto)) {
            imgPhoto.setImageResource(R.drawable.user_placeholder);
        } else {
            MoovUpApplication.imageLoaderUser.displayImage(item.getProfileImageUrl(), imgPhoto);
        }
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingFriendsHolderListener listener = getListener(PendingFriendsHolderListener.class);
                if (listener != null) {
                    listener.onUserClicked(getItem());
                }
            }
        });


        fabAcceptFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingFriendsHolderListener listener = getListener(PendingFriendsHolderListener.class);
                if (listener != null) {
                    listener.onAcceptClicked(getItem());
                }
            }
        });

        fabRecuseFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PendingFriendsHolderListener listener = getListener(PendingFriendsHolderListener.class);
                if (listener != null) {
                    listener.onRecuseClicked(getItem());
                }
            }
        });

    }

    public interface PendingFriendsHolderListener {
        void onUserClicked(User user);
        void onAcceptClicked(User user);
        void onRecuseClicked(User user);
    }
}