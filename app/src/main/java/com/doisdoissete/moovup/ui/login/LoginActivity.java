package com.doisdoissete.moovup.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.doisdoissete.moovup.BuildConfig;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.response.ResponseResult;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.parse.ParseInstallation;

import java.util.List;

import butterknife.Bind;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 11/10/15.
 */
public class LoginActivity extends LoginBaseActivity {

    private static final String EXTRA_ENABLE_BACK_BUTTON = "EXTRA_ENABLE_BACK_BUTTON";

    @NotEmpty(messageResId = R.string.required_field)
    @Email(messageResId = R.string.invalid_email)
    @Bind(R.id.txt_login_email)
    public EditText emailEditText;

    @Password(messageResId = R.string.invalid_password, scheme = Password.Scheme.ALPHA_NUMERIC)
    @Bind(R.id.txt_login_password)
    public EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        UserCacheManager.getInstance().deleteAll();

        if (getSupportActionBar() != null) {
            boolean enableBackButton = getExtraBoolean(EXTRA_ENABLE_BACK_BUTTON);
            getSupportActionBar().setDisplayHomeAsUpEnabled(enableBackButton);
        }

        if (BuildConfig.DEBUG) {
            emailEditText.setText("rani.pieper@doisdoissete.com");
            passwordEditText.setText("qwerty1");
        }
    }

    //Clicks
    @OnClick(R.id.lbl_login_forgot_password)
    public void onForgotPasswordClick() {
        if (emailEditText.getText().toString().isEmpty()) {
            emailEditText.setError("Email not valid");
        } else {
            forgotPasswordClicked(emailEditText.getText().toString());
        }
    }

    @OnClick(R.id.lbl_login_wanna_signup)
    public void signupClick(View view) {
        LoginSignupActivity.startActivity(getContext());
    }

    @OnClick(R.id.btn_signin)
    public void btnLoginEnterClick(View view) {
        Validator validator = new Validator(this);
        validator.setValidationListener(new Validator.ValidationListener() {
            @Override
            public void onValidationSucceeded() {
                login(emailEditText.getText().toString(), passwordEditText.getText().toString());
            }

            @Override
            public void onValidationFailed(List<ValidationError> errors) {
                for (ValidationError error : errors) {
                    View view = error.getView();
                    String message = error.getCollatedErrorMessage(getContext());

                    // Display error messages ;)
                    if (view instanceof EditText) {
                        ((EditText) view).setError(message);
                    }
                }
            }
        });
        validator.validate();
    }

    @OnClick(R.id.btn_socialnetwork_facebook)
    public void btnLoginFacebookClick(View view) {
        callFacebookLogin();
    }

    @OnClick(R.id.btn_socialnetwork_google)
    public void btnLoginGoogleClick(View view) {
        callGoogleLogin();
    }

    //serviços
    private void forgotPasswordClicked(String email) {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().requestRecover(email, new Callback<ResponseResult>() {
            @Override
            public void success(ResponseResult responseResult, Response response) {
                hideLoadingView();
                showSuccessDialog(getContext(), R.string.success_recovery_password);
            }

            @Override
            public void failure(RetrofitError error) {
                showError(error, R.string.error_recovery_password);
            }
        });
    }

    private void login(final String email, final String password) {
        showLoadingView();
        RetrofitManager.getInstance().getUserService().login(email,
                password,
                RetrofitManager.PROVIDER,
                SharedPrefManager.getInstance().getParseDeviceToken(),
                RetrofitManager.PROVIDER,
                ParseInstallation.getCurrentInstallation().getInstallationId(),
                new Callback<ResponseResult>() {
                    @Override
                    public void success(ResponseResult responseResult, Response response) {
                        processLoginResult(responseResult, R.string.error_login_in, false);
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        processLoginError(error, email);
                    }
                });
    }

    public static void startActivity(Context ctx, boolean enableBackButton) {
        Intent intent = new Intent(ctx, LoginActivity.class);
        intent.putExtra(EXTRA_ENABLE_BACK_BUTTON, enableBackButton);
        ctx.startActivity(intent);
    }
}
