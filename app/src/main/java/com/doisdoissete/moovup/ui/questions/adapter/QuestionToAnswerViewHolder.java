package com.doisdoissete.moovup.ui.questions.adapter;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Question;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.util.DateUtil;
import com.doisdoissete.moovup.ui.base.ViewUtil;
import com.doisdoissete.moovup.ui.custom.CircleImageView;
import com.doisdoissete.moovup.ui.helper.QuestionHelper;

import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_question_to_answer)
public class QuestionToAnswerViewHolder extends QuestionViewHolder {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.img_feed_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_feed_name)
    TextView lblName;

    @ViewId(R.id.lbl_feed_date)
    TextView lblDate;

    @ViewId(R.id.lbl_feed_category)
    TextView lblCategory;

    @ViewId(R.id.lbl_feed_content)
    TextView lblContent;

    @ViewId(R.id.lbl_feed_comments)
    TextView lblComments;

    @ViewId(R.id.btn_answer)
    Button btAnswer;

    @ViewId(R.id.btn_not_answer)
    Button btNotAnswer;

    public QuestionToAnswerViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Question itemTmp, PositionInfo positionInfo) {
        Question item = QuestionCacheManager.getInstance().get(itemTmp.getId());
        User user = UserCacheManager.getInstance().get(item.getUserId());
        if (user != null) {
            ViewUtil.displayUserImage(user, imgPhoto);
            lblName.setText(user.getFullName());
        }

        Category category = CategoryCacheManager.getInstance().get(item.getCategoryId());
        if (category != null) {
            lblCategory.setText(category.getTitle());
        }

        lblContent.setText(item.getBodyText());
        lblComments.setText(QuestionHelper.getAnswersCount(item));

        lblDate.setText(DateUtil.DATE_DAY_MONTH_YEAR.get().format(item.getCreatedAt()));
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionHolderListener listener = getListener(QuestionHolderListener.class);
                if (listener != null) {
                    listener.onQuestionSelected(getItem());
                }
            }
        });

        btAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionToAnswerListener listener = getListener(QuestionToAnswerListener.class);
                if (listener != null) {
                    listener.onAnswerClick(getItem());
                }
            }
        });

        btNotAnswer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                QuestionToAnswerListener listener = getListener(QuestionToAnswerListener.class);
                if (listener != null) {
                    listener.onNoAnswerClick(getItem());
                }
            }
        });
    }

    public interface QuestionToAnswerListener {
        void onAnswerClick(Question question);
        void onNoAnswerClick(Question question);
    }

}