package com.doisdoissete.moovup.ui.questions.adapter;

import com.doisdoissete.moovup.service.model.Question;

/**
 * Created by ranipieper on 12/8/15.
 */
public interface QuestionHolderListener {
    void onQuestionSelected(Question question);
}
