package com.doisdoissete.moovup.ui.goals.adapter;

import android.view.View;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.Category;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_category)
public class CategoryViewHolder extends ItemViewHolder<Category> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.lbl_row_category)
    TextView lblTitle;

    public CategoryViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Category item, PositionInfo positionInfo) {
        lblTitle.setText(item.getTitle());
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryHolderListener listener = getListener(CategoryHolderListener.class);
                if (listener != null) {
                    listener.onCategoryClicked(getItem());
                }
            }
        });
    }

    public interface CategoryHolderListener {
        void onCategoryClicked(Category category);
    }
}