package com.doisdoissete.moovup.ui.settings.adapter;

import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Interest;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 11/3/15.
 */
@LayoutId(R.layout.row_interest)
public class InterestViewHolder extends ItemViewHolder<Interest> {

    private static final Long ALL_QUESTIONS = null;

    @ViewId(R.id.lbl_interest_title)
    TextView lblTitle;

    @ViewId(R.id.lbl_interest_subtitle)
    TextView lblSubtitle;

    @ViewId(R.id.chk_interest)
    SwitchCompat chkEnabled;

    public InterestViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(final Interest item, PositionInfo positionInfo) {
        Category category = CategoryCacheManager.getInstance().get(item.getCategoryId());
        lblTitle.setText(category.getTitle());

        setLblSubtitle(item);
        chkEnabled.setOnCheckedChangeListener(null);
        chkEnabled.setChecked(item.isActive());

        chkEnabled.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isPressed()) {
                    if (isChecked) {
                        showFrequencyDialog(item);
                        item.setActive(true);
                    } else {
                        lblSubtitle.setText("");
                        item.setActive(false);
                        InterestHolderListener listener = getListener(InterestHolderListener.class);
                        if (listener != null) {
                            listener.onInterestChanged(item);
                        }
                    }
                }
            }
        });


    }

    private void showFrequencyDialog(final Interest interest) {
        Category category = CategoryCacheManager.getInstance().get(getItem().getCategoryId());
        interest.setActive(true);

        int selectedItem = 0;
        if (interest.getMaxAnswersCount() == ALL_QUESTIONS || interest.getMaxAnswersCount() >= 0) {
            if (interest.getMaxAnswersCount() == ALL_QUESTIONS) {
                selectedItem = 4;
            } else if (interest.getMaxAnswersCount() == 5) {
                selectedItem = 1;
            } else if (interest.getMaxAnswersCount() == 10) {
                selectedItem = 2;
            } else if (interest.getMaxAnswersCount() == 20) {
                selectedItem = 3;
            } else if (interest.getMaxAnswersCount() == 0) {
                selectedItem = 5;
            }
        }
        new MaterialDialog.Builder(getContext())
                .title(String.format(getContext().getString(R.string.how_many_questions_you_wanna_answer_about), category.getTitle()))
                .positiveText(R.string.agree)
                .alwaysCallSingleChoiceCallback()
                .negativeText(R.string.disagree)
                .items(R.array.questions_amount)
                .itemsCallbackSingleChoice(selectedItem, new MaterialDialog.ListCallbackSingleChoice() {
                    @Override
                    public boolean onSelection(MaterialDialog dialog, View view, int which, CharSequence text) {
                        switch (which) {
                            case 0:
                                interest.setMaxAnswersCount(1l);
                                break;
                            case 1:
                                interest.setMaxAnswersCount(5l);
                                break;
                            case 2:
                                interest.setMaxAnswersCount(10l);
                                break;
                            case 3:
                                interest.setMaxAnswersCount(20l);
                                break;
                            case 4:
                                interest.setMaxAnswersCount(ALL_QUESTIONS);
                                break;
                            case 5:
                                interest.setMaxAnswersCount(0l);
                                break;
                            default:
                                break;
                        }
                        return true;
                    }
                })
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        chkEnabled.setChecked(!chkEnabled.isChecked());
                        interest.setActive(false);
                    }
                })
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                        setLblSubtitle(interest);
                        InterestHolderListener listener = getListener(InterestHolderListener.class);
                        if (listener != null) {
                            listener.onInterestChanged(interest);
                        }
                    }
                })
                .show();
    }


    private void setLblSubtitle(Interest interest) {
        lblSubtitle.setText("");
        if (interest.isActive()) {
            if (interest.getMaxAnswersCount() == ALL_QUESTIONS) {
                lblSubtitle.setText(getContext().getString(R.string.answer_all));
            } else if (interest.getMaxAnswersCount() == 1) {
                lblSubtitle.setText(getContext().getString(R.string.answer_up_to_singular, interest.getMaxAnswersCount()));
            } else if (interest.getMaxAnswersCount() <= 0) {
                lblSubtitle.setText(getContext().getString(R.string.answer_nothing));
            } else {
                lblSubtitle.setText(getContext().getString(R.string.answer_up_to, interest.getMaxAnswersCount()));
            }
        }
    }

    public interface InterestHolderListener {
        void onInterestChanged(Interest interest);
    }
}