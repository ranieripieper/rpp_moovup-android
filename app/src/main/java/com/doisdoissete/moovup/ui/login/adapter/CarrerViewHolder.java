package com.doisdoissete.moovup.ui.login.adapter;

import android.view.View;
import android.widget.TextView;

import com.doisdoissete.moovup.service.model.Carrer;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;


/**
 * Created by broto on 11/3/15.
 */
@LayoutId(android.R.layout.simple_spinner_dropdown_item)
public class CarrerViewHolder extends ItemViewHolder<Carrer> {

    @ViewId(android.R.id.text1)
    TextView text;

    public CarrerViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Carrer item, PositionInfo positionInfo) {
        text.setText(item.getTitle());
    }

    @Override
    public void onSetListeners() {
    }
}