package com.doisdoissete.moovup.ui.notification.adapter;

import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.model.Notification;
import com.doisdoissete.moovup.ui.custom.CircleImageView;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by ranipieper on 2/18/16.
 */
@LayoutId(R.layout.row_notification)
public class NotificationViewHolder extends ItemViewHolder<Notification> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.img_photo)
    CircleImageView imgPhoto;

    @ViewId(R.id.lbl_notification)
    TextView lblNotification;

    @ViewId(R.id.layout_row)
    View layoutRow;

    public NotificationViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Notification item, PositionInfo positionInfo) {
        lblNotification.setText(item.getMessage());

        if (item.isRead()) {
            layoutRow.setAlpha(0.5f);
        } else {
            layoutRow.setAlpha(1f);
        }

        if (!TextUtils.isEmpty(item.getNotificableImageUrl())) {
            MoovUpApplication.imageLoaderUser.displayImage(item.getNotificableImageUrl(), imgPhoto);
            imgPhoto.setVisibility(View.VISIBLE);
        } else if (!TextUtils.isEmpty(item.getSenderUserImageUrl())) {
            MoovUpApplication.imageLoaderUser.displayImage(item.getSenderUserImageUrl(), imgPhoto);
            imgPhoto.setVisibility(View.VISIBLE);
        } else {
            imgPhoto.setVisibility(View.GONE);
        }
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationViewHolderListener listener = getListener(NotificationViewHolderListener.class);
                if (listener != null) {
                    listener.onNotificationSelected(getItem());
                }
            }
        });
    }

    public interface NotificationViewHolderListener {
        void onNotificationSelected(Notification notification);
    }
}