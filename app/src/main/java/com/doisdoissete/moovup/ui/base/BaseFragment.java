package com.doisdoissete.moovup.ui.base;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.model.response.Meta;
import com.doisdoissete.moovup.ui.custom.OnKeyboardVisibilityListener;

import java.util.List;

import butterknife.ButterKnife;
import retrofit.RetrofitError;
import uk.co.ribot.easyadapter.recyclerview.EasyLoadMoreRecyclerView;

/**
 * Created by ranipieper on 11/11/15.
 */
public abstract class BaseFragment extends Fragment implements ViewDialogInterface {

    private ProgressDialog progress;

    protected abstract int getLayoutResource();

    protected boolean mHasMenu = false;

    private User mCurrentUser;

    public enum TOOLBAR_TYPE {
        DRAWER,
        BACK_BUTTON,
        NOTHING
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }

    /**
     * Closes keyboard on fragment first appearance.
     *
     * @param savedInstanceState
     */
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        configToolbar();
        hideKeyboard();
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
    }

    public void setHasOptionsMenu(boolean hasMenu) {
        super.setHasOptionsMenu(true);
        mHasMenu = hasMenu;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutResource(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    public void onResumeFromBackStack() {
        if (!isAdded()) {
            return;
        }
        configToolbar();
        hideKeyboard();
        getActivity().invalidateOptionsMenu();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hideKeyboard();
    }

    @Override
    public void setMenuVisibility(final boolean visible) {
        super.setMenuVisibility(visible);
        if (visible && isAdded()) {
            configToolbar();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * Used by Leak Canary
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void hideKeyboard() {
        if (isAdded() && getView() != null) {
            final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        }
    }


    public void showLoadingView() {
        hideKeyboard();
        if (progress == null || !progress.isShowing()) {
            progress = ProgressDialog.show(getActivity(), getString(R.string.wait),
                    getString(R.string.loading), true);
            hideKeyboard();
        }
    }

    public void hideLoadingView() {
        if (isAdded() && progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }
    }

    protected void hideLoadingView(final SwipeRefreshLayout mSwipeRefreshLayout) {
        if (isAdded() && progress != null && progress.isShowing()) {
            progress.dismiss();
            progress = null;
        }

        if (isAdded() && mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    if (isAdded()) {
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                }
            });
        }
    }

    protected void showLoadingView(final SwipeRefreshLayout mSwipeRefreshLayout) {

        if (isAdded() && mSwipeRefreshLayout != null) {
            mSwipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
            mSwipeRefreshLayout.post(new Runnable() {
                @Override
                public void run() {
                    mSwipeRefreshLayout.setVisibility(View.VISIBLE);
                    mSwipeRefreshLayout.setRefreshing(true);
                }
            });
        }
    }

    protected void configToolbar() {
        if (needConfigToolbar()) {
            setToolbarTitle(getToolbarTitle());
            if (TOOLBAR_TYPE.BACK_BUTTON.equals(getToolbarType())) {
                setNavBackMode();
            } else if (TOOLBAR_TYPE.DRAWER.equals(getToolbarType())) {
                resetBackMode();
            } else if (TOOLBAR_TYPE.NOTHING.equals(getToolbarType())) {
                hideDrawerAndBackButton();
            }
        }
    }

    protected boolean needConfigToolbar() {
        return true;
    }

    //ViewDialogInterface

    @Override
    public void showError(RetrofitError error) {
        if (isAdded()) {
            showError(error, R.string.error_generic);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError, boolean ignoreUnauthorized) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showError(getActivity(), error, defaultError, ignoreUnauthorized, null);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError, ErrorListener errorListener) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showError(getActivity(), error, defaultError, false, errorListener);
        }
    }

    @Override
    public void showError(RetrofitError error, ErrorListener errorListener) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showError(getActivity(), error, R.string.error_generic, false, errorListener);
        }
    }

    @Override
    public void showError(RetrofitError error, int defaultError) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showError(getActivity(), error, defaultError);
        }
    }

    @Override
    public void showErrorDialog(List<String> lstMessage) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showErrorDialog(getActivity(), lstMessage, null);
        }
    }

    @Override
    public void showErrorDialog(String message) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showErrorDialog(getActivity(), message);
        }
    }

    @Override
    public void showErrorDialog(int message) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            showErrorDialog(getString(message));
        }
    }

    @Override
    public void showToastError(Context ctx, String message) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showToastError(ctx, message);
        }
    }

    @Override
    public void showToastError(Context ctx, RetrofitError error) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showToastError(ctx, error);
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message) {
        if (isAdded() && getActivity() != null) {
            showSuccessDialog(ctx, ctx.getString(message));
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, String message) {
        if (isAdded() && getActivity() != null) {
            ViewUtil.showSuccessDialog(ctx, getView(), message);
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final boolean finish) {
        hideLoadingView();
        if (isAdded() && getActivity() != null) {
            ViewUtil.showSuccessDialog(ctx, getView(), message, new MaterialDialog.SingleButtonCallback() {
                @Override
                public void onClick(MaterialDialog materialDialog, DialogAction dialogAction) {
                    if (finish) {
                        finish();
                    }
                }
            });
        }
    }

    @Override
    public void showSuccessDialog(Context ctx, int message, final MaterialDialog.SingleButtonCallback callback) {
        if (isAdded() && getActivity() != null) {
            ViewUtil.showSuccessDialog(ctx, getView(), message, callback);
        }
    }

    /**
     * Call back when user presses back button or back on the navigation.
     */
    protected void onBackPressed() {
        hideKeyboard();
    }

    protected abstract TOOLBAR_TYPE getToolbarType();

    protected String getToolbarTitle() {
        return "";
    }

    public void setNavBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).setNavBackMode();
        }
    }

    public void resetBackMode() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).resetBackMode();
        }
    }

    public void hideDrawerAndBackButton() {
        if (getActivity() instanceof BaseFragmentActivity) {
            ((BaseFragmentActivity) getActivity()).hideDrawerAndBackButton();
        }
    }


    public void setToolbarTitle(String s) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(s);
        }
    }

    public void setToolbarTitle(int stringId) {
        if (getActivity() instanceof BaseActivity) {
            ((BaseActivity) getActivity()).setToolbarTitle(stringId);
        }
    }

    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        if (!mHasMenu) {
            menu.clear();
        }

        super.onCreateOptionsMenu(menu, inflater);
    }

    protected void finish() {
        if (getActivity() != null) {
            getActivity().onBackPressed();
        }
    }

    protected void TODO() {
        Toast.makeText(getActivity(), "TODO", Toast.LENGTH_SHORT).show();
    }

    protected void TODO(String msg ) {
        Toast.makeText(getActivity(), "TODO " + msg, Toast.LENGTH_SHORT).show();
    }

    protected User getCurrentUser(boolean force) {
        if (force || mCurrentUser == null) {
            mCurrentUser = UserCacheManager.getInstance().getCurrentUser();
        }
        return mCurrentUser;
    }

    protected User getCurrentUser() {
        return getCurrentUser(false);
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView, Meta meta, int totalItems) {
        if (isAdded() && getActivity() != null) {
            ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, meta, totalItems, isAdded());
        }
    }

    protected void disableLoadingMore(EasyLoadMoreRecyclerView easyLoadMoreRecyclerView) {
        if (isAdded() && getActivity() != null) {
            ViewUtil.disableLoadingMore(easyLoadMoreRecyclerView, isAdded());
        }
    }

    protected void logout(boolean redirect) {
        ((BaseFragmentActivity)getActivity()).logout(redirect);
    }

    protected void invalidateOptionsMenu() {
        if (getActivity() != null) {
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putString("WORKAROUND_FOR_BUG_19917_KEY", "WORKAROUND_FOR_BUG_19917_VALUE");
        super.onSaveInstanceState(outState);
    }

    public final void setKeyboardListener(final OnKeyboardVisibilityListener listener) {
        final View activityRootView = ((ViewGroup) getActivity().findViewById(android.R.id.content)).getChildAt(0);

        activityRootView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            private boolean wasOpened;

            private final int DefaultKeyboardDP = 100;

            // From @nathanielwolf answer...  Lollipop includes button bar in the root. Add height of button bar (48dp) to maxDiff
            private final int EstimatedKeyboardDP = DefaultKeyboardDP + (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP ? 48 : 0);

            private final Rect r = new Rect();

            @Override
            public void onGlobalLayout() {
                // Convert the dp to pixels.
                int estimatedKeyboardHeight = (int) TypedValue
                        .applyDimension(TypedValue.COMPLEX_UNIT_DIP, EstimatedKeyboardDP, activityRootView.getResources().getDisplayMetrics());

                // Conclude whether the keyboard is shown or not.
                activityRootView.getWindowVisibleDisplayFrame(r);
                int heightDiff = activityRootView.getRootView().getHeight() - (r.bottom - r.top);
                boolean isShown = heightDiff >= estimatedKeyboardHeight;

                if (isShown == wasOpened) {
                    Log.d("Keyboard state", "Ignoring global layout change...");
                    return;
                }

                wasOpened = isShown;
                listener.onVisibilityChanged(isShown);
            }
        });
    }
}
