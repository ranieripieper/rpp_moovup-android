package com.doisdoissete.moovup.ui.goals.adapter;

import android.view.View;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.model.Category;
import com.doisdoissete.moovup.service.model.Goal;
import com.doisdoissete.moovup.ui.helper.GoalHelper;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_my_goal)
public class MyGoalsViewHolder extends ItemViewHolder<Goal> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.lbl_category)
    TextView lblCategory;

    @ViewId(R.id.lbl_goal)
    TextView lblGoal;

    public MyGoalsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(Goal item, PositionInfo positionInfo) {
        Category category = CategoryCacheManager.getInstance().get(item.getCategoryId());
        if (category != null) {
            lblCategory.setText(category.getTitle());
        } else {
            lblCategory.setText("");
        }
        lblGoal.setText(GoalHelper.getGoalDescription(getContext(), item));
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyGoalsHolderListener listener = getListener(MyGoalsHolderListener.class);
                if (listener != null) {
                    listener.onGoalClicked(getItem());
                }
            }
        });
    }

    public interface MyGoalsHolderListener {
        void onGoalClicked(Goal goal);
    }
}