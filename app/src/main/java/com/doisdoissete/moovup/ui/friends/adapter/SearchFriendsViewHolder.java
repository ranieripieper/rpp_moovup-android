package com.doisdoissete.moovup.ui.friends.adapter;

import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.balysv.materialripple.MaterialRippleLayout;
import com.doisdoissete.moovup.MoovUpApplication;
import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.helper.UserHelper;

import uk.co.ribot.easyadapter.ItemViewHolder;
import uk.co.ribot.easyadapter.PositionInfo;
import uk.co.ribot.easyadapter.annotations.LayoutId;
import uk.co.ribot.easyadapter.annotations.ViewId;

/**
 * Created by broto on 10/14/15.
 */
@LayoutId(R.layout.row_search_friend)
public class SearchFriendsViewHolder extends ItemViewHolder<User> {

    @ViewId(R.id.ripple)
    MaterialRippleLayout card;

    @ViewId(R.id.lbl_name)
    TextView lblName;

    @ViewId(R.id.lbl_status)
    TextView lblStatus;

    @ViewId(R.id.img_photo)
    ImageView imgPhoto;

    @ViewId(R.id.fab_accept_friend)
    FloatingActionButton fabAddFriend;

    @ViewId(R.id.fab_recuse_friend)
    FloatingActionButton fabDeclineFriend;

    @ViewId(R.id.layout_action_buttons)
    View layoutActionButtons;

    private int position;

    public SearchFriendsViewHolder(View view) {
        super(view);
    }

    @Override
    public void onSetValues(User itemTmp, PositionInfo positionInfo) {
        position = positionInfo.getPosition();

        User item = UserCacheManager.getInstance().get(itemTmp.getId());
        if (item == null) {
            item = itemTmp;
        }

        lblName.setText(item.getFullName());
        String urlPhoto = UserHelper.getImageUrl(item);
        if (TextUtils.isEmpty(urlPhoto)) {
            imgPhoto.setImageResource(R.drawable.user_placeholder);
        } else {
            MoovUpApplication.imageLoaderUser.displayImage(item.getProfileImageUrl(), imgPhoto);
        }

        layoutActionButtons.setVisibility(View.GONE);
        lblStatus.setVisibility(View.GONE);

        if (User.FRIENDSHIP_STATUS_NO_FRIENDSHIP.equalsIgnoreCase(item.getFriendshipStatus())) {
            showNoFriendship(item);
        } else if (User.FRIENDSHIP_STATUS_SELF_ACCEPTED.equalsIgnoreCase(item.getFriendshipStatus()) ||
                User.FRIENDSHIP_STATUS_USER_ACCEPTED.equalsIgnoreCase(item.getFriendshipStatus())) {
            showFriendship(item);
        } else if (User.FRIENDSHIP_STATUS_SELF_PENDING.equalsIgnoreCase(item.getFriendshipStatus())) {
            showSelfPending(item);
        } else if (User.FRIENDSHIP_STATUS_USER_PENDING.equalsIgnoreCase(item.getFriendshipStatus())) {
            showUserPending(item);
        } else {
            showNoFriendship(item);
        }
    }

    private void showNoFriendship(User item) {
        layoutActionButtons.setVisibility(View.VISIBLE);
        fabDeclineFriend.setVisibility(View.GONE);
        fabAddFriend.setImageResource(R.drawable.ic_add_24dp);
    }

    private void showFriendship(User item) {
        layoutActionButtons.setVisibility(View.GONE);
    }

    private void showUserPending(User item) {
        lblStatus.setText(R.string.friendship_sent_status);
        lblStatus.setVisibility(View.VISIBLE);
    }

    private void showSelfPending(User item) {
        layoutActionButtons.setVisibility(View.VISIBLE);
        fabDeclineFriend.setVisibility(View.VISIBLE);
        fabAddFriend.setVisibility(View.VISIBLE);
        fabAddFriend.setImageResource(R.drawable.ic_action_done);
        lblStatus.setText(R.string.friendship_user_pending);
        lblStatus.setVisibility(View.GONE);
    }

    @Override
    public void onSetListeners() {
        card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFriendsHolderListener listener = getListener(SearchFriendsHolderListener.class);
                if (listener != null) {
                    listener.onUserClicked(getItem());
                }
            }
        });

        fabAddFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFriendsHolderListener listener = getListener(SearchFriendsHolderListener.class);
                if (listener != null) {
                    listener.onAcceptClicked(getItem(), position);
                }
            }
        });

        fabDeclineFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SearchFriendsHolderListener listener = getListener(SearchFriendsHolderListener.class);
                if (listener != null) {
                    listener.onDeclineClicked(getItem(), position);
                }
            }
        });
    }

    public interface SearchFriendsHolderListener {
        void onUserClicked(User user);
        void onAcceptClicked(User user, int position);
        void onDeclineClicked(User user, int position);
    }
}