package com.doisdoissete.moovup.ui.login;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.cache.ConfigurationManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.ui.navigation.Navigator;

import butterknife.OnClick;

/**
 * Created by ranipieper on 11/10/15.
 */
public class LoginIntroActivity extends LoginBaseActivity {

    public LoginIntroActivity() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_intro);

        User userCache = UserCacheManager.getInstance().getCurrentUser();
        if (userCache != null) {
            showMainActivity();
        } else {
            ConfigurationManager.getInstance().deleteAll();
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            }
        }
    }

    private void showMainActivity() {
        Navigator.navigateToMainActivity(LoginIntroActivity.this);
        finish();
    }

    //Clicks
    @OnClick(R.id.lbl_login_wanna_signup)
    public void signupClick(View view) {
        LoginSignupActivity.startActivity(getContext());
    }

    @OnClick(R.id.btn_login_enter)
    public void btnLoginEnterClick(View view) {
        Navigator.navigateToLoginActivity(getContext(), true);
    }

    @OnClick(R.id.btn_socialnetwork_facebook)
    public void btnLoginFacebookClick(View view) {
        callFacebookLogin();
    }

    @OnClick(R.id.btn_socialnetwork_google)
    public void btnLoginGoogleClick(View view) {
        callGoogleLogin();
    }

    public static void startActivity(Context ctx) {
        ctx.startActivity(new Intent(ctx, LoginIntroActivity.class));
    }
}
