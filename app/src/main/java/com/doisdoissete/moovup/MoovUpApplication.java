package com.doisdoissete.moovup;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Build;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.text.TextUtils;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.core.CrashlyticsCore;
import com.doisdoissete.moovup.service.cache.AnswerCacheManager;
import com.doisdoissete.moovup.service.cache.CarrerCacheManager;
import com.doisdoissete.moovup.service.cache.CategoryCacheManager;
import com.doisdoissete.moovup.service.cache.CommercialActivityCacheManager;
import com.doisdoissete.moovup.service.cache.GoalCacheManager;
import com.doisdoissete.moovup.service.cache.QuestionCacheManager;
import com.doisdoissete.moovup.service.cache.base.RealmManager;
import com.doisdoissete.moovup.service.model.response.CarrerResponse;
import com.doisdoissete.moovup.service.model.response.CategoryResponse;
import com.doisdoissete.moovup.service.model.response.CommercialActivityResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.custom.UserImageLoader;
import com.doisdoissete.moovup.util.SharedPrefManager;
import com.nostra13.universalimageloader.cache.memory.impl.WeakMemoryCache;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.SaveCallback;

import java.util.Locale;

import io.fabric.sdk.android.Fabric;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MoovUpApplication extends MultiDexApplication {

    public static ImageLoader imageLoader;
    public static ImageLoader imageLoaderUser;

    @Override
    public void onCreate() {
        super.onCreate();

        //init SharedPrefManager
        SharedPrefManager.init(this);

        //inicia crashlytics
        initCrashlytics();

        //inicia o parse (Push Notification)
        initParse();

        //init retrofit
        initRetrofitManager();

        RealmManager.getInstance(this);

        initCalligraphy();
        initImageLoader();

        initCache();
    }

    private void initParse() {
        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY);
        if (TextUtils.isEmpty(SharedPrefManager.getInstance().getParseDeviceToken())) {
            ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                        String deviceToken = (String) installation.get("deviceToken");
                        SharedPrefManager.getInstance().setParseDeviceToken(deviceToken);
                    }
                }
            });
        }
    }

    private void initCache() {
        QuestionCacheManager.getInstance().deleteAll();
        GoalCacheManager.getInstance().deleteAll();
        AnswerCacheManager.getInstance().deleteAll();

        if (CarrerCacheManager.getInstance().isExpired()) {
            RetrofitManager.getInstance().getCarrerService().getCareers(RetrofitManager.MAX_PER_PAGE, new Callback<CarrerResponse>() {
                @Override
                public void success(CarrerResponse carrerResponse, Response response) {
                    if (carrerResponse != null && carrerResponse.getCarrers() != null) {
                        CarrerCacheManager.getInstance().deleteAll();
                        CarrerCacheManager.getInstance().putAll(carrerResponse.getCarrers());
                    }
                    updateCatetory();
                }

                @Override
                public void failure(RetrofitError error) {
                    updateCatetory();
                }
            });
        } else {
            updateCatetory();
        }
    }

    private void updateCatetory() {
        if (CategoryCacheManager.getInstance().isExpired()) {
            RetrofitManager.getInstance().getCategoryService().getCategories(1, RetrofitManager.MAX_PER_PAGE, new Callback<CategoryResponse>() {
                @Override
                public void success(CategoryResponse categoryResponse, Response response) {
                    if (categoryResponse != null && categoryResponse.getCategories() != null) {
                        CategoryCacheManager.getInstance().deleteAll();
                        CategoryCacheManager.getInstance().putAll(categoryResponse.getCategories());
                    }

                    updateCommercialActivities();
                }

                @Override
                public void failure(RetrofitError error) {
                    updateCommercialActivities();
                }
            });
        } else {
            updateCommercialActivities();
        }
    }

    private void updateCommercialActivities() {
        if (CommercialActivityCacheManager.getInstance().isExpired()) {
            RetrofitManager.getInstance().getCommercialActivityService().getAllCommercialActivities(new Callback<CommercialActivityResponse>() {
                @Override
                public void success(CommercialActivityResponse commercialActivityResponse, Response response) {
                    if (commercialActivityResponse != null && commercialActivityResponse.getCommercialActivities() != null) {
                        CommercialActivityCacheManager.getInstance().deleteAll();
                        CommercialActivityCacheManager.getInstance().putAll(commercialActivityResponse.getCommercialActivities());
                    }
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    private void initCrashlytics() {
        // Set up Crashlytics, disabled for debug builds
        Crashlytics crashlyticsKit = new Crashlytics.Builder()
                .core(new CrashlyticsCore.Builder()
                        .disabled(BuildConfig.DEBUG)
                        .build())
                .build();

        // Initialize Fabric with the debug-disabled crashlytics.
        Fabric.with(this, crashlyticsKit, new Answers());


    }

    private void initRetrofitManager() {
        String language = "";
        Locale current = getResources().getConfiguration().locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            language = current.toLanguageTag();
        } else {
            StringBuilder builder = new StringBuilder();
            builder.append(current.getLanguage());
            if (current.getCountry() != null) {
                builder.append("-");
                builder.append(current.getCountry());
            }
            language = builder.toString();
        }

        RetrofitManager.getInstance().initialize(language, BuildConfig.API_BASE_URL);
    }

    private void initCalligraphy() {
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                        .setDefaultFontPath("fonts/Calibri.ttf")
                        .setFontAttrId(R.attr.fontPath)
                        .build()
        );
    }

    private void initImageLoader() {
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_placeholder)
                .showImageForEmptyUri(R.drawable.user_placeholder)
                .showImageOnFail(R.drawable.user_placeholder)
                .delayBeforeLoading(50)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .cacheInMemory(true)
                .considerExifParams(false)
                .build();

        DisplayImageOptions optionsUser = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.user_placeholder)
                .showImageForEmptyUri(R.drawable.user_placeholder)
                .showImageOnFail(R.drawable.user_placeholder)
                .delayBeforeLoading(50)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .imageScaleType(ImageScaleType.EXACTLY)
                .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
                .resetViewBeforeLoading(true)
                .cacheOnDisk(true)
                .considerExifParams(false)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .memoryCache(new WeakMemoryCache())
                .threadPoolSize(3)
                .build();

        ImageLoaderConfiguration configUser = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(optionsUser)
                .memoryCache(new WeakMemoryCache())
                .threadPoolSize(2)
                .build();

        imageLoader = ImageLoader.getInstance();
        imageLoaderUser = UserImageLoader.getInstance();
        imageLoader.init(config);
        imageLoaderUser.init(configUser);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
