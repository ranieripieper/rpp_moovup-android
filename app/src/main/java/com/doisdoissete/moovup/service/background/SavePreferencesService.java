package com.doisdoissete.moovup.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.doisdoissete.moovup.service.model.Preferences;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.helper.PreferencesHelper;

import java.util.Map;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 2/18/16.
 */
public class SavePreferencesService extends IntentService {

    public static String TAG = "SavePreferencesService";
    public static String EXTRA_PREFERENCES_TO_SAVE = "EXTRA_PREFERENCES_TO_SAVE";

    public SavePreferencesService() {
        super("SavePreferencesService");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        synchronized (TAG) {
            Preferences preferences = intent.getParcelableExtra(EXTRA_PREFERENCES_TO_SAVE);
            if (preferences != null) {
                Map<String, String> hashSettings = PreferencesHelper.getPreferencesToSendService(preferences);


                RetrofitManager.getInstance().getUserService().updateUserPreferences(hashSettings, new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                        Log.d(TAG, "Preferências salvas");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, "Erro ao salvar preferências", error);
                    }
                });
            }
        }
    }

    public static void callService(Context context, Preferences preferences) {
        Intent intentService = new Intent(context, SavePreferencesService.class);
        intentService.putExtra(EXTRA_PREFERENCES_TO_SAVE, preferences);
        context.startService(intentService);
    }


}
