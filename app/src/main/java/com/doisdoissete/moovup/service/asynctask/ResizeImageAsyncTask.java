package com.doisdoissete.moovup.service.asynctask;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Environment;

import com.doisdoissete.moovup.R;
import com.doisdoissete.moovup.service.util.BitmapUtil;

import java.io.File;

/**
 * Created by ranipieper on 12/18/15.
 */
public class ResizeImageAsyncTask extends AsyncTask<Void, Void, String> {

    private static final int MAX_IMG_WIDTH_HEIGHT = 500;

    private ResizeImageListener mResizeImageListener;
    private String mFilePath;
    private Context mContext;

    public ResizeImageAsyncTask(Context context, String filePath, ResizeImageListener resizeImageListener) {
        this.mResizeImageListener = resizeImageListener;
        this.mFilePath = filePath;
        this.mContext = context;
    }

    @Override
    protected String doInBackground(Void... params) {


        try {
            Bitmap bitmap = BitmapUtil.media_getBitmapFromFile(new File(mFilePath), MAX_IMG_WIDTH_HEIGHT);
            bitmap = getResizedBitmap(bitmap, MAX_IMG_WIDTH_HEIGHT, MAX_IMG_WIDTH_HEIGHT, ExifInterface.ORIENTATION_NORMAL);
            File saveFile = saveBitmap(bitmap);

            return saveFile.getAbsolutePath();
        } catch(Exception e) {

        }
        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (this.mResizeImageListener != null) {
            this.mResizeImageListener.onPostExecute(s);
        }
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        if (this.mResizeImageListener != null) {
            this.mResizeImageListener.onCancelled();
        }
    }

    public interface ResizeImageListener {
        void onCancelled();
        void onPostExecute(String filePath);
    }

    private static File getExternalPublicFolder(String folderType, String folder, boolean createFolder) {
        File res = null;
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            res = new File(android.os.Environment.getExternalStoragePublicDirectory(folderType), folder);

            if (!res.exists()) {
                //We create the folder if is desired.
                if (folder != null && folder.length() > 0 && createFolder) {
                    if (!res.mkdir()) {
                        res = null;
                    }
                } else {
                    res = null;
                }
            }
        }
        return res;
    }

    private File saveBitmap(Bitmap bitmap) {

        File file = createOutFile();
        BitmapUtil.copyJpg(bitmap, file);
        return file;
    }

    private File createOutFile() {
        try {
            File outputDir = getExternalPublicFolder(
                    Environment.DIRECTORY_PICTURES,
                    this.mContext.getString(R.string.app_name), true);
            File outFile = BitmapUtil.createUniqueFileName("doc",
                    ".jpg", outputDir);
            return outFile;
        } catch (Exception e) {
        }
        return null;
    }

    private Bitmap getResizedBitmap(Bitmap bm, int h, int w, int orientation) {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) h) / width;
        float scaleHeight = ((float) w) / height;
        // CREATE A MATRIX FOR THE MANIPULATION
        Matrix matrix = new Matrix();
        Matrix matrixRotate = BitmapUtil.getMatrixRotate(matrix, orientation);
        if (matrixRotate != null) {
            matrix = matrixRotate;
        }
        // RESIZE THE BIT MAP
        if (width <= w) {
            return bm;
        }

        float scale = Math.max(scaleWidth, scaleHeight);

        matrix.postScale(scale, scale);

        // "RECREATE" THE NEW BITMAP
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);

        return resizedBitmap;
    }
}
