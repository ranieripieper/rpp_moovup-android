package com.doisdoissete.moovup.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;

import com.doisdoissete.moovup.service.cache.ConfigurationManager;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Configuration;
import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 2/18/16.
 */
public class LogoutService extends IntentService {

    public static String EXTRA_TOKEN = "EXTRA_TOKEN";
    public static String TAG = "LogoutService";

    public LogoutService() {
        super("LogoutService");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        synchronized (TAG) {
            String token = intent.getStringExtra(EXTRA_TOKEN);
            RetrofitManager.getInstance().getUserService().logout(token, new Callback<BaseResponse>() {
                @Override
                public void success(BaseResponse baseResponse, Response response) {
                }

                @Override
                public void failure(RetrofitError error) {
                }
            });
        }
    }

    public static void callService(Context context) {
        Intent intentService = new Intent(context, LogoutService.class);
        Configuration conf = ConfigurationManager.getInstance().get();
        if (conf != null) {
            intentService.putExtra(EXTRA_TOKEN, conf.getCurrentAuthToken());
            context.startService(intentService);
        }
        UserCacheManager.getInstance().deleteAll();
        ConfigurationManager.getInstance().deleteAll();
    }


}
