package com.doisdoissete.moovup.service.background;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.doisdoissete.moovup.service.model.response.BaseResponse;
import com.doisdoissete.moovup.service.net.RetrofitManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by ranipieper on 2/18/16.
 */
public class ReadNotificationService extends IntentService {

    public static String TAG = "ReadNotificationService";
    public static String EXTRA_NOTIFICATION_ID = "NOTIFICATION_ID";

    public ReadNotificationService() {
        super("ReadNotificationService");
    }

    @Override
    protected void onHandleIntent(final Intent intent) {

        synchronized (TAG) {
            Long notificationId = intent.getLongExtra(EXTRA_NOTIFICATION_ID, -1);
            if (notificationId > 0) {
                RetrofitManager.getInstance().getNotificationService().readNotifications(notificationId, "", new Callback<BaseResponse>() {
                    @Override
                    public void success(BaseResponse baseResponse, Response response) {
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        Log.e(TAG, "" + error.getMessage());
                    }
                });
            }
        }
    }

    public static void callService(Context context, Long notificationId) {
        Intent intentService = new Intent(context, ReadNotificationService.class);
        intentService.putExtra(EXTRA_NOTIFICATION_ID, notificationId);
        context.startService(intentService);
    }


}
