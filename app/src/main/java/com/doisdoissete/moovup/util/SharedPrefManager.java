package com.doisdoissete.moovup.util;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefManager {

    private static SharedPrefManager sharedPrefManager;
    private Context mContext;

    private static final String PREFERENCES = SharedPrefManager.class + "";

    private static final String PARSE_DEVICE_TOKEN = "PARSE_DEVICE_TOKEN";

    private static final String SHOW_TOUR = "SHOW_TOUR";

    private SharedPrefManager(Context context) {
        this.mContext = context;
    }

    public static SharedPrefManager getInstance() {
        return sharedPrefManager;
    }

    public static void init(Application application) {
        sharedPrefManager = new SharedPrefManager(application);
    }

    private SharedPreferences getSharedPreferences() {
        return mContext.getSharedPreferences(PREFERENCES, 0);
    }

    public boolean showTour(String login) {
        //if (TextUtils.isEmpty(login)) {
            login = "";
        //}
        return getSharedPreferences().getBoolean(String.format("%s_%s", SHOW_TOUR, login), true);
    }

    public void setShowTour(String login, boolean value) {
        SharedPreferences.Editor editor = getEditor();
        //if (TextUtils.isEmpty(login)) {
            login = "";
        //}
        editor.putBoolean(String.format("%s_%s", SHOW_TOUR, login), value);
        editor.commit();
    }

    public String getParseDeviceToken() {
        return getSharedPreferences().getString(PARSE_DEVICE_TOKEN, null);
    }

    public void setParseDeviceToken(String value) {
        SharedPreferences.Editor editor = getEditor();
        editor.putString(PARSE_DEVICE_TOKEN, value);
        editor.commit();
    }

    private SharedPreferences.Editor getEditor() {
        return getSharedPreferences().edit();
    }

}
