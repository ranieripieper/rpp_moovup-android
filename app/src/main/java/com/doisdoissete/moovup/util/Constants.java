package com.doisdoissete.moovup.util;

/**
 * Created by ranipieper on 11/13/15.
 */
public class Constants {

    public static int DEFAULT_ITEMS_PER_PAGE = 20;

    public static int MIN_CHARACTER_TO_SEARCH = 3;

    public static String URL_CATEGORY_PHOTOS = "https://s3-sa-east-1.amazonaws.com/moovupfiles/images_bg/category_%d.jpg";

    public static String SITE_MOOVUP_URL = "http://moovup.com.br";

}
