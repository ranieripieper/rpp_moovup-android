package com.doisdoissete.moovup.util;

import android.content.Context;
import android.content.Intent;

import com.doisdoissete.moovup.R;

import java.text.Normalizer;

/**
 * Created by ranipieper on 2/19/16.
 */
public class FunctionsUtil {

    public static String getStringResourceByName(Context ctx, String aString) {
        String packageName = ctx.getPackageName();
        int resId = ctx.getResources()
                .getIdentifier(aString, "string", packageName);
        if (resId == 0) {
            return aString;
        } else {
            return ctx.getString(resId);
        }
    }

    public static void shareUrl(Context ctx, String urlText) {
        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        share.putExtra(Intent.EXTRA_SUBJECT, ctx.getString(R.string.app_name));
        share.putExtra(Intent.EXTRA_TEXT, urlText);

        ctx.startActivity(Intent.createChooser(share, ctx.getString(R.string.share_moovup_via)));
    }

    public static String getSlugString(String value) {
        if (value != null) {
            value = Normalizer.normalize(value, Normalizer.Form.NFD);
            value = value.replaceAll("[^\\p{ASCII}]", "");

            return value.toLowerCase().replaceAll("\\r|\\n| |\\?", "-").trim();
        }

        return value;
    }
}
