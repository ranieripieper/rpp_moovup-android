package com.doisdoissete.moovup.push;

import android.os.Parcel;
import android.os.Parcelable;

import com.doisdoissete.moovup.service.model.Notification;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * formato
{:alert=>"Hello Hip welcome to HipApp!",
 :data=>
  {:receiver_user_id=>382,
   :sender_user_id=>nil,
   :receiver_user_image_url=>"/images/development/user/profile_image/382/thumb_eden-young-daft-punk.jpg",
   :sender_user_image_url=>nil,
   :notificable_image_url=>nil,
   :notificable_id=>nil,
   :notificable_type=>nil,
   :message=>"Hello Hip welcome to HipApp!",
   :notification_type=>:welcome}

 */
public class PushNotificationMessage implements Parcelable {

	@SerializedName("alert")
	@Expose
	private String message;

	@Expose
	private Notification data;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * Gets the data
	 *
	 * @return data
	 */
	public Notification getData() {
		return data;
	}

	/**
	 * Sets the data
	 *
	 * @param data
	 */
	public void setData(Notification data) {
		this.data = data;
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(this.message);
		dest.writeParcelable(this.data, 0);
	}

	public PushNotificationMessage() {
	}

	protected PushNotificationMessage(Parcel in) {
		this.message = in.readString();
		this.data = in.readParcelable(Notification.class.getClassLoader());
	}

	public static final Parcelable.Creator<PushNotificationMessage> CREATOR = new Parcelable.Creator<PushNotificationMessage>() {
		public PushNotificationMessage createFromParcel(Parcel source) {
			return new PushNotificationMessage(source);
		}

		public PushNotificationMessage[] newArray(int size) {
			return new PushNotificationMessage[size];
		}
	};
}
