package com.doisdoissete.moovup.push;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/*
 * formato
{:alert=>"Hello Hip welcome to HipApp!",
 :data=>
  {:receiver_user_id=>382,
   :sender_user_id=>nil,
   :receiver_user_image_url=>"/images/development/user/profile_image/382/thumb_eden-young-daft-punk.jpg",
   :sender_user_image_url=>nil,
   :notificable_image_url=>nil,
   :notificable_id=>nil,
   :notificable_type=>nil,
   :message=>"Hello Hip welcome to HipApp!",
   :notification_type=>:welcome}

 */
public class PushNotificationMessageData {

    @SerializedName("receiver_user_id")
    @Expose
    private Long receiverUserId;

    @SerializedName("sender_user_id")
    @Expose
    private Long senderUserId;

    @SerializedName("receiver_user_image_url")
    @Expose
    private String receiverUserImageUrl;

    @SerializedName("sender_user_image_url")
    @Expose
    private String senderUserImageUrl;

    @SerializedName("notificable_image_url")
    @Expose
    private String notificableImageUrl;

    @SerializedName("notificable_id")
    @Expose
    private Long notificableId;

    @SerializedName("notificable_type")
    @Expose
    private String notificableType;

    @SerializedName("message")
    @Expose
    private String message;

    @SerializedName("notification_type")
    @Expose
    private String notificationType;

    /**
     * @return the receiverUserId
     */
    public Long getReceiverUserId() {
        return receiverUserId;
    }

    /**
     * @param receiverUserId the receiverUserId to set
     */
    public void setReceiverUserId(Long receiverUserId) {
        this.receiverUserId = receiverUserId;
    }

    /**
     * @return the senderUserId
     */
    public Long getSenderUserId() {
        return senderUserId;
    }

    /**
     * @param senderUserId the senderUserId to set
     */
    public void setSenderUserId(Long senderUserId) {
        this.senderUserId = senderUserId;
    }

    /**
     * @return the receiverUserImageUrl
     */
    public String getReceiverUserImageUrl() {
        return receiverUserImageUrl;
    }

    /**
     * @param receiverUserImageUrl the receiverUserImageUrl to set
     */
    public void setReceiverUserImageUrl(String receiverUserImageUrl) {
        this.receiverUserImageUrl = receiverUserImageUrl;
    }

    /**
     * @return the senderUserImageUrl
     */
    public String getSenderUserImageUrl() {
        return senderUserImageUrl;
    }

    /**
     * @param senderUserImageUrl the senderUserImageUrl to set
     */
    public void setSenderUserImageUrl(String senderUserImageUrl) {
        this.senderUserImageUrl = senderUserImageUrl;
    }

    /**
     * @return the notificableImageUrl
     */
    public String getNotificableImageUrl() {
        return notificableImageUrl;
    }

    /**
     * @param notificableImageUrl the notificableImageUrl to set
     */
    public void setNotificableImageUrl(String notificableImageUrl) {
        this.notificableImageUrl = notificableImageUrl;
    }

    /**
     * @return the notificableId
     */
    public Long getNotificableId() {
        return notificableId;
    }

    /**
     * @param notificableId the notificableId to set
     */
    public void setNotificableId(Long notificableId) {
        this.notificableId = notificableId;
    }

    /**
     * @return the notificableType
     */
    public String getNotificableType() {
        return notificableType;
    }

    /**
     * @param notificableType the notificableType to set
     */
    public void setNotificableType(String notificableType) {
        this.notificableType = notificableType;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return the notificationType
     */
    public String getNotificationType() {
        return notificationType;
    }

    /**
     * @param notificationType the notificationType to set
     */
    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

}
