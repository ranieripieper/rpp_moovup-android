package com.doisdoissete.moovup;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;

import com.doisdoissete.moovup.push.PushNotificationMessage;
import com.doisdoissete.moovup.service.cache.UserCacheManager;
import com.doisdoissete.moovup.service.model.Notification;
import com.doisdoissete.moovup.service.model.User;
import com.doisdoissete.moovup.service.net.RetrofitManager;
import com.doisdoissete.moovup.ui.MainActivity;
import com.doisdoissete.moovup.ui.SplashScreenActivity;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import java.util.Random;


/**
 * Created by ranipieper on 12/10/15.
 */
public class ReceiverPush extends ParsePushBroadcastReceiver {

    private static final String TAG = "ReceiverPush";

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        //super.onPushReceive(context, intent);

        try {
            String data = intent.getStringExtra("com.parse.Data");
            PushNotificationMessage pushNotificationMsg = RetrofitManager.getGson().fromJson(data, PushNotificationMessage.class);

            if (pushNotificationMsg.getData() != null) {
                createNotification(context, intent, pushNotificationMsg.getData());
            } else {
                createBaseNotification(context, pushNotificationMsg.getMessage());
            }

        } catch (Exception e) {
            Log.e(ReceiverPush.class.toString(), "Unexpected JSONException when receiving push data: ", e);
        }
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {

        ParseAnalytics.trackAppOpenedInBackground(intent);

        PushNotificationMessage pushNotificationMsg = null;
        try {
            String data = intent.getStringExtra("com.parse.Data");
            pushNotificationMsg = RetrofitManager.getGson().fromJson(data, PushNotificationMessage.class);
        } catch (Exception e) {
            Log.e(ReceiverPush.class.toString(), "Unexpected JSONException when receiving push data: ", e);
        }

        Class<? extends Activity> cls = getActivity(context, intent);
        Intent activityIntent = MainActivity.getActivityIntent(context, pushNotificationMsg);
        if (intent != null) {
            activityIntent.putExtras(intent.getExtras());
        }

        if (Build.VERSION.SDK_INT >= 16) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
            stackBuilder.addParentStack(cls);
            stackBuilder.addNextIntent(activityIntent);
            stackBuilder.startActivities();
        } else {
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            activityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            context.startActivity(activityIntent);
        }
    }

    public static void createNotification(final Context context, final Intent intent, final Notification message) {
        User localUser = UserCacheManager.getInstance().getCurrentUser();
        if (BuildConfig.DEBUG || (localUser != null && message != null && message.getReceiverUserId() != null &&
                message.getReceiverUserId().equals(localUser.getId()))) {

            String img = message.getNotificableImageUrl();
            if (TextUtils.isEmpty(img)) {
                img = message.getSenderUserImageUrl();
            }
            if (!TextUtils.isEmpty(img)) {
                MoovUpApplication.imageLoaderUser.loadImage(img, new ImageLoadingListener() {
                    @Override
                    public void onLoadingStarted(String imageUri, View view) {
                    }

                    @Override
                    public void onLoadingFailed(String imageUri, View view,
                                                FailReason failReason) {
                        showNotification(context, intent, message, null);
                    }

                    @Override
                    public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                        showNotification(context, intent, message, loadedImage);
                    }

                    @Override
                    public void onLoadingCancelled(String imageUri, View view) {
                        showNotification(context, intent, message, null);
                    }
                });
            } else {
                showNotification(context, intent, message, null);
            }
        }
    }

    private static void showNotification(Context context, final Intent intent, Notification message, Bitmap bitmap) {

        Bitmap circularBitmap = getCircularBitmap(bitmap);

        if (circularBitmap == null) {
            circularBitmap = BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher);
        }

        Bundle extras = intent.getExtras();
        Random random = new Random();
        int contentIntentRequestCode = random.nextInt();
        int deleteIntentRequestCode = random.nextInt();
        String packageName = context.getPackageName();
        Intent contentIntent = new Intent("com.parse.push.intent.OPEN");
        contentIntent.putExtras(extras);
        contentIntent.setPackage(packageName);
        Intent deleteIntent = new Intent("com.parse.push.intent.DELETE");
        deleteIntent.putExtras(extras);
        deleteIntent.setPackage(packageName);
        PendingIntent pContentIntent = PendingIntent.getBroadcast(context, contentIntentRequestCode, contentIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent pDeleteIntent = PendingIntent.getBroadcast(context, deleteIntentRequestCode, deleteIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(message.getMessage())
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message.getMessage()))
                        .setAutoCancel(true)
                        .setCategory(NotificationCompat.CATEGORY_SOCIAL)
                        .setGroup(message.getNotificationType())
                        .setContentIntent(pContentIntent)
                        .setDeleteIntent(pDeleteIntent)
                        .setGroupSummary(true)
                        .setColor(context.getResources().getColor(R.color.colorPrimary))
                        .setLargeIcon(circularBitmap);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        int notificationId = Double.valueOf(Math.random() * 1000).intValue();
        if (message.getNotificableId() != null) {
            notificationId = Long.valueOf(message.getNotificableId()).intValue();
        }
        mNotificationManager.notify(notificationId, mBuilder.build());

    }

    private static void createBaseNotification(Context context, String msg) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_notification)
                        .setContentTitle(context.getString(R.string.app_name))
                        .setContentText(msg)
                        .setAutoCancel(true)
                        .setColor(context.getResources().getColor(R.color.colorPrimary));

        Intent resultIntent = new Intent(context, SplashScreenActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(SplashScreenActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_CANCEL_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }

    public static Bitmap getCircularBitmap(Bitmap bitmap) {
        Bitmap output;

        if (bitmap != null) {
            if (bitmap.getWidth() > bitmap.getHeight()) {
                output = Bitmap.createBitmap(bitmap.getHeight(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
            } else {
                output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getWidth(), Bitmap.Config.ARGB_8888);
            }

            Canvas canvas = new Canvas(output);

            final int color = 0xff424242;
            final Paint paint = new Paint();
            final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

            float r = 0;

            if (bitmap.getWidth() > bitmap.getHeight()) {
                r = bitmap.getHeight() / 2;
            } else {
                r = bitmap.getWidth() / 2;
            }

            paint.setAntiAlias(true);
            canvas.drawARGB(0, 0, 0, 0);
            paint.setColor(color);
            canvas.drawCircle(r, r, r, paint);
            paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
            canvas.drawBitmap(bitmap, rect, rect, paint);
            return output;
        }

        return null;

    }
}